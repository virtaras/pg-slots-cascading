var layout;
(function (layout) {
    var SpriteSheet = createjs.SpriteSheet;
    var StarlingSpriteSheet = (function () {
        function StarlingSpriteSheet() {
        }
        StarlingSpriteSheet.create = function (xml, image) {
            var tp = layout.XMLUtils.getElement(xml, "TextureAtlas");
            if (!tp) {
                console.log("TextureAtlas not found");
                return null;
            }
            var textures = layout.XMLUtils.getElements(tp, "SubTexture");
            var frames = [];
            if (textures.length == 0) {
                console.log("No textures found.");
                return null;
            }
            for (var i = 0; i < textures.length; i++) {
                var t = textures[i];
                var x = t.getAttribute("x");
                var y = t.getAttribute("y");
                var w = t.getAttribute("width");
                var h = t.getAttribute("height");
                var fx = t.getAttribute("frameX");
                var fy = t.getAttribute("frameY");
                frames.push([x, y, w, h, 0, fx, fy]);
            }
            var data = { "frames": frames, "images": [image] };
            return new SpriteSheet(data);
        };
        return StarlingSpriteSheet;
    })();
    layout.StarlingSpriteSheet = StarlingSpriteSheet;
})(layout || (layout = {}));
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var layout;
(function (layout) {
    var Bitmap = createjs.Bitmap;
    var Toggle = (function (_super) {
        __extends(Toggle, _super);
        function Toggle(textures) {
            var _this = this;
            this.state = Toggle.STATE_ON;
            this.isOn = true;
            this.textures = textures;
            var image = this.textures[this.state];
            _super.call(this, image);
            this.on("click", function (eventObj) {
                if (eventObj.nativeEvent instanceof MouseEvent) {
                    _this.isOn = !_this.isOn;
                    _this.onChangeState(_this.isOn ? Toggle.STATE_ON : Toggle.STATE_OFF);
                }
            });
            this.on("rollover", function () {
                _this.onChangeState(_this.isOn ? Toggle.STATE_ON_OVER : Toggle.STATE_OFF_OVER);
                document.body.style.cursor = "pointer";
            });
            this.on("rollout", function () {
                _this.onChangeState(_this.isOn ? Toggle.STATE_ON : Toggle.STATE_OFF);
                document.body.style.cursor = "default";
            });
        }
        Toggle.prototype.onChangeState = function (state) {
            if (this.state != Toggle.STATE_DISABLE) {
                this.changeState(state);
            }
        };
        Toggle.prototype.changeState = function (state) {
            if (this.state != state) {
                this.state = state;
                var image = this.textures[state];
                if (image != null) {
                    this.image = image;
                }
            }
        };
        Toggle.prototype.getIsOn = function () {
            return this.isOn;
        };
        Toggle.prototype.setIsOn = function (value) {
            this.isOn = value;
            this.onChangeState(this.isOn ? Toggle.STATE_ON : Toggle.STATE_OFF);
        };
        Toggle.prototype.getEnable = function () {
            return this.state != Toggle.STATE_DISABLE;
        };
        Toggle.prototype.setEnable = function (value) {
            if (value) {
                this.changeState(this.isOn ? Toggle.STATE_ON : Toggle.STATE_OFF);
            }
            else {
                this.changeState(Toggle.STATE_DISABLE);
            }
            this.mouseEnabled = value;
        };
        Toggle.STATE_ON = "on";
        Toggle.STATE_OFF = "off";
        Toggle.STATE_DISABLE = "dis";
        Toggle.STATE_ON_OVER = "on_over";
        Toggle.STATE_OFF_OVER = "off_over";
        return Toggle;
    })(Bitmap);
    layout.Toggle = Toggle;
})(layout || (layout = {}));
var layout;
(function (layout) {
    var Bitmap = createjs.Bitmap;
    var Button = (function (_super) {
        __extends(Button, _super);
        function Button(textures) {
            var _this = this;
            this.state = Button.STATE_UP;
            this.textures = textures;
            var image = this.textures[this.state];
            _super.call(this, image);
            this.on("mousedown", function () {
                _this.onChangeState(Button.STATE_DOWN);
            });
            this.on("pressup", function () {
                _this.onChangeState(Button.STATE_UP);
            });
            this.on("rollover", function () {
                _this.onChangeState(Button.STATE_OVER);
                document.body.style.cursor = "pointer";
            });
            this.on("rollout", function () {
                _this.onChangeState(Button.STATE_UP);
                document.body.style.cursor = "default";
            });
        }
        Button.prototype.onChangeState = function (state) {
            if (this.state != Button.STATE_DISABLE) {
                this.changeState(state);
            }
        };
        Button.prototype.changeState = function (state) {
            if (this.state != state) {
                this.state = state;
                var image = this.textures[state];
                if (image != null) {
                    this.image = image;
                }
            }
        };
        Button.prototype.getEnable = function () {
            return this.state != Button.STATE_DISABLE;
        };
        Button.prototype.setEnable = function (value) {
            this.changeState(value ? Button.STATE_UP : Button.STATE_DISABLE);
            this.mouseEnabled = value;
        };
        Button.STATE_UP = "up";
        Button.STATE_OVER = "over";
        Button.STATE_DOWN = "down";
        Button.STATE_DISABLE = "dis";
        return Button;
    })(Bitmap);
    layout.Button = Button;
})(layout || (layout = {}));
var layout;
(function (_layout) {
    var LoadQueue = createjs.LoadQueue;
    var EventDispatcher = createjs.EventDispatcher;
    var LayoutLoader = (function (_super) {
        __extends(LayoutLoader, _super);
        function LayoutLoader(url) {
            _super.call(this);
            this.retried_loading_times = 0;
            this.assetsCount = 0;
            this.assetsErrors = [];
            this.url = url;
        }
        LayoutLoader.prototype.load = function () {
            var _this = this;
            console.log("Start load layout URL = " + this.url);
            var assetVO = new _layout.AssetVO(LayoutLoader.LAYOUT_ASSET_ID, this.url, LoadQueue.JSON);
            var loader = new LoadQueue(false);
            loader.on("complete", function () {
                //console.log("Finish load layout URL = " + this.url);
                var layout = loader.getResult(LayoutLoader.LAYOUT_ASSET_ID);
                layout = _this.replaceFontsSpaces(layout, LayoutLoader.REPLACE_FONTS);
                _this.vo = _layout.BaseVO.parse(layout);
                _this.startLoadTexture();
            });
            loader.loadFile(assetVO);
        };
        LayoutLoader.prototype.replaceFontsSpaces = function (layout, dict) {
            var layoutstr = JSON.stringify(layout);
            for (var key in dict) {
                layoutstr = layoutstr.split(key).join(dict[key]);
            }
            return JSON.parse(layoutstr);
        };
        LayoutLoader.prototype.getVO = function () {
            return this.vo;
        };
        LayoutLoader.prototype.getAssets = function () {
            return this.assets;
        };
        LayoutLoader.prototype.startLoadTexture = function () {
            var _this = this;
            var assetFolderURL = _layout.URLUtils.getFolderUrl(this.url) + "/" + _layout.FileConst.MAIN_RES_FOLDER;
            var assetsFinder = new _layout.AssetFinder();
            assetsFinder.find(this.vo);
            var assetsVO = assetsFinder.getAssets();
            if (this.retried_loading_times >= LayoutLoader.RETRY_LOADING_TIMES) {
                for (var i = 0; i < this.assetsErrors.length; ++i) {
                    assetsVO[this.assetsErrors[i]].src = LayoutLoader.EMPTY_IMAGE_NAME;
                }
                this.assetsErrors = [];
            }
            this.assetsCount = assetsVO.length;
            this.assets = new LoadQueue(true);
            this.assets.on("complete", function () {
                _this.dispatchEvent(LayoutLoader.EVENT_ON_LOADED);
            });
            this.assets.on("fileload", function (eventobj) {
                _this.dispatchEvent(LayoutLoader.EVENT_ON_ASSETFILE_PROGRESS);
            });
            this.assets.stopOnError = true;
            this.assets.on("error", function (eventobj) {
                if (++_this.retried_loading_times < LayoutLoader.RETRY_LOADING_TIMES) {
                    setTimeout(function () {
                        _this.dispatchEvent(LayoutLoader.EVENT_ON_FAILED);
                    }, LayoutLoader.RETRY_LOADING_TIME);
                }
                else {
                    for (var i = 0; i < assetsVO.length; ++i) {
                        if (assetsVO[i].src == eventobj.data.src) {
                            _this.assetsErrors.push(i);
                            setTimeout(function () {
                                _this.dispatchEvent(LayoutLoader.EVENT_ON_FAILED);
                            }, LayoutLoader.RETRY_LOADING_TIME);
                        }
                    }
                }
            });
            this.assets.loadManifest(assetsVO, true, assetFolderURL);
        };
        LayoutLoader.EVENT_ON_LOADED = "layout_loaded";
        LayoutLoader.EVENT_ON_ASSETFILE_PROGRESS = "event_on_assetfile_loaded";
        LayoutLoader.EVENT_ON_FAILED = "layout_loading_failed";
        LayoutLoader.LAYOUT_ASSET_ID = "layout";
        LayoutLoader.RETRY_LOADING_TIMES = 3;
        LayoutLoader.RETRY_LOADING_TIME = 1000;
        LayoutLoader.EMPTY_IMAGE_NAME = "pixel.png";
        LayoutLoader.REPLACE_FONTS = { "PF DinDisplay Pro Regular": "PFDinDisplayPro-Regular" };
        return LayoutLoader;
    })(EventDispatcher);
    _layout.LayoutLoader = LayoutLoader;
})(layout || (layout = {}));
var layout;
(function (layout) {
    var XMLUtils = (function () {
        function XMLUtils() {
        }
        XMLUtils.getElements = function (xml, tagName) {
            var result = [];
            var childNodes = xml.childNodes;
            for (var i = 0; i < childNodes.length; i++) {
                var node = childNodes[i];
                if (node.nodeName == tagName) {
                    result.push(node);
                }
            }
            return result;
        };
        XMLUtils.getElement = function (xml, tagName) {
            var childNodes = xml.childNodes;
            for (var i = 0; i < childNodes.length; i++) {
                var node = childNodes[i];
                if (node.nodeName == tagName) {
                    return node;
                }
            }
            return null;
        };
        XMLUtils.getAttribute = function (xml, attributeName) {
            var attributes = xml.attributes;
            if (attributes.hasOwnProperty(attributeName)) {
                return xml.attributes[attributeName].value;
            }
        };
        XMLUtils.getAttributeInt = function (xml, attributeName) {
            var attribute = XMLUtils.getAttribute(xml, attributeName);
            if (attribute != null) {
                return parseInt(attribute);
            }
            return null;
        };
        XMLUtils.getAttributeFloat = function (xml, attributeName) {
            var attribute = XMLUtils.getAttribute(xml, attributeName);
            if (attribute != null) {
                return parseFloat(attribute);
            }
            return null;
        };
        return XMLUtils;
    })();
    layout.XMLUtils = XMLUtils;
})(layout || (layout = {}));
var layout;
(function (layout) {
    var FileConst = (function () {
        function FileConst() {
        }
        FileConst.PNG_EXTENSION = ".png";
        FileConst.JPEG_EXTENSION = ".jpeg";
        FileConst.JPEGXR_EXTENSION = ".jpeg";
        FileConst.WEBP_EXTENSION = ".webp";
        FileConst.XML_EXTENSION = ".xml";
        FileConst.MAIN_RES_FOLDER = "assets/";
        return FileConst;
    })();
    layout.FileConst = FileConst;
})(layout || (layout = {}));
var layout;
(function (layout) {
    var AssetVO = (function () {
        function AssetVO(id, src, type) {
            if (type === void 0) { type = ""; }
            this.id = id;
            this.src = src;
            this.type = type;
        }
        return AssetVO;
    })();
    layout.AssetVO = AssetVO;
})(layout || (layout = {}));
var layout;
(function (layout) {
    var LoadQueue = createjs.LoadQueue;
    var AssetFinder = (function () {
        function AssetFinder() {
            this.assets = [];
        }
        AssetFinder.prototype.find = function (vo) {
            var imageVO;
            var extension;
            switch (vo.className) {
                case layout.ImageVO.CLASS_NAME:
                    {
                        imageVO = vo;
                        extension = imageVO.hasAlpha ? AssetFinder.transparentFormat : layout.FileConst.JPEGXR_EXTENSION;
                        this.addAsset(new layout.AssetVO(imageVO.fileName, imageVO.fileName + extension, LoadQueue.IMAGE));
                        break;
                    }
                case layout.ButtonVO.CLASS_NAME:
                    {
                        var buttonVO = vo;
                        var states = buttonVO.states;
                        for (var i = 0; i < states.length; i++) {
                            var buttonStateVO = states[i];
                            imageVO = buttonStateVO.imageVO;
                            extension = imageVO.hasAlpha ? AssetFinder.transparentFormat : layout.FileConst.JPEGXR_EXTENSION;
                            this.addAsset(new layout.AssetVO(imageVO.fileName, imageVO.fileName + extension, LoadQueue.IMAGE));
                        }
                        break;
                    }
                case layout.SimpleMovieVO.CLASS_NAME:
                    {
                        var movieVO = vo;
                        extension = movieVO.hasAlpha ? AssetFinder.transparentFormat : layout.FileConst.JPEGXR_EXTENSION;
                        this.addAsset(new layout.AssetVO(movieVO.fileName, movieVO.fileName + extension, LoadQueue.IMAGE));
                        this.addAsset(new layout.AssetVO(movieVO.fileName + layout.FileConst.XML_EXTENSION, movieVO.fileName + layout.FileConst.XML_EXTENSION, LoadQueue.XML));
                        break;
                    }
                case layout.SpriteVO.CLASS_NAME:
                    {
                        this.findSpriteRes(vo);
                        break;
                    }
            }
        };
        AssetFinder.getTransparentFormat = function () {
            // Detect WebP-lossless support
            //			if (AssetFinder.isWebPSupported()) {
            //				return FileConst.WEBP_EXTENSION;
            //			}
            // Detect JPEG XR support
            //			else if (AssetFinder.isJpegXRSupported()) {
            //				return FileConst.JPEGXR_EXTENSION;
            //			}
            //			else {
            return layout.FileConst.PNG_EXTENSION;
            //			}
        };
        AssetFinder.isWebPSupported = function () {
            var webP = new Image();
            webP.src = 'data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA';
            return webP.height == 2;
        };
        AssetFinder.isJpegXRSupported = function () {
            var jxr = new Image();
            // is a 1x1 black image
            jxr.src = "data:image/vnd.ms-photo;base64,SUm8AQgAAAAFAAG8AQAQAAAASgAAAIC8BAABAAAAAQAAAIG8BAABAAAAAQAAAMC8BAABAAAAWgAAAMG8BAABAAAAHwAAAAAAAAAkw91vA07+S7GFPXd2jckNV01QSE9UTwAZAYBxAAAAABP/gAAEb/8AAQAAAQAAAA==";
            return jxr.height == 1;
        };
        AssetFinder.prototype.getAssets = function () {
            return this.assets;
        };
        AssetFinder.prototype.findSpriteRes = function (vo) {
            var displays = vo.displays;
            for (var i = 0; i < displays.length; i++) {
                this.find(displays[i]);
            }
        };
        AssetFinder.prototype.addAsset = function (vo) {
            for (var i = 0; i < this.assets.length; i++) {
                if (this.assets[i].id == vo.id) {
                    return;
                }
            }
            this.assets.push(vo);
        };
        AssetFinder.transparentFormat = AssetFinder.getTransparentFormat();
        return AssetFinder;
    })();
    layout.AssetFinder = AssetFinder;
})(layout || (layout = {}));
var layout;
(function (layout) {
    var URLUtils = (function () {
        function URLUtils() {
        }
        URLUtils.getFolderUrl = function (url) {
            var start = url.lastIndexOf("/");
            if (start != -1) {
                return url.substring(0, start);
            }
            return "";
        };
        return URLUtils;
    })();
    layout.URLUtils = URLUtils;
})(layout || (layout = {}));
var layout;
(function (layout) {
    var BaseVO = (function () {
        function BaseVO() {
        }
        BaseVO.parse = function (data) {
            var vo;
            switch (data.className) {
                case layout.ButtonVO.CLASS_NAME:
                    {
                        vo = new layout.ButtonVO();
                        break;
                    }
                case layout.ImageVO.CLASS_NAME:
                    {
                        vo = new layout.ImageVO();
                        break;
                    }
                case layout.SimpleMovieVO.CLASS_NAME:
                    {
                        vo = new layout.SimpleMovieVO();
                        break;
                    }
                case layout.SpriteVO.CLASS_NAME:
                    {
                        vo = new layout.SpriteVO();
                        break;
                    }
                case layout.TextVO.CLASS_NAME:
                    {
                        vo = new layout.TextVO();
                        break;
                    }
            }
            vo.read(data);
            return vo;
        };
        BaseVO.prototype.read = function (data) {
            this.className = data.className;
            this.name = data.name;
            this.x = data.x;
            this.y = data.y;
        };
        return BaseVO;
    })();
    layout.BaseVO = BaseVO;
})(layout || (layout = {}));
var layout;
(function (layout) {
    var TextVO = (function (_super) {
        __extends(TextVO, _super);
        function TextVO() {
            _super.call(this);
        }
        TextVO.prototype.read = function (data) {
            _super.prototype.read.call(this, data);
            this.text = data.text;
            this.width = data.width;
            this.height = data.height;
            this.font = data.font; //TODO: REMOVE SPACES if doesn't work correctly
            this.color = data.color;
            this.size = data.size;
            this.align = data.align;
            this.bold = data.bold;
            this.italic = data.italic;
            this.parseFilters(data.filters);
        };
        TextVO.prototype.parseFilters = function (data) {
            if (data != null) {
                this.filters = new Array(data.length);
                for (var i = 0; i < data.length; i++) {
                    this.filters[i] = layout.BaseFilterVO.parse(data[i]);
                }
            }
        };
        TextVO.CLASS_NAME = "t";
        return TextVO;
    })(layout.BaseVO);
    layout.TextVO = TextVO;
})(layout || (layout = {}));
var layout;
(function (layout) {
    var SimpleMovieVO = (function (_super) {
        __extends(SimpleMovieVO, _super);
        function SimpleMovieVO() {
            _super.call(this);
        }
        SimpleMovieVO.prototype.read = function (data) {
            _super.prototype.read.call(this, data);
            this.fileName = data.fileName;
            this.hasAlpha = data.hasAlpha;
        };
        SimpleMovieVO.CLASS_NAME = "sm";
        return SimpleMovieVO;
    })(layout.BaseVO);
    layout.SimpleMovieVO = SimpleMovieVO;
})(layout || (layout = {}));
var layout;
(function (layout) {
    var ImageVO = (function (_super) {
        __extends(ImageVO, _super);
        function ImageVO() {
            _super.call(this);
        }
        ImageVO.prototype.read = function (data) {
            _super.prototype.read.call(this, data);
            this.fileName = data.fileName;
            this.hasAlpha = data.hasAlpha;
        };
        ImageVO.CLASS_NAME = "i";
        return ImageVO;
    })(layout.BaseVO);
    layout.ImageVO = ImageVO;
})(layout || (layout = {}));
var layout;
(function (layout) {
    var ButtonVO = (function (_super) {
        __extends(ButtonVO, _super);
        function ButtonVO() {
            _super.call(this);
        }
        ButtonVO.prototype.read = function (data) {
            _super.prototype.read.call(this, data);
            var statesData = data.states;
            this.isToggle = data.isToggle;
            this.states = new Array(statesData.length);
            for (var i = 0; i < statesData.length; i++) {
                var state = new layout.ButtonStateVO();
                state.read(statesData[i]);
                this.states[i] = state;
            }
        };
        ButtonVO.CLASS_NAME = "b";
        return ButtonVO;
    })(layout.BaseVO);
    layout.ButtonVO = ButtonVO;
})(layout || (layout = {}));
var layout;
(function (layout) {
    var SpriteVO = (function (_super) {
        __extends(SpriteVO, _super);
        function SpriteVO() {
            _super.call(this);
        }
        SpriteVO.prototype.read = function (data) {
            _super.prototype.read.call(this, data);
            this.readDisplays(data.displays);
        };
        SpriteVO.prototype.readDisplays = function (data) {
            if (data != null) {
                this.displays = new Array(data.length);
                for (var i = 0; i < data.length; i++) {
                    this.displays[i] = layout.BaseVO.parse(data[i]);
                }
            }
        };
        SpriteVO.CLASS_NAME = "s";
        return SpriteVO;
    })(layout.BaseVO);
    layout.SpriteVO = SpriteVO;
})(layout || (layout = {}));
var layout;
(function (layout) {
    var BaseFilterVO = (function () {
        function BaseFilterVO() {
        }
        BaseFilterVO.parse = function (data) {
            var vo;
            switch (data.className) {
                case layout.BlurFilterVO.CLASS_NAME:
                    {
                        vo = new layout.BlurFilterVO();
                        break;
                    }
                case layout.GlowFilterVO.CLASS_NAME:
                    {
                        vo = new layout.GlowFilterVO();
                        break;
                    }
                case layout.DropShadowFilterVO.CLASS_NAME:
                    {
                        vo = new layout.DropShadowFilterVO();
                        break;
                    }
                default:
                    {
                        throw new Error("ERROR: This type is not support: " + data.className);
                    }
            }
            vo.read(data);
            return vo;
        };
        BaseFilterVO.prototype.read = function (data) {
            this.className = data.className;
        };
        return BaseFilterVO;
    })();
    layout.BaseFilterVO = BaseFilterVO;
})(layout || (layout = {}));
var layout;
(function (layout) {
    var BlurFilterVO = (function (_super) {
        __extends(BlurFilterVO, _super);
        function BlurFilterVO() {
            _super.call(this);
        }
        BlurFilterVO.prototype.read = function (data) {
            _super.prototype.read.call(this, data);
            this.blurX = data.blurX;
            this.blurY = data.blurY;
            this.quality = data.quality;
        };
        BlurFilterVO.CLASS_NAME = "bf";
        return BlurFilterVO;
    })(layout.BaseFilterVO);
    layout.BlurFilterVO = BlurFilterVO;
})(layout || (layout = {}));
var layout;
(function (layout) {
    var GlowFilterVO = (function (_super) {
        __extends(GlowFilterVO, _super);
        function GlowFilterVO() {
            _super.call(this);
        }
        GlowFilterVO.prototype.read = function (data) {
            _super.prototype.read.call(this, data);
            this.color = data.color;
            this.alpha = data.alpha;
            this.strength = data.strength;
            this.inner = data.inner;
            this.knockout = data.knockout;
        };
        GlowFilterVO.CLASS_NAME = "g";
        return GlowFilterVO;
    })(layout.BlurFilterVO);
    layout.GlowFilterVO = GlowFilterVO;
})(layout || (layout = {}));
var layout;
(function (layout) {
    var DropShadowFilterVO = (function (_super) {
        __extends(DropShadowFilterVO, _super);
        function DropShadowFilterVO() {
            _super.call(this);
        }
        DropShadowFilterVO.prototype.read = function (data) {
            _super.prototype.read.call(this, data);
            this.distance = data.distance;
            this.angle = data.angle;
            this.hideObject = data.hideObject;
        };
        DropShadowFilterVO.CLASS_NAME = "ds";
        return DropShadowFilterVO;
    })(layout.GlowFilterVO);
    layout.DropShadowFilterVO = DropShadowFilterVO;
})(layout || (layout = {}));
var layout;
(function (layout) {
    var ButtonStateVO = (function () {
        function ButtonStateVO() {
        }
        ButtonStateVO.prototype.read = function (data) {
            this.name = data.name;
            this.imageVO = new layout.ImageVO();
            this.imageVO.read(data.imageVO);
        };
        return ButtonStateVO;
    })();
    layout.ButtonStateVO = ButtonStateVO;
})(layout || (layout = {}));
var layout;
(function (layout) {
    var Text = createjs.Text;
    var Bitmap = createjs.Bitmap;
    var Sprite = createjs.Sprite;
    var Container = createjs.Container;
    var GlowFilter = createjs.GlowFilter;
    var BlurFilter = createjs.BlurFilter;
    var EventDispatcher = createjs.EventDispatcher;
    var DropShadowFilter = createjs.DropShadowFilter;
    var LayoutCreator = (function (_super) {
        __extends(LayoutCreator, _super);
        function LayoutCreator() {
            _super.call(this);
            this.assetsCount = 0;
        }
        LayoutCreator.prototype.load = function (url) {
            var _this = this;
            if (url === void 0) { url = null; }
            if (url == null) {
                throw new Error("ERROR: Argument url is null");
            }
            var loader = new layout.LayoutLoader(url);
            loader.on(LayoutCreator.EVENT_ON_ASSETFILE_COUNT, function (eventobj) {
                _this.assetsCount = loader.assetsCount;
                _this.dispatchEvent(LayoutCreator.EVENT_ON_ASSETFILE_COUNT);
            });
            loader.on(layout.LayoutLoader.EVENT_ON_ASSETFILE_PROGRESS, function () {
                _this.dispatchEvent(LayoutCreator.EVENT_ON_ASSETFILE_PROGRESS);
            });
            loader.on(layout.LayoutLoader.EVENT_ON_LOADED, function () {
                _this.vo = loader.getVO();
                _this.assets = loader.getAssets();
                _this.dispatchEvent(LayoutCreator.EVENT_LOADED);
            });
            loader.on(layout.LayoutLoader.EVENT_ON_FAILED, function () {
                setTimeout(loader.load(), layout.LayoutLoader.RETRY_LOADING_TIME);
            });
            loader.load();
        };
        LayoutCreator.prototype.create = function (container) {
            //console.log("create", this);
            this.createObjByVO(container, this.vo, this.assets, true);
        };
        LayoutCreator.prototype.createObjByVO = function (container, vo, assets, isFirst) {
            switch (vo.className) {
                case layout.ImageVO.CLASS_NAME:
                    {
                        LayoutCreator.createImage(container, vo, assets);
                        break;
                    }
                case layout.TextVO.CLASS_NAME:
                    {
                        LayoutCreator.createTextField(container, vo);
                        break;
                    }
                case layout.SimpleMovieVO.CLASS_NAME:
                    {
                        LayoutCreator.createMovieClip(container, vo, assets);
                        break;
                    }
                case layout.ButtonVO.CLASS_NAME:
                    {
                        LayoutCreator.createButton(container, vo, assets);
                        break;
                    }
                case layout.SpriteVO.CLASS_NAME:
                    {
                        this.createSprite(container, vo, assets, isFirst);
                        break;
                    }
            }
        };
        LayoutCreator.prototype.createSprite = function (container, vo, assets, isFirst) {
            var newContainer;
            if (isFirst) {
                newContainer = container;
            }
            else {
                newContainer = new Container();
                container.addChild(newContainer);
            }
            newContainer.name = vo.name;
            newContainer.x = vo.x;
            newContainer.y = vo.y;
            var displays = vo.displays;
            for (var i = 0; i < displays.length; i++) {
                this.createObjByVO(newContainer, displays[i], assets, false);
            }
        };
        LayoutCreator.createButton = function (container, vo, assets) {
            //console.log("createButton", vo);
            var states = vo.states;
            if (states.length == 0) {
                throw new Error("ERROR: Count frames is zero");
            }
            var bitmaps = {};
            for (var i = 0; i < states.length; i++) {
                var stateVO = states[i];
                var imageVO = stateVO.imageVO;
                bitmaps[stateVO.name] = assets.getResult(imageVO.fileName);
            }
            var btn;
            if (vo.isToggle) {
                btn = new layout.Toggle(bitmaps);
            }
            else {
                btn = new layout.Button(bitmaps);
            }
            btn.name = vo.name;
            btn.x = vo.x;
            btn.y = vo.y;
            container.addChild(btn);
        };
        LayoutCreator.createMovieClip = function (container, vo, assets) {
            var xml = assets.getResult(vo.fileName + layout.FileConst.XML_EXTENSION);
            var image = assets.getResult(vo.fileName);
            if (image) {
                var spriteSheet = layout.StarlingSpriteSheet.create(xml, image);
                var movieClip = new Sprite(spriteSheet);
                movieClip.name = vo.name;
                movieClip.x = vo.x;
                movieClip.y = vo.y;
                container.addChild(movieClip);
            }
        };
        LayoutCreator.createImage = function (container, vo, assets) {
            var image = assets.getResult(vo.fileName);
            if (image) {
                var bitmap = new Bitmap(image);
                bitmap.name = vo.name;
                bitmap.x = vo.x;
                bitmap.y = vo.y;
                container.addChild(bitmap);
            }
        };
        LayoutCreator.createTextField = function (container, textVO) {
            var textField = new Text();
            textField.textBaseline = "alphabetic";
            var fontName = textVO.size + "px " + textVO.font;
            if (textVO.bold) {
                fontName = "bold " + fontName;
            }
            else if (textVO.italic) {
                fontName = "italic " + fontName;
            }
            textField.name = textVO.name;
            textField.text = textVO.text;
            textField.font = fontName;
            textField.lineWidth = textVO.width;
            textField.lineHeight = textVO.height;
            textField.textAlign = textVO.align;
            if (textField.textAlign == "center") {
                textField.x = textVO.x + textField.lineWidth / 2;
            }
            else if (textField.textAlign == "right") {
                textField.x = textVO.x + textField.lineWidth;
            }
            else {
                textField.x = textVO.x;
            }
            textField.y = textVO.y;
            textField.color = "#" + textVO.color.toString(16);
            // TODO: create text field filters
            //			LayoutCreator.createFontFilters(textField, textVO.filters);
            container.addChild(textField);
        };
        LayoutCreator.createFontFilters = function (textField, filtersVOs) {
            if (filtersVOs == null || filtersVOs.length == 0) {
                return;
            }
            var filters = [];
            for (var i = 0; i < filtersVOs.length; i++) {
                var filterVO = filtersVOs[i];
                switch (filterVO.className) {
                    case layout.BlurFilterVO.CLASS_NAME:
                        {
                            var blurVO = filterVO;
                            filters.push(new BlurFilter(blurVO.blurX, blurVO.blurY, blurVO.quality));
                            break;
                        }
                    case layout.DropShadowFilterVO.CLASS_NAME:
                        {
                            var shadowVO = filterVO;
                            filters.push(new DropShadowFilter(shadowVO.distance, shadowVO.angle, shadowVO.color, shadowVO.alpha, shadowVO.blurX, shadowVO.blurY, shadowVO.strength, shadowVO.quality, shadowVO.inner, shadowVO.knockout, shadowVO.hideObject));
                            break;
                        }
                    case layout.GlowFilterVO.CLASS_NAME:
                        {
                            var glowVO = filterVO;
                            filters.push(new GlowFilter(glowVO.color, glowVO.alpha, glowVO.blurX, glowVO.blurY, glowVO.strength, glowVO.quality, glowVO.inner, glowVO.knockout));
                            break;
                        }
                }
            }
            textField.filters = filters;
        };
        LayoutCreator.EVENT_LOADED = "layout_loaded";
        LayoutCreator.EVENT_ON_ASSETFILE_PROGRESS = "event_on_assetfile_progress";
        LayoutCreator.EVENT_ON_ASSETFILE_COUNT = "event_on_assetfile_count";
        return LayoutCreator;
    })(EventDispatcher);
    layout.LayoutCreator = LayoutCreator;
})(layout || (layout = {}));
var layout;
(function (layout) {
    var Container = createjs.Container;
    var Layout = (function (_super) {
        __extends(Layout, _super);
        function Layout() {
            _super.call(this);
            this.isInit = false;
        }
        Layout.prototype.load = function (url, isAuto) {
            var _this = this;
            if (url === void 0) { url = null; }
            if (isAuto === void 0) { isAuto = false; }
            this.layoutCreator = new layout.LayoutCreator();
            this.layoutCreator.on(layout.LayoutCreator.EVENT_LOADED, function () {
                if (isAuto) {
                    _this.create();
                }
                _this.dispatchEvent(layout.LayoutCreator.EVENT_LOADED);
            });
            this.layoutCreator.on(layout.LayoutCreator.EVENT_ON_ASSETFILE_PROGRESS, function (eventobj) {
                _this.dispatchEvent(layout.LayoutCreator.EVENT_ON_ASSETFILE_PROGRESS);
            });
            this.layoutCreator.load(url);
        };
        Layout.prototype.create = function () {
            if (!this.isInit) {
                this.layoutCreator.create(this);
                this.onInit();
                this.isInit = true;
            }
        };
        Layout.prototype.onInit = function () {
        };
        return Layout;
    })(Container);
    layout.Layout = Layout;
})(layout || (layout = {}));
///<reference path="../../dts/createjs.d.ts" />
///<reference path="../../dts/easeljs.d.ts" />
///<reference path="../../dts/preloadjs.d.ts" />
//# sourceMappingURL=layoutjs.js.map