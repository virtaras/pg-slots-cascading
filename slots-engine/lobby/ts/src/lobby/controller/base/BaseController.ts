module lobby {
	export class BaseController {
		public manager:ControllerManager;

		constructor(manager:ControllerManager) {
			this.manager = manager;
		}

		public init():void {
			this.manager.register(this);
		}

		public listNotification():Array<string> {
			return [];
		}

		public handleNotification(message:string, data:any):void {
		}

		public send(message:string, data:any = null):void {
			this.manager.send(message, data);
		}

		public dispose():void {
			console.log("Dispose controller: " + Utils.getClassName(this));
			this.manager.remove(this);
		}

		public onEnterFrame():void {
		}
	}
}