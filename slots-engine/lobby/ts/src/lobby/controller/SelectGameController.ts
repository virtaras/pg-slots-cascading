module lobby {
	import Container = createjs.Container;
	import Sprite = createjs.Sprite;

	export class SelectGameController extends BaseController {
		private common:CommonRefs;
		private container:Container;
		private view:SelectGameView;
		private pageId:number;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
			super(manager);
			this.common = common;
			this.container = container;
			this.pageId = 0;
		}

		public init():void {
			super.init();
			this.view = this.common.layouts[SelectGameView.LAYOUT_NAME];
			this.view.create();

			this.view.getNextPageBtn().on("click", ()=> {
				this.pageId++;
				this.view.tweenPage(true);
				this.view.updatePage(this.common.config.games, this.pageId);
			});
			this.view.getPrevPageBtn().on("click", ()=> {
				this.pageId--;
				this.view.tweenPage(false);
				this.view.updatePage(this.common.config.games, this.pageId);
			});
			this.view.getBackBtn().on("click", ()=> {
				this.remove();
				super.send(NotificationList.LOGOUT);
			});
			var gameIcons:Array<Container> = this.view.getIcons();
			for (var i:number = 0; i < gameIcons.length; i++) {
				var gameIcon:Container = gameIcons[i];
				gameIcon.on("click", (eventObj:any)=> {
					this.onSelectGame(eventObj);
				});
				gameIcon.on("rollover", ()=> {
					document.body.style.cursor = "pointer";
				});
				gameIcon.on("rollout", ()=> {
					document.body.style.cursor = "default";
				});
			}
			this.view.updatePage(this.common.config.games, this.pageId);
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.AUTHORIZED);
			notifications.push(NotificationList.START_GAME);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.AUTHORIZED:
				{
					this.show();
					break;
				}
				case NotificationList.START_GAME:
				{
					this.remove();
					break;
				}
			}
		}

		private show():void {
			this.container.addChild(this.view);
		}

		public remove():void {
			this.view.dispose();
		}

		private onSelectGame(eventData:any):void {
			var targetGameIcon:Container = <Container>eventData.currentTarget;
			var gameIcons:Array<Container> = this.view.getIcons();
			var iconId:number = parseInt(targetGameIcon.name.substr(SelectGameView.GAME_ICON_PREFIX.length));
			var gameId:number = this.pageId * gameIcons.length + iconId - 1;
			this.common.currentGame = this.common.config.games[gameId];
			super.send(NotificationList.START_GAME);
		}
	}
}