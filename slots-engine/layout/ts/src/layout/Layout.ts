module layout {
	import Container = createjs.Container;

	export class Layout extends Container {
		private layoutCreator:LayoutCreator;
		private isInit:boolean;

		constructor() {
			super();
			this.isInit = false;
		}

		public load(url:string = null, isAuto:boolean = false):void {
			this.layoutCreator = new LayoutCreator();
			this.layoutCreator.on(LayoutCreator.EVENT_LOADED, ()=> {
				if (isAuto) {
					this.create();
				}
				this.dispatchEvent(LayoutCreator.EVENT_LOADED);
			});
			this.layoutCreator.on(LayoutCreator.EVENT_ON_ASSETFILE_PROGRESS, (eventobj:any)=> {
				this.dispatchEvent(LayoutCreator.EVENT_ON_ASSETFILE_PROGRESS);
			});
			this.layoutCreator.load(url);
		}

		public create():void {
			if (!this.isInit) {
				this.layoutCreator.create(this);
				this.onInit();
				this.isInit = true;
			}
		}

		public onInit():void {
		}
	}
}
