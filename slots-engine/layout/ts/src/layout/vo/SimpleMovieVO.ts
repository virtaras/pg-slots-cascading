module layout {

	export class SimpleMovieVO extends BaseVO {
		public static CLASS_NAME:string = "sm";

		public fileName:string;
		public hasAlpha:boolean;

		constructor() {
			super();
		}

		public read(data:any):void {
			super.read(data);
			this.fileName = data.fileName;
			this.hasAlpha = data.hasAlpha;
		}
	}
}
