module layout {

	export class TextVO extends BaseVO {
		public static CLASS_NAME:string = "t";

		public text:string;
		public width:number;
		public height:number;
		public font:string;
		public color:number;
		public size:number;
		public align:string;
		public bold:boolean;
		public italic:boolean;
		public filters:Array<BaseFilterVO>;

		constructor() {
			super();
		}

		public read(data:any):void {
			super.read(data);
			this.text = data.text;
			this.width = data.width;
			this.height = data.height;
			this.font = data.font; //TODO: REMOVE SPACES if doesn't work correctly
			this.color = data.color;
			this.size = data.size;
			this.align = data.align;
			this.bold = data.bold;
			this.italic = data.italic;
			this.parseFilters(data.filters);
		}

		private parseFilters(data:any):void {
			if (data != null) {
				this.filters = new Array(data.length);
				for (var i:number = 0; i < data.length; i++) {
					this.filters[i] = BaseFilterVO.parse(data[i]);
				}
			}
		}
	}
}
