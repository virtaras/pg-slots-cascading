module layout {

	export class BlurFilterVO extends BaseFilterVO {
		public static CLASS_NAME:string = "bf";

		public blurX:number;
		public blurY:number;
		public quality:number;

		constructor() {
			super();
		}

		public read(data:any):void {
			super.read(data);
			this.blurX = data.blurX;
			this.blurY = data.blurY;
			this.quality = data.quality;
		}
	}
}
