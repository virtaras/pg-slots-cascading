module layout {

	export class GlowFilterVO extends BlurFilterVO {
		public static CLASS_NAME:string = "g";

		public color:number;
		public alpha:number;
		public strength:number;
		public inner:boolean;
		public knockout:boolean;

		constructor() {
			super();
		}

		public read(data:any):void {
			super.read(data);
			this.color = data.color;
			this.alpha = data.alpha;
			this.strength = data.strength;
			this.inner = data.inner;
			this.knockout = data.knockout;
		}
	}
}
