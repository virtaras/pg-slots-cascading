module layout {
	import Text = createjs.Text;
	import Shadow = createjs.Shadow;
	import Filter = createjs.Filter;
	import Bitmap = createjs.Bitmap;
	import Sprite = createjs.Sprite;
	import Container = createjs.Container;
	import LoadQueue = createjs.LoadQueue;
	import GlowFilter = createjs.GlowFilter;
	import BlurFilter = createjs.BlurFilter;
	import SpriteSheet = createjs.SpriteSheet;
	import EventDispatcher = createjs.EventDispatcher;
	import DropShadowFilter = createjs.DropShadowFilter;

	export class LayoutCreator extends EventDispatcher {
		public static EVENT_LOADED:string = "layout_loaded";
		public static EVENT_ON_ASSETFILE_PROGRESS:string = "event_on_assetfile_progress";
		public static EVENT_ON_ASSETFILE_COUNT:string = "event_on_assetfile_count";

		private vo:BaseVO;
		private assets:LoadQueue;
		private assetsCount:number = 0;

		constructor() {
			super();
		}

		public load(url:string = null):void {
			if (url == null) {
				throw new Error("ERROR: Argument url is null");
			}
			var loader:LayoutLoader = new LayoutLoader(url);
			loader.on(LayoutCreator.EVENT_ON_ASSETFILE_COUNT, (eventobj:any)=> {
				this.assetsCount = loader.assetsCount;
				this.dispatchEvent(LayoutCreator.EVENT_ON_ASSETFILE_COUNT);
			});
			loader.on(LayoutLoader.EVENT_ON_ASSETFILE_PROGRESS, ()=> {
				this.dispatchEvent(LayoutCreator.EVENT_ON_ASSETFILE_PROGRESS);
			});
			loader.on(LayoutLoader.EVENT_ON_LOADED, ()=> {
				this.vo = loader.getVO();
				this.assets = loader.getAssets();
				this.dispatchEvent(LayoutCreator.EVENT_LOADED);
			});
			loader.on(LayoutLoader.EVENT_ON_FAILED, ()=> {
				setTimeout(loader.load(), LayoutLoader.RETRY_LOADING_TIME);
			});
			loader.load();
		}

		public create(container:Container):void {
			//console.log("create", this);
			this.createObjByVO(container, this.vo, this.assets, true);
		}

		private createObjByVO(container:Container, vo:BaseVO, assets:LoadQueue, isFirst:boolean):void {
			//console.log(vo.name, "BaseVO name");
			//console.log("createObjByVO", this);
			switch (vo.className) {
				//============== create Image =================
				case ImageVO.CLASS_NAME:
				{
					LayoutCreator.createImage(container, <ImageVO>vo, assets);
					break;
				}
				//============== create TextField =================
				case TextVO.CLASS_NAME:
				{
					LayoutCreator.createTextField(container, <TextVO>vo);
					break;
				}
				//============== create MovieClip =================
				case SimpleMovieVO.CLASS_NAME:
				{
					LayoutCreator.createMovieClip(container, <SimpleMovieVO>vo, assets);
					break;
				}
				//============== create Button || Toggle =================
				case ButtonVO.CLASS_NAME:
				{
					LayoutCreator.createButton(container, <ButtonVO>vo, assets);
					break;
				}
				//============== create Sprite =================
				case SpriteVO.CLASS_NAME:
				{
					this.createSprite(container, <SpriteVO>vo, assets, isFirst);
					break;
				}
			}
		}

		private createSprite(container:Container, vo:SpriteVO, assets:LoadQueue, isFirst:boolean):void {
			var newContainer:Container;
			if (isFirst) {
				newContainer = container;
			}
			else {
				newContainer = new Container();
				container.addChild(newContainer);
			}
			newContainer.name = vo.name;
			newContainer.x = vo.x;
			newContainer.y = vo.y;

			var displays:Array<BaseVO> = vo.displays;
			for (var i:number = 0; i < displays.length; i++) {
				this.createObjByVO(newContainer, displays[i], assets, false);
			}
		}

		private static createButton(container:Container, vo:ButtonVO, assets:LoadQueue):void {
			//console.log("createButton", vo);
			var states:Array<ButtonStateVO> = vo.states;
			if (states.length == 0) {
				throw new Error("ERROR: Count frames is zero");
			}
			var bitmaps:Object = {};
			for (var i:number = 0; i < states.length; i++) {
				var stateVO:ButtonStateVO = states[i];
				var imageVO:ImageVO = stateVO.imageVO;
				bitmaps[stateVO.name] = assets.getResult(imageVO.fileName);
			}
			var btn:Bitmap;
			if (vo.isToggle){
				btn = new Toggle(bitmaps);
			}
			else{
				btn = new Button(bitmaps);
			}
			btn.name = vo.name;
			btn.x = vo.x;
			btn.y = vo.y;
			container.addChild(btn);
		}

		private static createMovieClip(container:Container, vo:SimpleMovieVO, assets:LoadQueue):void {
			var xml:any = assets.getResult(vo.fileName + FileConst.XML_EXTENSION);
			var image:any = assets.getResult(vo.fileName);
			if (image) {
				var spriteSheet:SpriteSheet = StarlingSpriteSheet.create(xml, image);
				var movieClip:Sprite = new Sprite(spriteSheet);
				movieClip.name = vo.name;
				movieClip.x = vo.x;
				movieClip.y = vo.y;
				container.addChild(movieClip);
			}
		}

		private static createImage(container:Container, vo:ImageVO, assets:LoadQueue):void {
			var image:any = assets.getResult(vo.fileName);
			if (image) {
				var bitmap:Bitmap = new Bitmap(image);
				bitmap.name = vo.name;
				bitmap.x = vo.x;
				bitmap.y = vo.y;
				container.addChild(bitmap);
			}
		}

		private static createTextField(container:Container, textVO:TextVO):void {
			var textField:Text = new Text();
			textField.textBaseline = "alphabetic";
			var fontName:string = textVO.size + "px " + textVO.font;
			if (textVO.bold) {
				fontName = "bold " + fontName;
			}
			else if (textVO.italic) {
				fontName = "italic " + fontName;
			}
			textField.name = textVO.name;
			textField.text = textVO.text;
			textField.font = fontName;
			textField.lineWidth = textVO.width;
			textField.lineHeight = textVO.height;
			textField.textAlign = textVO.align;
			if (textField.textAlign == "center") {
				textField.x = textVO.x + textField.lineWidth / 2;
			}
			else if (textField.textAlign == "right") {
				textField.x = textVO.x + textField.lineWidth;
			}
			else {
				textField.x = textVO.x;
			}
			textField.y = textVO.y;
			textField.color = "#" + textVO.color.toString(16);

			// TODO: create text field filters
//			LayoutCreator.createFontFilters(textField, textVO.filters);
			container.addChild(textField);
		}

		private static createFontFilters(textField:Text, filtersVOs:Array<BaseFilterVO>):void {
			if (filtersVOs == null || filtersVOs.length == 0) {
				return;
			}
			var filters:Array<Filter> = [];
			for (var i:number = 0; i < filtersVOs.length; i++) {
				var filterVO:BaseFilterVO = filtersVOs[i];
				switch (filterVO.className) {
					case BlurFilterVO.CLASS_NAME:
					{
						var blurVO:BlurFilterVO = <BlurFilterVO>filterVO;
						filters.push(new BlurFilter(
							blurVO.blurX,
							blurVO.blurY,
							blurVO.quality
						));
						break;
					}
					case DropShadowFilterVO.CLASS_NAME:
					{
						var shadowVO:DropShadowFilterVO = <DropShadowFilterVO>filterVO;
						filters.push(new DropShadowFilter(
							shadowVO.distance,
							shadowVO.angle,
							shadowVO.color,
							shadowVO.alpha,
							shadowVO.blurX,
							shadowVO.blurY,
							shadowVO.strength,
							shadowVO.quality,
							shadowVO.inner,
							shadowVO.knockout,
							shadowVO.hideObject
						));
						break;
					}
					case GlowFilterVO.CLASS_NAME:
					{
						var glowVO:GlowFilterVO = <GlowFilterVO>filterVO;
						filters.push(new GlowFilter(
							glowVO.color,
							glowVO.alpha,
							glowVO.blurX,
							glowVO.blurY,
							glowVO.strength,
							glowVO.quality,
							glowVO.inner,
							glowVO.knockout
						));
						break;
					}
				}
			}

			textField.filters = filters;
		}
	}
}
