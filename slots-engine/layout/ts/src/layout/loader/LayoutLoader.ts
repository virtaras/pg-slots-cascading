module layout {
	import LoadQueue = createjs.LoadQueue;
	import EventDispatcher = createjs.EventDispatcher;

	export class LayoutLoader extends EventDispatcher {
		public static EVENT_ON_LOADED:string = "layout_loaded";
		public static EVENT_ON_ASSETFILE_PROGRESS:string = "event_on_assetfile_loaded";
		public static EVENT_ON_FAILED:string = "layout_loading_failed";
		public static LAYOUT_ASSET_ID:string = "layout";
		public static RETRY_LOADING_TIMES:number = 3;
		public static RETRY_LOADING_TIME:number = 1000;
		public static EMPTY_IMAGE_NAME:string = "pixel.png";
		public static REPLACE_FONTS:any = {"PF DinDisplay Pro Regular":"PFDinDisplayPro-Regular"};

		private url:string;
		private vo:BaseVO;
		private assets:LoadQueue;
		public retried_loading_times:number = 0;
		public assetsCount:number = 0;
		public assetsErrors:any[] = [];

		constructor(url:string) {
			super();
			this.url = url;
		}

		public load():void {
			console.log("Start load layout URL = " + this.url);
			var assetVO:AssetVO = new AssetVO(LayoutLoader.LAYOUT_ASSET_ID, this.url, LoadQueue.JSON);
			var loader:LoadQueue = new LoadQueue(false);
			loader.on("complete", () => {
				//console.log("Finish load layout URL = " + this.url);
				var layout:Object = loader.getResult(LayoutLoader.LAYOUT_ASSET_ID);
				layout = this.replaceFontsSpaces(layout, LayoutLoader.REPLACE_FONTS);
				this.vo = BaseVO.parse(layout);
				this.startLoadTexture();
			});
			loader.loadFile(assetVO);
		}

		public replaceFontsSpaces(layout:Object, dict:any):Object{
			var layoutstr:string = JSON.stringify(layout);
			for(var key in dict) {
				layoutstr = layoutstr.split(key).join(dict[key]);
			}
			return <Object>JSON.parse(layoutstr);
		}

		public getVO():BaseVO {
			return this.vo;
		}

		public getAssets():LoadQueue {
			return this.assets;
		}

		private startLoadTexture():void {
			var assetFolderURL:string = URLUtils.getFolderUrl(this.url) + "/" + FileConst.MAIN_RES_FOLDER;
			var assetsFinder:AssetFinder = new AssetFinder();
			assetsFinder.find(this.vo);
			var assetsVO:Array<AssetVO> = assetsFinder.getAssets();

			if(this.retried_loading_times>=LayoutLoader.RETRY_LOADING_TIMES) {
				for (var i:number = 0; i < this.assetsErrors.length; ++i) {
					assetsVO[this.assetsErrors[i]].src = LayoutLoader.EMPTY_IMAGE_NAME;
				}
				this.assetsErrors = [];
			}
			this.assetsCount = assetsVO.length;

			this.assets = new LoadQueue(true);
			this.assets.on("complete", () => {
				this.dispatchEvent(LayoutLoader.EVENT_ON_LOADED);
			});
			this.assets.on("fileload", (eventobj:any) => {
				this.dispatchEvent(LayoutLoader.EVENT_ON_ASSETFILE_PROGRESS);
			});
			this.assets.stopOnError = true;
			this.assets.on("error", (eventobj:any) => {
				if(++this.retried_loading_times<LayoutLoader.RETRY_LOADING_TIMES){
					setTimeout(()=>{this.dispatchEvent(LayoutLoader.EVENT_ON_FAILED)}, LayoutLoader.RETRY_LOADING_TIME);
				}else{
					for(var i:number=0; i<assetsVO.length; ++i){
						if(assetsVO[i].src==eventobj.data.src){
							this.assetsErrors.push(i);
							setTimeout(()=>{this.dispatchEvent(LayoutLoader.EVENT_ON_FAILED)}, LayoutLoader.RETRY_LOADING_TIME);
						}
					}
				}
			});
			this.assets.loadManifest(assetsVO, true, assetFolderURL);
		}
	}
}
