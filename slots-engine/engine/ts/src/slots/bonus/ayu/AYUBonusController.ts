module engine {
	import Container = createjs.Container;
	import Button = layout.Button;
	import Sound = createjs.Sound;

	export class AYUBonusController extends BaseBonusController {
		private view:AYUBonusView;
		private itemId:number;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
			super(manager, common, container, 0, false);
			common.originBonus = common.server.bonus;
		}

		public init():void {
			super.init();
		}

		public create():void {
			super.create();
			this.send(NotificationList.REMOVE_HEADER);
			this.view = <AYUBonusView>this.common.layouts[AYUBonusView.LAYOUT_NAME];
			this.view.create();
			this.startBonusGame();
			this.container.addChild(this.view);
		}

		private initHandlers():void {
			console.log("initHandlers");
			var items:Array<Button> = this.view.getItems();
			for (var i:number = 0; i < items.length; ++i) {
				var item:Button = items[i];
				item.on("click", (eventObj:any)=> {
					if (eventObj.nativeEvent instanceof MouseEvent) {
						SoundController.playSoundjs(SoundsList.REGULAR_CLICK_BTN);
						var currentItem:Button = <Button>eventObj.currentTarget;
						this.itemId = AYUBonusView.getItemIdByName(currentItem.name);
						this.view.processItemClick(this.itemId, ()=>{this.sendRequest(this.itemId);});
						this.removeHandlers();
					}
				});
			}
		}

		private removeHandlers():void {
			var items:Array<Button> = this.view.getItems();
			for (var i:number = 0; i < items.length; i++) {
				items[i].removeAllEventListeners("click");
			}
		}

		public startBonusGame():void{
			this.view.showBonus(()=>{this.view.startBonusGame(); this.initHandlers();});
		}

		public onGotResponse(xml:any):void {
			//var data:any = XMLUtils.getElement(xml, "data");
			//var counter = XMLUtils.getAttributeInt(xml, "counter");
			//var all:number = XMLUtils.getAttributeInt(xml, "all");
			//this.common.server.bonus.step = all - counter;
			this.common.skipFSStartPopup = true;
			this.send(NotificationList.START_AYU_BONUS, this.itemId); //send new freespins notification and provide spinsCnt and freespinstype
		}

		public sendRequest(param:number = 0):void {
			super.sendRequest(param);
		}

		public dispose():void {
			super.dispose();
			this.container.removeChild(this.view);
			this.send(NotificationList.SHOW_HEADER);
		}
	}
}