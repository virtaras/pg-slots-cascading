module engine {
	import Container = createjs.Container;
	import Button = layout.Button;
	import Layout = layout.Layout;
	import Ticker = createjs.Ticker;
	import Sound = createjs.Sound;
	import LayoutCreator = layout.LayoutCreator;

	export class ReSpinsControllerHotsevens extends BaseBonusController {
		public static RS_RESTORE_POPUP_LAYOUT_NAME:string = "RespinsRestoreView";
		public static LAYOUT_PARAMS:any = {"RESPINS_IDS":[2,4], "FREESPINS_IDS":[3]};
		public static RESPINS_STATE:string = "respins_state";
		public static FREESPINS_STATE:string = "freespins_state";
		public static UPDATE_WIN_TIME_SEC:number = 0.5;
		public static UPDATE_WIN_TIME:number = 0;

		private view:FreeSpinsView;
		private state:string;
		private finishedRespins:boolean = false;
		private finishedBonus:boolean = false;
		private isFSStart:boolean = false;
		private isFS:boolean = false;
		private countFS:number;
		private countFSPrev:number = 0;
		private allFS:number;
		private madeSpins:number = 0;
		private gameWin:number = 0;
		private featureWin:number = 0;
		private addFSCount:number = 0;
		private multiplicator:number = 1;
		private spinWin:number = 0;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
			super(manager, common, container, 0, false);
			ReSpinsControllerHotsevens.UPDATE_WIN_TIME = Utils.float2int(ReSpinsControllerHotsevens.UPDATE_WIN_TIME_SEC * Ticker.getFPS());
		}

		public init():void {
			super.init();
		}

		public create():void {
			super.create();
			this.send(NotificationList.REMOVE_HEADER);
			//this.send(NotificationList.SHOW_WIN_TF);
			this.view = <FreeSpinsView>this.common.layouts[FreeSpinsView.LAYOUT_NAME];
			this.view.create();
			this.onCreate();
			this.setFields();
			console.log("create this.common.restore="+this.common.restore+" this.common.server.bonus",this.common.server.bonus);
			if(this.common.restore || (this.state==ReSpinsControllerHotsevens.RESPINS_STATE && this.common.server.bonus.step==0 && !this.common.server.bonus.serverData.textContent)){
				this.processRestore();
				//this.onStartFS();
				//this.processStep();
			}
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.START_BONUS);
			notifications.push(NotificationList.STOPPED_ALL_REELS);
			notifications.push(NotificationList.RECURSION_PROCESSED);
			notifications.push(NotificationList.SHOW_LINES);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			super.handleNotification(message, data);
			switch (message) {
				case NotificationList.START_BONUS:
				{
					break;
				}
				case NotificationList.STOPPED_ALL_REELS:
				{
					console.log("this.featureWin",this.featureWin);
					this.view.setWin(this.featureWin, ReSpinsControllerHotsevens.UPDATE_WIN_TIME);
					this.send(NotificationList.SHOW_WIN_LINES, true);
					//this.send(NotificationList.SHOW_WIN_TF);
					break;
				}
				case NotificationList.RECURSION_PROCESSED:
				{
					if(!this.common.restore) {
						this.processStep();
					}
					break;
				}
				case NotificationList.SHOW_LINES:
				{
					this.send(NotificationList.SHOW_WIN_TF);
					break;
				}
			}
		}

		public processStep():void{
			console.log("this.finishedRespins="+this.finishedRespins+" this.finishedBonus="+this.finishedBonus+" this.isFSStart="+this.isFSStart);
			this.onStartFS();
			if(this.finishedRespins){
				this.common.isAllSticky = false;
				this.send(NotificationList.SHOW_WIN_TF);
				this.send(NotificationList.UPDATE_BALANCE_TF);
			}
			if(this.finishedBonus){
				this.onFinish();
				return;
			}
			if(this.isFSStart){
				this.isFSStart = false;
				this.showStartPopup();
			}else {
				this.nextSpin();
			}
		}

		private nextSpin():void{
			//console.log("nextSpin this.common.isAllSticky", this.common.isAllSticky);
			this.common.restore = false;
			++this.madeSpins;
			this.send(NotificationList.REMOVE_WIN_LINES);
			this.send(NotificationList.START_SPIN);
			this.sendRequest();
		}

		public onGotResponse(data:any):void {
			this.finishedBonus = false;
			this.finishedRespins = false;
			this.parseResponse(data);
			this.send(NotificationList.SERVER_GOT_SPIN);
			this.setLeftSpins();
		}

		private parseResponse(xml:any):void {
			this.allFS = XMLUtils.getAttributeInt(xml, "all") || 0;
			this.countFS = XMLUtils.getAttributeInt(xml, "counter") || 0;
			this.gameWin = XMLUtils.getAttributeFloat(xml, "gamewin") || 0;
			this.multiplicator = XMLUtils.getAttributeFloat(xml, "mp") || 0;
			this.state = (this.countFS) ? ReSpinsControllerHotsevens.FREESPINS_STATE : ReSpinsControllerHotsevens.RESPINS_STATE;
			this.finishedBonus = (this.state==ReSpinsControllerHotsevens.FREESPINS_STATE && !this.countFS);

			var data:any = XMLUtils.getElement(xml, "data");
			this.common.server.setBalance(parseFloat(XMLUtils.getElement(xml, "balance").textContent));
			this.common.server.serverWin = this.common.server.win = parseFloat(XMLUtils.getElement(xml, "win").textContent);
			this.featureWin = (data) ? (XMLUtils.getAttributeFloat(data, "summ") || 0) : 0;
			this.parseSpinData(data);
			switch(this.state){
				case ReSpinsControllerHotsevens.FREESPINS_STATE:
				{
					this.isFSStart = (!this.common.restore && (this.countFS>this.countFSPrev));
					this.isFS = this.isFS || (this.countFS>this.countFSPrev);
					this.countFSPrev = this.countFS;
					break;
				}
				case ReSpinsControllerHotsevens.RESPINS_STATE:
				{
					//this.common.isAllSticky = true;
					break;
				}
			}
			//console.log("this.state="+this.state+" this.countFS="+this.countFS+" this.allFS="+this.allFS+" this.common.restore="+this.common.restore+" this.isFSStart="+this.isFSStart+" this.madeSpins="+this.madeSpins);
		}

		private parseSpinData(xml:any):void {
			if(xml) {
				this.common.wild_reels_expanding_static = [];
				var spinData:any = XMLUtils.getElement(xml, "spin");
				var wheelsData:any = XMLUtils.getElement(spinData, "wheels").childNodes;
				this.spinWin = parseFloat(XMLUtils.getElement(spinData, "win").textContent) || 0;
				for (var i = 0; i < wheelsData.length; i++) {
					var wheelData:Array<number> = Utils.toIntArray(wheelsData[i].childNodes[0].data.split(","));
					for (var j:number = 0; j < wheelData.length; j++) {
						this.common.server.wheel[i * wheelData.length + j] = wheelData[j];
					}
				}
				this.common.server.winLines = [];
				this.parseWinLinesData(spinData, "winposition");
				this.parseWinLinesData(spinData, "position");
				this.parseScattersWinlines();
				var bonus:any = XMLUtils.getElement(spinData, "bonus");
				//this.state = (ReSpinsController.LAYOUT_PARAMS.RESPINS_IDS.indexOf(XMLUtils.getAttributeInt(bonus, "id"))>=0) ? ReSpinsController.RESPINS_STATE : ReSpinsController.FREESPINS_STATE;
				//if(!this.countFS && !this.countFSPrev && ReSpinsController.LAYOUT_PARAMS.FREESPINS_IDS.indexOf(XMLUtils.getAttributeInt(bonus, "id"))<0){
				//	this.state = ReSpinsController.RESPINS_STATE;
				//}
				this.finishedRespins = (ReSpinsControllerHotsevens.LAYOUT_PARAMS.RESPINS_IDS.indexOf(XMLUtils.getAttributeInt(bonus, "id"))>=0) ? false : true;
				this.common.isAllSticky = !this.finishedRespins;
				//if(ReSpinsController.LAYOUT_PARAMS.RESPINS_IDS.indexOf(XMLUtils.getAttributeInt(bonus, "id"))>=0){
				//	this.finishedRespins = false;
				//	this.common.isAllSticky = true;
				//}else{
				//	this.finishedRespins = true;
				//	this.common.isAllSticky = false;
				//}
				if(this.state==ReSpinsControllerHotsevens.RESPINS_STATE){
					this.finishedBonus = !XMLUtils.getAttributeInt(bonus, "id");
				}
			}
		}

		private parseWinLinesData(spinData:any, nodeName:string):void{
			try {
				var winLinesData:any = XMLUtils.getElement(spinData, nodeName).childNodes;
				for (var i:number = 0; i < winLinesData.length; i++) {
					var winLine:any = winLinesData[i];
					var winLineVO:WinLineVO = new WinLineVO();
					winLineVO.lineId = XMLUtils.getAttributeInt(winLine, "line");
					winLineVO.win = XMLUtils.getAttributeFloat(winLine, "win");
					winLineVO.winPos = Utils.toIntArray(winLine.childNodes[0].data.split(","));
					this.common.server.winLines.push(winLineVO);
					if(!this.spinWin){
						this.common.server.win += winLineVO.win;
						this.featureWin += winLineVO.win;
					}
				}
			}catch(e){
				console.log("parseWinLinesData "+nodeName, e);
			}
			return;
		}

		private parseScattersWinlines():void{
			if(ReelsView.LAYOUT_PARAMS.SCATTERS_IDS) {
				var scatter_cnt:number = 0;
				var scatterWinlines:Array<WinLineVO> = [];
				var scatterWinlinesId:number = 0;
				for (var i:number = 0; i < this.common.server.wheel.length; ++i) {
					if (ReelsView.LAYOUT_PARAMS.SCATTERS_IDS.indexOf(this.common.server.wheel[i]) >= 0) {
						scatterWinlinesId = i % ReelMover.SYMBOLS_IN_REEL;
						scatterWinlines[scatterWinlinesId] = (scatterWinlines[scatterWinlinesId]) ? scatterWinlines[scatterWinlinesId] : new WinLineVO();
						scatterWinlines[scatterWinlinesId].lineId = 0;
						scatterWinlines[scatterWinlinesId].win = 0;
						scatterWinlines[scatterWinlinesId].winPos = (scatterWinlines[scatterWinlinesId].winPos) ? scatterWinlines[scatterWinlinesId].winPos : [0, 0, 0, 0, 0];
						scatterWinlines[scatterWinlinesId].winPos[parseInt(i/ReelMover.SYMBOLS_IN_REEL)] = scatterWinlinesId + 1;
						++scatter_cnt;
					}
				}
				if(scatter_cnt>=ReelMover.SYMBOLS_IN_REEL) {
					for (var key in scatterWinlines) {
						this.common.server.winLines.push(scatterWinlines[key]);
					}
				}
			}
		}

		private onFinish():void{
			if(this.isFS){
				this.showResultPopup();
			}else{
				this.send(NotificationList.END_BONUS);
			}
			//switch(this.state){
			//	case ReSpinsController.FREESPINS_STATE:
			//	{
			//		this.showResultPopup();
			//		break;
			//	}
			//	case ReSpinsController.RESPINS_STATE:
			//	{
			//		this.send(NotificationList.END_BONUS);
			//		break;
			//	}
			//}
		}

		public showStartPopup():void{
			console.log("showStartPopup");
			var startPopup:FreeSpinsStartView = <FreeSpinsStartView>this.common.layouts[FreeSpinsStartView.LAYOUT_NAME];
			if(startPopup) {
				startPopup.create();
				startPopup.alpha = 1;
				startPopup.setFreeSpinsCount(this.countFS);
				this.container.addChild(startPopup);
				startPopup.on("click", ()=>{
					startPopup.removeAllEventListeners("click");
					startPopup.hide(()=> {
							this.container.removeChild(startPopup);
							this.onStartFS();
						}
					);
					this.nextSpin();
				})
			}
		}

		private onStartFS():void{
			if(this.isFS) {
				this.container.addChild(this.view);
			}
		}

		private showResultPopup():void {
			var resultView:FreeSpinsResultView = <FreeSpinsResultView>this.common.layouts[FreeSpinsResultView.LAYOUT_NAME];
			resultView.create();
			resultView.alpha = 1;
			resultView.setTextPositions(this.common.isMobile);
			resultView.setGameWin(this.gameWin);
			resultView.setFeatureWin(this.featureWin);
			var totalWin:number = this.gameWin + this.featureWin;
			resultView.setTotalWin(totalWin);

			var collectBtn:Button = resultView.getCollectBtn();
			collectBtn.removeAllEventListeners("click");
			collectBtn.on("click", (eventObj:any)=> {
				if (eventObj.nativeEvent instanceof MouseEvent) {
					collectBtn.removeAllEventListeners("click");
					this.hideResultPopup();
				}
			});
			this.container.addChild(resultView);
		}

		private hideResultPopup():void{
			var resultView:FreeSpinsResultView = <FreeSpinsResultView>this.common.layouts[FreeSpinsResultView.LAYOUT_NAME];
			resultView.hide(()=> {
					this.common.server.bonus = null;
					var collectSound:string = SoundsList.COLLECT;
					SoundController.playSoundjs(collectSound);
					this.send(NotificationList.END_BONUS);
					this.send(NotificationList.UPDATE_BALANCE_TF);
					this.send(NotificationList.START_NORMAL_GAME_BG_SOUND);
					this.common.canRemoveWildsExpandingStatic = true;
					this.container.removeChild(resultView);
					this.container.removeChild(this.view);
				}
			)
		}

		public onCreate():void{
			if(ReSpinsControllerHotsevens.LAYOUT_PARAMS.RESPINS_IDS.indexOf(this.common.server.bonus.id)<0){
				this.state = ReSpinsControllerHotsevens.FREESPINS_STATE;
			}else{
				this.state = ReSpinsControllerHotsevens.RESPINS_STATE;
			}
		}
		public processWinLines():void{
			if(this.common.server.winLines){
				this.send(NotificationList.SHOW_WIN_LINES, true);
			}
		}
		public processRestore():void{
			this.processWinLines();
			switch(this.state){
				case ReSpinsControllerHotsevens.FREESPINS_STATE:
				{
					this.showRestorePopupFS();
					break;
				}
				default:
				{
					this.common.isAllSticky = true;
					this.showRestorePopupRS();
					break;
				}
			}
		}
		public showRestorePopupFS():void{
			console.log("showRestorePopupFS");
			var startPopup:FreeSpinsRestoreView = <FreeSpinsRestoreView>this.common.layouts[FreeSpinsRestoreView.LAYOUT_NAME];
			if(startPopup) {
				startPopup.create();
				this.container.addChild(startPopup);
				startPopup.on("click", ()=>{
					startPopup.removeAllEventListeners("click");
					this.container.removeChild(startPopup);
					this.onStartFS();
					this.nextSpin();
				})
			}
		}
		public showRestorePopupRS():void{
			console.log("showRestorePopupRS");
			//var startPopup:Layout = <Layout>this.common.layouts[ReSpinsControllerHotsevens.RS_RESTORE_POPUP_LAYOUT_NAME];
			var startPopup:RespinsRestoreView = <RespinsRestoreView>this.common.layouts[RespinsRestoreView.LAYOUT_NAME];
			if(startPopup) {
				startPopup.create();
				this.container.addChild(startPopup);
				startPopup.on("click", ()=>{
					startPopup.removeAllEventListeners("click");
					this.container.removeChild(startPopup);
					this.onStartFS();
					this.nextSpin();
				})
			}
		}

		private setFields():void{
			this.setLeftSpins();
			this.view.setMultiplicator(this.multiplicator);
			this.view.setWin(this.featureWin);
		}
		public setLeftSpins():void{
			if(!this.addFSCount) {
				this.view.setLeftSpins(this.countFS || 0);
			}
		}
		public onEnterFrame():void {
			if (this.view != null) {
				this.view.update();
			}
		}
	}
}