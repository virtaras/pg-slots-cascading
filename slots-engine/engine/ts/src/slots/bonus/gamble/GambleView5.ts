module engine {
	import Tween = createjs.Tween;
	import Ticker = createjs.Ticker;
	import Text = createjs.Text;
	import Container = createjs.Container;
	import DisplayObject = createjs.DisplayObject;
	import Sprite = createjs.Sprite;
	import Layout = layout.Layout;
	import Shape = createjs.Shape;
	import Rectangle = createjs.Rectangle;
	import Button = layout.Button;
	import Sound = createjs.Sound;

	export class GambleView5 extends Layout {
		public static LAYOUT_NAME:string = "GambleView5";

		public static INVISIBLE_TIME:number = 0.5;
		public static DELAY_REMOVE:number = 3;
		public static DELAY_SHOW_ITEM:number = 300;

		private static MESSAGE_TEXT:string = "massageTf";
		private static DOUBLE_TEXT:string = "doubleTf";
		private static BANK_TEXT:string = "bankTf";
		private static HALF_TEXT:string = "halfTf";
		private static TOTAL_BET_TEXT:string = "betTf";
		public static COLLECT_BTN:string = "collectBtn";
		public static HALF_BTN:string = "halfBtn";
		public static DOUBLE_BTN:string = "doubleBtn";

		private textsPos:boolean;
		private cardNumbersFrom:number = 2;
		private cardNumbersTo:number = 6;

		public buttonsNames:Array<string> = [
			GambleView5.COLLECT_BTN,
			GambleView5.HALF_BTN,
			GambleView5.DOUBLE_BTN
		];

		private allTitles:Array<string> = [
			GambleView5.MESSAGE_TEXT,
			GambleView5.HALF_TEXT,
			GambleView5.DOUBLE_TEXT,
			GambleView5.BANK_TEXT,
			GambleView5.TOTAL_BET_TEXT
		];

		private historyData:Array<number>;

		constructor() {
			super();
			GambleView5.INVISIBLE_TIME = Utils.float2int(GambleView5.INVISIBLE_TIME  * Ticker.getFPS());
			GambleView5.DELAY_REMOVE = Utils.float2int(GambleView5.DELAY_REMOVE  * Ticker.getFPS());
		}

		public setTextPositions(isMobile:boolean){
			if(!this.textsPos){
				for(var n:number = 0; n < this.allTitles.length; n++){
					var title:Text = <Text>this.getChildByName(this.allTitles[n]);
					if(title != null){
						title.y = (isMobile) ? title.y + title.lineHeight / 2.5 : title.y + title.lineHeight / 2;
						title.color = '#fff';
						title.alpha = 1;
					}
				}
			}
			this.alphaTextFild(4,0);
			this.textsPos = true;
		}

		public alphaTextFild(index:number,a:number):void {
			var title:Text = <Text>this.getChildByName(this.allTitles[index]);
			title.alpha = a;
		}

		public getButton(name:string):Button {
			return <Button>this.getChildByName(name);
		}

		public setBankValue(value:number):void {
			var tf:Text = <Text>this.getChildByName(GambleView5.BANK_TEXT);
			tf.text = MoneyFormatter.format(value, true);
		}

		public setTotalValue(value:number):void {
			var tf:Text = <Text>this.getChildByName(GambleView5.TOTAL_BET_TEXT);
			if(tf != null)tf.text = MoneyFormatter.format(value, true);
			tf.alpha = 1;
		}

		public setBetValue(value:number, index:number):void {
			console.log(value, index);
			var tf:Text = <Text>this.getChildByName(this.allTitles[index]);
			tf.text = MoneyFormatter.format(value, true);
		}

		public setMessageText(text:string):void {
			var tf:Text = <Text>this.getChildByName(GambleView5.MESSAGE_TEXT);
			if(tf != null)tf.text = text;
		}

		public changeButtonState(btnName:string, visible:boolean):void {
			var button:Button = this.getButton(btnName);
			if (button) {
				button.visible = visible;
			}
		}

		public showCard(cardId:number = 0):void {
			var cardName:string = 'card_1';
			var back:DisplayObject = this.getChildByName(cardName);
			back.visible = false;
			var card:Sprite = <Sprite>this.getChildByName(cardName);
			card.gotoAndStop(cardId);
			card.visible = true;
		}

		public hideCard():void {
			for (var i:number = 1; i < 6; i++){
				var cardName:string = 'card_' + i;
				var card:Sprite = <Sprite>this.getChildByName(cardName);
				this.resetLot(card);
			}
		}

		private resetLot(card:Sprite):void {
			Tween.get(card, {useTicks: true}).wait(GambleView5.DELAY_REMOVE).call(function(){card.gotoAndStop(0);card.alpha = 1;});
		}

		public setHistory(historyData:Array<number>):void {
			this.historyData = [];
			this.historyData = historyData;
		}

		public showHistory(i:number):void {
			var cardIndex:number = i + 1;
			var cardName:string = 'card_' + cardIndex;
			var back:DisplayObject = this.getChildByName(cardName);
			back.visible = false;
			var card:Sprite = <Sprite>this.getChildByName(cardName);
			card.gotoAndStop(this.historyData[i - 1]);
			card.visible = true;
			var n:number = 1;
			for (var c:number = 2; c < 6; c++){
				if((i + 1) != c){
					this.showItem(c, 0.5, n * GambleView5.DELAY_SHOW_ITEM);
					n++;
				}
			}
		}

		private showItem(index:number, alpha:number, sec:number):void {

			var cardName:string = 'card_' + index;
			var card:Sprite = <Sprite>this.getChildByName(cardName);
			card.visible = false;
			card.gotoAndStop(0);card.alpha = 1;card.visible=true;

			var back:DisplayObject = this.getChildByName(cardName);

			var self:any = this;
			Tween.get(card).wait(sec).call(function(){
				back.visible = false;card.visible = true;card.gotoAndStop(self.historyData[index - 2]); card.alpha = alpha;
				SoundController.playSoundjs(SoundsList.CARD_TURN);
			}, {self : self, alpha: alpha});

		}


		public hideBonus(isDelay:boolean, callback:(...params:any[]) => any):void {
			this.removeCardHandlers();
			var tween:Tween = Tween.get(this, {useTicks: true});
			if (isDelay) {
				tween.wait(GambleView5.DELAY_REMOVE);
			}
			tween.to({alpha: 0}, GambleView5.INVISIBLE_TIME);
			tween.call(callback);
		}

		public removeCardHandlers():void{
			for (var n:number = 2; n < 6; n++) {
				var cardName:string = 'card_' + n;
				var targetCard:Button = this.getButton(cardName);
				targetCard.cursor = "default";
				targetCard.removeAllEventListeners("click");
			}
		}
	}
}