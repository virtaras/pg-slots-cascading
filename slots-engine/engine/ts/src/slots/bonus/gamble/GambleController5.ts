module engine {
	import Tween = createjs.Tween;
	import Ticker = createjs.Ticker;
	import Button = layout.Button;
	import Container = createjs.Container;
	import MoneyFormatter = engine.MoneyFormatter;

	export class GambleController5 extends BaseBonusController {
		private static SHOW_MESSAGE_TIME:number = 2;
		private static SHOW_MESSAGE_TIME_DEFAULT:number = 2;
		private static CARDS_COUNT:number = 13;

		private static MESSAGE_DEFAULT:string = "CHOOSE DOUBLE OR DOUBLE HALF";
		private static MESSAGE_DEFAULT_SECOND:string = "PICK A HIGHER CARD";
		private static MESSAGE_WIN:string = "YOU WIN: ";
		private static MESSAGE_LOSE:string = "DEALER WINS";
		public static CARD_PREFIX:string = "card_"

		private view:GambleView5;
		private history:Array<number>;
		private isFirst:boolean;
		private stepBonus:number;
		private selectCardIndex:number;

		private XMLBalance:number;

		private serverBet:number = 0;
		private serverBank:number = 0;
		private serverWin:number = 0;
		private serverGBet:number = 0;
		private currentBank:number = 0;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
			super(manager, common, container, -1, true);
			GambleController5.SHOW_MESSAGE_TIME = Utils.float2int(GambleController5.SHOW_MESSAGE_TIME_DEFAULT * Ticker.getFPS());
			this.setMessagesLang();
		}

		public setMessagesLang():void{
			GambleController5.MESSAGE_DEFAULT = this.common.config.gamble5_message_default || GambleController5.MESSAGE_DEFAULT;
			GambleController5.MESSAGE_DEFAULT_SECOND = this.common.config.gamble5_message_default_second || GambleController5.MESSAGE_DEFAULT_SECOND;
			GambleController5.MESSAGE_WIN = this.common.config.gamble5_message_win || GambleController5.MESSAGE_WIN;
			GambleController5.MESSAGE_LOSE = this.common.config.gamble5_message_lose || GambleController5.MESSAGE_LOSE;
		}

		public init():void {
			super.init();
			this.isFirst = true;
		}

		public create():void {
			super.create();
			this.view = <GambleView5>this.common.layouts[GambleView5.LAYOUT_NAME];
			this.view.create();
			this.view.alpha = 1;
			this.stepBonus = 1;
			this.container.addChild(this.view);
			this.initHandlers();
			this.send(NotificationList.REMOVE_HEADER);
			this.view.setTextPositions(this.common.isMobile);
		}

		private initCardsCover():void {
			var cardName:string;
			for (var n:number = 2; n < 6; n++) {
				cardName = GambleController5.CARD_PREFIX + n;
				var targetCard:Button = this.view.getButton(cardName);
				targetCard.cursor = "pointer";
				targetCard.on("click", (eventObj:any)=> {
					if (eventObj.nativeEvent instanceof MouseEvent) {
						this.onCardCoverClick(eventObj.currentTarget.name);
					}
				});
			}
		}
		private disableCardsCover():void {
			var cardName:string;
			for (var n:number = 1; n < 6; n++) {
				cardName = GambleController5.CARD_PREFIX + n;
				var targetCard:Button = this.view.getButton(cardName);
				targetCard.cursor = "default";
			}
		}

		public onGotResponse(data:any):void {
			this.parseResponse(data);
			// show card
			switch(this.stepBonus){
				case 1:
					this.resetGame();
					this.setBankValues();
					this.disableCardsCover();
					break;
				case 2:
					this.view.showCard(this.history.shift());
					break;
				case 3:
					this.setBankValues();
					this.view.showCard(this.history.shift());
					this.setBankMessage();
					this.disableCardsCover();
					break;
			}

			this.common.restore = false;
		}

		public setBankValues():void{
			this.view.setBankValue(this.currentBank);
			this.view.setBetValue(this.currentBank * 2, 2);
			this.view.setBetValue(this.currentBank * 1.5, 1);
		}

		private setBankMessage():void{
			this.view.setHistory(this.history);
			this.view.showHistory(this.selectCardIndex);
			var self = this;
			if ((this.serverWin + this.serverBet) == 0){
				this.view.setMessageText(GambleController5.MESSAGE_LOSE);
				this.view.setTotalValue(this.currentBank);
				this.removeHandlers();
				this.view.hideCard();
				this.common.server.win = this.currentBank; //TO FINISH
				this.view.hideBonus(true, ()=> {
					this.common.server.bonus = null;
					this.send(NotificationList.END_BONUS);
					this.send(NotificationList.UPDATE_BALANCE_TF);
					this.send(NotificationList.SHOW_WIN_TF);
					this.send(NotificationList.SHOW_HEADER);
				});
				this.common.server.setBalance(this.XMLBalance);
			}
			else {
				this.common.server.setBalance(this.XMLBalance + this.currentBank);
				this.view.setMessageText(GambleController5.MESSAGE_WIN + MoneyFormatter.format(this.currentBank, false));
				this.view.hideCard();
				this.common.server.bonus.step = 0;
				this.common.server.win = this.currentBank; //TO FINISH
				setTimeout(function(self){self.resetGame()},3300,self); //think
			}
		}


		public sendRequest(param:number = 0):void {
			super.sendRequest(param);
			this.lockBonus(true);
		}

		public dispose():void {
			super.dispose();
			this.container.removeChild(this.view);
			//this.view = null;
		}

		private parseResponse(xml:any):void {
			this.history = [];
			var XMLBalance:any = XMLUtils.getElement(xml, "balance");
			//console.log("XMLBalance",XMLBalance);
			if(XMLBalance.textContent) {
				this.XMLBalance = parseFloat(XMLBalance.textContent);
			}
			this.serverBank = Math.abs(parseFloat(XMLUtils.getElement(xml, "win").textContent));

			var data:any = XMLUtils.getElement(xml, "data");
			if(data != null){
				var wheels:any = XMLUtils.getElement(data, "wheels");

				this.serverBet = XMLUtils.getAttributeFloat(wheels, "bet") || 0;
				this.serverBank = XMLUtils.getAttributeFloat(wheels, "bank") || 0;
				this.serverWin = XMLUtils.getAttributeFloat(wheels, "win") || 0;
				this.serverGBet = XMLUtils.getAttributeFloat(wheels, "gbet") || 0;

				this.selectCardIndex = XMLUtils.getAttributeInt(wheels,"selected");

				//var items:any = wheels.children;
				var items:any = XMLUtils.getChildrenElements(wheels);
				var item:any;

				for (var i:number = items.length - 1; i >= 0; i--) {
					item = items[i];
					//TODO: нужно чтобы сервер присылал реальный id карты в калоде от 1 - 52
					// on server suit from 1 to 4
					var suit:number = parseInt(item.attributes.suit.value) - 1;
					// on server card id from 2 to 14
					var cardId:number = parseInt(item.textContent) - 1;
					var frameId:number = cardId + (GambleController5.CARDS_COUNT * suit);
					this.history.unshift(frameId);
				}

				if(this.common.restore){
					if(this.serverBet>0 && (this.serverBank==0 || this.serverBank==this.serverBet) && this.serverWin==0){
						//step2
						this.stepBonus = 2;
						this.isFirst = false;
						var bet = 1;
						if(this.serverBank==0) bet = 2;
						this.showSecondStep(bet, false);
					}else if(this.serverBank>0 && this.serverGBet<this.serverBank){
						//step1
						this.stepBonus = 1;
						this.isFirst = true;
						//this.resetGame();
					}else{
						//step3
						this.stepBonus = 1;
						this.isFirst = false;
					}
				}

			}
			this.currentBank = Math.max(this.serverBet, this.serverBank + this.serverWin, this.serverBank + this.serverBet);
			this.stepBonus %= 4;
		}

		private resetGame():void {
			this.view.setMessageText(GambleController5.MESSAGE_DEFAULT);
			this.lockBonus(false);
			this.view.alphaTextFild(1,1);
			this.view.alphaTextFild(2,1);
			this.view.alphaTextFild(3,1);
			this.view.alphaTextFild(4,0);
			//this.view.setWhite(1);
			this.stepBonus = 1;
			GambleController5.SHOW_MESSAGE_TIME = Utils.float2int(GambleController5.SHOW_MESSAGE_TIME_DEFAULT * Ticker.getFPS());
		}

		private showSecondStep(bet:number, is_send_request:boolean = true):void {
			if(is_send_request){
				this.sendRequest(bet);
			}
			this.lockBonus(true);
			this.view.alphaTextFild(1,0);
			this.view.alphaTextFild(2,0);
			if(bet == 2)this.view.alphaTextFild(3,0);
			this.view.alphaTextFild(bet,1);
			var value:number = (bet == 1) ? 1.5 : 2;
			var bank:number = (bet == 1) ? 0.5 : 1;
			var currentbet = Math.max(this.serverBet, this.serverBank + this.serverWin, this.serverBank + this.serverBet);
			//var currentbank = (bet == 1) ? currentbet*bank : 0;
			this.view.setTotalValue(currentbet*bank);
			this.view.setBetValue(currentbet*value, bet);
			this.view.setBankValue(currentbet);
			this.initCardsCover();
			this.view.setMessageText(GambleController5.MESSAGE_DEFAULT_SECOND);
		}

		private initHandlers():void {
			var buttonsNames:Array<string> = this.view.buttonsNames;
			var buttonsCount:number = buttonsNames.length;
			for (var i:number = 0; i < buttonsCount; i++) {
				this.view.changeButtonState(buttonsNames[i], true);
				this.view.getButton(buttonsNames[i]).on("click", (eventObj:any)=> {
					if (eventObj.nativeEvent instanceof MouseEvent) {
						this.onBtnClick(eventObj.currentTarget.name);
					}
				});
			}
		}

		private removeHandlers():void {
			var buttonsNames:Array<string> = this.view.buttonsNames;
			var buttonsCount:number = buttonsNames.length;
			for (var i:number = 0; i < buttonsCount; i++) {
				this.view.getButton(buttonsNames[i]).removeAllEventListeners("click");
				//this.view.changeButtonState(buttonsNames[i], false);
			}
		}

		private onCardCoverClick(buttonName:string):void {
			if(this.stepBonus == 2){
				var name:Array<string> = buttonName.split('_');
				var indexCard:number = parseInt(name[1]);
				this.sendRequest(indexCard - 1);
				this.stepBonus++;
			}
		}

		private onBtnClick(buttonName:string):void {
			this.stepBonus++;
			switch (buttonName) {
				case GambleView5.COLLECT_BTN:
				{
					// remove bonus
					this.removeHandlers();
					this.common.server.setBalance(this.XMLBalance + this.currentBank);
					this.common.server.win = this.currentBank;
					this.view.hideBonus(false, ()=> {
						this.sendRequest();
						this.common.server.bonus = null;
						this.send(NotificationList.END_BONUS);
						this.send(NotificationList.UPDATE_BALANCE_TF);
						this.send(NotificationList.SHOW_WIN_TF);
						this.send(NotificationList.SHOW_HEADER);
					});
					break;
				}
				case GambleView5.HALF_BTN:
				{
					this.showSecondStep(1);
					break;
				}
				case GambleView5.DOUBLE_BTN:
				{
					this.showSecondStep(2);
					break;
				}
			}
		}

		private endGame():void {

		}

		private lockBonus(value:boolean):void {
			if (this.view != null) {
				var buttonsNames:Array<string> = this.view.buttonsNames;
				var buttonsCount:number = buttonsNames.length;
				for (var i:number = 0; i < buttonsCount; i++) {
					this.view.getButton(buttonsNames[i]).setEnable(!value);
					//this.view.getButton(buttonsNames[i]).visible = !value;
				}
			}
		}
	}
}