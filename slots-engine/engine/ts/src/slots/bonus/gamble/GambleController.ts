module engine {
	import Tween = createjs.Tween;
	import Ticker = createjs.Ticker;
	import Button = layout.Button;
	import Container = createjs.Container;

	export class GambleController extends BaseBonusController {
		private static SHOW_MESSAGE_TIME:number = 2;
		private static SHOW_MESSAGE_TIME_COMP:number;
		private static CARDS_COUNT:number = 13;

		private static MESSAGE_DEFAULT:string = "";
		//private static MESSAGE_DEFAULT:string = "CHOOSE RED OR BLACK";
		private static MESSAGE_WIN:string = "YOU WIN: ";
		private static MESSAGE_LOSE:string = "DEALER WINS";

		private view:GambleView;
		private bank:number;
		private win:number;
		private history:Array<number>;
		private isFirst:boolean;

		private serverStep:number = null;
		private restore:boolean = false;
		private XMLBalance:number = 0;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
			// TODO: нужно чтобы на сервере был стандартный step=0
			super(manager, common, container, -1, true);
			GambleController.SHOW_MESSAGE_TIME_COMP = Utils.float2int(GambleController.SHOW_MESSAGE_TIME * Ticker.getFPS());
			this.setMessagesLang();
		}

		public setMessagesLang():void{
			GambleController.MESSAGE_DEFAULT = this.common.config.gamble_message_default || GambleController.MESSAGE_DEFAULT;
			GambleController.MESSAGE_WIN = this.common.config.gamble_message_win || GambleController.MESSAGE_WIN;
			GambleController.MESSAGE_LOSE = this.common.config.gamble_message_lose || GambleController.MESSAGE_LOSE;
		}

		public init():void {
			super.init();
			this.isFirst = true;
			this.restore = this.common.restore;
		}

		public create():void {
			super.create();
			this.view = <GambleView>this.common.layouts[GambleView.LAYOUT_NAME];
			this.view.create();
			this.view.alpha = 1;
			this.bank = this.common.server.win;
			this.send(NotificationList.REMOVE_HEADER);
			this.container.addChild(this.view);
			this.initHandlers();
			this.view.setTextPositions();
		}

		public processStep(){
			//console.log(this.serverStep, "serverStep");
			switch(this.serverStep){
				case 0:{
					if (this.isFirst) {
						this.view.setMessageText(GambleController.MESSAGE_DEFAULT);
						this.lockBonus(false);
					}
					this.hideCard();
					break;
				}
				case 1:{
					this.send(NotificationList.SHOW_CARD);
					if (this.win == 0) {
						this.common.server.setBalance(this.XMLBalance);
						this.view.setMessageText(GambleController.MESSAGE_LOSE);
						// remove bonus
						this.removeHandlers();
						this.common.server.win = this.bank;
						this.view.hideBonus(true, ()=> {
							this.common.server.bonus = null;
							this.send(NotificationList.SHOW_WIN_TF);
							this.send(NotificationList.UPDATE_BALANCE_TF);
							this.send(NotificationList.END_BONUS);
						})
					}
					else {
						//this.bank = this.win;
						this.common.server.setBalance(this.XMLBalance + this.bank);
						this.view.setMessageText(GambleController.MESSAGE_WIN + this.bank);
						Tween.get(this.view, {useTicks: true})
							.wait(GambleController.SHOW_MESSAGE_TIME_COMP)
							.call(()=> {
								this.hideCard();
								this.view.setMessageText(GambleController.MESSAGE_DEFAULT);
								this.lockBonus(false);
							});
					}
					if(this.history.length>0) {
						this.view.showCard(this.history[this.history.length - 1]);
					}
					break;
				}
			}
			this.view.setBankValue(this.bank);
			this.view.setDoubleValue(this.bank * 2);
			this.view.setQuadrapleValue(this.bank * 4);
			this.view.showHistory(this.history);
			if (this.isFirst) {
				this.isFirst = false;
			}
			if (this.restore) {
				this.restore = false;
			}
		}

		private hideCard():void {
			this.send(NotificationList.HIDE_CARD);
			this.view.hideCard();
		}

		public onGotResponse(data:any):void {
			this.parseResponse(data);
			this.processStep();
		}

		public sendRequest(param:number = 0):void {
			super.sendRequest(param);
			this.lockBonus(true);
		}

		public dispose():void {
			super.dispose();
			this.container.removeChild(this.view);
			this.view = null;
		}

		private parseResponse(xml:any):void {
			var XMLBalance:any = XMLUtils.getElement(xml, "balance");
			if(XMLBalance.textContent) {
				this.XMLBalance = parseFloat(XMLBalance.textContent);
				if(parseFloat(XMLBalance.textContent)<=0){
					this.XMLBalance = this.common.server.getBalance();
				}
			}
			this.bank = Math.abs(parseFloat(XMLUtils.getElement(xml, "win").textContent));
			var data:any = XMLUtils.getElement(xml, "data");
			this.history = [];
			if(data != null){
				var serverStep = XMLUtils.getElement(data, "step");

				if(this.serverStep == null) this.serverStep = 0;
				else this.serverStep = 1;
				var wheels:any = XMLUtils.getElement(data, "wheels");
				this.win = (wheels) ? XMLUtils.getAttributeFloat(wheels, "bet") || 0 : 0;
				this.bank = Math.max(this.bank, this.win);
				if(wheels){
					var items:any = XMLUtils.getChildrenElements(wheels);
					var item:any;
					var suit:number;
					var cardId:number;
					var frameId:number;
					for (var i:number = items.length - 1; i >= 0; i--) {
						item = items[i];
						//TODO: нужно чтобы сервер присылал реальный id карты в калоде от 1 - 52
						// on server suit from 1 to 4
						suit = parseInt(item.attributes.suit.value) - 1;
						// on server card id from 2 to 14
						cardId = parseInt(item.textContent) - 2;
						frameId = cardId + (GambleController.CARDS_COUNT * suit);
						this.history.push(frameId);
						if(this.history.length > 5)this.history.shift();
					}
				}

			}
		}

		private initHandlers():void {
			var buttonsNames:Array<string> = this.view.buttonsNames;
			var buttonsCount:number = buttonsNames.length;
			for (var i:number = 0; i < buttonsCount; i++) {
				var button:Button = this.view.getButton(buttonsNames[i]);
				if(button) {
					button.on("click", (eventObj:any)=> {
						if (eventObj.nativeEvent instanceof MouseEvent) {
							this.onBtnClick(eventObj.currentTarget.name);
						}
					});
				}
			}
		}

		private removeHandlers():void {
			var buttonsNames:Array<string> = this.view.buttonsNames;
			var buttonsCount:number = buttonsNames.length;
			for (var i:number = 0; i < buttonsCount; i++) {
				var button:Button = this.view.getButton(buttonsNames[i]);
				if(button){
					button.removeAllEventListeners("click");
				}
			}
		}

		private onBtnClick(buttonName:string):void {
			switch (buttonName) {
				case GambleView.COLLECT_BTN:
				{
					this.common.server.setBalance(this.XMLBalance + this.bank);
					this.removeHandlers();
					this.send(NotificationList.SHOW_HEADER);
					this.view.hideBonus(false, ()=> {
						this.common.server.win = this.bank;
						this.sendRequest();
						this.common.server.bonus = null;
						this.send(NotificationList.END_BONUS);
						this.send(NotificationList.SHOW_WIN_TF);
						this.send(NotificationList.UPDATE_BALANCE_TF);
					});
					break;
				}
				case GambleView.RED_BTN:
				{
					this.sendRequest(1);
					break;
				}
				case GambleView.BLACK_BTN:
				{
					this.sendRequest(2);
					break;
				}
				case GambleView.HEARTS_BTN:
				{
					this.sendRequest(3);
					break;
				}
				case GambleView.DIAMONDS_BTN:
				{
					this.sendRequest(4);
					break;
				}
				case GambleView.SPADES_BTN:
				{
					this.sendRequest(5);
					break;
				}
				case GambleView.CLUBS_BTN:
				{
					this.sendRequest(6);
					break;
				}
			}
		}

		private lockBonus(value:boolean):void {
			if (this.view != null) {
				var buttonsNames:Array<string> = this.view.buttonsNames;
				var buttonsCount:number = buttonsNames.length;
				for (var i:number = 0; i < buttonsCount; i++) {
					var button:Button = this.view.getButton(buttonsNames[i]);
					if(button){
						button.setEnable(!value);
					}
				}
			}
		}
	}
}