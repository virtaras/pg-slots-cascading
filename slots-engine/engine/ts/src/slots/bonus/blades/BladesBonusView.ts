module engine {
	import Text = createjs.Text;
	import Tween = createjs.Tween;
	import Ticker = createjs.Ticker;
	import Button = layout.Button;
	import Layout = layout.Layout;
	import Sprite = createjs.Sprite;
	import Container = createjs.Container;
	import DisplayObject = createjs.DisplayObject;

	export class BladesBonusView extends Layout {
		public static LAYOUT_NAME:string = "BladesBonusView";
		public static DELAY_BEFORE_FS:number = 1000;

		private static INTRO_CONTAINER_NAME:string = "intro";
		private static GAME_CONTAINER_NAME:string = "game";

		private static HEADER_MC:string = "bonustxt";
		private static ITEM_BTN_PREFIX:string = "BladesBtn";
		private static ITEM_BTN_DISABLE_PREFIX:string = "BladesBtnDis";

		private intro:Sprite;
		private game:Container;
		private header:Sprite;
		private items:Array<Button>;
		private itemsDis:Array<Button>;

		constructor() {
			super();
		}

		public onInit():void {
			this.intro = <Sprite>this.getChildByName(BladesBonusView.INTRO_CONTAINER_NAME);
			this.game = <Container>this.getChildByName(BladesBonusView.GAME_CONTAINER_NAME);
			this.header = <Sprite>this.game.getChildByName(BladesBonusView.HEADER_MC);
			this.items = <Array<Button>>Utils.fillDisplayObject(this.game, BladesBonusView.ITEM_BTN_PREFIX);
			this.itemsDis = <Array<Button>>Utils.fillDisplayObject(this.game, BladesBonusView.ITEM_BTN_DISABLE_PREFIX);
		}

		public startBonusGame():void{
			this.updateBtns(this.itemsDis, false, false);
			this.updateBtns(this.items, true, true);
			this.header.gotoAndPlay(0);
			this.game.alpha = 1;
			this.intro.alpha = 0;
		}

		public updateBtns(items:Array<Button>,visible:boolean, enable:boolean):void{
			for(var i:number = 0; i<items.length; ++i){
				this.changeButtonState(items[i], visible, enable);
			}
		}

		public showBonus(callback:Function):void {
			this.updateBtns(this.itemsDis, false, false);
			this.updateBtns(this.items, true, true);
			this.game.alpha = 0;
			this.intro.alpha = 1;
			var frameCount:number = this.intro.spriteSheet.getNumFrames(null);
			this.intro.addEventListener("tick", (eventObj:any)=> {
				var currentAnimation:Sprite = eventObj.currentTarget;
				if (currentAnimation.currentFrame == frameCount - 1) {
					currentAnimation.stop();
					currentAnimation.removeAllEventListeners("tick");
					callback() || null;
				}
			});
			this.intro.gotoAndPlay(0);
		}

		public getItems():Array<Button> {
			return this.items;
		}

		public processItemClick(itemId:number, callback:Function){
			this.updateBtns(this.itemsDis, true, false);
			this.updateBtns(this.items, false, false);
			this.changeButtonState(this.itemsDis[itemId-1], false, false);
			this.changeButtonState(this.items[itemId-1], true, false);
			Tween.get(this).wait(BladesBonusView.DELAY_BEFORE_FS).call(()=>{callback() || null});
		}

		public static getItemIdByName(itemName:string):number {
			return parseInt(itemName.substr(BladesBonusView.ITEM_BTN_PREFIX.length));
		}

		public changeButtonState(button:Button, visible:boolean, enable:boolean):void {
			if (button) {
				button.visible = visible;
				button.setEnable(enable);
			}
		}

		public dispose():void{

		}
	}
}