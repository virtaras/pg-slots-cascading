module engine {
	import Container = createjs.Container;
	import Button = layout.Button;
	import Sound = createjs.Sound;

	export class BladesBonusController extends BaseBonusController {
		private view:BladesBonusView;
		private itemId:number;
		private isInited:boolean = false;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
			super(manager, common, container, 0, false);
			common.originBonus = common.server.bonus;
		}

		public init():void {
			super.init();
			this.isInited = true;
		}

		public create():void {
			super.create();
			this.send(NotificationList.REMOVE_HEADER);
			this.view = <BladesBonusView>this.common.layouts[BladesBonusView.LAYOUT_NAME];
			this.view.create();
			this.startBonusGame();
			this.container.addChild(this.view);
		}

		private initHandlers():void {
			var items:Array<Button> = this.view.getItems();
			for (var i:number = 0; i < items.length; ++i) {
				var item:Button = items[i];
				item.on("click", (eventObj:any)=> {
					if (eventObj.nativeEvent instanceof MouseEvent) {
						SoundController.playSoundjs(SoundsList.REGULAR_CLICK_BTN);
						var currentItem:Button = <Button>eventObj.currentTarget;
						this.itemId = BladesBonusView.getItemIdByName(currentItem.name);
						this.view.processItemClick(this.itemId, ()=>{this.sendRequest(this.itemId);});
						this.removeHandlers();
					}
				});
			}
		}

		private removeHandlers():void {
			var items:Array<Button> = this.view.getItems();
			for (var i:number = 0; i < items.length; i++) {
				items[i].removeAllEventListeners("click");
			}
		}

		public startBonusGame():void{
			this.view.showBonus(()=>{this.view.startBonusGame(); this.initHandlers();});
		}

		public onGotResponse(xml:any):void {
			this.common.skipFSStartPopup = true;
			if(this.isInited){
				this.send(NotificationList.START_BLADES_BONUS, this.itemId); //send new freespins notification and provide spinsCnt and freespinstype
			}
		}

		public sendRequest(param:number = 0):void {
			super.sendRequest(param);
		}

		public dispose():void {
			super.dispose();
			this.container.removeChild(this.view);
			this.send(NotificationList.SHOW_HEADER);
		}
	}
}