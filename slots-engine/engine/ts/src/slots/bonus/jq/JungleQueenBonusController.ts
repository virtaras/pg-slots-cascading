module engine {
	import Container = createjs.Container;
	import Button = layout.Button;

	export class JungleQueenBonusController extends BaseBonusController {
		private view:JungleQueenBonusView;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
			super(manager, common, container, 0, false);
			common.originBonus = common.server.bonus;
		}

		public init():void {
			super.init();
		}

		public create():void {
			super.create();
			this.send(NotificationList.REMOVE_HEADER);
			this.view = <JungleQueenBonusView>this.common.layouts[JungleQueenBonusView.LAYOUT_NAME];
			this.view.create();
			this.startBonusGame();
			this.container.addChild(this.view);
			this.view.on(JungleQueenBonusView.START_FS_EVENT, (event)=>{
				this.view.removeAllEventListeners(JungleQueenBonusView.START_FS_EVENT);
				this.send(NotificationList.START_JUNGLE_QUEEN_BONUS, this.view.bonusSector+3);
			});
		}

		public startBonusGame():void{
			this.view.prepareAnimation();
			this.view.initSpinHandler(()=>{this.sendRequest();});
		}

		public onGotResponse(xml:any):void {
			var bonus:any = XMLUtils.getElement(xml, "feature");
			var bonusStr:Array<string> = bonus.textContent.split(",");
			var bonusSector:number = parseInt(bonusStr[2].substr(bonusStr[2].indexOf("bonus:")-1)) - 3;
			this.view.bonusSector = bonusSector;
		}

		public sendRequest(param:number = 0):void {
			super.sendRequest(param);
		}

		public dispose():void {
			super.dispose();
			this.container.removeChild(this.view);
			this.send(NotificationList.SHOW_HEADER);
			this.common.server.bonus.step = 0;
		}
	}
}