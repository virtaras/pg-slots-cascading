module engine {
	import Text = createjs.Text;
	import Tween = createjs.Tween;
	import Ticker = createjs.Ticker;
	import Button = layout.Button;
	import Layout = layout.Layout;
	import Sprite = createjs.Sprite;
	import Container = createjs.Container;
	import DisplayObject = createjs.DisplayObject;
	import Shape = createjs.Shape;
	import Ease = createjs.Ease;

	export class JungleQueenBonusView extends Layout {
		public static LAYOUT_NAME:string = "JungleQueenBonusView";
		public static START_FS_EVENT:string = "start_fs_event";

		private static WEEL_ROTATION_TIME:number = 100;
		private static WEEL_ROTATION_STOP_TIME:number = 100;
		public static ROTATION_TIMES:number = 2; //4
		private static DEGRESS_IN_A_FULL_ROTATION:number = 360;
		private static DELAY_TIME:number = 30;
		private static SETORS_CNT:number = 6;
		private static DELAY_BEFORE_FS:number = 3000;

		public static WHEEL_MASK_NAME:string = "mcWheelMask";
		public static WHEEL_BG_NAME:string = "mcBgBtn";
		public static WHEEL_WHEEL_NAME:string = "mcWheel";
		public static WHEEL_ARROW_NAME:string = "mcArrow";
		public static WHEEL_PRESS_TO_SPIN_NAME:string = "mcPressToSpin";
		public static WHEEL_INTRO_NAME:string = "mcIntroLabel";

		private mcWheelMask:Sprite;
		private mcBg:Sprite;
		private mcWheel:Sprite;
		private mcArrow:Sprite;
		private mcPressToSpin:Sprite;
		private mcIntroLabel:Sprite;

		public bonusSector:number = 3;

		constructor() {
			super();
		}

		public onInit():void {
			this.mcWheelMask = <Sprite>this.getChildByName(JungleQueenBonusView.WHEEL_MASK_NAME);
			this.mcBg = <Sprite>this.getChildByName(JungleQueenBonusView.WHEEL_BG_NAME);
			this.mcWheel = <Sprite>this.getChildByName(JungleQueenBonusView.WHEEL_WHEEL_NAME);
			this.mcArrow = <Sprite>this.getChildByName(JungleQueenBonusView.WHEEL_ARROW_NAME);
			this.mcPressToSpin = <Sprite>this.getChildByName(JungleQueenBonusView.WHEEL_PRESS_TO_SPIN_NAME);
			this.mcIntroLabel = <Sprite>this.getChildByName(JungleQueenBonusView.WHEEL_INTRO_NAME) || null;

			this.prepareWheel();
			this.mcWheelMask.visible = false;
		}

		public prepareAnimation():void{
			if(this.mcIntroLabel) {
				var mcIntroLabel:Sprite = this.mcIntroLabel;
				mcIntroLabel.visible = true;
				mcIntroLabel.gotoAndPlay(0);
				mcIntroLabel.addEventListener("tick", (eventObj:any)=> {
					var currentAnimation:Sprite = eventObj.currentTarget;
					currentAnimation.visible = true;
					if (currentAnimation.currentFrame == (mcIntroLabel.spriteSheet.getNumFrames(null) - 1 * 2)) {
						currentAnimation.stop();
						currentAnimation.visible = false;
						currentAnimation.removeAllEventListeners("tick");
					}
				});
			}
			this.mcPressToSpin.visible = true;
			this.mcArrow.gotoAndStop(0);
			this.mcPressToSpin.gotoAndPlay(0);
			Tween.get(this.mcWheel).to({rotation:0});
		}

		private prepareWheel():void{
			this.createWheelMask();
		}

		private createWheelMask():void {
			var mask:Shape = new Shape();
			mask.graphics.beginFill("0").drawRect(this.mcWheelMask.x, this.mcWheelMask.y, this.mcWheelMask.getBounds().width, this.mcWheelMask.getBounds().height);
			this.mcWheel.mask = mask;
			this.mcArrow.mask = mask;
		}

		public initSpinHandler(callback:Function = null):void{
			var button:Button = <Button>this;
			button.on("click", (eventObj:any)=> {
				if (eventObj.nativeEvent instanceof MouseEvent) {
					this.mcPressToSpin.visible = false;
					this.mcIntroLabel.visible = false;
					button.removeAllEventListeners("click");
					this.prepareRotation();
					callback();
				}
			});
		}

		private prepareRotation():void{
			this.launchWeel(<DisplayObject>this.mcWheel);
		}

		private launchWeel(ring:DisplayObject):void {
			Tween.get(ring, {useTicks: true}).to({"rotation": JungleQueenBonusView.DEGRESS_IN_A_FULL_ROTATION}, JungleQueenBonusView.WEEL_ROTATION_TIME, Ease.getBackIn(1) ).call(()=> {
				this.rotateWeel(ring);
			});
		}

		private rotateWeel(ring:DisplayObject):void {
			ring.rotation = 0;
			var degrees:number = JungleQueenBonusView.DEGRESS_IN_A_FULL_ROTATION * JungleQueenBonusView.ROTATION_TIMES;
			Tween.get(ring, {useTicks: true}).to({"rotation": degrees}, JungleQueenBonusView.DELAY_TIME / 2, Ease.linear).call(()=> {
				this.stopRotate(ring);
			});
		}

		private stopRotate(ring:DisplayObject):void {
			ring.rotation = 0;
			var sectorDegr:number = JungleQueenBonusView.DEGRESS_IN_A_FULL_ROTATION / JungleQueenBonusView.SETORS_CNT;
			var degrees:number = JungleQueenBonusView.DEGRESS_IN_A_FULL_ROTATION + (sectorDegr * this.bonusSector) + 360;

			Tween.get(ring, {useTicks: true}).to({"rotation": degrees}, JungleQueenBonusView.WEEL_ROTATION_STOP_TIME, Ease.getBackOut(1)).call(()=> {
				this.showSectorAnimation();
			});
		}

		private showSectorAnimation():void {
			this.mcArrow.gotoAndPlay(0);
			Tween.get(this).wait(JungleQueenBonusView.DELAY_BEFORE_FS).call(()=>{this.dispatchEvent(JungleQueenBonusView.START_FS_EVENT);});
		}

		public changeButtonState(button:Button, visible:boolean, enable:boolean):void {
			if (button) {
				button.visible = visible;
				button.setEnable(enable);
			}
		}
	}
}