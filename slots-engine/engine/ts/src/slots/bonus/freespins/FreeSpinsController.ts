module engine {
	import Container = createjs.Container;
	import Button = layout.Button;
	import Ticker = createjs.Ticker;
	import Sound = createjs.Sound;
	import LayoutCreator = layout.LayoutCreator;

	export class FreeSpinsController extends BaseBonusController {
		private static UPDATE_WIN_TIME:number = 0.5;
		private static UPDATE_WIN_TIME_UP:number;
		private static START_POPUP_DELAY:number = 2;

		private view:FreeSpinsView;
		private count:number;
		private leftSpins:number;
		private multiplicator:number;
		private gameWin:number;
		private featureWin:number = 0;
		private featureWinPrev:number = 0;
		private isFist:boolean;
		private isBonusStarted:boolean;
		private isSecondGame:boolean = false;
		private addFSCount:number = 0; //show winpopup if win add spins
		private currentWildReel:number = 2;
		private multiplier:number = 0;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
			super(manager, common, container, 0, true);
		}

		public init():void {
			super.init();
			FreeSpinsController.UPDATE_WIN_TIME_UP = Utils.float2int(FreeSpinsController.UPDATE_WIN_TIME * Ticker.getFPS());
		}

		public create():void {
			super.create();
			this.common.skipFSStartPopup = this.common.config.skipFSStartPopup || this.common.skipFSStartPopup;
			this.send(NotificationList.REMOVE_HEADER);

			this.view = <FreeSpinsView>this.common.layouts[FreeSpinsView.LAYOUT_NAME];
			this.view.create();
			this.view.setStrokes(this.common.config.FSNoStrokes);
			this.container.addChild(this.view);
			this.common.canRemoveWildsExpandingStatic = false;
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.START_BONUS);
			notifications.push(NotificationList.START_BONUS_FS);
			notifications.push(NotificationList.STOPPED_ALL_REELS);
			notifications.push(NotificationList.WIN_LINES_SHOWED);
			notifications.push(NotificationList.RECURSION_PROCESSED);
			notifications.push(NotificationList.RECURSION_PROCESS_FALLING);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			super.handleNotification(message, data);
			switch (message) {
				case NotificationList.START_BONUS:
				case NotificationList.START_BONUS_FS:
				{
					this.isBonusStarted = true;
					this.setLeftSpins();

					if(this.common.restore && this.common.skipFSStartPopup){
						this.removeStartPopup();
					}
					if(this.isSecondGame){
						this.isSecondGame = false;
					}
					else {
						console.log("this.common.originBonus", this.common.originBonus);
						if(data == 'spins' || (this.common.originBonus && this.common.originBonus.type == BonusTypes.SELECT_GAME)  || this.common.skipFSStartPopup){
							this.removeStartPopup();
						}
					}
					if((!this.common.config.no_wild_reel_fadein_on_start_spin) && (this.leftSpins < (this.count-1) || data == 'spins' || (this.common.originBonus && this.common.originBonus.type == BonusTypes.SELECT_GAME) || this.common.restore)){
						this.send(NotificationList.SHOW_WILD_REEL_FADEIN, this.currentWildReel);
					}
					if(this.common.config.wildSymbolIdExpandingStatic){
						this.send(NotificationList.SHOW_WILDS_EXPANDING_STATIC, null);
					}
					break;
				}
				case NotificationList.STOPPED_ALL_REELS:
				{
					this.tryShowMoreFreeSpinsPopupForTime();
					if (this.view != null) {
						this.view.setWin(this.featureWin, FreeSpinsController.UPDATE_WIN_TIME_UP);
					}

					this.send(NotificationList.SHOW_WIN_LINES, true);
					this.send(NotificationList.SHOW_WIN_TF);
					break;
				}
				case NotificationList.RECURSION_PROCESSED:
				{
					if (this.isBonusStarted) {
						if (this.leftSpins == 0) {
							this.container.removeChild(this.view);
							this.send(NotificationList.SHOW_HEADER);
							this.showResultPopup();
						}
						else {
							this.nextSpin();
						}
					}
					break;
				}
				case NotificationList.RECURSION_PROCESS_FALLING:
				{
					if(this.common.server.recursion.length > 1) {
						this.showNextMultiplier();
					}
					break;
				}
			}
		}

		private nextSpin():void{
			this.setLeftSpins();
			this.send(NotificationList.REMOVE_WIN_LINES);
			this.send(NotificationList.START_SPIN);
			//this.send(NotificationList.START_SPIN_AUDIO);
			this.sendRequest();
		}

		private setFields():void{
			this.setLeftSpins();
			this.view.setMultiplicator(this.multiplicator);
			this.view.setWin(this.featureWin);
			//this.send(NotificationList.SHOW_WIN_TF);
			if(this.leftSpins == (this.count - 1)){
				this.view.setWin(this.featureWinPrev);
			}
		}

		public onGotResponse(data:any):void {
			this.parseResponse(data);
			if (this.isFist && !this.isSecondGame) {
				this.setFields();
				if(!this.common.restore)this.showStartPopup();
			}
			else {
				this.send(NotificationList.SERVER_GOT_SPIN);
			}
			this.common.restore = false;
			this.multiplier = 0;
			this.showNextMultiplier();
		}

		public onEnterFrame():void {
			if (this.view != null) {
				this.view.update();
			}
		}

		private showStartPopup():void {
			var startPopup:FreeSpinsStartView = <FreeSpinsStartView>this.common.layouts[FreeSpinsStartView.LAYOUT_NAME];
			if(startPopup) {
				startPopup.create();
				startPopup.alpha = 1;
				startPopup.setFreeSpinsCount(this.count);
				this.container.addChild(startPopup);
			}
		}

		private tryShowMoreFreeSpinsPopupForTime():void{
			if(this.addFSCount>0){
				this.send(NotificationList.PLAY_MORE_FREE_SPINS_SOUND);
				this.showMorePopupForTime();
				this.addFSCount = 0;
			}
		}

		private showMorePopupForTime():void {
			var morePopup:FreeSpinsMoreView = <FreeSpinsMoreView>this.common.layouts[FreeSpinsMoreView.LAYOUT_NAME];
			morePopup.create();
			morePopup.alpha = 1;
			morePopup.setFreeSpinsCount(this.addFSCount);
			this.container.addChild(morePopup);
			morePopup.hide(()=> {this.container.removeChild(morePopup);}, FreeSpinsController.START_POPUP_DELAY);
		}

		private removeStartPopup():void {
			this.send(NotificationList.START_SPIN);
			this.send(NotificationList.SERVER_GOT_SPIN);
			var startPopup:FreeSpinsStartView = <FreeSpinsStartView>this.common.layouts[FreeSpinsStartView.LAYOUT_NAME];
			if(startPopup) {
				startPopup.hide(()=> {
						this.container.removeChild(startPopup);
					}
				)
			}
		}

		private showResultPopup():void {
			var resultPopup:FreeSpinsResultView = <FreeSpinsResultView>this.common.layouts[FreeSpinsResultView.LAYOUT_NAME];
			resultPopup.create();
			resultPopup.alpha = 1;
			resultPopup.setTextPositions(this.common.isMobile);
			resultPopup.setGameWin(this.gameWin);
			resultPopup.setFeatureWin(this.featureWin);
			var totalWin:number = this.gameWin + this.featureWin;
			resultPopup.setTotalWin(totalWin);

			var collectBtn:Button = resultPopup.getCollectBtn();
			collectBtn.removeAllEventListeners("click");
			collectBtn.on("click", (eventObj:any)=> {
				if (eventObj.nativeEvent instanceof MouseEvent) {
					collectBtn.removeAllEventListeners("click");
					this.hideResultPopup();
				}
			});
			this.container.addChild(resultPopup);
		}

		private hideResultPopup():void{
			var resultView:FreeSpinsResultView = <FreeSpinsResultView>this.common.layouts[FreeSpinsResultView.LAYOUT_NAME];
			resultView.hide(()=> {
					this.common.server.bonus = null;
					SoundController.playSoundjs(SoundsList.COLLECT);
					SoundController.playSoundjs(SoundsList.REGULAR_CLICK_BTN);
					this.send(NotificationList.END_BONUS);
					this.send(NotificationList.UPDATE_BALANCE_TF);
					this.send(NotificationList.START_NORMAL_GAME_BG_SOUND);
					this.common.canRemoveWildsExpandingStatic = true;

					//this.send(NotificationList.REMOVE_WILDS_EXPANDING_STATIC); //REMOVE THIS LINE
					//this.send(NotificationList.SHOW_ALL_SYMBOLS); //REMOVE THIS LINE

					this.container.removeChild(resultView);
				}
			)
		}

		private parseResponse(xml:any):void {
			try {
				var XMLbalance:number = parseFloat(XMLUtils.getElement(xml, "balance").textContent);
				if(XMLbalance){
					this.common.server.setBalance(XMLbalance);
				}
				var bonusnew:any = XMLUtils.getElement(XMLUtils.getElement(XMLUtils.getElement(xml, "data"), "spin"), "bonus");
			}catch (ex){
				//no bonusnew so sad
			}
			if(bonusnew && XMLUtils.getAttributeInt(bonusnew, "id")){
				this.addFSCount = XMLUtils.getAttributeInt(bonusnew, "addFSCount");
				if(this.common.restore) this.tryShowMoreFreeSpinsPopupForTime();
			}
			//haldle JungleQueen bonus restore before first step
			try{
				if(this.common.restore && XMLUtils.getElement(xml, "feature").textContent){
					this.sendRequest(0);
					return;
				}
			}catch (e){}
			var data:any = XMLUtils.getElement(xml, "data");
			var items:any = (data) ? XMLUtils.getElement(data, "items") : null;
			if(items){
				var finishBonus:number = XMLUtils.getAttributeInt(items, "finishBonus");
				if(finishBonus && finishBonus == 1){
					var items:any = XMLUtils.getElement(data, "items");
					this.count = XMLUtils.getAttributeInt(items, "spins");
					this.leftSpins = this.count;
					this.multiplicator = XMLUtils.getAttributeInt(items, "spinsMultiplier");
					this.isFist = true;
					this.gameWin = XMLUtils.getAttributeFloat(xml, "gamewin") || 0;
					this.featureWin = 0;
					this.common.server.bonus.step = 0;
					this.isSecondGame = true;
					this.setFields();
					this.sendRequest(1);
				}
			}
			else {
				this.count = XMLUtils.getAttributeInt(xml, "all");
				this.leftSpins = XMLUtils.getAttributeInt(xml, "counter");
				this.multiplicator = XMLUtils.getAttributeInt(xml, "mp");
				this.gameWin = XMLUtils.getAttributeFloat(xml, "gamewin") || 0;
				this.isFist = (this.common.restore)? true : (this.isSecondGame) ? this.leftSpins == this.count : this.leftSpins == this.count - 1;

				this.featureWin = (data) ? (XMLUtils.getAttributeFloat(data, "summ") || 0) : 0;
				console.log("this.featureWin",this.featureWin);
				var serverWin:any = XMLUtils.getElement(xml, "win");
				this.featureWinPrev = this.featureWin - parseFloat(serverWin.textContent);
				this.parseSpinData(data);
				var data = null;
				if(!this.common.restore && this.leftSpins < (this.count-1)){
					data = 'spins';
				}
				this.send(NotificationList.START_BONUS_FS, data);
			}
		}

		private parseSpinData(xml:any):void {
			if(xml) {
				this.common.wild_reels_expanding_static = [];
				var spinData:any = XMLUtils.getElement(xml, "spin");
				this.parseRecursion(spinData);
				var wheelsData:any = XMLUtils.getElement(spinData, "wheels").childNodes;
				for (var i = 0; i < wheelsData.length; i++) {
					var wheelData:Array<number> = Utils.toIntArray(wheelsData[i].childNodes[0].data.split(","));
					for (var j:number = 0; j < wheelData.length; j++) {
						this.common.server.wheel[i * wheelData.length + j] = wheelData[j];
					}
					if (wheelData[0] == this.common.config.wildSymbolIdExpanding) this.currentWildReel = i;
					if (wheelData[0] == this.common.config.wildSymbolIdExpandingStatic) this.common.wild_reels_expanding_static.push(i);
				}
				this.common.server.winLines = [];
				this.parseWinLinesData(spinData, "winposition");
				this.parseWinLinesData(spinData, "position");

				//TODO: server return 0;
				//this.common.server.setBalance(parseFloat(XMLUtils.getElement(spinData, "balance").textContent));
				if(XMLUtils.getAttributeFloat(spinData, "firstwin")) {
					this.common.server.win = XMLUtils.getAttributeFloat(spinData, "firstwin") * this.common.server.bet;
				}else{
					this.common.server.win = parseFloat(XMLUtils.getElement(spinData, "win").textContent);
				}
			}
		}

		private parseWinLinesData(spinData:any, nodeName:string):void{
			try {
				var winLinesData:any = XMLUtils.getElement(spinData, nodeName).childNodes;
				for (var i:number = 0; i < winLinesData.length; i++) {
					var winLine:any = winLinesData[i];
					var winLineVO:WinLineVO = new WinLineVO();
					winLineVO.lineId = XMLUtils.getAttributeInt(winLine, "line");
					winLineVO.win = XMLUtils.getAttributeFloat(winLine, "win");
					winLineVO.winPos = Utils.toIntArray(winLine.childNodes[0].data.split(","));
					this.common.server.winLines.push(winLineVO);
				}
			}catch(e){
				console.log("parseWinLinesData "+nodeName, e);
			}
			return;
		}

		public parseRecursion(data:any):void{
			this.common.server.recursion = [];
			var recursionData:any = XMLUtils.getElement(data, "recursion");
			if(recursionData) {
				var recursionDataArr:any = recursionData.childNodes;
				for (var i:number = 0; i < recursionDataArr.length; i++) {
					var wheelData:Array<number> = eval(recursionDataArr[i].attributes[0].nodeValue);
					this.common.server.recursion[i] = [];
					this.common.server.recursion[i]["wheels"] = wheelData;
					this.common.server.recursion[i]["win"] = XMLUtils.getAttributeInt(recursionDataArr[i], "win");

					this.common.server.recursion[i]["winLines"] = [];
					this.common.server.recursion[i]["arrLinesIds"] = [];
					var winLinesData:any = XMLUtils.getElement(recursionDataArr[i], "winposition").childNodes;
					for (var j:number = 0; j < winLinesData.length; ++j) {
						var winLine:any = winLinesData[j];
						var winLineVO:WinLineVO = new WinLineVO();
						winLineVO.lineId = XMLUtils.getAttributeInt(winLine, "line");
						winLineVO.win = XMLUtils.getAttributeFloat(winLine, "win");
						winLineVO.winPos = Utils.toIntArray(winLine.childNodes[0].data.split(","));
						this.common.server.recursion[i]["winLines"].push(winLineVO);
						this.common.server.recursion[i]["arrLinesIds"].push(winLineVO.lineId);
					}
				}
			}
			this.common.server.recursion_len = this.common.server.recursion.length;
		}

		public setLeftSpins():void{
			if(!this.addFSCount) {
				this.view.setLeftSpins(this.leftSpins || 0);
			}
		}

		public showNextMultiplier():void{
			this.view.showMultiplierAnimation(++this.multiplier);
		}
	}
}