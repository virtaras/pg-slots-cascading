module engine {
	import Tween = createjs.Tween;
	import Ticker = createjs.Ticker;
	import Layout = layout.Layout;
	import Button = layout.Button;
	import Text = createjs.Text;

	export class FreeSpinsResultView extends Layout {
		public static LAYOUT_NAME:string = "FreeSpinsResultView";

		private static INVISIBLE_TIME:number = 0.5;

		private static COLLECT_BTN:string = "collectBtn";

		private static GAME_WIN:string = "TxtWinBaseGame";
		private static FEATURE_WIN:string = "TxtWinBonusGame";
		private static TOTAL_WIN:string = "TxtWin";
		private textsPos:boolean;

		private allTitles:Array<string> = [
			FreeSpinsResultView.GAME_WIN,
			FreeSpinsResultView.FEATURE_WIN,
			FreeSpinsResultView.TOTAL_WIN
		];

		constructor() {
			super();
		}

		public onInit():void{
			FreeSpinsResultView.INVISIBLE_TIME = Utils.float2int(FreeSpinsResultView.INVISIBLE_TIME * Ticker.getFPS());
			var tf:Text = <Text>this.getChildByName(FreeSpinsResultView.TOTAL_WIN);
		}

		public getCollectBtn():Button {
			return <Button>this.getChildByName(FreeSpinsResultView.COLLECT_BTN);
		}

		public setTextPositions(isMobile:boolean = false){
			if(!this.textsPos){
				for(var n:number = 0; n < this.allTitles.length; n++){
					var title:Text = <Text>this.getChildByName(this.allTitles[n]);
					if(title) {
						title.y += title.lineHeight / 2;
						GameController.setTextStroke(title, 0, GameController.SHADOW_COLOR, 2, 2, 2);
					}
				}
			}
			this.textsPos = true;
		}

		public setTotalWin(value:number):void {
			var tf:Text = <Text>this.getChildByName(FreeSpinsResultView.TOTAL_WIN);
			if (tf != null) {
				tf.text = MoneyFormatter.format(value, true);
			}
		}

		public setGameWin(value:number):void {
			var tf:Text = <Text>this.getChildByName(FreeSpinsResultView.GAME_WIN);
			if (tf != null) {
				tf.text = MoneyFormatter.format(value, true);
			}
		}

		public setFeatureWin(value:number):void {
			var tf:Text = <Text>this.getChildByName(FreeSpinsResultView.FEATURE_WIN);
			if (tf != null) {
				tf.text = MoneyFormatter.format(value, true);
			}
		}

		public hide(callback:(...params:any[]) => any):void {
			var tween:Tween = Tween.get(this, {useTicks: true});
			tween.to({alpha: 0}, FreeSpinsResultView.INVISIBLE_TIME);
			tween.call(callback);
		}
	}
}