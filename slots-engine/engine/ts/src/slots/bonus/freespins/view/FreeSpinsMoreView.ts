module engine {
    import Layout = layout.Layout;
    import Text = createjs.Text;
    import Tween = createjs.Tween;
    import Ticker = createjs.Ticker;

    export class FreeSpinsMoreView extends Layout {
        public static LAYOUT_NAME:string = "FreeSpinsMoreView";

        public static FREE_SPINS_COUNT:string = "freeSpinsCountTf";
        private static INVISIBLE_TIME:number = 0.05;
        private static INVISIBLE_TIME_UP:number;

        private textsPos:boolean = false;

        constructor() {
            super();
        }

        public onInit():void {
            FreeSpinsMoreView.INVISIBLE_TIME_UP = Utils.float2int(FreeSpinsMoreView.INVISIBLE_TIME * Ticker.getFPS());
            this.setTextPositions();
        }

        public setTextPositions(){
            if(!this.textsPos){
                var title:Text = <Text>this.getChildByName(FreeSpinsMoreView.FREE_SPINS_COUNT);
                if(title != null){
                    title.y += title.lineHeight/2;
                    console.log("height="+title.lineHeight+" measureheight="+title.getMeasuredLineHeight());
                }
            }
            this.textsPos = true;
        }

        public setFreeSpinsCount(value:number):void {
            var tf:Text = <Text>this.getChildByName(FreeSpinsMoreView.FREE_SPINS_COUNT);
            if (tf != null) {
                tf.text = value.toString();
            }
        }

        public hide(callback:(...params:any[]) => any, time:number = 0):void {
            time = Utils.float2int(time * Ticker.getFPS());
            var tween:Tween = Tween.get(this, {useTicks: true});
            tween.wait(time).to({alpha: 0}, FreeSpinsMoreView.INVISIBLE_TIME_UP);
            tween.call(callback);
        }
    }
}