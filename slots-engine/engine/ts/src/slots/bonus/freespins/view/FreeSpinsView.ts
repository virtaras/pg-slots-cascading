module engine {
	import Layout = layout.Layout;
	import Text = createjs.Text;
	import Sprite = createjs.Sprite;

	export class FreeSpinsView extends Layout {
		public static LAYOUT_NAME:string = "FreeSpinsView";

		public static LEFT_SPINS_COUNT_TEXT:string = "leftSpinsTf";
		private static MULTIPLICATOR_TEXT:string = "multiplicatorTf";
		public static WIN_TEXT:string = "winTf";

		public static MULTIPLIER_PREFFIX:string = "multipleX";
		public static MULTIPLIER_BLOCK:string = "multipleFeature";
		public static MULTIPLIER_MAX_VALUE:number = 5;

		private win:AnimationTextField;
		public static titleFields:Array<string> = [FreeSpinsView.LEFT_SPINS_COUNT_TEXT, FreeSpinsView.MULTIPLICATOR_TEXT, FreeSpinsView.WIN_TEXT];

		private isSetStrokes:boolean = false;
		private FSNoStrokes:boolean = false;
		private isSetTextPositions:boolean = false;
		private multiplier:number = 0;

		constructor() {
			super();
		}

		public onInit():void {
			this.win = new AnimationTextField(<Text>this.getChildByName(FreeSpinsView.WIN_TEXT));
		}

		public setStrokes(setFSNoStrokes:boolean):void{
			this.FSNoStrokes = setFSNoStrokes;
			if(!this.isSetStrokes && !this.FSNoStrokes) {
				GameController.setFieldStroke(this, FreeSpinsView.LEFT_SPINS_COUNT_TEXT, 0, GameController.SHADOW_COLOR, 1, 1, 2);
				GameController.setFieldStroke(this, FreeSpinsView.WIN_TEXT, 0, GameController.SHADOW_COLOR, 1, 1, 2);
			}
			this.isSetStrokes = true;
		}

		public setLeftSpins(value:number):void {
			console.log("setLeftSpinі="+value);
			var tf:Text = <Text>this.getChildByName(FreeSpinsView.LEFT_SPINS_COUNT_TEXT);
			if (tf != null) {
				tf.text = value.toString();
			}
			//this.setStrokes();
		}

		public setMultiplicator(value:number):void {
			var tf:Text = <Text>this.getChildByName(FreeSpinsView.MULTIPLICATOR_TEXT);
			if (tf != null) {
				tf.text = "x" + value.toString();
			}
		}

		public setWin(value:number, updateTime:number = 0):void {
			this.win.setValue(value, updateTime)
		}

		public update():void{
			if (this.win != null){
				this.win.update();
			}
		}

		public showMultiplierAnimation(multiplier:number):void{
			if(this.multiplier != multiplier) {
				this.multiplier = multiplier;
				this.stopMultipliersAnimation();
				multiplier = Math.min(FreeSpinsView.MULTIPLIER_MAX_VALUE, multiplier);
				var sprite:Sprite = <Sprite>this.getChildByName(FreeSpinsView.MULTIPLIER_PREFFIX + multiplier);
				if (sprite) {
					sprite.gotoAndPlay(0);
					var frameCount:number = sprite.spriteSheet.getNumFrames(null);
					sprite.addEventListener("tick", (eventObj:any)=> {
						var currentAnimation:Sprite = eventObj.currentTarget;
						if (currentAnimation.currentFrame == (frameCount - 1 * 2)) {
							currentAnimation.stop();
							currentAnimation.removeAllEventListeners("tick");
						}
					});
				}
			}
		}

		public stopMultipliersAnimation():void{
			var mult:number;
			mult = 0;
			var sprite:Sprite;
			while(sprite = <Sprite>this.getChildByName(FreeSpinsView.MULTIPLIER_PREFFIX+(++mult))){
				sprite.gotoAndStop(0);
			}
		}
	}
}