module engine {
	export class ItemVO {
		public id:number;
		public isSelected:Boolean;
		public winValue:number;
		public type:number;

		constructor(id:number) {
			this.id = id;
			this.isSelected = false;
			this.winValue = -1;
			this.type = 0;
		}
	}
}
