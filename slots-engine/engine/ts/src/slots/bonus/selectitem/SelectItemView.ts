module engine {
	import Text = createjs.Text;
	import Tween = createjs.Tween;
	import Ticker = createjs.Ticker;
	import Button = layout.Button;
	import Layout = layout.Layout;
	import Sprite = createjs.Sprite;
	import Container = createjs.Container;
	import DisplayObject = createjs.DisplayObject;

	export class SelectItemView extends Layout {
		public static LAYOUT_PARAMS:any = {};
		public static LAYOUT_NAME:string = "SelectItemView";

		private static WIN_POPUP:string = "winPopup";
		private static BONUS:string = "bonus";

		private static WIN_ANIMATION:string = "winAnimation_";
		private static ITEM_TEXT:string = "itemTf_";
		private static ITEM_BTN:string = "itemBtn_";
		public static START_BTN:string = "ButtonGStart";


		private static TOTAL_WIN_TEXT:string = "totalWin";
		private static COLLECT_BTN:string = "collectBtn";
		private static SPINS_COUNT:string = "TxtRot";
		private static MULTIPLIER_COUNT:string = "TxtMul";
		private textsPos:boolean;

		private allTitles:Array<string> = [
			SelectItemView.SPINS_COUNT,
			SelectItemView.MULTIPLIER_COUNT
		];

		private static INTRO_ANIM:string = "intro";

		private static DELAY_FOR_NEXT_STEP:number = 2;
		private static HIDE_POPUP_TIME:number = 0.5;
		private static DELAY_FOR_NEXT_STEP_UP:number;
		private static HIDE_POPUP_TIME_UP:number;

		private bonus:Container;
		private intro:Sprite;
		private winPopup:Container;
		private items:Array<Button>;
		private texts:Array<Text>;
		private animations:Array<Sprite>;
		private itemsCount:number;

		constructor() {
			super();
			SelectItemView.DELAY_FOR_NEXT_STEP_UP = Utils.float2int(SelectItemView.DELAY_FOR_NEXT_STEP *Ticker.getFPS());
			SelectItemView.HIDE_POPUP_TIME_UP = Utils.float2int(SelectItemView.HIDE_POPUP_TIME *Ticker.getFPS());
		}

		public onInit():void {
			this.bonus = <Container>this.getChildByName(SelectItemView.BONUS);
			this.winPopup = <Container>this.getChildByName(SelectItemView.WIN_POPUP);
			this.intro = <Sprite>this.getChildByName(SelectItemView.INTRO_ANIM);
			this.items = <Array<Button>>Utils.fillDisplayObject(this.bonus, SelectItemView.ITEM_BTN);
			this.itemsCount = this.items.length;
			this.texts = <Array<Text>>Utils.fillDisplayObject(this.bonus, SelectItemView.ITEM_TEXT);
			this.animations = <Array<Sprite>>Utils.fillDisplayObject(this.bonus, SelectItemView.WIN_ANIMATION);
			//this.startButtonAnimation(SelectItemView.START_BTN);

			for(var i:number = 0; i<this.texts.length; ++i){
				GameController.setTextStroke(this.texts[i], 2, "rgba(0,0,0,1)", 2, 2, 4);
			}
		}

		public showWinAnimation(vo:ItemVO, callback:(...params:any[]) => any):void {
			this.items[vo.id].setEnable(false);
			if(SelectItemView.LAYOUT_PARAMS.hide_buttons_on_click){
				this.items[vo.id].visible = false;
			}
			var animation:Sprite = this.animations[vo.id];
			var frameCount:number = animation.spriteSheet.getNumFrames(null);
			animation.visible = true;
			animation.addEventListener("tick", (eventObj:any)=> {
				var currentAnimation:Sprite = eventObj.currentTarget;
				if (currentAnimation.currentFrame == frameCount - 1) {
					currentAnimation.stop();
					//this.items[vo.id].visible = true;
				}
			});
			animation.gotoAndPlay(0);

			var itemText:Text = this.texts[vo.id];
			itemText.text = (vo.type == 2)? 'x'+ vo.winValue : vo.winValue.toString();
			itemText.visible = true;
			itemText.alpha = 0;
			Tween.get(itemText, {useTicks: true})
				.to({alpha: 1}, frameCount / 2)
				.wait(SelectItemView.DELAY_FOR_NEXT_STEP_UP)
				.call(callback);
		}

		public changeButtonState(btnName:string, visible:boolean, enable:boolean):void {
			var button:Button = this.getBtn(btnName);
			if (button) {
				button.cursor = "pointer";
				button.visible = visible;
			}
		}

		public setTextPositions(){
			if(!this.textsPos){
				for(var n:number = 0; n < this.allTitles.length; n++){
					var title:Text = <Text>this.bonus.getChildByName(this.allTitles[n]);
					title.y = title.y + title.lineHeight / 2;
					GameController.setTextStroke(title, 2, "rgba(0,0,0,1)", 0, 0, 4);
				}
				for(var i:number = 0; i < this.texts.length; i++){
					var animText:Text = this.texts[i];
					animText.y = animText.y + animText.lineHeight / 2;
					GameController.setTextStroke(this.texts[i], 0, GameController.SHADOW_COLOR, 2, 2, 0);
				}
				var spinsText:Text = <Text>this.winPopup.getChildByName(this.allTitles[0]);
				spinsText.y = spinsText.y + spinsText.lineHeight / 2;
				var multiText:Text = <Text>this.winPopup.getChildByName(this.allTitles[1]);
				multiText.y = multiText.y + multiText.lineHeight / 2;

				GameController.setFieldStroke(this.winPopup, this.allTitles[0], 2, "rgba(0,0,0,1)", 2, 2, 4);
				GameController.setFieldStroke(this.winPopup, this.allTitles[1], 2, "rgba(0,0,0,1)", 2, 2, 4);
			}
			this.textsPos = true;
		}

		public getBtn(btnName:string):Button {
			return <Button>this.getChildByName(btnName);
		}

		public startBonusGame():void{
			this.setTextPositions();
			this.changeButtonState(SelectItemView.START_BTN, false, false);
			var bonus:Container = this.bonus;
			bonus.alpha = 1;
			var animation:Sprite = this.intro;
			animation.alpha = 0;
		}

		public startButtonAnimation(btnName:string):void{
			var btn:Sprite = <Sprite>this.getChildByName(btnName);
			btn.gotoAndPlay(0);
			return;
		}

		public updateCounters(index:number,count:string):void {
			var title:Text = <Text>this.bonus.getChildByName(this.allTitles[index]);
			title.text = count;
		}

		public showWinPopup():void {
			this.winPopup.visible = true;
			this.winPopup.alpha = 1;
			this.bonus.visible = false;
		}

		public setupMobile():void {
			var startBtn:Button = this.getBtn(SelectItemView.START_BTN);
			startBtn.x = startBtn.x + startBtn.getBounds().width / 2;
		}

		public showBonus():void {
			var bonus:Container = this.bonus;
			bonus.visible = true;
			bonus.alpha = 0;
			var animation:Sprite = this.intro;
			animation.alpha = 1;
			var frameCount:number = animation.spriteSheet.getNumFrames(null);
			animation.addEventListener("tick", (eventObj:any)=> {
				var currentAnimation:Sprite = eventObj.currentTarget;
				if (currentAnimation.currentFrame == frameCount - 1) {
					currentAnimation.stop();
				}
			});
			animation.gotoAndPlay(0);
			this.winPopup.visible = false;
		}

		public hideAllWinAnimation():void {
			for (var i:number = 0; i < this.animations.length; i++) {
				this.animations[i].visible = false;
				this.texts[i].visible = false;
			}
		}

		public hideWinPopup(callback:(...params:any[]) => any):void {
			Tween.get(this.winPopup, {useTicks: true})
				.to({alpha: 0}, SelectItemView.HIDE_POPUP_TIME_UP)
				.call(callback);
		}

		public getCollectBtn():Button {
			return <Button>this.winPopup.getChildByName(SelectItemView.COLLECT_BTN);
		}

		public getItems():Array<Button> {
			return this.items;
		}

		public getItemCount():number {
			return this.itemsCount;
		}

		public setTotalWin(spins:number, multi:number):void {
			var spinsText:Text = <Text>this.winPopup.getChildByName(this.allTitles[0]);
			var multiText:Text = <Text>this.winPopup.getChildByName(this.allTitles[1]);
			spinsText.text = spins.toString();
			multiText.text = multi.toString();
			//var totalWin:Text = <Text>this.winPopup.getChildByName(SelectItemView.TOTAL_WIN_TEXT);
			//totalWin.text = MoneyFormatter.format(value, true);
			//totalWin.text = value.toString();
		}

		public static getItemIdByName(itemName:string):number {
			return parseInt(itemName.substr(SelectItemView.ITEM_TEXT.length + 1));
		}
	}
}