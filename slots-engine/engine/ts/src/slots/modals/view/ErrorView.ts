module engine {
    import Layout = layout.Layout;
    import Text = createjs.Text;
    import Tween = createjs.Tween;
    import Ticker = createjs.Ticker;
    import Button = layout.Button;
    import Shape = createjs.Shape;
    import Rectangle = createjs.Rectangle;

    export class ErrorView extends Layout {
        public static LAYOUT_NAME:string = "ErrorView";
        public static OK_BTN:string = "okBtnError";
        public static ERROR_MESSAGE:string = "errorMessage";

        public buttonsNames:Array<string> = [
            ErrorView.OK_BTN
        ];

        constructor() {
            super();
        }

        public onInit():void {

        }

        public setText(txt:any):void{
            var tf:Text = <Text>this.getChildByName(ErrorView.ERROR_MESSAGE);
            if(tf){
                tf.text = (Array.isArray(txt)) ? txt.join("\n") : txt;
            }
        }

        public getButton(name:string):Button {
            return <Button>this.getChildByName(name);
        }

        public changeButtonState(btnName:string, visible:boolean, enable:boolean):void {
            var button:Button = this.getButton(btnName);
            if (button) {
                button.visible = visible;
                button.setEnable(enable);
            }
        }
    }
}