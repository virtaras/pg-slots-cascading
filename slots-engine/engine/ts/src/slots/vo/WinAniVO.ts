module engine {
	import Point = createjs.Point;
	import Rectangle = createjs.Rectangle;

	export class WinAniVO {
		public winSymbolView:WinSymbolView;
		public rect:Rectangle;
		public posIdx:Point;

		constructor() {
		}
	}
}