module engine {
	export class BonusVO {
		public id:number;
		public step:number;
		public key:string;
		public type:string;
		public className:string;
		public data:any;
		public serverData:any;
		public sequence_type:any;
		public bonusConfig:any;

		constructor() {
		}
	}
}