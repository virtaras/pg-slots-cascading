module engine {
	import Sprite = createjs.Sprite;
	import Container = createjs.Container;
	import Rectangle = createjs.Rectangle;
	import Point = createjs.Point;

	export class ReelControllerCascade extends BaseController {
		private common:CommonRefs;
		private view:ReelsView;
		private runnerReels:number;
		private movers:Array<ReelMoverCascade>;

		constructor(manager:ControllerManager, common:CommonRefs, view:ReelsView) {
			super(manager);
			this.common = common;
			this.view = view;
			this.movers = [];
			this.runnerReels = 0;
		}

		public init():void {
			super.init();
			this.view.create();

			//mobiletoweb
			if(this.common.isMobile){
				this.view.scaleX *= GameController.MOBILE_TO_WEB_SCALE;
				this.view.scaleY *= GameController.MOBILE_TO_WEB_SCALE;
			}
			//mobiletoweb

			this.view.setMobileTrue(this.common.isMobile);
			this.setSymbolsPos();
			this.createMovers();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.START_SPIN);
			notifications.push(NotificationList.SERVER_GOT_SPIN);
			notifications.push(NotificationList.EXPRESS_STOP);
			notifications.push(NotificationList.SHOW_ALL_SYMBOLS);
			notifications.push(NotificationList.HIDE_SYMBOLS);
			notifications.push(NotificationList.OPEN_PAY_TABLE);
			notifications.push(NotificationList.CLOSE_PAY_TABLE);
			notifications.push(NotificationList.CREATE_BONUS);
			notifications.push(NotificationList.END_BONUS);
			notifications.push(NotificationList.RECURSION_PROCESS_FALLING);
			notifications.push(NotificationList.WIN_LINES_SHOWED);
			notifications.push(NotificationList.UPDATE_TURBO_MODE);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.START_SPIN:
				{
					this.startSpin();
					break;
				}
				case NotificationList.SERVER_GOT_SPIN:
				{
					this.gotSpin();
					break;
				}
				case NotificationList.EXPRESS_STOP:
				{
					//this.stopSpin();
					break;
				}
				case NotificationList.SHOW_ALL_SYMBOLS:
				{
					this.showAllSymbols();
					break;
				}
				case NotificationList.HIDE_SYMBOLS:
				{
					this.hideSymbols(data);
					break;
				}
				case NotificationList.CREATE_BONUS:
				case NotificationList.END_BONUS:
				{
					//this.updateSequenceType();
					break;
				}
				case NotificationList.OPEN_PAY_TABLE:
				{
					this.view.visible = false;
					break;
				}
				case NotificationList.CLOSE_PAY_TABLE:
				{
					this.view.visible = true;
					break;
				}
				case NotificationList.WIN_LINES_SHOWED:
				{
					this.processRecursion();
					break;
				}
				case NotificationList.RECURSION_PROCESS_FALLING:
				{
					this.recursionProcessFalling();
				}
				case NotificationList.UPDATE_TURBO_MODE:
				{
					this.updateTurboMode();
					break;
				}
			}
		}

		public processRecursion():void{
			var self:ReelControllerCascade = this;
			if(this.common.server.recursion.length){
				for(var i:number = 0; i<this.movers.length; ++i) {
					this.movers[i].processRecursionRotation(this.common);
				}
			}
		}

		public recursionProcessFalling():void{
			for (var reelId:number = 0; reelId < this.movers.length; reelId++) {
				this.movers[reelId].recursionProcessFalling(this.common);
			}
		}

		public onEnterFrame():void {
			for (var i:number = 0; i < this.movers.length; i++) {
				this.movers[i].onEnterFrame();
			}
		}

		public dispose():void {
			super.dispose();
			this.view.dispose();
			this.view = null;
			this.common = null;
			this.movers = null;
		}

		private getSequenceType():string {
			var bonus:BonusVO = this.common.server.bonus;
			if (bonus != null && bonus.type == BonusTypes.FREE_SPINS) {
				return ReelMoverCascade.TYPE_FREE_SPIN;
			}
			return ReelMoverCascade.TYPE_REGULAR;
		}

		//private updateSequenceType():void {
		//	var type:string = this.getSequenceType();
		//	for (var i:number = 0; i < this.movers.length; i++) {
		//		this.movers[i].changeSequenceType(type);
		//	}
		//}

		private showAllSymbols():void {
			this.view.showAllSymbols();
		}

		private hideSymbols(positions:Array<Point>):void {
			console.log("hideSymbols", positions);
			this.view.hideSymbols(positions);
		}

		private setSymbolsPos():void {
			var reelCount:number = this.view.getReelCount();
			var symbolsRect:Array<Array<Rectangle>> = new Array(reelCount);
			for (var reelId:number = 0; reelId < reelCount; reelId++) {
				var reelView:ReelView = this.view.getReel(reelId);
				var symbolCount:number = reelView.getSymbolCount();
				var reelSymbolsRect:Array<Rectangle> = new Array(symbolCount);
				for (var symbolId:number = 0; symbolId < symbolCount; symbolId++) {
					var symbol:Sprite = reelView.getSymbol(symbolId);
					var symbolPos:Point = symbol.localToGlobal(0, 0);
					var symbolRect:Rectangle = symbol.getBounds().clone();
					symbolRect.x = symbolPos.x;
					symbolRect.y = symbolPos.y;
					symbolRect.width = parseFloat(<any>symbolRect.width);
					symbolRect.height = parseFloat(<any>symbolRect.height);
					reelSymbolsRect[symbolId] = symbolRect;
				}
				symbolsRect[reelId] = reelSymbolsRect;
			}
			this.common.symbolsRect = symbolsRect;
		}

		private createMovers():void {
			var sequenceType:string = this.getSequenceType();
			for (var reelId:number = 0; reelId < this.view.getReelCount(); reelId++) {
				var reelView:ReelView = this.view.getReel(reelId);
				var reelMoverCascade:ReelMoverCascade = new ReelMoverCascade(this.common.config.reelStrips, reelView, reelId);
				reelMoverCascade.on(ReelMoverCascade.EVENT_REEL_STOPPED, (stoppedReelId:number)=> {
					this.send(NotificationList.STOPPED_REEL_ID, this.movers.length-this.runnerReels);
					if (--this.runnerReels == 0) {
						this.send(NotificationList.STOPPED_ALL_REELS);
					}
					console.log("this.runnerReels",this.runnerReels);
				});
				reelMoverCascade.on(ReelMoverCascade.EVENT_REEL_PREPARE_STOPPED, (stoppedReelId:number)=> {
					this.send(NotificationList.STOPPED_REEL_ID_PREPARE, this.movers.length-this.runnerReels);
				});
				reelMoverCascade.init(this.common.server.wheel, sequenceType);
				this.movers.push(reelMoverCascade);
			}

			if (this.common.config.spinOnReelsClick) {
				this.view.addEventListener("click", (eventObj:any) => {
					if (eventObj.nativeEvent instanceof MouseEvent) {
						this.send(NotificationList.ON_SCREEN_CLICK);
					}
				});
			}
		}

		private startSpin():void {
			if (this.runnerReels > 0) {
				return;
			}
			for (var reelId:number = 0; reelId < this.movers.length; reelId++) {
				this.runnerReels++;
				this.movers[reelId].start();
			}
		}

		private gotSpin():void {
			for (var reelId:number = 0; reelId < this.movers.length; reelId++) {
				this.movers[reelId].onGetSpin(this.common.server.wheel);
			}
		}

		//private stopSpin():void {
		//	for (var reelId:number = 0; reelId < this.movers.length; reelId++) {
		//		this.movers[reelId].expressStop();
		//	}
		//}

		public updateTurboMode():void{
			ReelMoverCascade.DELAYS = (this.common.isTurbo) ? ReelMoverCascade.DELAYS_TURBO : ReelMoverCascade.DELAYS_NORMAL;
		}
	}
}