module engine {
	import Sprite = createjs.Sprite;
	import Ticker = createjs.Ticker;
	import Container = createjs.Container;
	import LayoutCreator = layout.LayoutCreator;

	export class HeaderController extends BaseController {
		public static REGULAR_LAYOUT_NAME:string = "HeaderView";
		public static FREE_SPINS_LAYOUT_NAME:string = "FreeSpinsHeaderView";
		public static ANIMATION_CONTAINER:string = "animation";

		private static DELAY:number = 2;

		private common:CommonRefs;
		private container:Container;
		private view:Container;
		private delay:number;
		private animation:Sprite;
		private animationFrames:number;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
			super(manager);
			this.common = common;
			this.container = container;
			HeaderController.DELAY = Utils.float2int(HeaderController.DELAY * Ticker.getFPS());
		}

		public init():void {
			super.init();
		}

		private createView(type:string):void {
			var creator:LayoutCreator = this.common.layouts[type];
			if (creator != null) {
				this.view = new Container();
				creator.create(this.view);
				this.delay = 0;
				this.animation = <Sprite>this.view.getChildByName(HeaderController.ANIMATION_CONTAINER);
				if (this.animation != null) {
					this.animationFrames = this.animation.spriteSheet.getNumFrames(null) - 1;
				}
			}
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.SHOW_HEADER);
			notifications.push(NotificationList.REMOVE_HEADER);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.SHOW_HEADER:
				{
					if (this.view == null) {
						this.createView(HeaderController.REGULAR_LAYOUT_NAME);
					}
					if (!this.container.contains(this.view)){
						this.container.addChild(this.view);
					}
					break;
				}
				case NotificationList.REMOVE_HEADER:
				{
					if (this.view != null && this.container.contains(this.view)) {
						this.container.removeChild(this.view);
					}
					break;
				}
			}
		}

		public onEnterFrame():void {
			if (this.animation != null) {
				if (this.animation.paused) {
					if (this.delay == 0) {
						this.animation.gotoAndPlay(0);
					}
					this.delay -= 1;
				}
				else {
					if (this.animation.currentFrame == this.animationFrames) {
						this.animation.stop();
						this.delay = HeaderController.DELAY;
					}
				}
			}
		}

		public dispose():void {
			super.dispose();
		}
	}
}