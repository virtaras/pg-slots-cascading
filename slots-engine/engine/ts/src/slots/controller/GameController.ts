module engine {
	import Stage = createjs.Stage;
	import Ticker = createjs.Ticker;
	import Touch = createjs.Touch;
	import Container = createjs.Container;
	import LayoutCreator = layout.LayoutCreator;
	import Shape = createjs.Shape;
	import Text = createjs.Text;
	import Sound = createjs.Sound;

	export class GameController extends BaseController {
		public static isSoundLoaded:boolean = false;
		public static MOBILE_TO_WEB_SCALE:number = 1;
		public static SHADOW_COLOR:string = "rgba(0,0,0,1)";
		public static EVENT_BACK_TO_LOBBY:string = "back_to_lobby";
		public static EVENT_HIDE_PRELOADER:string = "hide_preloader";
		public static TICKER_FPS:number = 30;
		public static REELS_CNT:number = 5;

		private stage:Stage;
		private common:CommonRefs;
		private container:Container;
		private orientationView:RotateScreenView;
		private fingerFullScreenView:FingerFullScreenView;
		private bgTemp:Shape;
		public loaderView:LoaderView;
		public windowParams:any = {};
		public payTablesInited:boolean = false;

		//public sound_reinit_times:number = 0;
		//public sound_reinit_max_times:number = 5;

		constructor() {
			super(new ControllerManager());
		}

		/*public start(gameName:string, key:string, container:Container):void {
			super.init();

			this.container = container;
			this.common = new CommonRefs();
			this.common.gameName = gameName;
			this.common.key = key;
			this.common.isMobile = Device.isMobile();

			this.initBaseControllers();
		}*/

		public startSingle(stage:Stage, artWidth:number, artHeight:number, gameName:string, mobile:boolean, isDemo:boolean, login:string, password:string, isDebug:boolean, room:string, getParams:any = []):void {
			this.getWindowParams();
			//alert("startSingle: window.outerWidth="+window.outerWidth+" window.outerHeight="+window.outerHeight+" window.innerWidth="+this.windowParams.innerWidth+" window.innerHeight="+this.windowParams.innerHeight);
			super.init();
			this.stage = stage;

			this.common = new CommonRefs();
			this.common.isMobile = mobile;
			//this.common.isMobile = true; //always load mobile version //mobiletoweb
			this.common.gameName = this.common.isMobile ? gameName + "_mobile" : gameName;
			this.common.dirName = gameName + "/"; //ALLINONEGAMES
			//this.common.dirName = ""; //SEPARETEGAMES
			this.common.login = login;
			this.common.password = password;
			this.common.room = room;
			this.common.isDemo = isDemo;
			this.common.isDebug = isDebug;
			this.common.artWidth = artWidth;
			this.common.artHeight = artHeight;
			this.common.getParams = getParams;
			this.common.key = (getParams['token']) ? getParams['token'] : null;
			this.common.soundPreload = (typeof(getParams['soundPreload']) != "undefined") ? (getParams['soundPreload']==="1") : null;
			console.log("getParams",getParams);
			console.log("this.common.soundPreload", this.common.soundPreload);

			Ticker.setFPS(GameConst.GAME_FPS);

			this.container = new Container();
			if (!this.common.isMobile) {
				this.stage.enableMouseOver(Ticker.getFPS());
			}

			//this.stage.enableMouseOver(Ticker.getFPS()); //mobiletoweb

			this.stage.mouseMoveOutside = true;
			if (Touch.isSupported()) {
				Touch.enable(this.stage, true, true);
			}
			this.stage.addChild(this.container);

			Ticker.addEventListener("tick", ()=> {
				this.gameUpdate();
			});

			if(!this.common.getParams["preventFullScreen"]){
				FullScreen.init();
			}

			this.onResize();
			this.initBaseControllers();
		}

		public parseConfig():void{
			this.common.reelsWayCnt = GameController.REELS_CNT = this.common.config.reels_cnt || GameController.REELS_CNT;
			ReelMover.SYMBOLS_IN_REEL = this.common.config.symbols_in_reel || ReelMover.SYMBOLS_IN_REEL;
			HudView.HUDCONTAINER_PATH = this.common.config.hudContainerPath ? this.common.config.hudContainerPath : HudView.HUDCONTAINER_PATH; //prepare color container for buttons when needed
			this.common.artWidth = this.common.config.artWidth || this.common.artWidth;
			this.common.artHeight = this.common.config.artHeight || this.common.artHeight;
			this.common.artDiffX = this.common.config.artDiffX || this.common.artDiffX;
			this.common.artDiffY = this.common.config.artDiffY || this.common.artDiffY;
			HudView.LAYOUT_PARAMS = (this.common.config.layout_params && this.common.config.layout_params[HudView.LAYOUT_NAME]) ? this.common.config.layout_params[HudView.LAYOUT_NAME] : HudView.LAYOUT_PARAMS;
			PayTableHudView.LAYOUT_PARAMS = (this.common.config.layout_params && this.common.config.layout_params[PayTableHudView.LAYOUT_NAME]) ? this.common.config.layout_params[PayTableHudView.LAYOUT_NAME] : PayTableHudView.LAYOUT_PARAMS;
			ReelsView.LAYOUT_PARAMS = (this.common.config.layout_params && this.common.config.layout_params[ReelsView.LAYOUT_NAME]) ? this.common.config.layout_params[ReelsView.LAYOUT_NAME] : ReelsView.LAYOUT_PARAMS;
			JackpotViewPopup.LAYOUT_PARAMS = (this.common.config.layout_params && this.common.config.layout_params[JackpotViewPopup.LAYOUT_NAME]) ? this.common.config.layout_params[JackpotViewPopup.LAYOUT_NAME] : JackpotViewPopup.LAYOUT_PARAMS;
			LinesView.LAYOUT_PARAMS = (this.common.config.layout_params && this.common.config.layout_params[LinesView.LAYOUT_NAME]) ? this.common.config.layout_params[LinesView.LAYOUT_NAME] : LinesView.LAYOUT_PARAMS;
			Lucky88BonusView.LAYOUT_PARAMS = (this.common.config.layout_params && this.common.config.layout_params[Lucky88BonusView.LAYOUT_NAME]) ? this.common.config.layout_params[Lucky88BonusView.LAYOUT_NAME] : Lucky88BonusView.LAYOUT_PARAMS;
			SelectItemView.LAYOUT_PARAMS = (this.common.config.layout_params && this.common.config.layout_params[SelectItemView.LAYOUT_NAME]) ? this.common.config.layout_params[SelectItemView.LAYOUT_NAME] : SelectItemView.LAYOUT_PARAMS;
			LinesView.LAYOUT_PARAMS.highlight_line_hor = (this.common.config.highlight_line_hor) ? this.common.config.highlight_line_hor : LinesView.LAYOUT_PARAMS.highlight_line_hor;
			LinesView.LAYOUT_PARAMS.highlight_line_ver = (this.common.config.highlight_line_ver) ? this.common.config.highlight_line_ver : LinesView.LAYOUT_PARAMS.highlight_line_ver;
		}

		public gameUpdate():void {
			this.manager.onEnterFrame();
		}

		public onEnterFrame():void {
			if (this.stage != null) {
				this.stage.update();
			}
		}

        public listNotification():Array<string> {
            var notifications:Array<string> = super.listNotification();
            notifications.push(NotificationList.SERVER_INIT);
            notifications.push(NotificationList.BACK_TO_LOBBY);
            notifications.push(NotificationList.HIDE_PRELOADER);
            notifications.push(NotificationList.LAZY_LOAD_COMP);
            notifications.push(NotificationList.CONFIG_LOADED);
            //notifications.push(NotificationList.LOADER_LOADED);
            return notifications;
        }

        public handleNotification(message:string, data:any):void {
            switch (message) {
                case NotificationList.SERVER_INIT:
                {
					this.common.server_inited = true;
                    this.initGameControllers();
					this.initKeyboardHandler();
                    break;
                }
                case NotificationList.BACK_TO_LOBBY:
                {
					if(this.common.config.back_to_lobby_url){
						window.location.href = this.common.config.back_to_lobby_url;
					}
                    this.container.dispatchEvent(GameController.EVENT_BACK_TO_LOBBY);
                    break;
                }
                case NotificationList.HIDE_PRELOADER:
                {
					this.stage.dispatchEvent(GameController.EVENT_HIDE_PRELOADER);
                    break;
                }
                case NotificationList.LAZY_LOAD_COMP:
                {
                    this.initPayTables();
                    break;
                }
				case NotificationList.CONFIG_LOADED:
                {
                    this.parseConfig();
                    break;
                }
            }
        }

		private initBaseControllers():void {
			var baseController:BaseController;

			baseController = new ServerController(this.manager, this.common);
			baseController.init();

			this.loaderView = new LoaderView();
			baseController = new LoaderController(this.manager, this.common, this.loaderView);
			baseController.init();

			this.container.addChild(this.loaderView);
		}

        private initPayTables():void {
			if(!this.payTablesInited) {
				this.payTablesInited = true;
				if(this.common.layouts[PayTableHudView.LAYOUT_NAME]) {
					var baseController:BaseController;
					baseController = new PayTableController(this.manager, this.common, this.container);
					baseController.init();
				}

				// create background
				var backgroundContainer:Container = new Container();
				baseController = new BackgroundController(this.manager, this.common, backgroundContainer);
				baseController.init();

				this.container.addChildAt(backgroundContainer, 0);

				this.common.payTableLoaded = true;
				this.send(NotificationList.LOAD_PAY_TABLE);
			}
        }

		private initGameControllers():void {
			this.common.initedGameControllers = true;
			//Ticker.setFPS(GameController.TICKER_FPS);
			var baseController:BaseController;

			//this.onResize();

			baseController = new SoundController(this.manager, this.common);
			baseController.init();
			// create header
			var headerContainer:Container = new Container();
			baseController = new HeaderController(this.manager, this.common, headerContainer);
			baseController.init();

			this.common.moverType = (this.common.config.reelMoverType) ? this.common.config.reelMoverType : ReelMoverTypes.DEFAULT;
			switch(this.common.moverType){
				case ReelMoverTypes.TAPE:{
					var reelsView:ReelsView = this.common.layouts[ReelsView.LAYOUT_NAME];
					baseController = new ReelController(this.manager, this.common, reelsView);
					break;
				}
				case ReelMoverTypes.CASCADE:{
					var reelsView:ReelsView = this.common.layouts[ReelsView.LAYOUT_NAME];
					baseController = new ReelControllerCascade(this.manager, this.common, reelsView);
					break;
				}
			}
			baseController.init();

			// create lines
			var linesView:LinesView = this.common.layouts[LinesView.LAYOUT_NAME] || null;
			baseController = new LinesController(this.manager, this.common, linesView);
			baseController.init();

			// create win animation
			var winViewOver:Container = new Container();
			var winView:Container = new Container();
			baseController = new WinController(this.manager, this.common, winView);
			baseController.init();
			if(this.common.config.add_reelsview_mask_to_winview) winView.mask = reelsView.mask;

			// create bonus
			var bonusContainer:Container = new Container();
			baseController = new BonusHolderController(this.manager, this.common, bonusContainer);
			baseController.init();

			//create Wild Views
			baseController = new ReelWildExpandingController(this.manager, this.common, reelsView, winView);
			baseController.init();
			baseController = new ReelWildStickyController(this.manager, this.common, reelsView, winViewOver);
			baseController.init();

			// create game hud
			var hudView:HudView = this.common.layouts[HudView.LAYOUT_NAME];
			hudView.create();

			//JACKPOT
			if(this.common.layouts[JackpotView.LAYOUT_NAME]) {
				var jackpotView:JackpotView = this.common.layouts[JackpotView.LAYOUT_NAME];
				jackpotView.create();
				baseController = new JackpotController(this.manager, this.common, jackpotView);
				baseController.init();
				//headerContainer.addChild(jackpotView); //jackpot over bonus holder
				hudView.addChildAt(jackpotView, 0); //jackpot under bonus holder
			}
			if(this.common.layouts[JackpotViewPopup.LAYOUT_NAME]) {
				var jackpotViewPopup:JackpotViewPopup = this.common.layouts[JackpotViewPopup.LAYOUT_NAME];
				jackpotViewPopup.create();
				var jackpotBounds:any = jackpotViewPopup.getBounds();
				var jackpotViewPopupX:number = JackpotViewPopup.LAYOUT_PARAMS.x || ((this.common.isMobile) ? 15 : 0);
				var jackpotViewPopupY:number = JackpotViewPopup.LAYOUT_PARAMS.y || ((this.common.isMobile) ? 0 : 20);
				jackpotViewPopup.setParams(jackpotViewPopupX, jackpotViewPopupY, JackpotViewPopup.LAYOUT_PARAMS.scaleX || 1, JackpotViewPopup.LAYOUT_PARAMS.scaleY || this.common.artHeight/jackpotBounds.height);
			}

			// create bonus
			var modalContainer:Container = new Container();
			baseController = new ModalWindowController(this.manager, this.common, modalContainer);
			baseController.init();

			// error controller
			baseController = new ErrorController(this.manager, this.common, modalContainer);
			baseController.init();

			var autoSpinTableView:Container = <Container>hudView.getChildByName(HudView.AUTO_SPIN_TABLE);
			if (autoSpinTableView != null) {
				var autoSpinTableMask:Container = <Container>hudView.getChildByName(HudView.AUTO_SPIN_TABLE_MASK);
				baseController = new AutoSpinTableController(this.manager, this.common, autoSpinTableView, autoSpinTableMask);
				baseController.init();
			}

			var colorPanelView:Container = <Container>hudView.getChildByName(HudView.COLOR_PANEL);
			if (colorPanelView != null) {
				baseController = new ColorPanelController(this.manager, this.common, colorPanelView);
				baseController.init();
			}

			baseController = new StatsController(this.manager, this.common, hudView);
			baseController.init();

			var hudController:HudController = new HudController(this.manager, this.common, hudView);
			hudController.init();

			var settingsView:Container = <Container>hudView.getChildByName(HudView.SETTINGS_MENU);
			if (settingsView != null) {
				baseController = new SettingsController(this.manager, this.common, hudController, settingsView);
				baseController.init();
			}

			if(this.common.layouts[MenuView.LAYOUT_NAME]) {
				var menuView:MenuView = this.common.layouts[MenuView.LAYOUT_NAME];
				menuView.create();
				baseController = new MenuController(this.manager, this.common, this.container, menuView);
				baseController.init();
			}

			//baseController = new SetWheelController(this.manager, new SetWheelView(GameController.REELS_CNT, ReelMover.SYMBOLS_IN_REEL));
			//baseController.init();

			this.container.addChild(reelsView);
			this.container.addChild(linesView);
			this.container.addChild(winView);
			this.container.addChild(winViewOver);
			this.container.addChild(hudView);
			this.container.addChild(bonusContainer);
			this.container.addChild(headerContainer);
			this.container.addChild(menuView);
			this.container.addChild(modalContainer);
			if (this.common.isMobile) {
				hudView.resizeForMobile();
			}
			this.onResize();
		}

		private initKeyboardHandler():void {
			document.onkeydown = (event)=> {
				this.send(NotificationList.KEYBOARD_CLICK, event.keyCode);
			};
		}

		private showChangeRotation():void {
			if (this.windowParams.innerHeight > this.windowParams.innerWidth) {
				if (this.orientationView == undefined){
					if (this.common.layouts != null && this.common.layouts[RotateScreenView.LAYOUT_NAME]) {
						this.orientationView = this.common.layouts[RotateScreenView.LAYOUT_NAME];
						if (this.orientationView != null) {
							this.orientationView.create();
						}
					}
				}
				if (this.orientationView != null && !this.stage.contains(this.orientationView)){
					this.orientationView.play();
					this.stage.addChild(this.orientationView);
					this.container.visible = false;
				}
			}
			else{
				if (this.orientationView != undefined && this.stage.contains(this.orientationView)){
					this.orientationView.stop();
					this.stage.removeChild(this.orientationView);
					this.container.visible = true;
				}
			}
		}

		public resizeForIphone5():void {
			document.body.style.width = ((<any>document).width+1) + "px";
			document.body.style.height = ((<any>document).height+1) + "px";
		}

		public onResize():void {
			this.getWindowParams();
			this.processRotation();
			if(!this.common.getParams["preventFullScreen"]){
				this.resizeForIphone5();
				this.processFullScreenCheck();
			}
			window.scrollTo(0, 0);
			var canvas:HTMLCanvasElement = this.stage.canvas;

			var maxWidth:number = this.windowParams.innerWidth;
			var maxHeight:number = this.windowParams.innerHeight;

			// keep aspect ratio
			var scale:number = this.getScale();


			if (scale > 1 && !this.common.isMobile) {
				scale = 1;
			}
			this.common.maxscale = Math.min(scale, this.common.maxscale);
			this.stage.scaleX = scale;
			this.stage.scaleY = scale;

			this.stage.x = (maxWidth - this.common.artWidth * scale) / 2 + this.common.artDiffX * scale;
			this.stage.y = (maxHeight - this.common.artHeight * scale) / 2 + this.common.artDiffY * scale;

			// adjust canvas size
			canvas.width = maxWidth;
			canvas.height = maxHeight;

			if (this.common.isMobile && this.common.layouts != null) {
				var hudView:HudView = this.common.layouts[HudView.LAYOUT_NAME];
				if(hudView){
					hudView.resizeForMobile();
				}
				var payView:PayTableHudView = this.common.layouts[PayTableHudView.LAYOUT_NAME];
				if(payView && !PayTableHudView.LAYOUT_PARAMS.preventButtonsCodePosition){
					payView.resizeForMobile();
				}
			}
		}

		public getScale():number{
			var scale:number = Math.min(this.windowParams.innerWidth / this.common.artWidth, this.windowParams.innerHeight / this.common.artHeight);
			try {
				var windowRatio:number = this.windowParams.innerWidth / this.windowParams.innerHeight;
				for(var i:number=0; i<this.common.config.scaleParams.length; ++i){
					if(((this.common.config.scaleParams[i].from && windowRatio>=this.common.config.scaleParams[i].from) || !this.common.config.scaleParams[i].from) && ((this.common.config.scaleParams[i].to && windowRatio<this.common.config.scaleParams[i].to) || !this.common.config.scaleParams[i].to)){
						scale *= this.common.config.scaleParams[i].multiplier;
						break;
					}
				}
			}catch (e){console.log("ERROR:",e);}
			return scale;
		}

		public dispose():void {
			this.send(NotificationList.SERVER_DISCONNECT);
		}

		public static setFieldStroke(container:Container, field:string, outline:number, color:string, offsetX:number, offsetY:number, blur:number):void{
			var text:Text = <Text>container.getChildByName(field);
			GameController.setTextStroke(text, outline, color, offsetX, offsetY, blur);
		}

		public static setTextStroke(text:Text, outline:number, color:string, offsetX:number, offsetY:number, blur:number){
			if(text) {
				text.outline = outline;
				text.shadow = new createjs.Shadow(color, offsetX, offsetY, blur);
			}
		}

		public getWindowParams():any{
			this.windowParams.innerWidth = document.getElementsByTagName('body')[0].clientWidth;
			this.windowParams.zoomLevel = document.documentElement.clientWidth / window.innerWidth;
			this.windowParams.innerHeight = window.innerHeight * this.windowParams.zoomLevel;

			this.windowParams.innerWidth = window.innerWidth;
			this.windowParams.innerHeight = window.innerHeight;
			return;
		}

		public processRotation():void{
			if(this.common.initedGameControllers) {
				if (!this.common.config.enablePortraitMode) {
					this.showChangeRotation();
				}
				if (this.windowParams.innerWidth >= this.windowParams.innerHeight && this.common.screenMode != GameConst.SCREEN_MODE_LANDSCAPE) { //landscape
					this.common.screenMode = GameConst.SCREEN_MODE_LANDSCAPE;
				} else if (this.windowParams.innerWidth < this.windowParams.innerHeight && this.common.screenMode != GameConst.SCREEN_MODE_PORTRAIT) { //portrait
					this.common.screenMode = GameConst.SCREEN_MODE_PORTRAIT;
				}
				this.updateScreenMode();
			}
		}

		public updateScreenMode():void{
			switch (this.common.screenMode){
				case GameConst.SCREEN_MODE_LANDSCAPE:
				{
					this.common.artWidth = this.common.config.artWidth || this.common.artWidth;
					this.common.artHeight = this.common.config.artHeight || this.common.artHeight;
					this.common.artDiffX = this.common.config.artDiffX || this.common.artDiffX;
					this.common.artDiffY = this.common.config.artDiffY || this.common.artDiffY;
					HudView.LAYOUT_PARAMS = (this.common.config.layout_params && this.common.config.layout_params[HudView.LAYOUT_NAME]) ? this.common.config.layout_params[HudView.LAYOUT_NAME] : HudView.LAYOUT_PARAMS;
					break;
				}
				case GameConst.SCREEN_MODE_PORTRAIT:
				{
					this.common.artWidth = this.common.config.artWidthPortrait || this.common.artWidth;
					this.common.artHeight = this.common.config.artHeightPortrait || this.common.artHeight;
					this.common.artDiffX = this.common.config.artDiffXPortrait || this.common.artDiffX;
					this.common.artDiffY = this.common.config.artDiffYPortrait || this.common.artDiffY;
					HudView.LAYOUT_PARAMS = (this.common.config.layout_params && this.common.config.layout_params[HudView.LAYOUT_NAME_PORTRAIT]) ? this.common.config.layout_params[HudView.LAYOUT_NAME_PORTRAIT] : HudView.LAYOUT_PARAMS;
					break;
				}
			}
			this.send(NotificationList.UPDATE_SCREEN_MODE, this.common.screenMode);
		}

		public blockMove(event){
			if (!this.common.server_inited || FullScreen.isFullScreen() || (!GameController.isIOSChrome() && this.windowParams.innerHeight >= document.documentElement.clientHeight) || this.isIOSChromeFullScreen()) {
				event.preventDefault();
			}else{
				if (this.common.isMobile && this.common.initedGameControllers) {
					this.fullScreen();
				}
			}
			return;
		}

		public windowScrollTop() {
			//window.scrollTo(0, 0);
		}

		public isFullScreen():boolean{
			return (FullScreen.isFullScreen() || this.windowParams.innerHeight >= document.documentElement.clientHeight); //TODO: correct for iOS portrait mode
		}

		public isIOSChromeFullScreen():boolean{
			return (GameController.isIOSChrome() &&  Math.max(this.windowParams.innerHeight, screen.width) <= document.documentElement.clientHeight+50);
		}

		public processFullScreenCheck():void{
			if(!this.isFullScreen() && (this.windowParams.innerWidth>=this.windowParams.innerHeight) && !this.isIOSChromeFullScreen()){
				if (this.fingerFullScreenView == undefined){
					if (this.common.layouts != null) {
						this.fingerFullScreenView = this.common.layouts[FingerFullScreenView.LAYOUT_NAME];
						if (this.fingerFullScreenView != null) {
							this.fingerFullScreenView.create();
							this.fingerFullScreenView.addEventListener("click", function(){});
						}
					}
				}
				if (this.fingerFullScreenView != null && !this.stage.contains(this.fingerFullScreenView) && this.common.initedGameControllers){
					this.fingerFullScreenView.play();
					this.stage.addChild(this.fingerFullScreenView);
				}
			}else{
				if (this.fingerFullScreenView != null && this.stage.contains(this.fingerFullScreenView)) {
					this.fingerFullScreenView.stop();
					this.stage.removeChild(this.fingerFullScreenView);
				}
			}
		}

		public static isIOSChrome():void{
			var isChromium:boolean = <boolean>window.chrome,
				winNav = window.navigator,
				vendorName = winNav.vendor,
				isOpera = winNav.userAgent.indexOf("OPR") > -1,
				isIEedge = winNav.userAgent.indexOf("Edge") > -1,
				isIOSChrome = winNav.userAgent.match("CriOS");
			return isIOSChrome || (isChromium !== null && isChromium !== undefined && vendorName === "Google Inc." && isOpera == false && isIEedge == false);
		}

		public fullScreen():void{
			if(!this.common.getParams['preventFullScreen']){
				FullScreen.fullScreen();
			}
		}
	}
}