module engine {
	import Button = layout.Button;
	import Toggle = layout.Toggle;
	import Text = createjs.Text;
	import Sound = createjs.Sound;
	import Container = createjs.Container;
	import Shape = createjs.Shape;

	export class MenuController extends BaseController {
		public static DEFAULT_MAX_AUTO_SPIN_COUNT:number = 100;
		public static DEFAULT_MAX_BET_PER_LINE_COUNT:number = 100;
		public static DEFAULT_MAX_LINES_COUNT:number = 100;
		public static DEFAULT_MIN_AUTO_SPIN_COUNT:number = 2;
		public static DEFAULT_MIN_LINES_COUNT:number = 1;
		private common:CommonRefs;
		private gameContainer:Container;
		private view:MenuView;
		public autoSpinCountCrawler:Crawler = null;
		public reelsWayCountCrawler:Crawler = null;
		public betPerLine:Crawler = null;
		public linesCountCrawler:Crawler = null;

		constructor(manager:ControllerManager, common:CommonRefs, gameContainer:Container, view:MenuView) {
			super(manager);
			this.common = common;
			this.gameContainer = gameContainer;
			this.view = view;
			MenuController.DEFAULT_MAX_AUTO_SPIN_COUNT = this.common.config.default_max_auto_spin_count || MenuController.DEFAULT_MAX_AUTO_SPIN_COUNT;
			MenuController.DEFAULT_MAX_BET_PER_LINE_COUNT = this.common.server.availableBets[this.common.server.availableBets.length - 1];
			MenuController.DEFAULT_MAX_LINES_COUNT = this.common.server.maxLineCount;
		}

		public init():void {
			super.init();
			this.initButtons();
			this.initSoundToggle();
			this.updateSoundToggle();
			this.initTurboToggle();
			this.updateTurboMode();
			this.initAutoSpinCountCrawler();
			this.initReelsWayCountCrawler();
			this.initBetPerLineCrawler();
			this.initLinesCountCrawler();
			this.initPaytables();
			this.updateBetPerLineText();
			this.updateTotalBetText();
			this.initViewClickListener();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.OPEN_GAME_MENU);
			notifications.push(NotificationList.UPDATE_TURBO_MODE);
			notifications.push(NotificationList.SOUND_TOGGLE_UPDATE);
			notifications.push(NotificationList.BET_PER_LINE_COUNT_UPDATE);
			notifications.push(NotificationList.LINES_COUNT_UPDATE);
			notifications.push(NotificationList.SOUNDS_LOADED);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.OPEN_GAME_MENU:
				{
					this.onOpen();
					break;
				}
				case NotificationList.UPDATE_TURBO_MODE:
				{
					this.updateTurboMode();
					break;
				}
				case NotificationList.SOUND_TOGGLE_UPDATE:
				{
					this.updateSoundToggle();
					break;
				}
				case NotificationList.BET_PER_LINE_COUNT_UPDATE:
				{
					this.updateBetPerLineText();
					this.updateTotalBetText();
					break;
				}
				case NotificationList.LINES_COUNT_UPDATE:
				{
					this.updateTotalBetText();
					break;
				}
				case NotificationList.SOUNDS_LOADED:
				{
					this.updateSoundToggle();
					break;
				}
			}
		}

		public onBtnClick(buttonName:string):void {
			switch (buttonName) {
				case MenuView.MENU_CLOSE_BTN:
				{
					this.onClose();
					break;
				}
				case MenuView.MENU_SOUND_BTN:
				{
					this.view.showBtnPage(MenuView.MENU_SOUND_BTN);
					this.view.showPage(MenuView.MENU_SOUND_PAGE);
					break;
				}
				case MenuView.MENU_SPIN_SETTINGS_BTN:
				{
					this.view.showBtnPage(MenuView.MENU_SPIN_SETTINGS_BTN);
					this.view.showPage(MenuView.MENU_SPIN_SETTINGS_PAGE);
					break;
				}
				case MenuView.MENU_BET_SETTINGS_BTN:
				{
					this.view.showBtnPage(MenuView.MENU_BET_SETTINGS_BTN);
					this.view.showPage(MenuView.MENU_BET_SETTINGS_PAGE);
					break;
				}
				case MenuView.MENU_PAYTABLES_BTN:
				{
					this.view.showBtnPage(MenuView.MENU_PAYTABLES_BTN);
					this.view.showPage(MenuView.MENU_PAYTABLES_PAGE);
					break;
				}
			}
			MenuController.playBtnSound(buttonName);
		}

		private static playBtnSound(buttonName:string):void {
			SoundController.playSoundjs(SoundsList.REGULAR_CLICK_BTN);
		}

		private onOpen():void {
			this.send(NotificationList.START_PAY_TABLE_SOUND);
			this.updateSoundToggle();
			this.view.visible = true;
		}

		private onClose():void {
			this.send(NotificationList.END_PAY_TABLE_SOUND);
			this.send(NotificationList.START_NORMAL_GAME_BG_SOUND);
			this.send(NotificationList.CLOSE_PAY_TABLE);
			this.view.visible = false;
		}

		private initButtons():void {
			var buttonsNames:Array<string> = this.view.buttonsNames;
			for (var i:number = 0; i < buttonsNames.length; i++) {
				var buttonName:string = buttonsNames[i];
				var button:Button = this.view.getBtn(buttonName);
				if (button != null) {
					button.on("click", (eventObj:any)=> {
						if (eventObj.nativeEvent instanceof MouseEvent) {
							this.onBtnClick(eventObj.currentTarget.name);
						}
					})
				}
			}
		}

		public initSoundToggle():void {
			var soundToggle:Toggle = <Toggle>this.view.getChildByName(MenuView.MENU_SOUND_PAGE).getChildByName(MenuView.MENU_SOUND_TOGGLE);
			if (soundToggle != null) {
				soundToggle.setIsOn(this.common.isSoundLoaded);
				soundToggle.on("click", (eventObj:any)=> {
					if (eventObj.nativeEvent instanceof MouseEvent) {
						this.send(NotificationList.SOUND_TOGGLE);
						SoundController.playSoundjs(SoundsList.REGULAR_CLICK_BTN);
					}
				});
			}
		}

		public initTurboToggle():void {
			var turboToggle:Toggle = <Toggle>this.view.getChildByName(MenuView.MENU_SPIN_SETTINGS_PAGE).getChildByName(MenuView.MENU_TURBO_TOGGLE);
			if (turboToggle != null) {
				turboToggle.on("click", (eventObj:any)=> {
					if (eventObj.nativeEvent instanceof MouseEvent) {
						this.common.isTurbo = !this.common.isTurbo;
						this.send(NotificationList.UPDATE_TURBO_MODE);
						SoundController.playSoundjs(SoundsList.REGULAR_CLICK_BTN);
					}
				});
			}
		}

		public updateTurboMode():void{
			try {
				var turboToggle:Toggle = <Toggle>this.view.getChildByName(MenuView.MENU_SPIN_SETTINGS_PAGE).getChildByName(MenuView.MENU_TURBO_TOGGLE);
				turboToggle.setIsOn(this.common.isTurbo);
			}catch(e){}
		}

		public updateSoundToggle():void{
			this.view.updateSoundToggle(this.common.isSoundLoaded, this.common.isSoundOn);
		}

		public initAutoSpinCountCrawler():void{
			this.view.setText(<Text>this.view.getChildByName(MenuView.MENU_SPIN_SETTINGS_PAGE).getChildByName(MenuView.AUTO_SPINS_MAX_TF), MenuController.DEFAULT_MAX_AUTO_SPIN_COUNT.toString());
			var crawlerLine:Container = <Container>this.view.getChildByName(MenuView.MENU_SPIN_SETTINGS_PAGE).getChildByName(MenuView.AUTO_SPIN_COUNT_CRAWLER_LINE);
			var crawler:Container = <Container>this.view.getChildByName(MenuView.MENU_SPIN_SETTINGS_PAGE).getChildByName(MenuView.AUTO_SPIN_COUNT_CRAWLER);
			var crawl:Container = <Container>this.view.getChildByName(MenuView.MENU_SPIN_SETTINGS_PAGE).getChildByName(MenuView.AUTO_SPIN_COUNT_CRAWLER).getChildByName(MenuView.AUTO_SPIN_COUNT_CRAWL);
			var crawlTf:Text = <Text>this.view.getChildByName(MenuView.MENU_SPIN_SETTINGS_PAGE).getChildByName(MenuView.AUTO_SPIN_COUNT_CRAWLER).getChildByName(MenuView.AUTO_SPIN_COUNT_CRAWL_TF);
			this.autoSpinCountCrawler = new Crawler(this.view);
			this.autoSpinCountCrawler.initCrawler(crawlerLine, crawler, crawl, crawlTf, MenuController.DEFAULT_MAX_AUTO_SPIN_COUNT, MenuController.DEFAULT_MIN_AUTO_SPIN_COUNT, ()=>{}, ()=>{}, ()=>{this.common.autoSpinsCnt = parseInt(crawlTf.text); this.send(NotificationList.AUTO_PLAY_COUNT_UPDATE, parseInt(crawlTf.text));}, null);
			this.autoSpinCountCrawler.setValue(this.common.autoSpinsCnt);
		}

		public initReelsWayCountCrawler():void{
			if(this.view.getChildByName(MenuView.MENU_SPIN_SETTINGS_PAGE).getChildByName(MenuView.REELS_WAY_MAX_TF)) {
				this.view.setText(<Text>this.view.getChildByName(MenuView.MENU_SPIN_SETTINGS_PAGE).getChildByName(MenuView.REELS_WAY_MAX_TF), GameController.REELS_CNT.toString());
				var crawlerLine:Container = <Container>this.view.getChildByName(MenuView.MENU_SPIN_SETTINGS_PAGE).getChildByName(MenuView.REELS_WAY_COUNT_CRAWLER_LINE);
				var crawler:Container = <Container>this.view.getChildByName(MenuView.MENU_SPIN_SETTINGS_PAGE).getChildByName(MenuView.REELS_WAY_COUNT_CRAWLER);
				var crawl:Container = <Container>this.view.getChildByName(MenuView.MENU_SPIN_SETTINGS_PAGE).getChildByName(MenuView.REELS_WAY_COUNT_CRAWLER).getChildByName(MenuView.REELS_WAY_COUNT_CRAWL);
				var crawlTf:Text = <Text>this.view.getChildByName(MenuView.MENU_SPIN_SETTINGS_PAGE).getChildByName(MenuView.REELS_WAY_COUNT_CRAWLER).getChildByName(MenuView.REELS_WAY_COUNT_CRAWL_TF);
				this.reelsWayCountCrawler = new Crawler(this.view);
				this.reelsWayCountCrawler.initCrawler(crawlerLine, crawler, crawl, crawlTf, GameController.REELS_CNT, 1, ()=> {
				}, ()=> {
				}, ()=> {
					var reelsWayCnt:number = parseInt(crawlTf.text);
					this.common.reelsWayCnt = reelsWayCnt;
					this.common.server.waysCount = this.getWaysCountForReelsWayCnt(reelsWayCnt) || this.common.server.waysCount;
					this.send(NotificationList.REELS_WAY_COUNT_UPDATE, this.common.reelsWayCnt);
				}, null);
				this.reelsWayCountCrawler.setValue(this.common.reelsWayCnt);
			}
		}

		public getWaysCountForReelsWayCnt(reelsWayCnt:number):number{
			var waysCount:number = 0;
			for(var i:number=0; i<this.common.config.reelsWays.length; ++i){
				if(this.common.config.reelsWays[i].reels == reelsWayCnt){
					waysCount = this.common.config.reelsWays[i].ways;
				}
			}
			return waysCount;
		}

		public initBetPerLineCrawler():void{
			this.view.setText(<Text>this.view.getChildByName(MenuView.MENU_BET_SETTINGS_PAGE).getChildByName(MenuView.TOTAL_BET_MAX_TF), MenuController.DEFAULT_MAX_BET_PER_LINE_COUNT.toString());
			var crawlerLine:Container = <Container>this.view.getChildByName(MenuView.MENU_BET_SETTINGS_PAGE).getChildByName(MenuView.TOTAL_BET_COUNT_CRAWLER_LINE);
			var crawler:Container = <Container>this.view.getChildByName(MenuView.MENU_BET_SETTINGS_PAGE).getChildByName(MenuView.TOTAL_BET_COUNT_CRAWLER);
			var crawl:Container = <Container>this.view.getChildByName(MenuView.MENU_BET_SETTINGS_PAGE).getChildByName(MenuView.TOTAL_BET_COUNT_CRAWLER).getChildByName(MenuView.TOTAL_BET_COUNT_CRAWL);
			var crawlTf:Text = <Text>this.view.getChildByName(MenuView.MENU_BET_SETTINGS_PAGE).getChildByName(MenuView.TOTAL_BET_COUNT_CRAWLER).getChildByName(MenuView.TOTAL_BET_COUNT_CRAWL_TF);
			this.betPerLine = new Crawler(this.view);
			this.betPerLine.initCrawler(crawlerLine, crawler, crawl, crawlTf, MenuController.DEFAULT_MAX_BET_PER_LINE_COUNT, 0, ()=>{}, ()=>{}, ()=>{this.common.server.bet = parseFloat(crawlTf.text); this.send(NotificationList.BET_PER_LINE_COUNT_UPDATE, parseFloat(crawlTf.text));}, this.common.server.availableBets);
			this.betPerLine.setValue(this.common.server.bet);
		}

		public initLinesCountCrawler():void{
			if(this.view.getChildByName(MenuView.MENU_BET_SETTINGS_PAGE).getChildByName(MenuView.LINES_MAX_TF)) {
				this.view.setText(<Text>this.view.getChildByName(MenuView.MENU_BET_SETTINGS_PAGE).getChildByName(MenuView.LINES_MAX_TF), MenuController.DEFAULT_MAX_LINES_COUNT.toString());
				var crawlerLine:Container = <Container>this.view.getChildByName(MenuView.MENU_BET_SETTINGS_PAGE).getChildByName(MenuView.LINES_COUNT_CRAWLER_LINE);
				var crawler:Container = <Container>this.view.getChildByName(MenuView.MENU_BET_SETTINGS_PAGE).getChildByName(MenuView.LINES_COUNT_CRAWLER);
				var crawl:Container = <Container>this.view.getChildByName(MenuView.MENU_BET_SETTINGS_PAGE).getChildByName(MenuView.LINES_COUNT_CRAWLER).getChildByName(MenuView.LINES_COUNT_CRAWL);
				var crawlTf:Text = <Text>this.view.getChildByName(MenuView.MENU_BET_SETTINGS_PAGE).getChildByName(MenuView.LINES_COUNT_CRAWLER).getChildByName(MenuView.LINES_COUNT_CRAWL_TF);
				this.linesCountCrawler = new Crawler(this.view);
				this.linesCountCrawler.initCrawler(crawlerLine, crawler, crawl, crawlTf, MenuController.DEFAULT_MAX_LINES_COUNT, MenuController.DEFAULT_MIN_LINES_COUNT, ()=> {
				}, ()=> {
				}, ()=> {
					this.common.server.lineCount = parseInt(crawlTf.text);
					this.send(NotificationList.LINES_COUNT_UPDATE, parseInt(crawlTf.text));
				}, null);
				this.linesCountCrawler.setValue(this.common.server.lineCount);
			}
		}

		public initPaytables():void{
			var paytablesPage:Container = <Container>this.view.getChildByName(MenuView.MENU_PAYTABLES_PAGE);
			var paytablesPageScroll:Container = <Container>paytablesPage.getChildByName(MenuView.PAYTABLES_SCROLLAREA);
			var paytablesMask:Container = <Container>paytablesPageScroll.getChildByName(MenuView.PAYTABLES_MASK);
			var paytablesCrawlerLine:Container = <Container>paytablesPage.getChildByName(MenuView.PAYTABLES_CRAWLER_LINE);
			var paytablesCrawler:Container = <Container>paytablesPage.getChildByName(MenuView.PAYTABLES_CRAWLER);
			var paytablesCrawlerLineHeight:number = paytablesCrawlerLine.getBounds().height;
			var paytablesCrawlerHeight:number = paytablesCrawler.getBounds().height;
			paytablesMask.visible = false;
			var paytablesContainer:Container = <Container>paytablesPageScroll.getChildByName(MenuView.PAYTABLES_CONTAINER);
			var mask:Shape = new Shape();
			mask.graphics.beginFill("0").drawRect(paytablesMask.x, paytablesMask.y, paytablesMask.getBounds().width, paytablesMask.getBounds().height);
			paytablesContainer.mask = mask;
			paytablesPageScroll.hitArea = mask;
			var startY:number = paytablesMask.y - paytablesContainer.getBounds().height + paytablesMask.getBounds().height;
			var startYCrawler:number = paytablesCrawlerLine.y;
			var endY:number = paytablesMask.y;
			var currentY:number = endY;
			paytablesCrawler.on("mousedown", (eventDownObj:any)=>{
				this.view.on("pressmove", (eventMoveObj:any)=>{
					var div:number = Utils.float2int((eventMoveObj.stageY - eventDownObj.stageY))/this.view.getStage().scaleY*(paytablesPageScroll.getBounds().height/paytablesCrawlerLineHeight);
					if((currentY - div)<=startY && (currentY - div)>endY) {
						eventDownObj.stageY = eventMoveObj.stageY;
					}
					eventDownObj.stageY = eventMoveObj.stageY;
					currentY = Math.min(Math.max(currentY - div, startY), endY);
					paytablesContainer.y = currentY;
					paytablesCrawler.y = paytablesCrawlerLineHeight - paytablesCrawlerHeight/4 - (startY - currentY)*paytablesCrawlerLineHeight/(startY-endY)+startYCrawler;
				});
				this.view.on("pressup", ()=>{
					this.view.removeAllEventListeners("pressmove");
					this.view.removeAllEventListeners("pressup");
				});
			});
			paytablesPageScroll.on("mousedown", (eventDownObj:any)=>{
				this.view.on("pressmove", (eventMoveObj:any)=>{
					var div:number = Utils.float2int((eventMoveObj.stageY - eventDownObj.stageY))/this.view.getStage().scaleY*2;
					if((currentY + div)>=startY && (currentY + div)<endY) {
						eventDownObj.stageY = eventMoveObj.stageY;
					}
					currentY = Math.min(Math.max(currentY + div, startY), endY);
					paytablesContainer.y = currentY;
					paytablesCrawler.y = paytablesCrawlerLineHeight - paytablesCrawlerHeight/4 - (startY - currentY)*paytablesCrawlerLineHeight/(startY-endY)+startYCrawler;

				});
				this.view.on("pressup", ()=>{
					this.view.removeAllEventListeners("pressmove");
					this.view.removeAllEventListeners("pressup");
				});
			});
		}

		public updateBetPerLineText():void{
			this.view.setText(<Text>this.view.getChildByName(MenuView.MENU_BET_SETTINGS_PAGE).getChildByName(MenuView.BET_PER_LINE_TF), MoneyFormatter.format(this.common.server.bet, false).toString());
		}

		public updateTotalBetText():void{
			this.view.setText(<Text>this.view.getChildByName(MenuView.MENU_BET_SETTINGS_PAGE).getChildByName(MenuView.TOTAL_BET_TF), MoneyFormatter.format(this.common.server.getTotalBet(), false).toString());
		}

		public initViewClickListener():void{
			this.view.addEventListener("click", function(){});
		}

		public dispose():void {
			super.dispose();
		}
	}
}