module engine {
	import EventDispatcher = createjs.EventDispatcher;

	export class BaseWinLines extends EventDispatcher {
		public static EVENT_CREATE_LINE:string = "create_line";
		public static EVENT_All_LINES_SHOWED:string = "lines_showed";
		public static EVENT_LINE_SHOWED:string = "line_showed";
		public static SHOW_WILD_EXPANDING:string = "shoow_wild_expanding";
		public static ADD_WILD_STICKY:string = "shoow_wild_sticky";

		public winLines:Array<WinLine>;
		public leftTime:number;
		public time:number;
		public isAllLineShowed:boolean;

		constructor(winLines:Array<WinLine>, time:number) {
			super();
			this.winLines = winLines;
			this.time = time;
			this.leftTime = time;
			this.isAllLineShowed = false;
		}

		public onEnterFrame():void {
			if (--this.leftTime == 0) {
				this.onEndTime();
			}
		}

		public create():void {
		}

		public onEndTime():void {
		}

		public dispose():void {
			for (var i:number = 0; i < this.winLines.length; i++) {
				this.winLines[i].remove();
			}
			this.winLines = null;
		}

		public processRecursionRotation(isMobile:boolean):void{

		}
	}
}