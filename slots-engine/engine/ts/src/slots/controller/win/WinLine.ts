module engine {
	import Container = createjs.Container;
	import Point = createjs.Point;
	import EventDispatcher = createjs.EventDispatcher;
	import Tween = createjs.Tween;
	import Shape = createjs.Shape;

	export class WinLine extends EventDispatcher {
		private container:Container;
		private lineId:number;
		private winVOs:Array<WinAniVO>;
		private wild_expanding_lineid:number = -1;
		private wild_sticky_winsymbolview:WinSymbolView;

		constructor(container:Container, lineId:number, winVOs:Array<WinAniVO>) {
			super();
			this.container = container;
			this.lineId = lineId;
			this.winVOs = winVOs;
		}

		public create():void {
			for (var i:number = 0; i < this.winVOs.length; i++) {
				var vo:WinAniVO = this.winVOs[i];
				var winSymbolView:WinSymbolView = vo.winSymbolView;
				if (winSymbolView.parent != null) {
					winSymbolView.parent.removeChild(winSymbolView);
				}
				winSymbolView.x = vo.rect.x;
				winSymbolView.y = vo.rect.y;

				//console.log("winSymbolView", winSymbolView);
				if(winSymbolView._is_wild_expanding || winSymbolView._is_in_wild_expanding_reel) {
					this.wild_expanding_lineid = i;
					this.dispatchEvent(BaseWinLines.SHOW_WILD_EXPANDING);
				}
				if(winSymbolView._is_played_with_winlines){
					winSymbolView.play();
					this.container.addChild(winSymbolView);
				}
				if(winSymbolView._is_wild_sticky) {
					this.wild_sticky_winsymbolview = winSymbolView;
					this.dispatchEvent(BaseWinLines.ADD_WILD_STICKY);
				}
			}
		}

		public remove():void {
			for (var i:number = 0; i < this.winVOs.length; i++) {
				var vo:WinAniVO = this.winVOs[i];
				var winSymbolView:WinSymbolView = vo.winSymbolView;
				if(!winSymbolView._is_wild_expanding) {
					this.container.removeChild(winSymbolView);
				}
			}
		}

		public getPositions():Array<Point> {
			var positions:Array<Point> = new Array(this.winVOs.length);
			for (var i:number = 0; i < this.winVOs.length; i++) {
				positions[i] = this.winVOs[i].posIdx.clone();
			}
			return positions;
		}

		public getLineId():number {
			return this.lineId;
		}

		public processRecursionRotation(isMobile:boolean):void{
			for (var i:number = 0; i < this.winVOs.length; i++) {
				var vo:WinAniVO = this.winVOs[i];
				var mask:Shape = new Shape();
				mask.graphics.beginFill("0");
				if(isMobile){
					mask.graphics.drawRect(vo.rect.x, vo.rect.y, vo.rect.width*GameController.MOBILE_TO_WEB_SCALE, vo.rect.height*GameController.MOBILE_TO_WEB_SCALE);
				}else {
					mask.graphics.drawRect(vo.rect.x, vo.rect.y, vo.rect.width, vo.rect.height);
				}
				vo.winSymbolView.mask = mask;

				//vo.winSymbolView.regX = vo.winSymbolView.getBounds().width/2;
				//vo.winSymbolView.regY = vo.winSymbolView.getBounds().height/2;
				//vo.winSymbolView.x = vo.winSymbolView.x + vo.winSymbolView.regX;
				//vo.winSymbolView.y = vo.winSymbolView.y + vo.winSymbolView.regY;

				Tween.get(vo.winSymbolView).to({rotation: 90, scaleX:ReelView.SYMBOL_CASCADE_SCALE_TO, scaleY:ReelView.SYMBOL_CASCADE_SCALE_TO}, ReelMoverCascade.DELAYS.RECURSION_ROTATION_TIME);
			}
			return;
		}
	}
}