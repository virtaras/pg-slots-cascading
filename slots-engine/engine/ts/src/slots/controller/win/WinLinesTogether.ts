module engine {
	import Point = createjs.Point;

	export class WinLinesTogether extends BaseWinLines {
		constructor(winLines:Array<WinLine>, time:number) {
			super(winLines, time);
		}

		public create():void {
			var positions:Array<Point> = [];
			var winLinesId:Array<number> = [];
			for (var i:number = 0; i < this.winLines.length; i++) {
				var winLine:WinLine = this.winLines[i];
				var lineId:number = winLine.getLineId();
				if (lineId > 0) {
					winLinesId.push(lineId);
				}
				WinLinesTogether.pushUniquePos(positions, winLine.getPositions());
				winLine.create();
			}
			this.dispatchEvent(BaseWinLines.EVENT_CREATE_LINE, [positions, winLinesId]);
		}

		private static pushUniquePos(allPositions:Array<Point>, positions:Array<Point>):void {
			for (var i:number = 0; i < positions.length; i++) {
				var pos:Point = positions[i];
				var isFind:boolean = false;
				for (var j:number = 0; j < allPositions.length; j++) {
					var pos2:Point = allPositions[j];
					if (pos.x == pos2.x && pos.y == pos2.y) {
						isFind = true;
						break;
					}
				}
				if (!isFind) {
					allPositions.push(pos);
				}
			}
		}

		public onEndTime():void {
			if (!this.isAllLineShowed) {
				this.dispatchEvent(BaseWinLines.EVENT_All_LINES_SHOWED);
				this.isAllLineShowed = true;
			}
		}

		public processRecursionRotation(isMobile:boolean):void{
			for (var i:number = 0; i < this.winLines.length; i++) {
				this.winLines[i].processRecursionRotation(isMobile);
			}
		}
	}
}