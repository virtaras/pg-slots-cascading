module engine {
	import Sound = createjs.Sound;
	import SoundInstance = createjs.SoundInstance;

	export class SoundController extends BaseController {
		private common:CommonRefs;

		private isShowAllLines:boolean;
		private soundsStarted:boolean = false;

		public allSoundInstances:SoundInstance[] = [];

		constructor(manager:ControllerManager, common:CommonRefs) {
			super(manager);
			this.common = common;
		}

		public init():void {
			super.init();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.TRY_START_AUTO_PLAY_AUDIO);
			notifications.push(NotificationList.END_AUTO_PLAY);
			notifications.push(NotificationList.START_SPIN);
			notifications.push(NotificationList.START_SPIN_AUDIO);
			notifications.push(NotificationList.STOPPED_ALL_REELS);
			notifications.push(NotificationList.SHOW_WIN_LINES);
			notifications.push(NotificationList.STOPPED_REEL_ID);
			notifications.push(NotificationList.STOPPED_REEL_ID_PREPARE);
			notifications.push(NotificationList.START_PAY_TABLE_SOUND);
			notifications.push(NotificationList.END_PAY_TABLE_SOUND);
			notifications.push(NotificationList.SHOW_LINES);
			notifications.push(NotificationList.SOUND_TOGGLE);
			notifications.push(NotificationList.START_BONUS_AUDIO);
			notifications.push(NotificationList.END_BONUS);
			notifications.push(NotificationList.SHOW_HEADER);
			notifications.push(NotificationList.HIDE_CARD);
			notifications.push(NotificationList.SHOW_CARD);
			notifications.push(NotificationList.CREATE_BONUS);
			notifications.push(NotificationList.RECURSION_PROCESS_ROTATION);
			notifications.push(NotificationList.RECURSION_PROCESSED_FALLING);
			notifications.push(NotificationList.START_AYU_BONUS);
			notifications.push(NotificationList.START_JUNGLE_QUEEN_BONUS);
			notifications.push(NotificationList.PLAY_MORE_FREE_SPINS_SOUND);
			notifications.push(NotificationList.SOUNDS_LOADED);
			notifications.push(NotificationList.LAZY_LOAD_COMP);
			notifications.push(NotificationList.SERVER_INIT);
			notifications.push(NotificationList.START_NORMAL_GAME_BG_SOUND);
			notifications.push(NotificationList.SHOW_WILD_EXPANDING);
			notifications.push(NotificationList.REMOVE_WIN_LINES);
			notifications.push(NotificationList.SHOW_JACKPOT_POPUP);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.SOUND_TOGGLE:
				{
					if (!this.common.isSoundLoaded) {
						this.send(NotificationList.LOAD_SOUNDS);
					}
					this.common.isSoundOn = !this.common.isSoundOn;
					Sound.setMute(!this.common.isSoundOn);
					this.send(NotificationList.SOUND_TOGGLE_UPDATE);
					break;
				}
				case NotificationList.TRY_START_AUTO_PLAY_AUDIO:
				{
					this.startSoundjs(SoundsList.AUTO_SPIN_BG, {loop:-1});
					if(this.allSoundInstances[SoundsList.AUTO_SPIN_BG] && this.allSoundInstances[SoundsList.AUTO_SPIN_BG].src) {
						this.stopSoundjs(SoundsList.NORMAL_GAME_BG);
					}
					break;
				}
				case NotificationList.END_AUTO_PLAY:
				{
					this.stopSoundjs(SoundsList.AUTO_SPIN_BG);
					if(this.allSoundInstances[SoundsList.AUTO_SPIN_BG] && this.allSoundInstances[SoundsList.AUTO_SPIN_BG].src) {
						this.startSoundjs(SoundsList.NORMAL_GAME_BG, {loop:-1});
					}
					break;
				}
				case NotificationList.START_SPIN:
				{
					break;
				}
				case NotificationList.START_SPIN_AUDIO:
				{
					this.stopSoundjs(SoundsList.REGULAR_WIN);
					if (this.common.server.win > 0) {
						SoundController.playSoundjs(SoundsList.COLLECT);
					}
					if (!this.allSoundInstances[SoundsList.AUTO_SPIN_BG] || this.allSoundInstances[SoundsList.AUTO_SPIN_BG].playState != Sound.PLAY_SUCCEEDED) {
						this.startSoundjs(SoundsList.REGULAR_SPIN_BG);
					}
					break;
				}
				case NotificationList.STOPPED_ALL_REELS:
				{
					if (!this.allSoundInstances[SoundsList.AUTO_SPIN_BG] || this.allSoundInstances[SoundsList.AUTO_SPIN_BG].playState != Sound.PLAY_SUCCEEDED) {
						this.stopSoundjs(SoundsList.REGULAR_SPIN_BG);
					}
					break;
				}
				case NotificationList.SHOW_WIN_LINES:
				{
					this.isShowAllLines = <boolean>data;
					var winLinesVOs:Array<WinLineVO> = this.common.server.winLines;
					if (winLinesVOs.length > 0) {
						this.startSoundjs(SoundsList.REGULAR_WIN);
					}
					break;
				}
				case NotificationList.RECURSION_PROCESS_ROTATION:
				{
					SoundController.playSoundjs(SoundsList.RECURSION_ROTATION);
					break;
				}
				case NotificationList.STOPPED_REEL_ID_PREPARE:
				case NotificationList.RECURSION_PROCESSED_FALLING:
				{
					SoundController.playSoundjs(SoundsList.REEL_STOP);
					break;
				}
				case NotificationList.START_PAY_TABLE_SOUND:
				{
					this.stopAllSounds();
					this.startSoundjs(SoundsList.PAY_TABLE_BG, {loop: -1});
					break;
				}
				case NotificationList.END_PAY_TABLE_SOUND:
				{
					this.stopSoundjs(SoundsList.PAY_TABLE_BG);
					this.startSoundjs(SoundsList.NORMAL_GAME_BG, {loop: -1});
					break;
				}
				case NotificationList.SHOW_LINES:
				{
					if (!this.isShowAllLines || this.common.config.winLinesTogetherAndThenEachOne) {
						var lineSound:string = SoundsList.LINE_WIN_ENUM;
						try {
							var symbolid:number = this.common.server.wheel[data[0][0].y];
							if (SoundsList.SYMBOL_SOUNDS[SoundsList.SYMBOL_PREFIX + symbolid]===true){ //TODO: finish check if sound loaded
								lineSound = SoundsList.SYMBOL_PREFIX + symbolid;
							}
						}catch(e){
							console.log("SYMBOL_PREFIX ERROR:",e);
						}
						//SoundController.playSoundjs(lineSound);
						this.stopSoundjs(SoundsList.SYMBOL_PREFIX);
						this.createSoundjs(SoundsList.SYMBOL_PREFIX, lineSound);
						this.startSoundjs(SoundsList.SYMBOL_PREFIX);
					}
					break;
				}
				case NotificationList.START_BONUS_AUDIO:
				{
					var bonus:BonusVO = this.common.server.bonus;
					if (bonus != null) {
						switch (bonus.type){
							case BonusTypes.FREE_SPINS:
							case BonusTypes.SELECT_GAME:
							{
								this.stopAllSounds();
								this.startSoundjs(SoundsList.FREE_SPIN_BG, {loop: -1});
								break;
							}
							case BonusTypes.GAMBLE:{
								this.stopAllSounds();
								this.startSoundjs(SoundsList.GAMBLE_AMBIENT, {loop: -1});
								break;
							}
							case BonusTypes.AYU_SELECT_3:{
								this.stopAllSounds();
								this.startSoundjs(SoundsList.AYU_BONUS_CHOOSE_BG, {loop: -1});
								break;
							}
							case BonusTypes.BLADES_SELECT_5:{
								this.stopAllSounds();
								this.startSoundjs(SoundsList.BLADES_BONUS_CHOOSE_BG, {loop: -1});
								break;
							}
							case BonusTypes.JUNGLE_QUEEN_BONUS_WHEEL:{
								this.stopAllSounds();
								this.startSoundjs(SoundsList.JUNGLE_QUEEN_BONUS_WHEEL_BG, {loop: -1});
								break;
							}
						}
					}
					break;
				}
				case NotificationList.END_BONUS:
				{
					this.stopSoundjs(SoundsList.FREE_SPIN_BG);
					break;
				}
				case NotificationList.START_AYU_BONUS:
				{
					this.stopSoundjs(SoundsList.AYU_BONUS_CHOOSE_BG);
					break;
				}
				case NotificationList.START_BLADES_BONUS:
				{
					this.stopSoundjs(SoundsList.BLADES_BONUS_CHOOSE_BG);
					break;
				}
				case NotificationList.START_JUNGLE_QUEEN_BONUS:
				{
					this.stopSoundjs(SoundsList.JUNGLE_QUEEN_BONUS_WHEEL_BG);
					break;
				}
				case NotificationList.SHOW_HEADER:
				{
					this.stopSoundjs(SoundsList.GAMBLE_AMBIENT);
					break;
				}
				case NotificationList.HIDE_CARD:
				{
					SoundController.playSoundjs(SoundsList.GAMBLE_SHUFFLE);
					break;
				}
				case NotificationList.SHOW_CARD:
				{
					SoundController.playSoundjs(SoundsList.GAMBLE_TURN);
					break;
				}
				case NotificationList.PLAY_MORE_FREE_SPINS_SOUND:
				{
					SoundController.playSoundjs(SoundsList.MORE_FREE_SPINS_SOUND);
					break;
				}
				case NotificationList.START_NORMAL_GAME_BG_SOUND:
				{
					if(!this.common.server.bonus) {
						this.startSoundjs(SoundsList.NORMAL_GAME_BG, {loop: -1});
					}
					break;
				}
				case NotificationList.SHOW_WILD_EXPANDING:
				{
					this.startSoundjs(SoundsList.WILD_EXPANDING_APPEAR);
					break;
				}
				case NotificationList.REMOVE_WIN_LINES:
				{
					this.stopSoundjs(SoundsList.SYMBOL_PREFIX);
					this.stopSoundjs(SoundsList.WILD_EXPANDING_APPEAR);
					break;
				}
				case NotificationList.SERVER_INIT:
				case NotificationList.LAZY_LOAD_COMP:
				case NotificationList.SOUNDS_LOADED:
				{
					if(!this.soundsStarted && this.common.isSoundLoaded) {
						this.soundsStarted = true;
						this.send(NotificationList.START_NORMAL_GAME_BG_SOUND);
						this.send(NotificationList.START_BONUS_AUDIO);
					}
					break;
				}
				case NotificationList.SHOW_JACKPOT_POPUP:
				{
					SoundController.playSoundjs(SoundsList.JACKPOT_POPUP_APPEAR);
					break;
				}
			}
		}

		public createSoundjs(soundInstanceId:string, soundId:string):void{
			this.allSoundInstances[soundInstanceId] = (this.common.isSoundLoaded) ? Sound.createInstance(soundId) : null;
		}

		public startSoundjs(soundId:string, params:any = {}):void{
			this.allSoundInstances[soundId] = (this.allSoundInstances[soundId]) ? this.allSoundInstances[soundId] : (this.common.isSoundLoaded) ? Sound.createInstance(soundId) : null;
			if(this.allSoundInstances[soundId] && this.allSoundInstances[soundId].src && this.allSoundInstances[soundId]!=Sound.PLAY_SUCCEEDED) {
				this.allSoundInstances[soundId].play(params);
			}
		}

		public stopSoundjs(soundId:string):void{
			if(this.allSoundInstances[soundId]){
				this.allSoundInstances[soundId].stop();
			}
		}

		public static playSoundjs(soundName:string):void{
			if(GameController.isSoundLoaded){
				Sound.play(soundName);
			}
		}

		public stopAllSounds():void {
			Sound.stop();
		}

		public dispose():void {
			super.dispose();
			this.common = null;
		}
	}
}