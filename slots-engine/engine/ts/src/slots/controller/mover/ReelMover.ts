module engine {
	import EventDispatcher = createjs.EventDispatcher;
	import Ticker = createjs.Ticker;

	export class ReelMover extends EventDispatcher {
		public static SYMBOLS_IN_REEL:number = 3;
		// events
		public static EVENT_REEL_STOPPED:string = "event_reel_stopped";
		public static EVENT_REEL_PREPARE_STOPPED:string = "event_reel_prepare_stopped";
		// reel strips type
		public static TYPE_REGULAR:string = "regular";
		public static TYPE_FREE_SPIN:string = "freeSpins";
		// mover states
		private static STATE_NORMAL:string = "normal";
		private static STATE_UP:string = "up";
		private static STATE_MOVING:string = "moving";
		private static STATE_DOWN:string = "end";

		private static UP_MOVE_SYMBOLS:number = 1;
		private static DOWN_MOVE_SYMBOLS:number = 1;
		private static MOVE_SPEED:number = 2;
		private static DELAY_BETWEEN_START_REEL:number = 7;
		private static DELAY_BETWEEN_STOP_REEL:number = 0;
		private static REGULAR_STOP_SYMBOLS:number = 4;
		private static HARD_STOP_SYMBOLS:number = 4;
		private static TURBO_STOP_SYMBOLS:number = 2;
		private static TURBO_DELAY_START_REEL:number = 2;
		private static TURBO_DELAY_STOP_REEL:number = 0;

		private static EPSILON:number = 0.001;
		private static PREPARE_REEL_SOUND_TIME:number = 0.09;

		private reelStrips:Object;
		private reelView:ReelView;
		private reelId:number;
		// состояние вращения барабана (STATE_NORMAL, STATE_UP, STATE_MOVING, STATE_END)
		private state:string;
		// высота которая приходится на один видимый символ
		private dh:number;
		// позиция барабана при инициализации
		private startReelPos:number;
		// осталось до старта вращения барабана
		private delayStartReel:number;
		private delayStopReel:number;
		private isStarted:boolean;
		private isCanStop:boolean;
		private isServerResponce:boolean = false;
		public isTurbo:boolean = false;
		// тип ленты
		private moverType:string;
		// лента для текущего барабана
		private reelSequence:Array<number>;
		// длина лента для текущего барабана
		private sequenceLength:number;
		private upEasing:BezierEasing;
		private moveEasing:LinearEasing;
		private downEasing:BezierEasing;
		// текущее смещение слота в координатах
		private pos:number;
		// смещение на предыдущем шаге слота в координатах
		private posPrevState:number;
		// текущий индекс в ленте
		private currentIdx:number;
		// индекс старта вращения в ленте
		private startIdx:number;
		// индекс окончания вращения в ленте
		private targetIdx:number;
		private isCutSequence:boolean;
		private isHardStop:boolean;
		private isFirst:boolean = true;

		constructor(reelStrips:Object, reelView:ReelView, reelId:number) {
			super();
			this.reelStrips = JSON.parse(JSON.stringify(reelStrips));
			this.reelView = reelView;
			this.reelId = reelId;
			ReelMover.DELAY_BETWEEN_START_REEL = ReelsView.LAYOUT_PARAMS.DELAY_BETWEEN_START_REEL || ReelMover.DELAY_BETWEEN_START_REEL;
			ReelMover.TURBO_DELAY_START_REEL = ReelsView.LAYOUT_PARAMS.TURBO_DELAY_START_REEL || ReelMover.TURBO_DELAY_START_REEL;
			ReelMover.DELAY_BETWEEN_STOP_REEL = ReelsView.LAYOUT_PARAMS.DELAY_BETWEEN_STOP_REEL || ReelMover.DELAY_BETWEEN_STOP_REEL;
			ReelMover.TURBO_DELAY_STOP_REEL = ReelsView.LAYOUT_PARAMS.TURBO_DELAY_STOP_REEL || ReelMover.TURBO_DELAY_STOP_REEL;
			ReelMover.REGULAR_STOP_SYMBOLS = ReelsView.LAYOUT_PARAMS.regular_stop_symbols || ReelMover.REGULAR_STOP_SYMBOLS;
			ReelMover.MOVE_SPEED = ReelsView.LAYOUT_PARAMS.MOVE_SPEED || ReelMover.MOVE_SPEED;
		}

		public init(wheel:Array<number>, sequenceType:string):void {
			this.state = ReelMover.STATE_NORMAL;
			this.dh = this.reelView.getSymbol(0).getBounds().height;
			this.startReelPos = this.reelView.getY();

			this.changeSequenceType(sequenceType);
			this.resetData();
			this.setServerResponse(wheel, false);
		}

		public onEnterFrame():void {
			if (this.state == ReelMover.STATE_NORMAL) {
				return;
			}
			if (this.delayStartReel > 0 && this.state == ReelMover.STATE_UP) {
				this.delayStartReel -= 1;
				return;
			}
			if (this.isStarted) {
				this.isStarted = false;
			}
			if (this.state == ReelMover.STATE_UP) {
				this.up();
			}
			if (this.state == ReelMover.STATE_MOVING) {
				this.move();
			}
			if (this.state == ReelMover.STATE_DOWN) {
				this.down();
			}

			this.updatePos(false);
		}

		public start():void {
			this.isServerResponce = false;
			this.isCanStop = false;
			this.state = ReelMover.STATE_UP;
			this.setMovingParams();
		}

		public expressStop():void {
			this.isCutSequence = false;
			this.isHardStop = true;
		}

		public onGetSpin(wheel:Array<number>):void {
			setTimeout((wheel:Array<number>)=>{
				this.onGetSpinProcess(wheel);
			}, this.delayStopReel, wheel);
		}

		public onGetSpinProcess(wheel:Array<number>):void{
			this.isServerResponce = true;
			if (!this.isCanStop) {
				this.setServerResponse(wheel, false);
				this.updatePos(false);
				this.isCanStop = true;
			}
		}

		private up():void {
			if (this.upEasing.getTime() < 1) {
				this.pos = this.upEasing.getPosition();
			}
			else {
				this.posPrevState = this.pos;
				this.state = ReelMover.STATE_MOVING;
			}
		}

		private move():void {
			//console.log("move this.moveEasing.getTime()", this.moveEasing.getTime());
			if (this.moveEasing.getTime() >= 1) {
				if (this.isCanStop) {
					this.cutSequence();
					if (this.currentIdx == this.correctIdx(this.targetIdx - ReelMover.DOWN_MOVE_SYMBOLS)) {
						this.posPrevState = this.pos;
						this.state = ReelMover.STATE_DOWN;
						return;
					}
				}
				this.moveEasing.reset();
				this.posPrevState = this.pos;
			}
			this.pos = this.posPrevState + this.moveEasing.getPosition();
		}

		private down():void {
			if (this.downEasing.getTime() < 1) {
				this.pos = this.posPrevState + this.downEasing.getPosition();
				if(this.downEasing.getTime()<ReelMover.PREPARE_REEL_SOUND_TIME){
					this.dispatchEvent(ReelMover.EVENT_REEL_PREPARE_STOPPED, this.reelView.getReelId());
				}
			}
			else {
				this.stop();
			}
		}

		public changeSequenceType(moverType:string, bonus:any = null):void {
			if (this.moverType != moverType) {
				this.moverType = moverType;
				var reelStrips:Array<Array<number>> = (this.reelStrips["custom"] && bonus && this.reelStrips["custom"][bonus.id]) ? this.reelStrips["custom"][bonus.id].slice(0) : this.reelStrips[moverType].slice(0);
				var reelId:number = this.reelView.getReelId();

				this.reelSequence = reelStrips[reelId].slice(0).reverse();
				this.sequenceLength = this.reelSequence.length;

				this.upEasing = new BezierEasing(0, 0, 4, -3, 0);
				this.upEasing.setParameters(this.dh * ReelMover.UP_MOVE_SYMBOLS, 10);

				this.moveEasing = new LinearEasing();
				this.moveEasing.setParameters(this.dh, ReelMover.MOVE_SPEED);

				this.downEasing = new BezierEasing(0, -2, 10, -15, 8);
				this.downEasing.setParameters(this.dh * ReelMover.DOWN_MOVE_SYMBOLS, 15);

				this.resetForChangeSequence();
			}
		}

		private stop():void {
			this.isFirst = false;
			this.state = ReelMover.STATE_NORMAL;
			this.resetData();
			this.dispatchEvent(ReelMover.EVENT_REEL_STOPPED, this.reelView.getReelId());
		}

		private setServerResponse(wheel:Array<number>, is_synchro_stop:boolean = true):void {
			var symbolCount:number = ReelMover.SYMBOLS_IN_REEL;
			var targetWheels:Array<number> = new Array(symbolCount);
			for (var i:number = 0; i < symbolCount; i++) {
				targetWheels[i] = wheel[symbolCount - i + symbolCount * this.reelView.getReelId()-1];
			}
			if(is_synchro_stop){
				this.pasteSequenceReelsAtIdx(targetWheels);
			}
			var idx:number = this.getSequenceIdx(targetWheels);

			if (idx == -1) {
				var reverseReelSequence:Array<number> = this.reelSequence.slice(0).reverse();
				var reverseTargetWheels:Array<number> = targetWheels.slice(0).reverse();
				console.log("[ERROR]: reel id: " + this.reelView.getReelId() + " sequence type: " + this.moverType + " target wheels: " + reverseTargetWheels + " not found in sequence: " + reverseReelSequence.slice(0));

				this.reelSequence = this.reelSequence.concat(targetWheels); //TODO: check invalid concat
				this.sequenceLength = this.reelSequence.length;
				//var normalized:boolean = this.normalizeReelSequence(this.sequenceLength-targetWheels.length, this.sequenceLength);
				idx = this.getSequenceIdx(targetWheels);
			}
			if (this.state == ReelMover.STATE_NORMAL) {
				this.startIdx = idx;
				this.updatePos(true);
			}
			else {
				this.targetIdx = idx;
			}
			console.log("this.targetIdx",this.targetIdx);
		}

		public normalizeReelSequence(from:number=0, to:number=this.reelSequence.length):boolean{
			var normalized:boolean = false;
			if((ReelsView.LAYOUT_PARAMS).sequence_groups) {
				for(var ind:number=0; ind<this.sequenceLength; ++ind) {
					for (var i:number = 0; i < (ReelsView.LAYOUT_PARAMS).sequence_groups.length; ++i) {
						var sequence_index:number = (ReelsView.LAYOUT_PARAMS).sequence_groups[i].indexOf(this.reelSequence[ind]);
						if(sequence_index>=0){
							this.updateReelSequence((ReelsView.LAYOUT_PARAMS).sequence_groups[i], ind-sequence_index, from, to);
							normalized = true;
						}
					}
				}
			}
			return normalized;
		}

		public updateReelSequence(sequence_group:number[], ind, from:number, to:number){
			for(var i:number=0; i<sequence_group.length; ++i){
				var index:number = (i+ind)%this.reelSequence.length;
				var ifrom:number = (from)%this.reelSequence.length;
				var ito:number = (to-1)%this.reelSequence.length;
				if(index<from || index>=to){
					this.reelSequence[index] = sequence_group[i];
					if(((ifrom-index)%this.sequenceLength<sequence_group.length) && (this.reelSequence[index]==this.reelSequence[ifrom] || sequence_group.indexOf(this.reelSequence[ifrom])<0)){
						this.reelSequence[index] = 11;
					}
					if((((index-ito)%this.sequenceLength<sequence_group.length)) && (this.reelSequence[index]==this.reelSequence[ito] || sequence_group.indexOf(this.reelSequence[ito])<0)){
						this.reelSequence[index] = 11;
					}
				}
			}
		}

		private pasteSequenceReelsAtIdx(targetWheels:Array<number>):void{
			var idx:number = this.currentIdx + ReelMover.SYMBOLS_IN_REEL*2;
			for(var i:number=0; i<ReelMover.SYMBOLS_IN_REEL; ++i){
				this.reelSequence[(i+idx) % this.reelSequence.length] = targetWheels[i];
			}
			return;
		}

		private updatePos(isFist:boolean):void {
			var idx:number = this.getCurrentIdx();
			var dh:number = this.pos % this.dh - this.dh;
			this.reelView.setY(this.startReelPos + dh);

			if (this.currentIdx != idx || isFist) {
				var symbolCount:number = ReelMover.SYMBOLS_IN_REEL + 1;
				for (var i:number = 0; i < symbolCount; i++) {
					var symbolId:number = symbolCount - i - 1;
					var symbolIdx:number = this.correctIdx(idx + i);
					var frame:number = this.reelSequence[symbolIdx] - 1;
					this.reelView.setSymbolFrame(symbolId, frame);
				}
				this.currentIdx = idx;
			}
		}

		private getCurrentIdx():number {
			return Utils.float2int(this.correctIdx(this.startIdx + Math.abs(this.pos + ReelMover.EPSILON) / this.dh));
		}

		private getSequenceIdx(value:Array<number>):number {
			var i:number = 0;
			var valueLength:number = value.length;
			var j:number = 0;
			while (j < valueLength) {
				if (this.reelSequence.length == i) {
					// for set wheel
					return -1;
				}
				if (this.reelSequence[i] == value[j]) {
					j++;
					if (i == this.sequenceLength - 1) {
						i = -1;
					}
				}
				else if (j > 0) {
					i = this.correctIdx(i - j);
					j = 0;
				}
				i++;
			}
			var idx:number = i - valueLength;
			if (idx < 0) {
				idx = this.sequenceLength + idx;
			}
			return idx;
		}

		private correctIdx(value:number):number {
			if (value >= 0 && value < this.sequenceLength) {
				return value;
			}
			return (value + this.sequenceLength) % this.sequenceLength;
		}

		private cutSequence():boolean {
			if (this.isCutSequence) {
				return false;
			}
			var maxDiv:number = this.isTurbo ? ReelMover.TURBO_STOP_SYMBOLS : this.isHardStop ? ReelMover.HARD_STOP_SYMBOLS : ReelMover.REGULAR_STOP_SYMBOLS;
			this.isHardStop = false;
			var realDivIdx:number = this.correctIdx(this.targetIdx - ReelMover.DOWN_MOVE_SYMBOLS - this.currentIdx);
			if (realDivIdx <= maxDiv) {
				return;
			}
			var symbolId:number = this.reelSequence[this.correctIdx(this.currentIdx + maxDiv)];
			if (this.isGroupSymbol(symbolId)) {
				return;
			}
			var div:number = realDivIdx - maxDiv;
			while (this.isGroupSymbol(this.reelSequence[this.correctIdx(this.currentIdx + div + maxDiv)])) {
				div--;
			}
			if (div <= 0) {
				return;
			}
			this.pos += div * this.dh;
			this.currentIdx = this.getCurrentIdx();
			this.isCutSequence = true;
		}

		// TODO: need implemented
		private isGroupSymbol(symbolId:number):boolean {
			return false;
		}

		// Сброс данных после остановки барабана
		private resetData():void {
			this.startIdx = this.targetIdx;
			this.targetIdx = 0;
			this.isCutSequence = false;
			this.pos = 0;
			this.upEasing.reset();
			this.downEasing.reset();
			this.moveEasing.reset();
			this.setMovingParams();
		}

		private setMovingParams():void{
			this.delayStartReel = (this.isTurbo) ? ReelMover.TURBO_DELAY_START_REEL * this.reelId : ReelMover.DELAY_BETWEEN_START_REEL * this.reelId;
			this.delayStopReel = (this.isTurbo) ? ReelMover.TURBO_DELAY_STOP_REEL * (this.reelId+1) : ReelMover.DELAY_BETWEEN_STOP_REEL * (this.reelId+1);
		}

		// Сброс данных после изменения ленты
		private resetForChangeSequence():void {
			this.currentIdx = 0;
			this.startIdx = 0;
		}
	}
}