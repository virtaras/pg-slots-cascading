module engine {
	import EventDispatcher = createjs.EventDispatcher;
	import Ticker = createjs.Ticker;
	import Tween = createjs.Tween;

	export class ReelMoverCascade extends EventDispatcher {
		//public static MOVE_DOWN_TIME:number = 900;
		//public static MOVE_DOWN_DELAY_REELS:number = 100;
		//public static MOVE_DOWN_DELAY_SYMBOLS:number = 30;
		//public static RECURSION_ROTATION_TIME:number = 300;
		//public static RECURSION_SYMBOLS_FALLING_TIME:number = 300;

		public static DELAYS:any = {"MOVE_DOWN_TIME":900, "MOVE_DOWN_DELAY_REELS":100, "MOVE_DOWN_DELAY_SYMBOLS":30, "RECURSION_ROTATION_TIME":300, "RECURSION_SYMBOLS_FALLING_TIME":300};
		public static DELAYS_NORMAL:any = {"MOVE_DOWN_TIME":900, "MOVE_DOWN_DELAY_REELS":100, "MOVE_DOWN_DELAY_SYMBOLS":30, "RECURSION_ROTATION_TIME":300, "RECURSION_SYMBOLS_FALLING_TIME":300};
		public static DELAYS_TURBO:any = {"MOVE_DOWN_TIME":450, "MOVE_DOWN_DELAY_REELS":50, "MOVE_DOWN_DELAY_SYMBOLS":15, "RECURSION_ROTATION_TIME":150, "RECURSION_SYMBOLS_FALLING_TIME":150};

		public static STATE2_NORMAL:string = "state_normal";
		public static STATE2_DOWN_START:string = "state_down_start";
		public static STATE2_DOWN_STABLE:string = "state_down_stable";
		public static STATE2_SHOW_NEW_SYMBOLS:string = "state_show_new_symbols";
		public static STATE2_END_ANIMATION:string = "state_end_animation";
		public static STATE2_STOPPED_RECURSION:string = "state_stopped_recursion";

		public isSymbolsMovedDown:boolean = false;
		public isSymbolsAppeared:boolean = false;
		public symbolsAppeared:number = 0;

		public state2:string;
		public wheel:Array<number>;


		public static SYMBOLS_IN_REEL:number = 3;
		// events
		public static EVENT_REEL_STOPPED:string = "event_reel_stopped";
		public static EVENT_REEL_PREPARE_STOPPED:string = "event_reel_prepare_stopped";
		// reel strips type
		public static TYPE_REGULAR:string = "regular";
		public static TYPE_FREE_SPIN:string = "freeSpins";

		private reelStrips:Object;
		private reelView:ReelView;
		private reelId:number;
		private isCanStop:boolean;
		public isTurbo:boolean = false;

		constructor(reelStrips:Object, reelView:ReelView, reelId:number) {
			super();
			this.reelStrips = reelStrips;
			this.reelView = reelView;
			this.reelId = reelId;
		}

		public init(wheel:Array<number>, sequenceType:string):void {
			this.state2 = ReelMoverCascade.STATE2_NORMAL;
			this.setServerResponse(wheel);
			this.setSymbolFrames();
		}

		public onEnterFrame():void {
			//console.log("ReelMoverCascade.state2 "+this.state2+" this.isCanStop="+this.isCanStop+" this.isSymbolsMovedDown="+this.isSymbolsMovedDown);
			switch(this.state2){
				case ReelMoverCascade.STATE2_NORMAL:{
					//this.setSymbolFrames();
					return;
				}
				case ReelMoverCascade.STATE2_DOWN_START:{
					this.moveDown();
					break;
				}
				case ReelMoverCascade.STATE2_DOWN_STABLE:{
					if(this.isCanStop && this.isSymbolsMovedDown){
						this.state2 = ReelMoverCascade.STATE2_SHOW_NEW_SYMBOLS;
						this.isCanStop = false; this.isSymbolsMovedDown = false;
						this.symbolsAppeared = 0;
						var self:any = this;
						this.reelView.symbolsAppearFromTop(function(){ ++self.symbolsAppeared; });
					}
					break;
				}
				case ReelMoverCascade.STATE2_SHOW_NEW_SYMBOLS:{
					if(this.symbolsAppeared >= ReelMoverCascade.SYMBOLS_IN_REEL){
						this.stop();
					}
					break;
				}
				case ReelMoverCascade.STATE2_STOPPED_RECURSION:{

					break;
				}
			}
		}

		public moveDown():void{
			var self:any = this;
			this.isSymbolsMovedDown = false;
			this.reelView.symbolsMoveDown(this.reelId*ReelMoverCascade.DELAYS.MOVE_DOWN_DELAY_REELS, ReelMoverCascade.DELAYS.MOVE_DOWN_DELAY_SYMBOLS, function(){self.isSymbolsMovedDown = true; self.setSymbolFrames();});
			this.state2 = ReelMoverCascade.STATE2_DOWN_STABLE;
		}

		public start():void {
			this.state2 = ReelMoverCascade.STATE2_DOWN_START;
		}

		public onGetSpin(wheel:Array<number>):void {
			if (!this.isCanStop) {
				this.setServerResponse(wheel);
				this.isCanStop = true;
			}
		}

		private stop():void {
			this.state2 = ReelMoverCascade.STATE2_NORMAL;
			this.dispatchEvent(ReelMoverCascade.EVENT_REEL_STOPPED, this.reelView.getReelId());
			this.dispatchEvent(ReelMoverCascade.EVENT_REEL_PREPARE_STOPPED, this.reelView.getReelId());
		}

		public setSymbolFrames():void{
			for(var i:number=0; i<ReelMoverCascade.SYMBOLS_IN_REEL; ++i) {
				this.reelView.setSymbolFrame(i, this.wheel[ReelMoverCascade.SYMBOLS_IN_REEL*this.reelId + i]-1);
			}
			this.setRandSymbolsForLastRow();
		}

		public setRandSymbolsForLastRow():void{
			var framesCnt:number = this.reelView.getNumFrames();
			this.reelView.setSymbolFrame(ReelMoverCascade.SYMBOLS_IN_REEL, Math.random()*(framesCnt-1));
		}

		private setServerResponse(wheel:Array<number>, is_synchro_stop:boolean = true):void {
			this.wheel = wheel;
		}

		public recursionProcessFalling(common:any):void{
			this.reelView.recursionProcessFalling(common);
			//for(var i:number=0; i<ReelMoverCascade.SYMBOLS_IN_REEL; ++i) {
			//	this.reelView.recursionProcessFalling(common);
			//}
		}

		public processRecursionRotation(common:any):void{
			this.reelView.recursionProcessRotation(common);
		}
	}
}