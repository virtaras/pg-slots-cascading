/**
 * Created by Taras on 17.08.2015.
 */
module engine {
    export class ReelMoverTypes {
        public static TAPE:string = "ReelMoverTape";
        public static CASCADE:string = "ReelMoverCascade";
        public static DEFAULT:string = ReelMoverTypes.TAPE;
    }
}