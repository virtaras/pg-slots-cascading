module engine {
	import Point = createjs.Point;
	import Container = createjs.Container;

	export class ReelWildStickyController extends BaseController {
		public static wildStickySymbolsViews:Array<WinSymbolView> = [];

		private common:CommonRefs;
		private reelsView:ReelsView;
		private winView:Container;

		constructor(manager:ControllerManager, common:CommonRefs, reelsView:ReelsView, winView:Container) {
			super(manager);
			this.common = common;
			this.reelsView = reelsView;
			this.winView = winView;
		}

		public init():void {
			super.init();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.SHOW_WILD_STICKY);
			notifications.push(NotificationList.END_BONUS);
			notifications.push(NotificationList.START_SPIN);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.SHOW_WILD_STICKY:
				{
					this.showWilds();
					break;
				}
				case NotificationList.END_BONUS:
				{
					break;
				}
				case NotificationList.START_SPIN:
				{
					if(!this.common.isWildSticky && !this.common.isAllSticky && ReelWildStickyController.wildStickySymbolsViews.length) {
						this.removeWilds();
						this.clearWilds();
					}
					break;
				}
			}
		}

		public showWilds():void {
			for(var i:number=0; i<ReelWildStickyController.wildStickySymbolsViews.length; ++i){
				ReelWildStickyController.wildStickySymbolsViews[i].play();
				this.winView.addChild(ReelWildStickyController.wildStickySymbolsViews[i]);
			}
		}

		public removeWilds():void{
			for(var i:number=0; i<ReelWildStickyController.wildStickySymbolsViews.length; ++i) {
				this.winView.removeChild(ReelWildStickyController.wildStickySymbolsViews[i]);
			}
		}

		public clearWilds():void{
			ReelWildStickyController.wildStickySymbolsViews = [];
		}

		public dispose():void {
			super.dispose();
			this.reelsView.dispose();
			this.reelsView = null;
			this.winView = null;
			this.common = null;
		}
	}
}