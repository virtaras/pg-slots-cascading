module engine {
	import Point = createjs.Point;
	import Container = createjs.Container;

	export class ReelWildExpandingController extends BaseController {
		public static wildExpandingSymbolsViews:Array<WildSymbolView> = [];
		public static wildExpandingSymbolsViewsStatic:Array<WildSymbolView> = [];

		public static TIME_FADEIN:number = 90;
		public static WILD_LOOP_POSTFIX:string = "loop";
		public static WILD_LOOP_POSTFIX_STATIC:string = "loopStatic";

		private common:CommonRefs;
		private reelsView:ReelsView;
		private winView:Container;

		constructor(manager:ControllerManager, common:CommonRefs, reelsView:ReelsView, winView:Container) {
			super(manager);
			this.common = common;
			this.reelsView = reelsView;
			this.winView = winView;
			//wildExpandingSymbolsViews
			for(var i=0; i<GameController.REELS_CNT; ++i) {
				if(this.common.layouts[WinController.WIN_ANIMATION_PREFIX + this.common.config.wildSymbolIdExpanding] && this.common.layouts[WinController.WIN_ANIMATION_PREFIX + this.common.config.wildSymbolIdExpanding + ReelWildExpandingController.WILD_LOOP_POSTFIX]) {
					ReelWildExpandingController.wildExpandingSymbolsViews[i] = new WildSymbolView();
					ReelWildExpandingController.wildExpandingSymbolsViews[i].create(this.common.layouts[WinController.WIN_ANIMATION_PREFIX + this.common.config.wildSymbolIdExpanding], null, this.common.layouts[WinController.WIN_ANIMATION_PREFIX + this.common.config.wildSymbolIdExpanding + ReelWildExpandingController.WILD_LOOP_POSTFIX], this.common.isMobile);
				}
			}
			//wildExpandingSymbolsViewsStatic
			for(var i=0; i<GameController.REELS_CNT; ++i) {
				if(this.common.layouts[WinController.WIN_ANIMATION_PREFIX + this.common.config.wildSymbolIdExpandingStatic + ReelWildExpandingController.WILD_LOOP_POSTFIX_STATIC]) {
					ReelWildExpandingController.wildExpandingSymbolsViewsStatic[i] = new WildSymbolView();
					ReelWildExpandingController.wildExpandingSymbolsViewsStatic[i].create(null, null, this.common.layouts[WinController.WIN_ANIMATION_PREFIX + this.common.config.wildSymbolIdExpandingStatic + ReelWildExpandingController.WILD_LOOP_POSTFIX_STATIC], this.common.isMobile);
				}
			}
		}

		public init():void {
			super.init();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.SHOW_WILD_REEL_FADEIN);
			notifications.push(NotificationList.END_BONUS);
			notifications.push(NotificationList.SHOW_WILD_EXPANDING);
			notifications.push(NotificationList.REMOVE_WIN_LINES);
			notifications.push(NotificationList.SHOW_WILDS_EXPANDING_STATIC);
			notifications.push(NotificationList.REMOVE_WILDS_EXPANDING_STATIC);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.END_BONUS:
				{
					//END_BONUS
					break;
				}
				case NotificationList.SHOW_WILD_REEL_FADEIN:
				{
					this.showWildFadeIn(data);
					break;
				}
				case NotificationList.SHOW_WILD_EXPANDING:
				{
					this.showWild(data);
					break;
				}
				case NotificationList.REMOVE_WIN_LINES:
				{
					this.removeWilds();
					break;
				}
				case NotificationList.SHOW_WILDS_EXPANDING_STATIC:
				{
					this.showWildsExpandingStatic();
					break;
				}
				case NotificationList.REMOVE_WILDS_EXPANDING_STATIC:
				{
					if(this.common.canRemoveWildsExpandingStatic){
						this.removeWildsExpandingStatic();
					}
					break;
				}
			}
		}

		public showWild(reelId:number):void {
			if(ReelWildExpandingController.wildExpandingSymbolsViews[reelId] && !ReelWildExpandingController.wildExpandingSymbolsViews[reelId]._is_wild_started) {
				if(this.common.isMobile){
					ReelWildExpandingController.wildExpandingSymbolsViews[reelId].x = this.reelsView.getReel(reelId).getView().x*GameController.MOBILE_TO_WEB_SCALE;
					ReelWildExpandingController.wildExpandingSymbolsViews[reelId].y = (this.common.config.layout_params && this.common.config.layout_params[ReelsView.LAYOUT_NAME].reels_view_y) ? this.common.config.layout_params[ReelsView.LAYOUT_NAME].reels_view_y : 92;;
				}else {
					ReelWildExpandingController.wildExpandingSymbolsViews[reelId].x = this.reelsView.getReel(reelId).getView().x;
					ReelWildExpandingController.wildExpandingSymbolsViews[reelId].y = (this.common.config.layout_params && this.common.config.layout_params[ReelsView.LAYOUT_NAME].reels_view_y) ? this.common.config.layout_params[ReelsView.LAYOUT_NAME].reels_view_y : 68;
				}
				this.winView.addChild(ReelWildExpandingController.wildExpandingSymbolsViews[reelId]);
				ReelWildExpandingController.wildExpandingSymbolsViews[reelId].play();
			}
		}

		public showWildFadeIn(reelId:number):void {
			if(ReelWildExpandingController.wildExpandingSymbolsViews[reelId] && !ReelWildExpandingController.wildExpandingSymbolsViews[reelId]._is_wild_started) {
				if(this.common.isMobile){
					ReelWildExpandingController.wildExpandingSymbolsViews[reelId].x = this.reelsView.getReel(reelId).getView().x*GameController.MOBILE_TO_WEB_SCALE;
					ReelWildExpandingController.wildExpandingSymbolsViews[reelId].y = (this.common.config.layout_params && this.common.config.layout_params[ReelsView.LAYOUT_NAME].reels_view_y) ? this.common.config.layout_params[ReelsView.LAYOUT_NAME].reels_view_y : 92;
				}else {
					ReelWildExpandingController.wildExpandingSymbolsViews[reelId].x = this.reelsView.getReel(reelId).getView().x;
					ReelWildExpandingController.wildExpandingSymbolsViews[reelId].y = (this.common.config.layout_params && this.common.config.layout_params[ReelsView.LAYOUT_NAME].reels_view_y) ? this.common.config.layout_params[ReelsView.LAYOUT_NAME].reels_view_y : 68;
				}
				this.winView.addChild(ReelWildExpandingController.wildExpandingSymbolsViews[reelId]);
				ReelWildExpandingController.wildExpandingSymbolsViews[reelId].playFadeIn(ReelWildExpandingController.TIME_FADEIN);
			}
		}

		public removeWilds():void{
			for(var reelId=0; reelId<GameController.REELS_CNT; ++reelId) {
				this.removeWild(reelId);
			}
		}

		public removeWild(reelId:number):void{
			if (ReelWildExpandingController.wildExpandingSymbolsViews[reelId] && ReelWildExpandingController.wildExpandingSymbolsViews[reelId]._is_wild_started){
				this.winView.removeChild(ReelWildExpandingController.wildExpandingSymbolsViews[reelId]);
				ReelWildExpandingController.wildExpandingSymbolsViews[reelId]._is_wild_started = false;
			}
		}

		public showWildsExpandingStatic():void {
			for(var reelId:number = 0; reelId<GameController.REELS_CNT; ++reelId){
				if(this.common.wild_reels_expanding_static.indexOf(reelId)>-1){
					this.showWildExpandingStatic(reelId);
				}
			}
		}

		public showWildExpandingStatic(reelId:number):void{
			if(ReelWildExpandingController.wildExpandingSymbolsViewsStatic[reelId] && !ReelWildExpandingController.wildExpandingSymbolsViewsStatic[reelId]._is_wild_started) {
				ReelWildExpandingController.wildExpandingSymbolsViewsStatic[reelId].x = this.reelsView.getReel(reelId).getView().x*GameController.MOBILE_TO_WEB_SCALE;
				ReelWildExpandingController.wildExpandingSymbolsViewsStatic[reelId].y = (this.common.config.layout_params && this.common.config.layout_params[ReelsView.LAYOUT_NAME].reels_view_y) ? this.common.config.layout_params[ReelsView.LAYOUT_NAME].reels_view_y : 0;
				this.winView.addChild(ReelWildExpandingController.wildExpandingSymbolsViewsStatic[reelId]);
				ReelWildExpandingController.wildExpandingSymbolsViewsStatic[reelId].playFadeIn();
			}
		}

		public removeWildsExpandingStatic():void{
			for(var reelId=0; reelId<GameController.REELS_CNT; ++reelId) {
				this.removeWildExpandingStatic(reelId);
			}
		}

		public removeWildExpandingStatic(reelId:number):void{
			if (ReelWildExpandingController.wildExpandingSymbolsViewsStatic[reelId] && ReelWildExpandingController.wildExpandingSymbolsViewsStatic[reelId]._is_wild_started){
				this.winView.removeChild(ReelWildExpandingController.wildExpandingSymbolsViewsStatic[reelId]);
				ReelWildExpandingController.wildExpandingSymbolsViewsStatic[reelId]._is_wild_started = false;
			}
		}

		public dispose():void {
			super.dispose();
			this.reelsView.dispose();
			this.reelsView = null;
			this.common = null;
		}
	}
}