module engine {
	import Container = createjs.Container;

	export class JackpotController extends BaseController {
		public static UPDATE_TIME:number = 10000;
		private common:CommonRefs;
		private container:Container;
		private view:JackpotView;
		public update_time:number;

		constructor(manager:ControllerManager, common:CommonRefs, view:JackpotView) {
			super(manager);
			this.common = common;
			this.view = view;
			if(this.common.config.JackpotView_X) this.view.x = this.common.config.JackpotView_X;
			if(this.common.config.JackpotView_Y) this.view.y = this.common.config.JackpotView_Y;
		}

		public init():void {
			super.init();
			this.update_time = this.common.config.jackpot_update_time || JackpotController.UPDATE_TIME;
			this.send(NotificationList.UPDATE_JACKPOT_SERVER);
			this.view.initJackpot();
			//if(!this.common.isMobile) JackpotView.JACKPOT_TF_TEXT = JackpotView.JACKPOT_TF_TEXT_OLD;
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.UPDATE_JACKPOT);
			notifications.push(NotificationList.START_BONUS);
			//notifications.push(NotificationList.REMOVE_HEADER);
			notifications.push(NotificationList.CREATE_BONUS);
			notifications.push(NotificationList.END_BONUS);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.UPDATE_JACKPOT:
				{
					this.setJackpot(data);
					setTimeout(()=>{this.send(NotificationList.UPDATE_JACKPOT_SERVER);}, this.update_time);
					break;
				}
				case NotificationList.START_BONUS:
				case NotificationList.REMOVE_HEADER:
				case NotificationList.CREATE_BONUS:
				{
					if(this.common.isMobile) this.view.visible = false;
					break;
				}
				case NotificationList.END_BONUS:
				{
					this.view.visible = true;
					break;
				}
			}
		}

		public setJackpot(jackpot:any):void{
			this.view.setJackpot(parseFloat(jackpot));
		}

		public dispose():void {
			super.dispose();
			this.common = null;
			this.container = null;
		}
	}
}