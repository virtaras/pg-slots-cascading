module engine {
	import Button = layout.Button;
	import Toggle = layout.Toggle;
	import Text = createjs.Text;
	import Sound = createjs.Sound;
	import Container = createjs.Container;

	import ColorMatrix = createjs.ColorMatrix;
	import ColorMatrixFilter = createjs.ColorMatrixFilter;
	import Shape = createjs.Shape;
	import Sprite = createjs.Sprite;

	export class HudController extends BaseController {
		private currentState:IState;
		private states:Object;

		public common:CommonRefs;
		public view:HudView;
		public autoPlayCount:Array<number>;
		public autoPlayCountIdx:number;

		constructor(manager:ControllerManager, common:CommonRefs, view:HudView) {
			super(manager);
			this.common = common;
			this.view = view;
			this.autoPlayCount = this.common.config.autoPlayCount;
			this.autoPlayCountIdx = 0;
		}

		public init() {
			super.init();

			this.initHudState();
			this.initButtons();
			this.updateLineCountText();
			this.updateTotalBetText();
			this.updateBetText();
			this.updateBetPerLineText();
			this.setTextFields();
			this.initSoundToggle();
			this.initExtraChoiceFSToggle();
			this.updateView();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.KEYBOARD_CLICK);
			notifications.push(NotificationList.ON_SCREEN_CLICK);
			notifications.push(NotificationList.TRY_START_SPIN);
			notifications.push(NotificationList.START_SPIN);
			notifications.push(NotificationList.WIN_LINES_SHOWED);
			notifications.push(NotificationList.STOPPED_ALL_REELS);
			notifications.push(NotificationList.EXPRESS_STOP);
			notifications.push(NotificationList.CLOSE_PAY_TABLE);
			notifications.push(NotificationList.END_BONUS);
			notifications.push(NotificationList.TRY_START_AUTO_PLAY);
			notifications.push(NotificationList.AUTO_PLAY_COMP);
			notifications.push(NotificationList.AUTO_PLAY_CONT);
			notifications.push(NotificationList.LOAD_PAY_TABLE);
			notifications.push(NotificationList.CREATE_BONUS);
			notifications.push(NotificationList.RECURSION_PROCESSED);
			notifications.push(NotificationList.OK_BTN_ERROR_CLICKED);
			notifications.push(NotificationList.UPDATE_TURBO_MODE);
			notifications.push(NotificationList.SOUND_TOGGLE_UPDATE);
			notifications.push(NotificationList.BET_PER_LINE_COUNT_UPDATE);
			notifications.push(NotificationList.LINES_COUNT_UPDATE);
			notifications.push(NotificationList.AUTO_PLAY_COUNT_UPDATE);
			notifications.push(NotificationList.OPEN_GAME_MENU);
			notifications.push(NotificationList.SOUNDS_LOADED);
			notifications.push(NotificationList.UPDATE_HUD_COLOR);
			notifications.push(NotificationList.UPDATE_SCREEN_MODE);
			notifications.push(NotificationList.SOUND_TOGGLE);
			notifications.push(NotificationList.REELS_WAY_COUNT_UPDATE);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch(message) {
				case NotificationList.SOUNDS_LOADED:
				{
					this.updateSoundToggle();
					break;
				}
				case NotificationList.START_SPIN:
				{
					this.states[RegularSpinState.NAME].handleNotification(message, data);
					break;
				}
				case NotificationList.UPDATE_HUD_COLOR:
				{
					this.view.updateHudColor(data);
					break;
				}
				case NotificationList.UPDATE_SCREEN_MODE:
				{
					this.view.setScreenMode(this.common.screenMode);
					break;
				}
				case NotificationList.SOUND_TOGGLE:
				case NotificationList.EXTRA_CHOICE_FS_TOGGLE:
				case NotificationList.REELS_WAY_COUNT_UPDATE:
				{
					this.updateView();
					break;
				}
			}
			this.currentState.handleNotification(message, data);
		}

		private initHudState():void {
			this.states = {};
			this.states[DefaultState.NAME] = new DefaultState(this);
			this.states[PayTableState.NAME] = new PayTableState(this);
			this.states[RegularSpinState.NAME] = new RegularSpinState(this);
			this.states[AutoSpinState.NAME] = new AutoSpinState(this);
			this.states[BonusState.NAME] = new BonusState(this);

			if (this.common.server.bonus == null) {
				this.changeState(DefaultState.NAME);
			} else {
				this.changeState(BonusState.NAME);
			}
		}

		private initButtons():void {
			var buttonsNames:Array<string> = this.view.buttonsNames;
			for (var i:number = 0; i < buttonsNames.length; i++) {
				var buttonName:string = buttonsNames[i];
				var button:Button = this.view.getBtn(buttonName);
				if (button != null) {
					button.on("click", (eventObj:any)=> {
						if (eventObj.nativeEvent instanceof MouseEvent) {
							this.onBtnClick(eventObj.currentTarget.name);
						}
					});
				}
			}

			this.initButtonsInContainer(this.view.spinMenuButtonsNames, this.view.spinMenuContainer);
			this.initButtonsInContainer(this.view.spinMenuButtonsNamesPortraitLeft, this.view.spinMenuContainerPortraitLeft);
			this.initButtonsInContainer(this.view.spinMenuButtonsNamesPortraitRight, this.view.spinMenuContainerPortraitRight);
		}

		public initButtonsInContainer(buttonsNames:string[], container:Container):void{
			if(container) {
				for (var i:number = 0; i < buttonsNames.length; i++) {
					var buttonName:string = buttonsNames[i];
					var button:Button = this.view.getBtn(buttonName, container);
					if (button != null) {
						button.on("click", (eventObj:any)=> {
							if (eventObj.nativeEvent instanceof MouseEvent) {
								this.onBtnClick(eventObj.currentTarget.name);
							}
						});
					}
				}
			}
		}

		public onBtnClick(buttonName:string):void {
			switch (buttonName) {
				case HudView.BACK_TO_LOBBY_BTN:
				{
					this.send(NotificationList.BACK_TO_LOBBY);
					break;
				}
				case HudView.FULL_SCREEN_BTN:
				{
					FullScreen.toggleFullScreen();
					break;
				}
				case HudView.TURBO_OFF_BTN:
				{
					this.setTurboMode(true);
					this.updateView();
					//this.controller.send(NotificationList.TURBO_OFF_CLICKED);
					break;
				}
				case HudView.TURBO_ON_BTN:
				{
					this.setTurboMode(false);
					this.updateView();
					break;
				}
				case HudView.AUTO_OFF_BTN:
				{
					this.common.isAuto = true;
					this.send(NotificationList.TRY_START_AUTO_PLAY, 10);
					break;
				}
				case HudView.AUTO_ON_BTN:
				{
					break;
				}
				case HudView.SPIN_MENU_TURBO_MODE_OFF_BTN:
				{
					this.setTurboMode(true);
					this.updateView();
					break;
				}
				case HudView.SPIN_MENU_TURBO_MODE_ON_BTN:
				{
					this.setTurboMode(false);
					this.updateView();
					break;
				}
				case HudView.SPIN_MENU_AUTO_MODE_OFF_BTN:
				{
					this.common.isAutoMode = true;
					this.view.changeButtonState(HudView.SPIN_MENU_AUTO_MODE_OFF_BTN, false, false, [this.view.spinMenuContainer, this.view.spinMenuContainerPortraitLeft, this.view.spinMenuContainerPortraitRight]);
					this.view.changeButtonState(HudView.SPIN_MENU_AUTO_MODE_ON_BTN, true, true, [this.view.spinMenuContainer, this.view.spinMenuContainerPortraitLeft, this.view.spinMenuContainerPortraitRight]);
					this.view.changeButtonState(HudView.START_SPIN_BTN, false, false);
					this.view.changeButtonState(HudView.START_SPIN_AUTO_BTN, true, true);
					this.view.changeButtonState(HudView.STOP_SPIN_AUTO_BTN, false, false);
					this.updateAutoSpinCountText();
					break;
				}
				case HudView.SPIN_MENU_AUTO_MODE_ON_BTN:
				{
					this.common.isAutoMode = false;
					this.view.changeButtonState(HudView.SPIN_MENU_AUTO_MODE_OFF_BTN, true, true, [this.view.spinMenuContainer, this.view.spinMenuContainerPortraitLeft, this.view.spinMenuContainerPortraitRight]);
					this.view.changeButtonState(HudView.SPIN_MENU_AUTO_MODE_ON_BTN, false, false, [this.view.spinMenuContainer, this.view.spinMenuContainerPortraitLeft, this.view.spinMenuContainerPortraitRight]);
					this.view.changeButtonState(HudView.START_SPIN_BTN, true, true);
					this.view.changeButtonState(HudView.START_SPIN_AUTO_BTN, false, false);
					this.view.changeButtonState(HudView.STOP_SPIN_AUTO_BTN, false, false);
					this.updateAutoSpinCountText();
					break;
				}
			}
			HudController.playBtnSound(buttonName);
			this.currentState.onBtnClick(buttonName);
		}

		public updateView():void{
			this.updateTurboBtns();
			this.updateAutoPlayModeBtns();
			this.updateSoundToggle();
			this.updateLineCountText();
			this.updateWayCountText();
			this.updateTotalBetText();
			this.updateAutoSpinCountText();
			this.updateSpinBtns();
		}

		public updateSpinBtns():void{
			if(this.currentState == this.states[AutoSpinState.NAME]) {
				this.view.changeButtonState(HudView.START_SPIN_BTN, !this.common.config.hideStartSpinBtnOnAutoSpins, false);
				this.view.changeButtonState(HudView.START_SPIN_AUTO_BTN, false, false);
				this.view.changeButtonState(HudView.STOP_SPIN_AUTO_BTN, true, true);
				this.view.changeTextState(HudView.AUTO_SPINS_COUNT_TEXT, true);
			}
		}

		public updateTurboBtns():void{
			if(this.common.isTurbo) {
				this.view.changeButtonState(HudView.TURBO_OFF_BTN, false, false);
				this.view.changeButtonState(HudView.TURBO_ON_BTN, true, true);

				this.view.changeButtonState(HudView.SPIN_MENU_TURBO_MODE_OFF_BTN, false, false, [this.view.spinMenuContainer, this.view.spinMenuContainerPortraitLeft, this.view.spinMenuContainerPortraitRight]);
				this.view.changeButtonState(HudView.SPIN_MENU_TURBO_MODE_ON_BTN, true, true, [this.view.spinMenuContainer, this.view.spinMenuContainerPortraitLeft, this.view.spinMenuContainerPortraitRight]);
				var spinBtn:Button = this.view.getBtn(HudView.START_SPIN_BTN);
				this.view.setFieldVisible(HudView.START_SPIN_TURBO_SPRITE, true && spinBtn.visible);
			}else{
				this.view.changeButtonState(HudView.TURBO_OFF_BTN, true, true);
				this.view.changeButtonState(HudView.TURBO_ON_BTN, false, false);

				this.view.changeButtonState(HudView.SPIN_MENU_TURBO_MODE_OFF_BTN, true, true, [this.view.spinMenuContainer, this.view.spinMenuContainerPortraitLeft, this.view.spinMenuContainerPortraitRight]);
				this.view.changeButtonState(HudView.SPIN_MENU_TURBO_MODE_ON_BTN, false, false, [this.view.spinMenuContainer, this.view.spinMenuContainerPortraitLeft, this.view.spinMenuContainerPortraitRight]);
				this.view.setFieldVisible(HudView.START_SPIN_TURBO_SPRITE, false);
			}
		}

		public updateAutoPlayModeBtns():void{
			this.view.changeButtonState(HudView.SPIN_MENU_AUTO_MODE_OFF_BTN, !this.common.isAutoMode, !this.common.isAutoMode, [this.view.spinMenuContainer, this.view.spinMenuContainerPortraitLeft, this.view.spinMenuContainerPortraitRight]);
			this.view.changeButtonState(HudView.SPIN_MENU_AUTO_MODE_ON_BTN, this.common.isAutoMode, this.common.isAutoMode, [this.view.spinMenuContainer, this.view.spinMenuContainerPortraitLeft, this.view.spinMenuContainerPortraitRight]);
		}

		public updateSoundToggle(){
			this.view.updateSoundToggle(this.common.isSoundLoaded, this.common.isSoundOn);
		}

		private static playBtnSound(buttonName:string):void {
			var soundId:string = SoundsList.REGULAR_CLICK_BTN;
			switch(buttonName){
				case HudView.START_SPIN_BTN:
				{
					soundId = SoundsList.SPIN_CLICK_BTN;
				}
			}
			SoundController.playSoundjs(soundId);
		}

		public changeState(name:string, data:any = null):void {
			console.log("Change state: " + name);
			if (this.currentState != null) {
				this.currentState.end();
			}
			this.currentState = this.states[name];
			this.currentState.start(data);
		}

		public tryStartSpin():void {
			console.log("Try start spin");
			var balanceAfterSpin:number = this.common.server.getBalance() - this.common.server.getTotalBet();
			if (balanceAfterSpin >= 0) {
				if(!this.common.server.recursion.length) {
					var balance:number = this.common.server.getBalance() - this.common.server.getTotalBet();
					this.common.server.setBalance(balance);
					this.send(NotificationList.UPDATE_BALANCE_TF);
					this.send(NotificationList.TRY_START_SPIN);
				}
			}else {
				this.send(NotificationList.SHOW_ERRORS, [ErrorController.ERROR_NO_MONEY_STR]);
				//throw new Error("ERROR: no money");
			}
		}

		public updateTotalBetText():void {
			var totalBetText:Text = this.view.getText(HudView.TOTAL_BET_TEXT);
			if (totalBetText != null) {
				totalBetText.text = MoneyFormatter.format(this.common.server.getTotalBet(), true);
			}
		}

		public updateBetText():void {
			var betText:Text = this.view.getText(HudView.BET_TEXT);
			if (betText != null) {
				betText.text = MoneyFormatter.format(this.common.server.bet, true);
			}
		}

		public updateBetPerLineText():void {
			var betPerLineText:Text = this.view.getText(HudView.BET_PER_LINE_TEXT);
			if (betPerLineText != null) {
				betPerLineText.text = MoneyFormatter.format(this.common.server.getBetPerLine(), false);
			}
		}

		public updateLineCountText():void {
			this.view.setLineCountText(this.common.server.lineCount.toString());
			if(this.common.isMobile)this.updateTotalBetText();
		}

		public updateWayCountText():void{
			if(this.common.server.waysCount){
				this.view.setWayCountText(this.common.server.waysCount.toString());
			}
		}

		public updateAutoPlayBtn():void {
			this.view.changeButtonState(HudView.DEC_SPIN_COUNT_BTN, true, this.autoPlayCountIdx > 0);
			this.view.changeButtonState(HudView.INC_SPIN_COUNT_BTN, true, this.autoPlayCountIdx < this.autoPlayCount.length - 1);
		}

		public updateAutoSpinCountText():void {
			if(this.currentState != this.states[AutoSpinState.NAME]) {
				var autoSpinCountText:Text = this.view.getText(HudView.AUTO_SPINS_COUNT_TEXT);
				if (autoSpinCountText != null && this.autoPlayCountIdx > -1 && this.autoPlayCountIdx < this.autoPlayCount.length) {
					var autoPlayCount:number = this.autoPlayCount[this.autoPlayCountIdx];
					autoSpinCountText.text = autoPlayCount.toString();
				}
				try {
					var self:HudController = this;
					Utils.findChildrenByName(this.view, HudView.AUTO_SPINS_COUNT_TEXT_SPINMENU).forEach(function (text:Text) {
						text.text = self.common.autoSpinsCnt.toString();
					});
					autoSpinCountText = this.view.getText(HudView.AUTO_SPINS_COUNT_TEXT);
					autoSpinCountText.text = this.common.autoSpinsCnt.toString();
					autoSpinCountText.visible = this.common.isAutoMode;
				} catch (e) {
					console.log("error", e);
				}
			}
		}

		public setTextFields():void{
			if(this.common.config.hud_shadows){
				this.view.addFieldsShadows();
			}
		}

		public setTurboMode(mode:boolean):void{
			this.common.isTurbo = mode;
			this.send(NotificationList.UPDATE_TURBO_MODE, null);
		}

		public setAutoMode(mode:boolean):void{
			//set auto mode
			this.common.isAuto = mode;
			if(this.common.isAuto){
				this.changeState(AutoSpinState.NAME, 5);
			}else{
				//this.changeState(DefaultState.NAME);
			}
		}

		public initSoundToggle():void {
			var soundToggle:Toggle = <Toggle>this.view.getChildByName(HudView.HUD_SOUND_TOGGLE);
			if (soundToggle != null) {
				soundToggle.setIsOn(this.common.isSoundLoaded);
				soundToggle.on("click", (eventObj:any)=> {
					if (eventObj.nativeEvent instanceof MouseEvent) {
						this.send(NotificationList.SOUND_TOGGLE);
						SoundController.playSoundjs(SoundsList.REGULAR_CLICK_BTN);
					}
				});
			}
		}

		public initExtraChoiceFSToggle():void{
			var extraChoiceFSToggle:Toggle = <Toggle>this.view.getChildByName(HudView.EXTRA_CHOICE_FS_TOGGLE);
			if (extraChoiceFSToggle != null) {
				extraChoiceFSToggle.setIsOn(this.common.server.isExtraChoise);
				extraChoiceFSToggle.on("click", (eventObj:any)=> {
					if (eventObj.nativeEvent instanceof MouseEvent) {
						this.common.server.isExtraChoise = !this.common.server.isExtraChoise;
						this.send(NotificationList.EXTRA_CHOICE_FS_TOGGLE);
						SoundController.playSoundjs(SoundsList.REGULAR_CLICK_BTN);
					}
				});
			}
		}

		public dispose():void {
			super.dispose();
			this.view.dispose();
			this.view = null;
			this.common = null;
		}
	}
}