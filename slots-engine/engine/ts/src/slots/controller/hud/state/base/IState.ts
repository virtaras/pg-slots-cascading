module engine {
	export interface IState {
		start(data:any):void;
		handleNotification(message:string, data:any):void;
		onBtnClick(buttonName:string):void;
		end():void;
	}
}