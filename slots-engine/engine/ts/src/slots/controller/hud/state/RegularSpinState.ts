module engine {
	export class RegularSpinState implements IState {
		public static NAME:string = "RegularSpinState";

		private controller:HudController;
		private common:CommonRefs;
		private view:HudView;

		private restartWinLines:any = null;

		constructor(controller:HudController) {
			this.controller = controller;
			this.common = controller.common;
			this.view = controller.view;
		}

		public start(data:any):void {
			this.view.changeButtonState(HudView.SETTINGS_BTN, true, false);
			this.view.changeButtonState(HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, false);
			this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, true, false);
			this.view.changeButtonState(HudView.STOP_AUTO_PLAY_BTN, false, false);
			this.view.changeButtonState(HudView.GAMBLE_BTN, true, false);
			this.view.changeButtonState(HudView.INC_SPIN_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.DEC_SPIN_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.INC_LINE_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.DEC_LINE_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.NEXT_BET_BTN, true, false);
			this.view.changeButtonState(HudView.PREV_BET_BTN, true, false);
			this.view.changeButtonState(HudView.MAX_BET_BTN, true, false);
			this.view.changeButtonState(HudView.MAX_LINES_BTN, true, false);
			this.view.changeButtonState(HudView.START_BONUS_BTN, false, false);
			this.view.changeButtonState(HudView.PAY_TABLE_BTN, true, false);
			this.view.changeButtonState(HudView.MENU_OPEN_BTN, true, false);
			this.view.changeButtonState(HudView.BET_BTN, true, false);
			this.view.changeButtonState(HudView.LINE_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.MULTIPLY_BTN, true, false);

			if(this.view.getBtn(HudView.STOP_SPIN_BTN)){
				this.view.changeButtonState(HudView.START_SPIN_BTN, false, false);
			}else{
				this.view.changeButtonState(HudView.START_SPIN_BTN, true, false);
			}
			this.view.changeButtonState(HudView.STOP_SPIN_BTN, true, true);
			if(this.common.config.hideSpinBtnOnSpin){
				this.view.changeButtonState(HudView.START_SPIN_BTN, false, false);
				this.view.changeButtonState(HudView.STOP_SPIN_BTN, false, false);
				this.view.changeButtonState(HudView.SPIN_MENU_OPEN_BTN, false, false);
				this.view.changeButtonState(HudView.SPIN_MENU_OPEN_PORTRAIT_BTN, false, false);
				this.view.changeButtonState(HudView.SPIN_MENU_CLOSE_PORTRAIT_BTN, false, false);
				Utils.setElementProps(this.view.startSpinTurboSprite, {"visible":false});
			}

			this.controller.send(NotificationList.CLOSE_SETTINGS_MENU);
			this.controller.send(NotificationList.COLLECT_WIN_TF);
			this.controller.send(NotificationList.REMOVE_WIN_LINES);
			this.controller.send(NotificationList.START_SPIN);
			this.controller.send(NotificationList.START_SPIN_AUDIO);
			this.controller.send(NotificationList.SERVER_SEND_SPIN);
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.KEYBOARD_CLICK:
				{
					if (data == Keyboard.SPACE) {
						this.stopSpin();
					}
					break;
				}
				case NotificationList.ON_SCREEN_CLICK:
				{
					this.stopSpin();
					break;
				}
				case NotificationList.STOPPED_ALL_REELS:
				{
					var bonus:BonusVO = this.common.server.bonus;
					if (bonus == null || bonus.type == BonusTypes.GAMBLE) {
						this.controller.changeState(DefaultState.NAME);
						this.controller.send(NotificationList.SHOW_WIN_LINES, this.common.config.winLinesTogether);
						if(this.common.config.winLinesTogetherAndThenEachOne) {
							this.restartWinLines = setTimeout(()=>{
								this.controller.send(NotificationList.REMOVE_WIN_LINES);
								this.controller.send(NotificationList.SHOW_WIN_LINES_REPEAT, false);
							}, this.common.config.winLinesTogetherAndThenEachOneDelay);
						}
					}
					else {
						this.controller.send(NotificationList.SHOW_WIN_LINES, true);
					}
					if(!this.common.server.recursion.length){
						this.controller.send(NotificationList.UPDATE_BALANCE_TF, this.controller.common.server.getBalance());
					}
					this.controller.send(NotificationList.SHOW_WIN_TF);
					break;
				}
				case NotificationList.RECURSION_PROCESSED:
				{
					var bonus:BonusVO = this.common.server.bonus;
					if (bonus != null && bonus.type != BonusTypes.GAMBLE) {
						this.controller.changeState(BonusState.NAME);
					}
					break;
				}
				case NotificationList.EXPRESS_STOP:
				{
					this.view.changeButtonState(HudView.STOP_SPIN_BTN, false, false);
					this.view.changeButtonState(HudView.START_SPIN_BTN, true, false);
					break;
				}
				case NotificationList.START_SPIN:
				{
					clearTimeout(this.restartWinLines);
					break;
				}
			}
		}

		public onBtnClick(buttonName:string):void {
			switch (buttonName) {
				case HudView.STOP_SPIN_BTN:
				{
					this.stopSpin();
					break;
				}
			}
		}

		private stopSpin():void {
			this.controller.send(NotificationList.EXPRESS_STOP);
		}

		public end():void {
		}
	}
}