module engine {
	export class PayTableState implements IState {
		public static NAME:string = "PayTableState";

		private controller:HudController;
		private common:CommonRefs;
		private view:HudView;

		constructor(controller:HudController) {
			this.controller = controller;
			this.common = controller.common;
			this.view = controller.view;
		}

		public start(data:any):void {
			// update buttons
			this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, !this.common.isMobile, false);
			this.view.changeButtonState(HudView.GAMBLE_BTN, !this.common.isMobile, false);
			this.view.changeButtonState(HudView.START_SPIN_BTN, !this.common.isMobile, false);
			this.view.changeButtonState(HudView.PAY_TABLE_BTN, !this.common.isMobile, false);
			this.view.changeButtonState(HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, false);
			this.view.changeButtonState(HudView.SETTINGS_BTN, false, false);
			this.view.changeButtonState(HudView.STOP_AUTO_PLAY_BTN, false, false);
			this.view.changeButtonState(HudView.NEXT_BET_BTN, true, false);
			this.view.changeButtonState(HudView.INC_LINE_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.INC_SPIN_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.PREV_BET_BTN, true, false);
			this.view.changeButtonState(HudView.DEC_LINE_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.DEC_SPIN_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.MAX_LINES_BTN, true, false);
			this.view.changeButtonState(HudView.MAX_BET_BTN, true, false);
			this.view.changeButtonState(HudView.START_BONUS_BTN, false, false);
			this.view.changeButtonState(HudView.STOP_SPIN_BTN, false, false);
			this.view.changeButtonState(HudView.BET_BTN, true, false);
			this.view.changeButtonState(HudView.LINE_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.MULTIPLY_BTN, true, false);

			if(this.common.isMobile) {
				this.view.changeButtonState(HudView.AUTO_OFF_BTN, false, false);
				this.view.changeButtonState(HudView.AUTO_ON_BTN, false, false);
				this.view.changeButtonState(HudView.TURBO_OFF_BTN, false, false);
				this.view.changeButtonState(HudView.TURBO_ON_BTN, false, false);
			}

			if (this.common.config.showAllTimeSpinCount && this.view.getText(HudView.AUTO_SPINS_COUNT_TEXT)) {
				this.view.getText(HudView.AUTO_SPINS_COUNT_TEXT).visible = false;
			}
			this.controller.send(NotificationList.CLOSE_SETTINGS_MENU);
			this.controller.send(NotificationList.OPEN_PAY_TABLE);
			this.controller.send(NotificationList.START_PAY_TABLE_SOUND);

			if (this.common.config.fullHudDisableOnPaytableState) {
				this.view.changeButtonState(HudView.START_SPIN_BTN, false, false);
				this.view.changeButtonState(HudView.START_SPIN_AUTO_BTN, false, false);
				this.view.changeButtonState(HudView.STOP_SPIN_AUTO_BTN, false, false);
				this.view.changeButtonState(HudView.SPIN_MENU_OPEN_BTN, false, false);
				this.view.changeButtonState(HudView.SPIN_MENU_OPEN_PORTRAIT_BTN, false, false);
				this.view.changeButtonState(HudView.SPIN_MENU_CLOSE_PORTRAIT_BTN, false, false);
				Utils.setElementProps(this.view.startSpinTurboSprite, {"visible":false});
				this.view.changeButtonState(HudView.BACK_TO_LOBBY_BTN, true, false);
				this.view.changeButtonState(HudView.MENU_OPEN_BTN, true, false);
				//this.view.changeButtonState(HudView.HUD_SOUND_TOGGLE, true, false);
				//this.view.changeButtonState(HudView.HUD_SOUND_LOADING_SPRITE, false, false);
			}
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.CLOSE_PAY_TABLE:
				{
					this.controller.changeState(DefaultState.NAME);
					break;
				}
			}
		}

		public onBtnClick(buttonName:string):void {
		}

		public end():void {
			if (this.common.config.showAllTimeSpinCount && this.view.getText(HudView.AUTO_SPINS_COUNT_TEXT)) {
				this.view.getText(HudView.AUTO_SPINS_COUNT_TEXT).visible = true;
			}

			this.view.changeButtonState(HudView.AUTO_OFF_BTN, true, true);
			this.view.changeButtonState(HudView.AUTO_ON_BTN, true, false);
			if(this.common.isMobile) {
				this.view.changeButtonState(HudView.TURBO_OFF_BTN, !this.common.isTurbo, !this.common.isTurbo);
				this.view.changeButtonState(HudView.TURBO_ON_BTN, this.common.isTurbo, this.common.isTurbo);
			}
		}
	}
}