module engine {
	import Text = createjs.Text;
	import Ticker = createjs.Ticker;

	export class StatsController extends BaseController {
		private static SHOW_TIME:number = 0.5;
		private static HIDE_TIME:number = 0.3;

		public common:CommonRefs;
		public view:HudView;
		public winTf:AnimationTextField;
		public balanceTf:AnimationTextField;

		constructor(manager:ControllerManager, common:CommonRefs, view:HudView) {
			super(manager);
			this.common = common;
			this.view = view;
			StatsController.SHOW_TIME = Utils.float2int(StatsController.SHOW_TIME * Ticker.getFPS());
			StatsController.HIDE_TIME = Utils.float2int(StatsController.HIDE_TIME * Ticker.getFPS());
		}

		public init():void {
			super.init();

			this.winTf = new AnimationTextField(this.view.getText(HudView.WIN_TEXT));
			this.winTf.setValue(0);

			this.balanceTf = new AnimationTextField(this.view.getText(HudView.BALANCE_TEXT));
			this.balanceTf.setValue(this.common.server.getBalance());
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.SHOW_WIN_TF);
			notifications.push(NotificationList.COLLECT_WIN_TF);
			notifications.push(NotificationList.UPDATE_BALANCE_TF);
			notifications.push(NotificationList.START_SPIN);
			return notifications;
		}

		public onEnterFrame():void {
			this.winTf.update();
			this.balanceTf.update();
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.UPDATE_BALANCE_TF:
				{
					this.updateBalanceText(data);
					break;
				}
				case NotificationList.SHOW_WIN_TF:
				{
					this.updateWinText();
					break;
				}
				case NotificationList.COLLECT_WIN_TF:
				{
					this.updateWinText(0, StatsController.HIDE_TIME);
					break;
				}
				case NotificationList.START_SPIN:
				{
					if(this.common.server.bonus && this.common.server.bonus.type != BonusTypes.RE_SPINS) {
						this.updateWinText(0, 0);
					}
					break;
				}
			}
		}

		public updateBalanceText(value = null, time:number = StatsController.SHOW_TIME):void {
			value = value || this.common.server.getBalance();
			this.balanceTf.setValue(value, time);
		}

		public updateWinText(value:number = this.common.server.win, time:number = StatsController.SHOW_TIME):void {
			this.winTf.setValue(value, time);
		}
	}
}