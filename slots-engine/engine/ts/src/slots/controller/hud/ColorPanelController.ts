module engine {
	import Container = createjs.Container;
	import Shape = createjs.Shape;
	import Button = layout.Button;
	import Tween = createjs.Tween;

	export class ColorPanelController extends BaseController {
		private static COLOR_MASK:string = "colorMask";
		//private static ITEM_PREFIX:string = "coloritem";
		//private static ITEM_POSTFIX:string = "Btn";

		private static OPEN_TIME:number = 400;
		private static CLOSE_TIME:number = 300;

		private common:CommonRefs;
		private view:Container;
		private isOpen:boolean = true;
		private startY:number;
		private endY:number;

		private itemsBtn:Array<Button> = [];
		private itemsBtnParams:Array<any> = [];
		private maskContainer:Container;

		constructor(manager:ControllerManager, common:CommonRefs, view:Container) {
			super(manager);
			this.common = common;
			this.view = view;
		}

		public init():void {
			super.init();
			this.initMask();
			this.initViewClick();
			this.initItems();
			this.close(false);
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.TOGGLE_COLOR_MENU);
			notifications.push(NotificationList.CLOSE_COLOR_MENU);
			notifications.push(NotificationList.TRY_START_SPIN);
			notifications.push(NotificationList.OPEN_PAY_TABLE);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.TOGGLE_COLOR_MENU:
				{
					this.toggle();
					break;
				}
				case NotificationList.CLOSE_COLOR_MENU:
				case NotificationList.TRY_START_SPIN:
				case NotificationList.OPEN_PAY_TABLE:
				{
					this.close(!!data);
					break;
				}
			}
		}

		private initViewClick():void{
			this.view.on("click", (e)=>{e.preventDefault();});
		}

		private initMask():void {
			this.maskContainer = <Container>this.view.getChildByName(ColorPanelController.COLOR_MASK);
			this.startY = this.view.y + this.maskContainer.y;
			this.endY = this.startY + this.maskContainer.getBounds().height;
			var mask:Shape = new Shape();
			mask.graphics.beginFill("0").drawRect(this.maskContainer.x+this.view.x, this.startY, this.maskContainer.getBounds().width, this.maskContainer.getBounds().height);
			this.view.mask = mask;
			this.view.removeChild(this.maskContainer);
		}

		private toggle():void {
			if (this.isOpen) {
				this.close(true);
			}
			else {
				this.open(true);
			}
		}

		private initItems():void {
			for (var i:number = 0; i < this.common.config.hudColorParams.length; ++i) {
				var buttonId:string = this.common.config.hudColorParams[i]["id"];
				this.itemsBtn[buttonId] = <Button>this.view.getChildByName(buttonId);
				this.itemsBtnParams[buttonId] = this.common.config.hudColorParams[i];
				this.initClickHanlders();
			}
			//if(this.common.config.hudColorParams.length){
			//	this.setColor(this.common.config.hudColorParams[0]["id"]);
			//}
		}

		public initClickHanlders():void{
			for(var buttonId in this.itemsBtn) {
				this.itemsBtn[buttonId].on("click", (eventObj:any)=> {
					if (eventObj.nativeEvent instanceof MouseEvent) {
						this.setColor(eventObj.target.name);
					}
				});
			}
		}

		public removeClickHandlers():void{
			for(var buttonId in this.itemsBtn) {
				this.itemsBtn[buttonId].removeAllEventListeners("click");
			}
		}

		public setColor(buttonId:string){
			for (var key in this.itemsBtn) {
				this.itemsBtn[key].setEnable(true);
				if(key == buttonId){
					this.itemsBtn[key].setEnable(false);
				}
			}
			this.send(NotificationList.UPDATE_HUD_COLOR, this.itemsBtnParams[buttonId]);
			this.send(NotificationList.CLOSE_COLOR_MENU, true);
		}

		private close(animation:boolean = false):void {
			/*this.removeClickHandlers();*/
			Tween.get(this.view).to({y: this.endY}, (animation ? ColorPanelController.CLOSE_TIME : 0)).call(()=>{this.view.visible=false;});
			this.isOpen = false;
			//this.setButtonsEnable(false);
		}

		private open(animation:boolean):void {
			this.view.visible=true;
			Tween.get(this.view).to({y: this.startY}, (animation ? ColorPanelController.OPEN_TIME : 0), createjs.Ease.getBackOut(1.1));
			this.isOpen = true;
			//this.setButtonsEnable(true);
		}

		public setButtonsEnable(enable:boolean){
			for (var key in this.itemsBtn) {
				this.itemsBtn[key].setEnable(enable);
			}
		}

		public dispose():void {
			super.dispose();
		}
	}
}