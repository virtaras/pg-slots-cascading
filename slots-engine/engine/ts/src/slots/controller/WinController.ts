module engine {
	import Container = createjs.Container;
	import Sprite = createjs.Sprite;
	import Point = createjs.Point;
	import Ticker = createjs.Ticker;
	import Rectangle = createjs.Rectangle;
	import LayoutCreator = layout.LayoutCreator;
	import Tween = createjs.Tween;

	export class WinController extends BaseController {
		public static WIN_ANIMATION_PREFIX:string = "WinAnimation_";
		public static WIN_ANIMATION_HIGHLIGHT_PREFIX:string = "SymbolHighlight";

		private common:CommonRefs;
		private container:Container;
		private winLines:BaseWinLines;
		private symbolsToHide:Array<Array<Point>>;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
			super(manager);
			this.common = common;
			this.container = container;
		}

		public init():void {
			super.init();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.SHOW_WIN_LINES);
			notifications.push(NotificationList.SHOW_WIN_LINES_REPEAT);
			notifications.push(NotificationList.REMOVE_WIN_LINES);
			notifications.push(NotificationList.OPEN_PAY_TABLE);
			notifications.push(NotificationList.CLOSE_PAY_TABLE);
			notifications.push(NotificationList.WIN_LINES_SHOWED);
			notifications.push(NotificationList.END_BONUS);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.SHOW_WIN_LINES:
				case NotificationList.SHOW_WIN_LINES_REPEAT:
				{
					this.setWildReels();
					this.create(data);
					break;
				}
				case NotificationList.REMOVE_WIN_LINES:
				{
					this.send(NotificationList.SHOW_ALL_SYMBOLS);
					this.send(NotificationList.REMOVE_WILDS_EXPANDING_STATIC);
					this.remove();
					break;
				}
				case NotificationList.OPEN_PAY_TABLE:
				{
					this.container.visible = false;
					break;
				}
				case NotificationList.CLOSE_PAY_TABLE:
				{
					this.container.visible = true;
					break;
				}
				case NotificationList.WIN_LINES_SHOWED:
				{
					this.processRecursion();
					break;
				}
			}
		}

		public processRecursion():void{
			var self:WinController = this;
			if(this.common.server.recursion.length){
				this.send(NotificationList.RECURSION_PROCESS_ROTATION);
				this.winLines.processRecursionRotation(this.common.isMobile);
				Tween.get(this).wait(ReelMoverCascade.DELAYS.RECURSION_ROTATION_TIME).call(function(){self.afterRecursionRotation()});
			}else{
				var delay_time:number = this.common.server.recursion_len ? ReelMoverCascade.DELAYS.RECURSION_SYMBOLS_FALLING_TIME : 0;
				Tween.get(this).wait(delay_time).call(function(){self.afterProcessRecursion();});
			}
		}

		public afterRecursionRotation():void{
			this.send(NotificationList.REMOVE_WIN_LINES);
			this.send(NotificationList.HIDE_LINES);
			this.send(NotificationList.RECURSION_PROCESS_FALLING);
			if(this.common.server.recursion[0]) {
				this.common.server.winLines = this.common.server.recursion[0]["winLines"].slice(0);
				this.common.server.arrLinesIds = this.common.server.recursion[0]["arrLinesIds"].slice(0);
				this.common.server.wheel = this.common.server.recursion[0]["wheels"].slice(0);
			}
			var self:any = this;
			Tween.get(this).wait(ReelMoverCascade.DELAYS.RECURSION_SYMBOLS_FALLING_TIME).call(function(){self.afterRecursionFalling();});
		}

		public afterRecursionFalling():void{
			this.common.server.win += this.common.server.bet * this.common.server.recursion[0]['win'];
			this.send(NotificationList.SHOW_WIN_TF);
			this.common.server.recursion.shift();
			this.send(NotificationList.SHOW_WIN_LINES, true);
			this.send(NotificationList.RECURSION_PROCESSED_FALLING, true);
		}

		public afterProcessRecursion():void{
			if(!this.common.server.bonus){
				this.send(NotificationList.UPDATE_BALANCE_TF);
			}
			this.send(NotificationList.RECURSION_PROCESSED);
		}

		public onEnterFrame():void {
			if (this.winLines != null) {
				this.winLines.onEnterFrame();
			}
		}

		public dispose():void {
			super.dispose();
			this.remove();
			this.common = null;
			this.container = null;
		}

		private create(isTogether:Boolean):void {
			var winLinesVOs:Array<WinLineVO> = this.common.server.winLines;
			if (winLinesVOs.length == 0) {
				this.send(NotificationList.WIN_LINES_SHOWED);
				return;
			}
			if (isTogether) {
				this.winLines = new WinLinesTogether(this.createWinLines(), this.common.config.winAnimationTime);
			}
			else {
				this.winLines = new WinLines(this.createWinLines(), this.common.config.winAnimationTime);
			}
			this.winLines.on(BaseWinLines.EVENT_CREATE_LINE, (eventObj:any)=> {
				this.send(NotificationList.SHOW_ALL_SYMBOLS);
				if(this.common.config.hide_symbols_on_animation) {
					var hydeSymbols:Array<Point> = eventObj.target[0];
					this.send(NotificationList.HIDE_SYMBOLS, hydeSymbols);
				}
				this.send(NotificationList.HIDE_LINES);
				this.send(NotificationList.SHOW_LINES, eventObj.target);
			});
			this.winLines.on(BaseWinLines.EVENT_All_LINES_SHOWED, ()=> {
				this.send(NotificationList.WIN_LINES_SHOWED);
			});
			this.winLines.on(BaseWinLines.EVENT_LINE_SHOWED, (eventObj:any)=> {
				//this.send(NotificationList.SHOW_ALL_SYMBOLS);
				//var index:number = eventObj.currentTarget.index;
				//var hydeSymbols:Array<Point> = this.symbolsToHide[index];
				//this.send(NotificationList.HIDE_SYMBOLS, hydeSymbols);
			});
			for(var i=0; i<this.winLines.winLines.length; ++i) {
				this.winLines.winLines[i].on(BaseWinLines.SHOW_WILD_EXPANDING, (data:Event)=> {
					this.send(NotificationList.SHOW_WILD_EXPANDING, data.currentTarget.wild_expanding_lineid);
				});
				this.winLines.winLines[i].on(BaseWinLines.ADD_WILD_STICKY, (data:Event)=> {
					this.addWildStickyToCommon(data.currentTarget.wild_sticky_winsymbolview);
					this.send(NotificationList.SHOW_WILD_STICKY);
				});
			}
			this.winLines.create();
		}

		private createWinLines():Array<WinLine> {
			//console.log("this.common.server.wheel", this.common.server.wheel);
			console.log("this.common.server.winLines", this.common.server.winLines);
			var wheel:Array<number> = this.common.server.wheel;
			var symbolsRect:Array<Array<Rectangle>> = this.common.symbolsRect;
			var winLinesVOs:Array<WinLineVO> = this.common.server.winLines;
			var symbolCount:number = ReelMover.SYMBOLS_IN_REEL;
			var allWinAniVOs:Array<WinAniVO> = [];
			var winLines:Array<WinLine> = [];
			this.symbolsToHide = [];

			for (var i:number = 0; i < winLinesVOs.length; i++) {
				var maskedSymbols:Array<Point> = [];
				var winLinesVO:WinLineVO = winLinesVOs[i];
				var winPos:Array<number> = winLinesVO.winPos;
				var winAniVOs:Array<WinAniVO> = [];
				for (var reelId:number = 0; reelId < winPos.length; reelId++) {
					var symbolIdx:number = winPos[reelId] - 1;
					if (symbolIdx >= 0) {
						var rect:Rectangle = symbolsRect[reelId][symbolIdx];
						var posIdx:Point = new Point(reelId, symbolIdx);
						var winAniVO:WinAniVO = WinController.findWinVO(allWinAniVOs, posIdx);
						var winSymbolView:WinSymbolView;
						if (winAniVO == null) {
							winSymbolView = this.createWinSymbol(wheel[symbolIdx + symbolCount * reelId], reelId);
						}
						else {
							winSymbolView = winAniVO.winSymbolView;
						}
						winAniVO = new WinAniVO();
						winAniVO.winSymbolView = winSymbolView;
						winAniVO.rect = rect;
						winAniVO.posIdx = posIdx;

						var hideSymbol:Point = posIdx;
						allWinAniVOs.push(winAniVO);
						winAniVOs.push(winAniVO);
						maskedSymbols.push(hideSymbol);
					}
				}
				var winLine:WinLine = new WinLine(this.container, winLinesVO.lineId, winAniVOs);
				winLines.push(winLine);
			}
			this.symbolsToHide.push(maskedSymbols);
			return winLines;
		}

		private createWinSymbol(symbolId:number, reelId:number):WinSymbolView {
			var regularAni:LayoutCreator = this.common.layouts[WinController.WIN_ANIMATION_PREFIX + symbolId];
			var highlightAni:LayoutCreator = this.common.layouts[WinController.WIN_ANIMATION_HIGHLIGHT_PREFIX] || null;
			if(this.common.config.hide_highlight) highlightAni = null;
			var winSymbolView:WinSymbolView = new WinSymbolView();
			winSymbolView.create(regularAni, highlightAni, this.common);
			if(this.common.config.wildSymbolIdExpanding && symbolId == this.common.config.wildSymbolIdExpanding && this.common.layouts[WinController.WIN_ANIMATION_PREFIX + this.common.config.wildSymbolIdExpanding]){
				winSymbolView._is_wild_expanding = true;
			}
			if(this.common.wild_reels_expanding[reelId]){
				winSymbolView._is_in_wild_expanding_reel = true;
			}
			if(this.common.wild_reels_expanding_static.indexOf(reelId)>-1){
				winSymbolView._is_in_wild_expanding_reel_static = true;
			}
			if(symbolId == this.common.config.wildSymbolIdSticky || (symbolId == this.common.config.wildSymbolId && this.common.isWildSticky) || this.common.isAllSticky){
				winSymbolView._is_wild_sticky = true;
			}
			if(winSymbolView._is_wild_expanding || winSymbolView._is_in_wild_expanding_reel || winSymbolView._is_in_wild_expanding_reel_static || winSymbolView._is_wild_sticky){
				winSymbolView._is_played_with_winlines = false;
			}
			return winSymbolView;
		}

		private static findWinVO(winVOs:Array<WinAniVO>, posIdx:Point):WinAniVO {
			for (var i:number = 0; i < winVOs.length; i++) {
				var vo:WinAniVO = winVOs[i];
				if (vo.posIdx.x == posIdx.x && vo.posIdx.y == posIdx.y) {
					return vo;
				}
			}
			return null;
		}

		private remove():void {
			if (this.winLines != null) {
				this.winLines.dispose();
				this.winLines = null;
			}
		}

		public setWildReels():void{
			this.common.wild_reels_expanding = [];
			for(var i:number=0; i<ReelMover.SYMBOLS_IN_REEL; ++i){
				for(var j:number=0; j<GameController.REELS_CNT; ++j){
					if(this.common.server.wheel[i+j*ReelMover.SYMBOLS_IN_REEL]==this.common.config.wildSymbolIdExpanding && this.common.layouts[WinController.WIN_ANIMATION_PREFIX + this.common.config.wildSymbolIdExpanding]){
						this.common.wild_reels_expanding[j] = 1;
					}
				}
			}
		}

		public addWildStickyToCommon(sticky_winsymbolview:WinSymbolView):void{
			for(var i:number=0; i<ReelWildStickyController.wildStickySymbolsViews.length; ++i){
				if(sticky_winsymbolview.x==ReelWildStickyController.wildStickySymbolsViews[i].x && sticky_winsymbolview.y==ReelWildStickyController.wildStickySymbolsViews[i].y){
					return;
				}
			}
			ReelWildStickyController.wildStickySymbolsViews.push(sticky_winsymbolview);
		}
	}
}