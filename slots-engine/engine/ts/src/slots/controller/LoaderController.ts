module engine {
	import LoadQueue = createjs.LoadQueue;
	import Sound = createjs.Sound;
	import WebAudioPlugin = createjs.WebAudioPlugin;
	import HTMLAudioPlugin = createjs.HTMLAudioPlugin;
	import LayoutCreator = layout.LayoutCreator;
	import Layout = layout.Layout;
	import Button = layout.Button;

	export class LoaderController extends BaseController {
		private static SOUND_PERCENTAGE:number = 0.4;
		private static ASSETS_PERCENTAGE:number = 1 - LoaderController.SOUND_PERCENTAGE;
		public static MIN_PROGRESS_STEP:number = 0.005;

		private common:CommonRefs;
		private view:LoaderView;
		private isGameStarted:boolean;
        private breakPoint:number;
		private firstUp:number;
		private maxLeyers:number;
		private tempLayout:Object[];
		private progress:number = 0;
		private assetsProgress:number = 0;
		private assetsCount:number = 0;
		private currentAssetsCount:number = 0;
		private assetsCountCalculated:boolean = false;
		private assetsFilesLoaded:number = 0;

		constructor(manager:ControllerManager, common:CommonRefs, view:LoaderView) {
			super(manager);
			this.common = common;
			this.view = view;
			this.isGameStarted = false;
		}

		public init():void {
			super.init();
			this.common.isSoundLoaded = false;
			this.view.on(LayoutCreator.EVENT_LOADED, ()=> {
				this.send(NotificationList.HIDE_PRELOADER);
				this.send(NotificationList.LOADER_LOADED);
				this.initLoader();
			});
			this.firstUp = 0;
			this.tempLayout = [];
			this.view.load(this.common.dirName + GameConst.MAIN_RES_FOLDER + this.common.gameName + "/" + GameConst.LAYOUT_FOLDER + LoaderView.LAYOUT_NAME + FileConst.LAYOUT_EXTENSION, true);
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.SERVER_INIT);
			notifications.push(NotificationList.LOAD_SOUNDS);
            notifications.push(NotificationList.LAZY_LOAD);
            notifications.push(NotificationList.LAZY_LOAD_COMP);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.SERVER_INIT:
				{
					break;
				}
				case NotificationList.LOAD_SOUNDS:
				{
					this.loadSounds();
					break;
				}
			}
		}

		private initLoader():void {
			if(!this.view.getChildByName(this.view.buttonsNames[0])){
				this.common.soundPreload = false;
			}
			if(this.common.soundPreload !== null){
				this.common.isSoundOn = this.common.soundPreload;
				this.startLoadingGame();
			}else {
				this.initBtns();
			}
		}

		public initBtns():void{
			for (var i:number = 0; i < this.view.buttonsNames.length; i++) {
				var buttonName:string = this.view.buttonsNames[i];
				var button:Button = <Button>this.view.getChildByName(buttonName);
				button.on("click", (eventObj:any)=> {
					if (eventObj.nativeEvent instanceof MouseEvent) {
						this.onButtonClick(<Button>eventObj.currentTarget);
					}
				});
			}
		}

		private onButtonClick(button:Button):void {
			switch (button.name) {
				case LoaderView.BUTTON_YES:
				{
					this.common.isSoundOn = true;
					break;
				}
				case LoaderView.BUTTON_NO:
				{
					this.common.isSoundOn = false;
					break;
				}
			}
			this.startLoadingGame();
		}

		public startLoadingGame():void{
			if (this.common.isMobile) {
				this.fullScreen();
			}
			this.loadConfig();
			this.view.hideButtons();
			this.view.showProgressBar();
		}

		public fullScreen():void{
			if(!this.common.getParams['preventFullScreen']){
				FullScreen.fullScreen();
			}
		}

		private loadSounds():void {
			var sounds:any = this.common.config.sounds;
			//this.removeInvalidSounds(sounds);
			var totalFiles:number = sounds.length;
			var leftLoad:number = totalFiles;
			var self:LoaderController = this;
			if (leftLoad > 0) {
				Sound.alternateExtensions = ["mp3","mp4"];
				Sound.registerPlugins([WebAudioPlugin, HTMLAudioPlugin]);
				Sound.on("fileload", (event:any)=> {
					console.log("loaded sound id = " + event.id + " url = " + event.src);
					self.parseSoundFileloaded(event.id.toString());
					leftLoad--;
					if (leftLoad == 0) {
						this.onSoundsLoaded();
					}
				});
				Sound.registerManifest(sounds, this.common.dirName + GameConst.MAIN_RES_FOLDER + this.common.gameName + "/" + GameConst.SOUNDS_FOLDER);
			}
			else {
				this.onSoundsLoaded();
			}
		}

		public parseSoundFileloaded(soundId:string){
			if(soundId.indexOf(SoundsList.SYMBOL_PREFIX)===0){
				SoundsList.SYMBOL_SOUNDS[soundId] = true;
			}
		}

		public removeInvalidSounds(sounds:any){
			//for(var i:number=0; i<sounds.length; ++i){
			//	if(!LoaderController.urlExists(this.common.dirName + GameConst.MAIN_RES_FOLDER + this.common.gameName + "/" + GameConst.SOUNDS_FOLDER + sounds[i].src)){
			//		sounds.splice(i,1);
			//	}
			//}
		}

		private onSoundsLoaded():void {
			GameController.isSoundLoaded = this.common.isSoundLoaded = true;
			this.send(NotificationList.SOUNDS_LOADED);
			this.onLoaded();
		}

		private loadConfig():void {
			var configUrl:string = this.common.dirName + GameConst.MAIN_RES_FOLDER + this.common.gameName + "/" + GameConst.GAME_CONFIG_NAME + FileConst.JSON_EXTENSION + "?" + this.common.key;
			var assetVO:AssetVO = new AssetVO(GameConst.GAME_CONFIG_NAME, configUrl, LoadQueue.JSON);
            createjs.LoadQueue.loadTimeout = 1000;
			var configLoader:LoadQueue = new LoadQueue(false);
			configLoader.on("complete", () => {
				this.common.config = configLoader.getResult(GameConst.GAME_CONFIG_NAME);
				GameConst.COMMON_LAYOUTS_FOLDER = this.common.config.common_layouts_folder ? this.common.config.common_layouts_folder : GameConst.COMMON_LAYOUTS_FOLDER;
				LoaderController.MIN_PROGRESS_STEP = this.common.config.min_progress_step || LoaderController.MIN_PROGRESS_STEP;
				if(this.common.config.load_files_cnt){
					this.assetsCount = this.common.config.load_files_cnt;
					this.assetsCountCalculated = true;
				}
				this.send(NotificationList.CONFIG_LOADED);
				this.loadFonts();
			});
            configLoader.setMaxConnections(10);
            configLoader.loadFile(assetVO);
		}

		private loadFonts():void {
			var fonts:Array<any> = this.common.config.fonts;
			if (fonts != null && fonts.length > 0) {
				var style:HTMLElement = document.createElement('style');
				var manifest:Array<any> = [];

				for (var i:number = 0; i < fonts.length; i++) {
					var fontName:string = fonts[i].name;
					var baseFontUrl = (this.common.config["common_fonts"] && this.common.config["common_fonts"].indexOf(fontName)>=0) ? this.getCommonUrlDownl() : this.getUrlDownl();
					var fontURL:string = baseFontUrl + GameConst.FONTS_FOLDER + fonts[i].fileName + FileConst.WOFF_EXTENSION;
					style.appendChild(document.createTextNode("@font-face {font-family: '" + fontName + "'; src: url('" + fontURL + "') format('woff');}"));
					document.head.appendChild(style);
					manifest.push({"id": fontName, "src": fontURL});
				}

				var configLoader:LoadQueue = new LoadQueue(false);
				configLoader.on("complete", () => {
					this.getForFirst();
				});
				configLoader.on("fileload", () => {
					this.updateLoader();
				});
				configLoader.loadManifest(manifest);
			}
			else {
				this.getForFirst();
			}
		}

		private getUrlDownl():string {
			return this.common.dirName + GameConst.MAIN_RES_FOLDER + this.common.gameName + "/";
		}
		private getCommonUrlDownl():string {
			return GameConst.COMMON_LAYOUTS_FOLDER + "game" + ((this.common.isMobile) ? "_mobile" : "") + "/";
		}

		private loadLayouts(from:number, to:number, onComplete:Function = null, eachLoaded:Function = null, eachAssetsCountLoaded:Function = null, assetsCountLoaded:Function = null):void {
			if(to > this.maxLeyers)return;
			var layoutsName:Array<string> = this.common.config.layouts;
			var layouts:Object = this.tempLayout;
			for (var i:number = from; i < to; i++) {
				var layoutName:string = layoutsName[i];
				var LayoutClass:any = engine[layoutName];
				layouts[layoutName] = LayoutClass ? new LayoutClass() : new LayoutCreator();
			}
			var needLoad:number = to - from;
			var leftLoad:number = needLoad;
			var leftLoadAssetsCount:number = needLoad;
			var baseLayoutUrl:string = this.getUrlDownl();
			for (var i:number = from; i < to; i++) {
				var layoutName:string = layoutsName[i];
				var layoutObj:Layout = layouts[layoutName];

				layoutObj.on(LayoutCreator.EVENT_LOADED, (eventObj:any)=> {
					eventObj.currentTarget['isLoaded'] = true;
					eachLoaded != null && eachLoaded();
					leftLoad -= 1;
					if (leftLoad == 0) {
						onComplete != null && onComplete();
					}
				});
				layoutObj.on(LayoutCreator.EVENT_ON_ASSETFILE_PROGRESS, (eventObj:any)=> {
					++this.assetsFilesLoaded;
					this.updateLoader();
				});
				baseLayoutUrl = (this.common.config["common_layouts"] && this.common.config["common_layouts"].indexOf(layoutName)>=0) ? this.getCommonUrlDownl() : this.getUrlDownl();
				try{
					layoutName = this.common.config.layout_params[layoutName].use_layout || layoutName;
				}catch(e){}
				var url:string = baseLayoutUrl + GameConst.LAYOUT_FOLDER + layoutName + FileConst.LAYOUT_EXTENSION + "?" + this.common.key;
				try {
					layoutObj.load(url);
				}catch (e){
					console.log("LOADING ERROR", e);
				}
				layoutObj['url'] = url;
			}
            this.common.layouts = layouts;
		}

		private initGame():void {
			this.send(NotificationList.RES_LOADED);
			if (this.view != null) {
				this.view.dispose();
				this.view = null;
			}
			this.isGameStarted = true;
			this.loadLayouts(this.firstUp, this.common.config.layouts.length,
				()=>{
					this.common.isLayoutsLoaded = true;
					this.onLoaded();
				},
				()=>{}
			);
		}

		private getForFirst():void {
			if(this.common.isSoundOn){
				this.send(NotificationList.LOAD_SOUNDS);
			}
			this.breakPoint = this.common.config.priorityLevel;
			this.maxLeyers = this.common.config.layouts.length;
			console.log("getForFirst this.breakPoint="+this.breakPoint+" this.maxLeyers="+this.maxLeyers);
			this.loadLayouts(this.firstUp, this.breakPoint,
				()=>{
					this.firstUp = this.breakPoint;
					this.initGame();
				},
				()=>{++this.firstUp; this.updateLoader();},
				()=>{this.assetsCount += this.currentAssetsCount; this.updateLoader();},
				()=>{console.log("this.assetsCount",this.assetsCount);this.assetsCountCalculated = true;}
			);
		}

        private updateLoader(minProgressStep:number = LoaderController.MIN_PROGRESS_STEP):void {
			if(this.view) {
				var leftLoad = this.breakPoint - this.firstUp;
				var progress:number = ((this.breakPoint - leftLoad) / this.breakPoint) || 0;
				if(this.assetsCountCalculated){
					progress = (this.assetsFilesLoaded+1) / this.assetsCount;
				}
				this.progress = Math.min(Math.max(progress, this.progress+minProgressStep), 1);
				this.view.setProgress(this.progress);
			}
        }

		private onLoaded():void {
			if (this.common.isLayoutsLoaded && ((this.common.isSoundOn && this.common.isSoundLoaded) || !this.common.isSoundOn)) {
				this.send(NotificationList.LAZY_LOAD_COMP);
				this.dispose();
			}
		}

		public dispose():void {
			if (this.common.isSoundLoaded) {
				super.dispose();
				//this.common = null;
			}

		}

		public static urlExists(url:string):boolean{
			var http = new XMLHttpRequest();
			http.open('HEAD', url, false);
			http.send();
			return (http.status != 404);
		}
	}
}