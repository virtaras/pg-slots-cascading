module engine {

	export class ServerController extends BaseController {
		private demoAuthorizationUrl:string;
		private authorizationUrl:string;
		private authorizationRoom:string;
		private closeSessionUrl:string;
		private getBalanceUrl:string;
		private getBetsUrl:string;
		private spinUrl:string;
		private jackpotUrl:string;
		private bonusUrl:string;
		private setWheelUrl:string;

		private common:CommonRefs;
		private xhrTimeout:Object;
		private xhr:AjaxRequest;
		private jackpotData:any = null;

		constructor(manager:ControllerManager, common:CommonRefs) {
			super(manager);
			this.common = common;
		}

		public init():void {
			super.init();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.RES_LOADED);
			notifications.push(NotificationList.SERVER_SEND_SPIN);
			notifications.push(NotificationList.SERVER_SEND_BONUS);
			notifications.push(NotificationList.SERVER_SET_WHEEL);
			notifications.push(NotificationList.SERVER_DISCONNECT);
			notifications.push(NotificationList.UPDATE_JACKPOT_SERVER);
			notifications.push(NotificationList.SERVER_GET_BALANCE_REQUEST);
			notifications.push(NotificationList.RECURSION_PROCESSED);
			notifications.push(NotificationList.SHOW_WIN_LINES);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.RES_LOADED:
				{
					var gameName:string = this.common.config.serverGameName;
					var baseServerUrl:string = this.common.getParams['serverUrl'] ? this.common.getParams['serverUrl'] : this.common.config.serverUrl;

					this.demoAuthorizationUrl = (baseServerUrl + GameConst.DEMO_AUTHORIZATION_URL).toLowerCase();
					this.authorizationUrl = (baseServerUrl + GameConst.AUTHORIZATION_URL).toLowerCase();
					this.authorizationRoom = (baseServerUrl + GameConst.AUTHORIZATION_ROOM);
					this.getBalanceUrl = (baseServerUrl + GameConst.GET_BALANCE_URL).toLowerCase();
					this.closeSessionUrl = (baseServerUrl + GameConst.CLOSE_SESSION_URL).toLowerCase();
					this.getBetsUrl = (baseServerUrl + GameConst.GET_BETS_URL.replace("%s", gameName)).toLowerCase();
					this.spinUrl = (baseServerUrl + GameConst.SPIN_URL.replace("%s", gameName)).toLowerCase();
					this.jackpotUrl = (baseServerUrl + GameConst.JACKPOT_URL.replace("%s", gameName)).toLowerCase();
					this.bonusUrl = (baseServerUrl + GameConst.BONUS_URL.replace("%s", gameName)).toLowerCase();
					this.setWheelUrl = (baseServerUrl + GameConst.SET_WHEEL_URL.replace("%s", gameName)).toLowerCase();

					this.common.server = new ServerData();
					if (this.common.key == null) {
						this.startTimer();
						if (this.common.isDemo) {
							this.sendDemoLoginRequest(()=> {
								this.onAuthorized();
							});
						}
						else {
							this.sendLoginRequest(this.common.login, this.common.password, this.common.room, ()=> {
								this.onAuthorized();
							});
						}
					}
					else {
						this.onAuthorized();
					}
					break;
				}
				case NotificationList.SERVER_SEND_SPIN:
				{
					//this.send(NotificationList.SHOW_ALL_SYMBOLS);
					this.startTimer();
					this.sendSpinRequest(()=> {
						this.send(NotificationList.SERVER_GOT_SPIN);
					});
					break;
				}
				case NotificationList.UPDATE_JACKPOT_SERVER:
				{
					this.sendJackpotRequest(null);
					break;
				}
				case NotificationList.SERVER_SEND_BONUS:
				{
					//this.send(NotificationList.SHOW_ALL_SYMBOLS);
					this.startTimer();
					console.log(data);
					this.sendBonusRequest(data, ()=> {
						this.send(NotificationList.SERVER_GOT_BONUS);
					});
					break;
				}
				case NotificationList.SERVER_SET_WHEEL:
				{
					this.sendSetWheel(data);
					break;
				}
				case NotificationList.SERVER_DISCONNECT:
				{
					this.sendDisconnectRequest();
					break;
				}
				case NotificationList.SERVER_GET_BALANCE_REQUEST:
				{
					this.sendGetBalanceRequest(()=>{this.send(NotificationList.UPDATE_BALANCE_TF);});
					break;
				}
				case NotificationList.SHOW_WIN_LINES:
				{
					if(this.jackpotData && this.jackpotData.textContent && !this.common.server.bonus){
						this.send(NotificationList.SHOW_JACKPOT_POPUP, this.jackpotData.textContent);
					}
					break;
				}
			}
		}

		private startTimer():void{
			var xhr:any = this.xhr;
			var that:any = this;
			this.xhrTimeout = setTimeout(function(){
				if(xhr)xhr.abort();
				that.send(NotificationList.SERVER_NOT_RESPONSE);
			},15000);
		}

		private sendSpinRequest(onComplete:Function):void {
			var options:string = "";
			if(this.common.config.enableABPM){
				options += (this.common.server.isExtraChoise) ? "ABPM_"+GameController.REELS_CNT : "ABPM_0";
			}
			if(this.common.config.enableRB){
				options += ","+"RB_"+this.common.reelsWayCnt;
			}
			var serverDelay:any = this.xhrTimeout;
			var request:AjaxRequest = new AjaxRequest(AjaxRequest.XML, this.spinUrl, true, {
				"multiply": this.common.server.multiply,
				"key": this.common.key,
				"bet": this.common.server.bet,
				"lineBet": this.common.server.lineCount,
				"options": options
			});
			this.xhr = request;
			request.get((responseData)=> {
				//this.send(NotificationList.SHOW_JACKPOT_POPUP, 55555);
				clearTimeout(serverDelay);
				this.parseErrors(responseData);
				var spinData:any = XMLUtils.getElement(responseData, "spin");
				this.parseSpinData(spinData);
				this.parseBonusData(spinData);
				this.parseSpinJackpotData(XMLUtils.getElement(spinData, "jackpot"));
				onComplete != null && onComplete();
			});
		}

		public parseSpinJackpotData(jackpotData:any){
			this.jackpotData = jackpotData;
			this.common.server.setBalance(this.common.server.getBalance() - (parseFloat(this.jackpotData.textContent) || 0));
		}

		private sendBonusRequest(params:any, onComplete:Function):void {
			params.key = this.common.key;
			params.bonus = this.common.server.bonus.key;
			var request:AjaxRequest = new AjaxRequest(AjaxRequest.XML, this.bonusUrl, true, params);
			this.xhr = request;
			var serverDelay:any = this.xhrTimeout;
			request.get((responseData)=> {
				clearTimeout(serverDelay);
				this.parseErrors(responseData);
				var error:any = XMLUtils.getElement(responseData, "error");
				if (error == null) {
					if(this.common.server.bonus != null){
						this.common.server.bonus.data = XMLUtils.getElement(responseData, "bonus");
						onComplete != null && onComplete();
					}
				}
				else {
					throw new Error("ERROR: " + error.textContent);
				}
			});
		}

		private sendSetWheel(params:Array<Array<number>>):void {
			var XML:any = document.createElement("data");
			for (var i:number = 0; i < params.length; i++) {
				var column:any = document.createElement("i");
				column.setAttribute("id", i.toString());
				column.innerHTML = params[i].join(",");
				XML.appendChild(column);
			}
			var request:AjaxRequest = new AjaxRequest(AjaxRequest.XML, this.setWheelUrl, true, {"key": this.common.key, "wheel": XML.innerHTML});
			request.get();
		}

		private sendDemoLoginRequest(onComplete:Function):void {
			var request:AjaxRequest = new AjaxRequest(AjaxRequest.JSON, this.demoAuthorizationUrl, true, null);
			this.xhr = request;
			var serverDelay:any = this.xhrTimeout;
			request.get((responseData)=> {
				//this.parseErrors(responseData);
				clearTimeout(serverDelay);
				this.common.key = responseData.key;
				console.log("session key: " + this.common.key);
				onComplete();
			});
		}

		private sendLoginRequest(login:string, password:string, room:string, onComplete:Function):void {
			var passwordMD5:string = CryptoJS.MD5(password).toString();
			var roomUrl = (typeof(room)!="undefined" && room != null);
			console.log(roomUrl,"roomUrl");
			console.log(room,"room");
			if(roomUrl){
				 if(room != '-1'){
					 passwordMD5 = '*' + CryptoJS.SHA1(CryptoJS.SHA1(password));
				 }
				 var request:AjaxRequest = new AjaxRequest(AjaxRequest.XML, this.authorizationRoom, true, {"login": login, "password": passwordMD5, "room":room});
			}
			else {
				var request:AjaxRequest = new AjaxRequest(AjaxRequest.XML, this.authorizationUrl, true, {"login": login, "password": passwordMD5});
			}
			this.xhr = request;
			var serverDelay:any = this.xhrTimeout;
			request.get((responseData)=> {
				this.parseErrors(responseData);
				clearTimeout(serverDelay);
				var result:any = responseData.documentElement.textContent;
				switch (result) {
					case "1001":
					{
						alert("User already authorized! Please relogin!");
						throw new Error("ERROR: User already authorized!");
					}
				}
				if(roomUrl){
					result = XMLUtils.getSessionRoom(responseData, "key");
				}
				//if(login.length > 10) result = login;
				if (result == null || result.length == 0) {
					alert("Login or password is incorrect! Please relogin!");
					throw new Error("ERROR: Login or password is incorrect");
				}
				this.common.key = result;
				console.log("session key: " + this.common.key);
				onComplete();
			});
		}

		private sendGetBalanceRequest(onComplete:Function):void {
			var request:AjaxRequest = new AjaxRequest(AjaxRequest.XML, this.getBalanceUrl, true, {key: this.common.key});
			this.xhr = request;
			request.get((responseData)=> {
				this.parseErrors(responseData);
				this.common.server.setBalance(parseFloat(responseData.documentElement.textContent));
				onComplete != null && onComplete();
			});
		}

		private sendGetBetsRequest(onComplete:Function):void {
			var request:AjaxRequest = new AjaxRequest(AjaxRequest.XML, this.getBetsUrl, true, {key: this.common.key});
			request.get((responseData)=> {
				this.parseErrors(responseData);
				var bets:any = XMLUtils.getElement(responseData, "bets");
				this.common.server.jackpot = XMLUtils.getAttributeFloat(bets, "jackpot");
				this.common.server.bet = XMLUtils.getAttributeFloat(bets, "bet");
				this.common.server.multiply = XMLUtils.getAttributeInt(bets, "mp");
				this.common.server.lineCount = XMLUtils.getAttributeInt(bets, "lines");
				if(this.common.config.reelsWays){
					this.common.server.waysCount = this.getWaysCountForLinesCnt(this.common.server.lineCount);
				}
				console.log("this.common.server.waysCount", this.common.server.waysCount);
				this.common.server.maxLineCount = XMLUtils.getAttributeInt(bets, "maxlines");
				this.common.server.wheel = Utils.toIntArray(XMLUtils.getAttribute(bets, "wheel").split(","));
				this.common.server.availableMultiples = Utils.toIntArray(XMLUtils.getAttribute(bets, "multiply").split(","));
				this.common.server.availableBets = [];
				var betsData:any = XMLUtils.getElements(bets, "item");
				for (var i:number = 0; i < betsData.length; i++) {
					var bet:any = betsData[i].childNodes[0].data;
					this.common.server.availableBets.push(parseFloat(bet));
				}
				this.parseBonusData(bets);
				//if(this.common.server.bonus.type == BonusTypes.RE_SPINS && this.common.restore){
				//	this.parseSpinData(XMLUtils.);
				//}
				onComplete != null && onComplete();
			});
			this.xhr = request;
		}

		private sendDisconnectRequest():void {
			//if (this.common.key != null) {
			//	var request:AjaxRequest = new AjaxRequest(AjaxRequest.XML, this.closeSessionUrl, false, {key: this.common.key});
			//	request.get();
			//}
		}

		private onAuthorized():void {
			this.sendGetBalanceRequest(() => {
				this.sendGetBetsRequest(()=> {
					this.send(NotificationList.SERVER_INIT);
				});
			});
		}

		private findBonusConfig(bonusId:number):void {
			var bonuses:Array<any> = <Array<any>>this.common.config.bonuses;
			for (var i:number = 0; i < bonuses.length; i++) {
				var bonus:any = bonuses[i];
				if (bonus.id == bonusId) {
					return bonus;
				}
			}
			console.log("ERROR: No bonus in config id = " + bonusId);
		}

		private parseSpinData(data:any, setBalance:boolean = true):void {
			//console.log("parseSpinData", data);
			this.common.wild_reels_expanding_static = [];
			var wheelsData:any = XMLUtils.getElement(data, "wheels").childNodes;
			for (var i:number = 0; i < wheelsData.length; i++) {
				var wheelData:Array<number> = Utils.toIntArray(wheelsData[i].childNodes[0].data.split(","));
				for (var j:number = 0; j < wheelData.length; j++) {
					this.common.server.wheel[i * wheelData.length + j] = wheelData[j];
				}
			}

			if(setBalance){
				this.common.server.setBalance(parseFloat(XMLUtils.getElement(data, "balance").textContent));
			}
			this.common.server.serverWin = this.common.server.win = XMLUtils.getAttributeFloat(data, "firstwin") || parseFloat(XMLUtils.getElement(data, "win").textContent);
			this.common.server.winLines = [];
			this.common.server.arrLinesIds = [];
			this.parseWinLinesData(data, "winposition");
			this.parseWinLinesData(data, "bonusposition");
			this.parseScattersWinlines();

			//recursion
			this.parseRecursion(data);
		}

		private parseWinLinesData(data:any, nodeName:string):void{
			try {
				var winLinesData:any = XMLUtils.getElement(data, nodeName).childNodes;
				for (var i:number = 0; i < winLinesData.length; i++) {
					var winLine:any = winLinesData[i];
					var winLineVO:WinLineVO = new WinLineVO();
					winLineVO.lineId = XMLUtils.getAttributeInt(winLine, "line");
					winLineVO.win = XMLUtils.getAttributeFloat(winLine, "win");
					winLineVO.winPos = Utils.toIntArray(winLine.childNodes[0].data.split(","));
					this.common.server.winLines.push(winLineVO);
					this.common.server.arrLinesIds.push(winLineVO.lineId);
					if(!this.common.server.serverWin){
						this.common.server.win += winLineVO.win;
					}
				}
			}catch(e){
				console.log("parseWinLinesData "+nodeName, e);
			}
			return;
		}

		private parseScattersWinlines():void{
			if(ReelsView.LAYOUT_PARAMS.SCATTERS_IDS) {
				var scatter_cnt:number = 0;
				var scatterWinlines:Array<WinLineVO> = [];
				var scatterWinlinesId:number = 0;
				for (var i:number = 0; i < this.common.server.wheel.length; ++i) {
					if (ReelsView.LAYOUT_PARAMS.SCATTERS_IDS.indexOf(this.common.server.wheel[i]) >= 0) {
						scatterWinlinesId = i % ReelMover.SYMBOLS_IN_REEL;
						scatterWinlines[scatterWinlinesId] = (scatterWinlines[scatterWinlinesId]) ? scatterWinlines[scatterWinlinesId] : new WinLineVO();
						scatterWinlines[scatterWinlinesId].lineId = 0;
						scatterWinlines[scatterWinlinesId].win = 0;
						scatterWinlines[scatterWinlinesId].winPos = (scatterWinlines[scatterWinlinesId].winPos) ? scatterWinlines[scatterWinlinesId].winPos : [0, 0, 0, 0, 0];
						scatterWinlines[scatterWinlinesId].winPos[parseInt(i/ReelMover.SYMBOLS_IN_REEL)] = scatterWinlinesId + 1;
						++scatter_cnt;
					}
				}
				if(scatter_cnt>=ReelMover.SYMBOLS_IN_REEL) {
					for (var key in scatterWinlines) {
						this.common.server.winLines.push(scatterWinlines[key]);
					}
				}
				console.log("parseScattersWinlines this.common.server.winLines", this.common.server.winLines);
			}
		}

		private parseBonusData(data:any):void {
			this.common.server.bonus = null;
			var bonusData:any = XMLUtils.getElement(data, "bonus");
			if (bonusData != null && bonusData.attributes.length > 0) {
				var step:number, restoreBonus:any;
				step = Math.max(XMLUtils.getAttributeInt(bonusData, "step") - 1, 0);
				var bonusId:number = XMLUtils.getAttributeInt(bonusData, "id");
				var bonusConfig:any = this.findBonusConfig(bonusId);
				if (bonusConfig == null) {
					return;
				}

				var keyData:string = XMLUtils.getAttribute(bonusData, "key");
				var last:any = XMLUtils.getElement(bonusData, "last");
				if (last != null) {
					restoreBonus = XMLUtils.getElement(last, "bonus");
					step = XMLUtils.getAttributeInt(bonusData, "step") - 1;
					this.common.restore = true;
					//recursion
					if(XMLUtils.getElement(restoreBonus, "data")){
						var bonusSpinData:any =XMLUtils.getElement(XMLUtils.getElement(restoreBonus, "data"), "spin");
						this.parseRecursion(bonusSpinData);
						if(XMLUtils.getElement(bonusSpinData, "wheels")){
							this.parseSpinData(bonusSpinData, false);
						}
						this.parseScattersWinlines();
					}
				}else{
					//recursion
					//this.parseRecursion(XMLUtils.getElement(bonusData, "spin"));
				}
				var bonusVO:BonusVO = new BonusVO();
				bonusVO.id = bonusId;
				bonusVO.key = keyData != null ? keyData : bonusData.textContent;
				bonusVO.step = step;
				bonusVO.type = bonusConfig.type;
				bonusVO.className = bonusConfig.className;
				bonusVO.data = restoreBonus != null ? restoreBonus : null;
				bonusVO.serverData = bonusData;
				bonusVO.bonusConfig = bonusConfig;
				bonusVO.sequence_type = bonusConfig.sequence_type;

				//console.log("step", step);
				//console.log("bonusData", bonusData);
				this.common.server.bonus = bonusVO;
				if(bonusConfig.isAllSticky){
					this.common.isAllSticky = true;
				}
				console.log("parseBonusData this.common.isAllSticky", this.common.isAllSticky);
			}
		}

		public parseRecursion(data:any):void{
			this.common.server.recursion = [];
			if(!data) return;
			var recursionData:any = XMLUtils.getElement(data, "recursion");
			if(recursionData) {
				var recursionDataArr:any = recursionData.childNodes;
				for (var i:number = 0; i < recursionDataArr.length; i++) {
					var wheelData:Array<number> = eval(recursionDataArr[i].attributes[0].nodeValue);
					this.common.server.recursion[i] = [];
					this.common.server.recursion[i]["wheels"] = wheelData;
					this.common.server.recursion[i]["win"] = XMLUtils.getAttributeInt(recursionDataArr[i], "win");

					this.common.server.recursion[i]["winLines"] = [];
					this.common.server.recursion[i]["arrLinesIds"] = [];
					var winLinesData:any = XMLUtils.getElement(recursionDataArr[i], "winposition").childNodes;
					for (var j:number = 0; j < winLinesData.length; ++j) {
						var winLine:any = winLinesData[j];
						var winLineVO:WinLineVO = new WinLineVO();
						winLineVO.lineId = XMLUtils.getAttributeInt(winLine, "line");
						winLineVO.win = XMLUtils.getAttributeFloat(winLine, "win");
						winLineVO.winPos = Utils.toIntArray(winLine.childNodes[0].data.split(","));
						this.common.server.recursion[i]["winLines"].push(winLineVO);
						this.common.server.recursion[i]["arrLinesIds"].push(winLineVO.lineId);
					}
				}
			}
			this.common.server.recursion_len = this.common.server.recursion.length;
		}

		public dispose():void {
			super.dispose();
			this.common = null;
		}

		public parseErrors(responseData:any):void{
			var errors:any = ErrorController.parseXMLErrors(responseData);
			if(errors.length){
				this.send(NotificationList.SHOW_ERRORS, errors);
			}
		}

		public sendJackpotRequest(onComplete:Function):void {
			var serverDelay:any = this.xhrTimeout;
			var request:AjaxRequest = new AjaxRequest(AjaxRequest.XML, this.jackpotUrl, true, {
				"multiply": this.common.server.multiply,
				"key": this.common.key,
				"bet": this.common.server.bet,
				"lineBet": this.common.server.lineCount
			});
			this.xhr = request;
			request.get((responseData)=> {
				clearTimeout(serverDelay);
				this.parseErrors(responseData);
				this.parseJackpotData(responseData);
				onComplete != null && onComplete();
			});
		}

		public parseJackpotData(responseData:any){
			var jackpotText:string = (XMLUtils.getElement(responseData, "jackpot")) ? XMLUtils.getElement(responseData, "jackpot").textContent : "";
			this.send(NotificationList.UPDATE_JACKPOT, jackpotText);
		}

		public getWaysCountForLinesCnt(linesCnt:number):number{
			var waysCount:number = 0;
			for(var i:number=0; i<this.common.config.reelsWays.length; ++i){
				console.log("getWaysCountForLinesCnt i="+i+" this.common.config.reelsWays[i].ways="+this.common.config.reelsWays[i].ways, this.common.config.reelsWays[i]);
				console.log("getWaysCountForLinesCnt i="+i+" this.common.config.reelsWays[i].lines="+this.common.config.reelsWays[i].lines, linesCnt);
				if(this.common.config.reelsWays[i].lines == linesCnt){
					waysCount = this.common.config.reelsWays[i].ways;
				}
			}
			return waysCount;
		}
	}
}