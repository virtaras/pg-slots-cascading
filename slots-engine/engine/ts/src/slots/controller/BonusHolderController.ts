module engine {
	import Container = createjs.Container;

	export class BonusHolderController extends BaseController {
		private common:CommonRefs;
		private container:Container;
		private bonusController:BaseController;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
			super(manager);
			this.common = common;
			this.container = container;
		}

		public init():void {
			super.init();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.CREATE_BONUS);
			notifications.push(NotificationList.END_BONUS);
			notifications.push(NotificationList.START_SECOND_FREE_GAME);
			notifications.push(NotificationList.START_AYU_BONUS);
			notifications.push(NotificationList.START_BLADES_BONUS);
			notifications.push(NotificationList.START_JUNGLE_QUEEN_BONUS);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.CREATE_BONUS:
				{
					this.startBonus();
					break;
				}
				case NotificationList.END_BONUS:
				{
					this.removeBonus();
					this.common.isWildSticky = false;
					break;
				}
				case NotificationList.START_SECOND_FREE_GAME:
				{
					this.removeBonus();
					this.common.server.bonus.type = 'free_spins';
					this.common.server.bonus.className = 'FreeSpinsController';
					this.common.server.bonus.data = null;
					this.common.server.bonus.step = 0;
					this.startBonus();
					this.updateBonus();
					break;
				}
				case NotificationList.START_AYU_BONUS:
				{
					switch(data){
						case 1:
						{
							this.common.isWildSticky = true;
							break;
						}
					}
					this.removeBonus();
					this.common.server.bonus.type = 'free_spins';
					this.common.server.bonus.className = 'FreeSpinsController';
					this.common.server.bonus.data = null;
					this.common.server.bonus.id = (data+2);
					this.startBonus();
					this.updateBonus();
					break;
				}
				case NotificationList.START_BLADES_BONUS:
				{
					this.common.isWildSticky = false;
					this.removeBonus();
					this.common.server.bonus.type = 'free_spins';
					this.common.server.bonus.className = 'FreeSpinsController';
					this.common.server.bonus.data = null;
					this.common.server.bonus.step = 0;
					this.common.server.bonus.id = data;
					this.startBonus();
					this.updateBonus();
					break;
				}
				case NotificationList.START_JUNGLE_QUEEN_BONUS:
				{
					this.removeBonus();
					this.common.server.bonus.type = 'free_spins';
					this.common.server.bonus.className = 'FreeSpinsController';
					this.common.server.bonus.data = null;
					this.common.server.bonus.step = 0;
					this.common.server.bonus.id = data;
					this.startBonus();
					this.updateBonus();
					break;
				}
			}
		}

		private startBonus():void {
			var className:string = this.common.server.bonus.className;
			var BonusClass:any = engine[className];

			this.bonusController = <BaseController>new BonusClass(this.manager, this.common, this.container);
			this.bonusController.init();
			this.send(NotificationList.START_BONUS_AUDIO);
			console.log(className, "startBonus");
		}

		private removeBonus():void {
			this.bonusController.dispose();
			this.bonusController = null;
		}

		public updateBonus():void{
			this.send(NotificationList.UPDATE_SEQUENCE_TYPE);
		}

		public dispose():void {
			super.dispose();
			this.removeBonus();
		}
	}
}