module engine {
	import Container = createjs.Container;

	export class ErrorController extends BaseController {
		public static ERROR_NO_MONEY_STR:string = "NO MONEY. PLEASE, CHECK YOUR BALANCE.";
		private common:CommonRefs;
		private container:Container;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
			super(manager);
			this.common = common;
			this.container = container;
		}

		public init():void {
			super.init();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.SHOW_ERRORS);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.SHOW_ERRORS:
				{
					//console.log("show_errors", data);
					//this.send(NotificationList.SERVER_NOT_RESPONSE);
					break;
				}
			}
		}

		public dispose():void {
			super.dispose();
			this.common = null;
			this.container = null;
		}

		public static parseXMLErrors(xml:any):any{
			var errors:any = [];
			var xml_error:any = XMLUtils.getElement(xml, "error");
			if(xml_error){
				errors.push(xml_error.textContent);
			}
			return errors;
		}
	}
}