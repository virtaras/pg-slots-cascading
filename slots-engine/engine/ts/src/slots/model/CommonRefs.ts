module engine {
	import Point = createjs.Point;
	import Rectangle = createjs.Rectangle;

	export class CommonRefs {
		public config:any;
		public layouts:Object;
		public server:ServerData;
		public symbolsRect:Array<Array<Rectangle>>;
		public gameName:string;
		public dirName:string;
		public key:string;
		public login:string;
		public password:string;
		public room:string;
		public artWidth:number;
		public artHeight:number;
		public artDiffX:number = 0;
		public artDiffY:number = 0;
        public arrLinesIds:Array<number>;
		public isDemo:boolean;
		public isDebug:boolean;
		public isMobile:boolean;
		public isLayoutsLoaded:boolean = false;
		public isSoundLoaded:boolean;
		public isSoundOn:boolean;
		public soundPreload:boolean = null;
		public originBonus:BonusVO;
		public moverType:string = ReelMoverTypes.DEFAULT;
		public restore:boolean;
		public payTableLoaded:boolean;
		public wildSymbolIdExpanding:number = 2;
		public getParams:any;
		public skipFSStartPopup:boolean = false;
		public isTurbo:boolean = false;
		public isAuto:boolean = false;
		public isAutoMode:boolean = false;
		public autoSpinsCnt:number = 10;
		public reelsWayCnt:number = 0;
		public autoSpinsCntRestore:number = 0;
		public lastAutoSpinsCntRestore:number = 0;
		public isWildSticky:boolean = false;
		public isAllSticky:boolean = false;
		public wild_reels_expanding:Array<number> = [];
		public wild_reels_expanding_static:Array<number> = [];
		public maxscale:number;
		public server_inited:boolean = false;
		public canRemoveWildsExpandingStatic:boolean = false;
		public initedGameControllers:boolean = false;
		public screenMode:string = "";

		constructor() {
		}
	}
}