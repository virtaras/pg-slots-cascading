module engine {

	export class ServerData {
		private balance:number;

		public multiply:number;
		public bet:number;
		public maxLineCount:number;
		public lineCount:number;
		public waysCount:number;
		public isExtraChoise:boolean = false;
		public jackpot:number;
		public wheel:Array<number>;
		public availableMultiples:Array<number>;
		public availableBets:Array<number>;
		public win:number = 0;
		public serverWin:number = 0;
		public winLines:Array<WinLineVO>;
        public arrLinesIds:Array<number>;
		public bonus:BonusVO;
		public recursion:any = [];
		public recursion_len:number = 0;

		constructor() {
		}

		public setBalance(value:number):void{
			this.balance = value;
		}

		public getBalance():number{
			return this.balance;
		}

		public setMaxMultiply():void {
			this.multiply = this.availableMultiples[this.availableMultiples.length - 1];
		}

		public setMaxBet():void {
			this.bet = this.availableBets[this.availableBets.length-1];
		}

		public setMaxLines():void {
			this.lineCount = this.maxLineCount;
		}

		public setNextMultiply():void {
			var index:number = this.availableMultiples.indexOf(this.multiply);
			var nextIndex:number = (index + 1) % this.availableMultiples.length;
			this.multiply = this.availableMultiples[nextIndex];
		}

		public setNextBet(isCircle:boolean):void {
			var index:number = this.availableBets.indexOf(this.bet);
			if (++index == this.availableBets.length){
				index = isCircle ? 0 : this.availableBets.length - 1;
			}
			this.bet = this.availableBets[index];
		}

		public isHasNextBet():boolean{
			return this.availableBets.indexOf(this.bet) != this.availableBets.length - 1;
		}

		public setPrevBet(isCircle:boolean):void {
			var index:number = this.availableBets.indexOf(this.bet);
			if (--index == -1){
				index = isCircle ? this.availableBets.length - 1 : 0;
			}
			this.bet = this.availableBets[index];
		}

		public isHasPrevBet():boolean{
			return this.availableBets.indexOf(this.bet) != 0;
		}

		public setNextLineCount(isCircle:boolean):void {
			var lines:number = this.lineCount + 1;
			if (lines > this.maxLineCount) {
				lines = isCircle ? 1 : this.maxLineCount;
			}
			this.lineCount = lines;
		}

		public isHasNextLine():boolean{
			return this.lineCount != this.maxLineCount;
		}

		public setPrevLineCount(isCircle:boolean):void {
			var lines:number = this.lineCount - 1;
			if (lines < 1) {
				lines = isCircle ? this.maxLineCount : 1;
			}
			this.lineCount = lines;
		}

		public isHasPrevLine():boolean{
			return this.lineCount != 1;
		}

		public getTotalBet():number {
			return this.lineCount * this.multiply * this.bet;
		}

		public getBetPerLine():number {
			return this.multiply * this.bet;
		}
	}
}