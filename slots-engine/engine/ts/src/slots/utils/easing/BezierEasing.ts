module engine {
	export class BezierEasing extends BaseEasing {
		private a:number;
		private b:number;
		private c:number;
		private d:number;
		private e:number;

		constructor(a, b, c, d, e) {
			super();
			this.a = a;
			this.b = b;
			this.c = c;
			this.d = d;
			this.e = e;
		}

		public getRatio(time:number):number {
			var ts:number = time * time;
			var tc:number = ts * time;
			return this.a * tc * ts + this.b * ts * ts + this.c * tc + this.d * ts + this.e * time;
		}
	}
}