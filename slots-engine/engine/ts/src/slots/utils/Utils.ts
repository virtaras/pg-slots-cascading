module engine {
	import Container = createjs.Container;
	import DisplayObject = createjs.DisplayObject;
	import Button = layout.Button;

	export class Utils {
		public static float2int(value:number):number {
			return value | 0;
		}

		public static toIntArray(value:Array<string>):Array<number> {
			var returnVal:Array<number> = new Array(value.length);
			for (var i:number = 0; i < value.length; i++) {
				returnVal[i] = parseInt(value[i]);
			}
			return returnVal;
		}

		public static getClassName(obj:any):string {
			var funcNameRegex = /function (.{1,})\(/;
			var results = (funcNameRegex).exec(obj["constructor"].toString());
			return (results && results.length > 1) ? results[1] : "";
		}

		public static fillDisplayObject(container:Container, prefix:string):Array<DisplayObject> {
			var i:number = 1;
			var item:DisplayObject;
			var result:Array<DisplayObject> = [];
			while ((item = container.getChildByName(prefix + i)) != null) {
				result.push(item);
				i++;
			}
			return result;
		}

		public static findChildrenByName(root:Container, name:string, amount:number = 0):Array<DisplayObject> {
			var res:Array<DisplayObject> = [];
			var stack:Array<Container> = [root];
			do {
				root = stack.pop();
				var i:number = 0;
				var numChildren:number = root.getNumChildren();
				while (i < numChildren) {
					var child:DisplayObject = root.getChildAt(i);
					if (child.name == name) {
						res.push(child);
						if (res.length == amount) {
							return res;
						}
					}
					if (child instanceof Container) {
						stack.push(child);
					}
					i++;
				}
			} while (stack.length >  0);
			return res;
		}

		public static findChildByName(root:Container, name:string):DisplayObject {
			var stack:Array<Container> = [root];
			do {
				root = stack.pop();
				var i:number = 0;
				var numChildren:number = root.getNumChildren();
				while (i < numChildren) {
					var child:DisplayObject = root.getChildAt(i);
					if (child.name == name) {
						return child;
					}
					if (child instanceof Container) {
						stack.push(child);
					}
					i++;
				}
			} while (stack.length >  0);
			return null;
		}

		public static findChildInPath(root:Container, path:string, separator:string = "."):DisplayObject
		{
			var splittedPath:Array<string> = path.split(separator);
			for (var i:number = 0; i < splittedPath.length; i++) {
				var foundChild:DisplayObject = root.getChildByName(splittedPath[i]);
				if (foundChild) {
					if (foundChild instanceof Container) {
						root = foundChild;
					} else {
						return foundChild;
					}
				} else {
					break;
				}
			}
			return null;
		}

		public static setElementProps(element:any, props:any){
			if(element) {
				for (var key in props) {
					element[key] = props[key];
				}
			}
		}

		public static getBtn(btnName:string, container:Container):Button {
			return <Button>container.getChildByName(btnName);
		}
	}
}