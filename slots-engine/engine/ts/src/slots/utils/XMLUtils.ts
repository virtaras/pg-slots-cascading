module engine {
	export class XMLUtils {

		public static getElements(xml, tagName:string):Array<any> {
			var result:Array<any> = [];
			var childNodes = xml.childNodes;
			for (var i:number = 0; i < childNodes.length; i++) {
				var node = childNodes[i];
				if (node.nodeName == tagName) {
					result.push(node);
				}
			}
			return result;
		}

		public static getElement(xml, tagName:string):any {
			var childNodes = (xml) ? xml.childNodes : [];
			for (var i:number = 0; i < childNodes.length; i++) {
				var node = childNodes[i];
				if (node.nodeName == tagName) {
					return node;
				}
			}
			return null;
		}

		public static getChildrenElements(xml):any {
			var childArr:Array<any> = [];
			var childNodes = xml.childNodes;
			for (var i:number = 0; i < childNodes.length; i++) {
				var node = childNodes[i];
				childArr.push(node);
			}
			return childArr;
		}


		public static getSessionRoom(xml, tagName:string):any {
			var childNodes = xml.getElementsByTagName(tagName)[0];
			if (childNodes != null){
				return childNodes.textContent;
			}
			else return null;
		}

		public static getAttribute(xml, attributeName:string):string {
			var attributes:NamedNodeMap = xml.attributes;
			if (attributes.hasOwnProperty(attributeName)) {
				return xml.attributes[attributeName].value;
			}
		}

		public static getAttributeInt(xml, attributeName:string):number {
			var attribute:string = XMLUtils.getAttribute(xml, attributeName);
			if (attribute != null) {
				return parseInt(attribute);
			}
			return null;
		}

		public static getAttributeFloat(xml, attributeName:string):number {
			var attribute:string = XMLUtils.getAttribute(xml, attributeName);
			if (attribute != null) {
				return parseFloat(attribute);
			}
			return null;
		}
	}
}