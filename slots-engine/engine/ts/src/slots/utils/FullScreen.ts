module engine {
	export class FullScreen {
		public static fullscreenRequest:string;
		private static cancelFullScreen:string;

		public static init():void {
			var element:any = document.documentElement;
			if (element.requestFullscreen) {
				FullScreen.fullscreenRequest = "requestFullscreen"
			} else if (element.mozRequestFullScreen) {
				FullScreen.fullscreenRequest = "mozRequestFullScreen"
			} else if (element.webkitRequestFullscreen) {
				FullScreen.fullscreenRequest = "webkitRequestFullscreen"
			} else if (element.msRequestFullscreen) {
				FullScreen.fullscreenRequest = "msRequestFullscreen"
			}

			if ((<any>document).exitFullscreen) {
				FullScreen.cancelFullScreen = "exitFullscreen"
			} else if ((<any>document).mozCancelFullScreen) {
				FullScreen.cancelFullScreen = "mozCancelFullScreen"
			} else if ((<any>document).webkitExitFullscreen) {
				FullScreen.cancelFullScreen = "webkitExitFullscreen"
			} else if ((<any>document).msExitFullscreen) {
				FullScreen.cancelFullScreen = "msExitFullscreen"
			}
		}

		public static toggleFullScreen():void {
			if (FullScreen.isFullScreen()) {
				FullScreen.fullScreen();
			}
			else {
				FullScreen.closeFullScreen();
			}
		}

		public static isFullScreen():boolean {
			return (<any>document).fullscreenElement ||
				(<any>document).mozFullScreenElement ||
				(<any>document).webkitFullscreenElement ||
				(<any>document).msFullscreenElement;
		}

		public static fullScreen():void {
			var element:any = document.documentElement;
			console.log("FullScreen.fullscreenRequest", FullScreen.fullscreenRequest);
			if (FullScreen.fullscreenRequest != undefined) {
				element[FullScreen.fullscreenRequest]()
			}
		}

		public static closeFullScreen():void {
			if (FullScreen.cancelFullScreen != undefined) {
				document[FullScreen.cancelFullScreen]()
			}
		}
	}
}

