module engine {
	import Container = createjs.Container;
	import Shape = createjs.Shape;
	import Sprite = createjs.Sprite;
	import Rectangle = createjs.Rectangle;
	import Point = createjs.Point;
	import Layout = layout.Layout;
	import DisplayObject = createjs.DisplayObject;

	export class ReelsView extends Layout {
		public static LAYOUT_NAME:string = "ReelsView";
		public static LAYOUT_PARAMS:any = {};
		private static REEL_PREFIX:string = "reel_";
		public static SYMBOLS_SPRITE:Sprite = null;

		public reels:Array<ReelView>;
		public isMobile:boolean;
		public dark_symbols:Shape[] = [];

		constructor() {
			super();
		}

		public setMobileTrue(isMobile:boolean):void {
			this.isMobile = isMobile;
			this.createReelsMask();
		}

		public onInit():void {
			this.createReelsView();
		}

		private createReelsView():void {
			this.reels = [];
			var view:Container;
			var reelId:number = 1;
			while ((view = <Container>this.getChildByName(ReelsView.REEL_PREFIX + reelId)) != null) {
				var reelView:ReelView = new ReelView(view, reelId - 1);
				reelView.init();
				this.reels.push(reelView);
				reelId++;
			}
		}

		private createReelsMask():void {
			var firstReel:ReelView = this.reels[0];
			var symbolBounds:Rectangle = firstReel.getSymbol(0).getBounds();
			var lastReel:ReelView = this.reels[this.reels.length - 1];

			var mask:Shape = new Shape();
			var mask_y:number = (ReelsView.LAYOUT_PARAMS && ReelsView.LAYOUT_PARAMS.reels_view_y) ? ReelsView.LAYOUT_PARAMS.reels_view_y : firstReel.getY();
			var mask_height:number = (ReelsView.LAYOUT_PARAMS && ReelsView.LAYOUT_PARAMS.reels_view_y_height) ? ReelsView.LAYOUT_PARAMS.reels_view_y_height : this.getHeight() - symbolBounds.height;
			var mask_x:number = (ReelsView.LAYOUT_PARAMS && ReelsView.LAYOUT_PARAMS.reels_view_x) ? ReelsView.LAYOUT_PARAMS.reels_view_x : firstReel.getX();
			var mask_width:number = (ReelsView.LAYOUT_PARAMS && ReelsView.LAYOUT_PARAMS.reels_view_x_width) ? ReelsView.LAYOUT_PARAMS.reels_view_x_width : this.getWidth();

			if(this.isMobile){
				mask_y *= GameController.MOBILE_TO_WEB_SCALE;
				mask_height *= GameController.MOBILE_TO_WEB_SCALE;
				mask_x *= GameController.MOBILE_TO_WEB_SCALE;
				mask_width *= GameController.MOBILE_TO_WEB_SCALE;
			}
			mask.graphics.beginFill("0").drawRect(mask_x, mask_y, mask_width, mask_height);
			this.mask = mask;
			this.hitArea = mask;
			console.log("createReelsMask", mask);
			console.log("createReelsMask", ReelsView.LAYOUT_PARAMS);
		}

		public showAllSymbols():void {
			for (var i:number = 0; i < this.reels.length; i++) {
				this.reels[i].showAllSymbols();
			}
		}

		public hideSymbols(positions:Array<Point>, offset:number = 0):void {
			for (var i:number = 0; i < positions.length; i++) {
				var pos:Point = positions[i];
				this.reels[pos.x].getSymbol(pos.y + offset).visible = false;
			}
		}

		public darkSymbols(positions:any[], symbolsRect:Array<Array<Rectangle>>):void {
			for (var i:number = 0; i < positions.length; i++) {
				var darksymbol:Container = this.reels[positions[i].x].getSymbol(positions[i].y).parent;
				var rect:Rectangle = symbolsRect[positions[i].x][positions[i].y];
				var mask:Shape = new Shape();
				mask.graphics.beginFill("0").drawRect(rect.x, rect.y, rect.width, rect.height);
				mask.alpha = 0.7;
				this.dark_symbols.push(mask);
			}
		}

		public showDarkSymbols():void{
			for(var i:number=0; i<this.dark_symbols.length; ++i){
				this.addChild(this.dark_symbols[i]);
			}
		}

		public hideDarkSymbols():void{
			for(var i:number=0; i<this.dark_symbols.length; ++i){
				this.removeChild(this.dark_symbols[i]);
			}
			this.dark_symbols = [];
		}

		public dispose():void {
			this.parent.removeChild(this);
			this.reels = null;
		}

		/////////////////////////////////////////
		//	GETTERS
		/////////////////////////////////////////

		public getReel(id:number):ReelView {
			return this.reels[id];
		}

		public getReelCount():number {
			return this.reels.length;
		}

		public getHeight():number {
			return this.getBounds().height;
		}

		public getWidth():number {
			return this.getBounds().width;
		}

		public getReelsMask():Shape{
			return this.mask;
		}
	}
}