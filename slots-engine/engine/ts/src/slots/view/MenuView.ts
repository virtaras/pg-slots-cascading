module engine {
	import Button = layout.Button;
	import Layout = layout.Layout;
	import Toggle = layout.Toggle;
	import Container = createjs.Container;
	import Shape = createjs.Shape;
	import Sprite = createjs.Sprite;
	import Text = createjs.Text;

	export class MenuView extends Layout {
		public static LAYOUT_NAME:string = "MenuView";
		public static MENU_PAGE_MASK:string = "menu_pageMask";

		public static MENU_SOUND_BTN:string = "menu_soundBtn";
		public static MENU_SPIN_SETTINGS_BTN:string = "menu_spinSettingsBtn";
		public static MENU_BET_SETTINGS_BTN:string = "menu_betSettingsBtn";
		public static MENU_PAYTABLES_BTN:string = "menu_paytablesBtn";
		public static MENU_CLOSE_BTN:string = "menu_closeBtn";

		public static MENU_SOUND_PAGE:string = "menu_soundPage";
		public static MENU_SPIN_SETTINGS_PAGE:string = "menu_spinSettingsPage";
		public static MENU_BET_SETTINGS_PAGE:string = "menu_betSettingsPage";
		public static MENU_PAYTABLES_PAGE:string = "menu_paytablesPage";

		public static MENU_SOUND_TOGGLE:string = "menuSoundToggle";
		public static MENU_SOUND_LOADING_SPRITE:string = "menuSoundLoadingSprite";
		public static MENU_TURBO_TOGGLE:string = "menuTurboToggle";

		public static AUTO_SPINS_MAX_TF:string = "autoSpinMaxSpinsTf";
		public static AUTO_SPIN_COUNT_CRAWLER_LINE:string = "autoSpinCountCrawlerLine";
		public static AUTO_SPIN_COUNT_CRAWLER:string = "autoSpinCountClawler";
		public static AUTO_SPIN_COUNT_CRAWL:string = "autoSpinCountCrawl";
		public static AUTO_SPIN_COUNT_CRAWL_TF:string = "autoSpinCountCrawlTf";

		public static REELS_WAY_MAX_TF:string = "reelsWayMaxValueTf";
		public static REELS_WAY_COUNT_CRAWLER_LINE:string = "reelsWayCountCrawlerLine";
		public static REELS_WAY_COUNT_CRAWLER:string = "reelsWayCountClawler";
		public static REELS_WAY_COUNT_CRAWL:string = "reelsWayCountCrawl";
		public static REELS_WAY_COUNT_CRAWL_TF:string = "reelsWayCountCrawlTf";

		public static TOTAL_BET_MAX_TF:string = "totalBetMaxTf";
		public static TOTAL_BET_COUNT_CRAWLER_LINE:string = "totalBetCountCrawlerLine";
		public static TOTAL_BET_COUNT_CRAWLER:string = "totalBetCountClawler";
		public static TOTAL_BET_COUNT_CRAWL:string = "totalBetCountCrawl";
		public static TOTAL_BET_COUNT_CRAWL_TF:string = "totalBetCountCrawlTf";

		public static LINES_MAX_TF:string = "linesMaxTf";
		public static LINES_COUNT_CRAWLER_LINE:string = "linesCountCrawlerLine";
		public static LINES_COUNT_CRAWLER:string = "linesCountClawler";
		public static LINES_COUNT_CRAWL:string = "linesCountCrawl";
		public static LINES_COUNT_CRAWL_TF:string = "linesCountCrawlTf";

		public static PAYTABLES_SCROLLAREA:string = "menu_paytablesScrollArea";
		public static PAYTABLES_MASK:string = "menu_paytablesMask";
		public static PAYTABLES_CONTAINER:string = "menu_paytables";
		public static PAYTABLES_CRAWLER_LINE:string = "paytablesCrawlerLine";
		public static PAYTABLES_CRAWLER:string = "paytablesCrawler";
		public static BET_PER_LINE_TF:string = "betPerLineTf";
		public static TOTAL_BET_TF:string = "totalBetTf";

		public buttonsNames:Array<string> = [
			MenuView.MENU_SOUND_BTN,
			MenuView.MENU_SPIN_SETTINGS_BTN,
			MenuView.MENU_BET_SETTINGS_BTN,
			MenuView.MENU_PAYTABLES_BTN,
			MenuView.MENU_CLOSE_BTN
		];

		public pagesButtonsNames:Array<string> = [
			MenuView.MENU_SOUND_BTN,
			MenuView.MENU_SPIN_SETTINGS_BTN,
			MenuView.MENU_BET_SETTINGS_BTN,
			MenuView.MENU_PAYTABLES_BTN
		];

		public pagesNames:Array<string> = [
			MenuView.MENU_SOUND_PAGE,
			MenuView.MENU_SPIN_SETTINGS_PAGE,
			MenuView.MENU_BET_SETTINGS_PAGE,
			MenuView.MENU_PAYTABLES_PAGE
		];

		constructor() {
			super();
		}

		public onInit():void {
			this.visible = false;
			this.cratePagesMask();
			this.showBtnPage(MenuView.MENU_BET_SETTINGS_BTN);
			this.showPage(MenuView.MENU_BET_SETTINGS_PAGE);
		}

		public cratePagesMask():void{
			var menuPageMask:Container = <Container>this.getChildByName(MenuView.MENU_PAGE_MASK);
			var mask:Shape = new Shape();
			menuPageMask.visible = false;
			mask.graphics.beginFill("0").drawRect(menuPageMask.x, menuPageMask.y, menuPageMask.getBounds().width, menuPageMask.getBounds().height);
			for (var i:number = 0; i < this.pagesNames.length; i++) {
				var page:Container = <Container>this.getChildByName(this.pagesNames[i]);
				page.mask = mask;
			}
		}

		public getBtn(btnName:string):Button {
			return <Button>this.getChildByName(btnName);
		}

		public showPage(pageName:string){
			for (var i:number = 0; i < this.pagesNames.length; i++) {
				var page:Container = <Container>this.getChildByName(this.pagesNames[i]);
				if(this.pagesNames[i] == pageName){
					page.visible = true;
				}else{
					page.visible = false;
				}
			}
		}

		public showBtnPage(btnName:string){
			for (var i:number = 0; i < this.pagesButtonsNames.length; i++) {
				var btn:Button = <Button>this.getChildByName(this.pagesButtonsNames[i]);
				if(this.pagesButtonsNames[i] == btnName){
					btn.setEnable(false);
				}else{
					btn.setEnable(true);
				}
			}
		}

		public setText(tf:Text, txtValue:string):void{
			if(tf){
				tf.text = txtValue;
			}
		}

		public updateSoundToggle(isSoundLoaded:boolean, isSoundOn:boolean){
			try {
				var soundToggle:Toggle = <Toggle>this.getChildByName(MenuView.MENU_SOUND_PAGE).getChildByName(MenuView.MENU_SOUND_TOGGLE);
				soundToggle.setIsOn(isSoundOn);
				var soundLoadingSprite:Sprite = <Sprite>this.getChildByName(MenuView.MENU_SOUND_PAGE).getChildByName(MenuView.MENU_SOUND_LOADING_SPRITE);
				if(isSoundOn && !isSoundLoaded){
					soundToggle.visible = false;
					soundLoadingSprite.visible = true;
					soundLoadingSprite.gotoAndPlay(0);
				}else{
					soundToggle.visible = true;
					soundLoadingSprite.visible = false;
				}
			}catch(e){
				console.log("ERROR :", e);
			}
		}
	}
}