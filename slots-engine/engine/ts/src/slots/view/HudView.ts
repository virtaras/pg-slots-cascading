module engine {
	import DisplayObject = createjs.DisplayObject;
	import Container = createjs.Container;
	import Stage = createjs.Stage;
	import Text = createjs.Text;
	import Tween = createjs.Tween;
	import Sprite = createjs.Sprite;
	import Shape = createjs.Shape;
	import Layout = layout.Layout;
	import Button = layout.Button;
	import Toggle = layout.Toggle;

	export class HudView extends Layout {
		public static LAYOUT_NAME:string = "HudView";
		public static LAYOUT_NAME_PORTRAIT:string = "HudViewPortrait";
		//================================================================
		//------------------MENU------------------------------------------
		//================================================================
		public static AUTO_SPIN_TABLE:string = "autoSpinTable";
		public static AUTO_SPIN_TABLE_MASK:string = "autoSpinTableMask";
		public static OPEN_AUTO_SPIN_TABLE_BTN:string = "openAutoSpinTableBtn";
		public static SETTINGS_MENU:string = "settingsMenu";

		//================================================================
		//------------------BUTTONS NAMES---------------------------------
		//================================================================
		public static BACK_TO_LOBBY_BTN:string = "backToLobbyBtn";
		public static SETTINGS_BTN:string = "settingsBtn";
		public static FULL_SCREEN_BTN:string = "fullScreenBtn";

		//Запускает автоспины
		public static START_AUTO_PLAY_BTN:string = "autoSpinStartBtn";
		//Начинает спин
		public static START_SPIN_BTN:string = "spinBtn";

		//Останавлевает автоспины
		public static STOP_AUTO_PLAY_BTN:string = "autoStopBtn";
		//Ускоряет остановку барабанов
		public static STOP_SPIN_BTN:string = "stopBtn";

		//Запускает рисковую
		public static GAMBLE_BTN:string = "gambleBtn";
		//Запускает бонус
		public static START_BONUS_BTN:string = "bonusStartBtn";
		//Открывает HELP
		public static PAY_TABLE_BTN:string = "payTableBtn";

		//Устанавлевает следующее значение фриспинов из конфига autoPlayCount
		public static INC_SPIN_COUNT_BTN:string = "incSpinsBtn";
		//Устанавлевает предыдущее значение фриспинов из конфига autoPlayCount
		public static DEC_SPIN_COUNT_BTN:string = "decSpinsBtn";

		//Увеличевает количество линий на 1 до максимального
		public static INC_LINE_COUNT_BTN:string = "incLinesBtn";
		//Уменьшает количество линий на 1 до 1
		public static DEC_LINE_COUNT_BTN:string = "decLinesBtn";
		//Меняет количество линий по кругу
		public static LINE_COUNT_BTN:string = "linesBtn";
		//Устанавлевает максимальное количество линий и начинает спин
		public static MAX_LINES_BTN:string = "maxBetBtn"; //this is max_lines_btn that should be named as maxBetBtn as we don't have time to overload all games

		//Устанавлевает следующую ставку до максимальной
		public static NEXT_BET_BTN:string = "nextBetBtn";
		//Устанавлевает предыдущую ставку до первой
		public static PREV_BET_BTN:string = "prevBetBtn";
		//Устанавлевает следующую ставку по кругу
		public static BET_BTN:string = "betBtn";
		//Устанавлевает максимальный мультиплекатор и начинает спин
		public static MAX_BET_BTN:string = "maxLinesBtn";
		//Устанавлевает следующий мультиплекатор по кругу
		public static MULTIPLY_BTN:string = "multiplyBtn";

		//Ускоренный режим прокрутки барабанов
		public static TURBO_ON_BTN:string = "turboOnBtn";
		public static TURBO_OFF_BTN:string = "turboOffBtn";
		//Постоянные автоспины
		public static AUTO_ON_BTN:string = "autoOnBtn";
		public static AUTO_OFF_BTN:string = "autoOffBtn";
		//изменение цвета худа
		public static COLOR_BTN:string = "colorBtn";
		public static COLOR_PANEL:string = "colorPanel";
		public static EXTRA_CHOICE_FS_TOGGLE:string = "extraChoiseFSToggle";

		//================================================================
		//------------------TEXT FIELDS NAMES-----------------------------
		//================================================================
		public static WIN_TEXT:string = "winTf";
		public static BET_TEXT:string = "betTf";
		public static TOTAL_BET_TEXT:string = "totalBetTf";
		public static PREV_BET_PER_LINE_TEXT:string = "prevLineBetTf";
		public static BET_PER_LINE_TEXT:string = "lineBetTf";
		public static NEXT_BET_PER_LINE_TEXT:string = "nextLineBetTf";
		public static PREV_LINES_COUNT_TEXT:string = "prevLinesCountTf";
		public static LINES_COUNT_TEXT:string = "linesCountTf";
		public static WAYS_COUNT_TEXT:string = "waysCountTf";
		public static NEXT_LINES_COUNT_TEXT:string = "nextLinesCountTf";
		public static PREV_AUTO_SPINS_COUNT_TEXT:string = "prevSpinCountTf";
		public static AUTO_SPINS_COUNT_TEXT:string = "spinCountTf";
		public static AUTO_SPINS_COUNT_TEXT_SPINMENU:string = "autoSpinCountTf";
		public static NEXT_AUTO_SPINS_COUNT_TEXT:string = "nextSpinCountTf";
		public static BALANCE_TEXT:string = "balanceTf";

		//NETENT UI FIELDS
		public static SPIN_MENU_CONTAINER:string = "spinMenu";
		public static SPIN_MENU_CONTAINER_PORTRAIT_LEFT:string = "spinMenuPortraitLeft";
		public static SPIN_MENU_CONTAINER_PORTRAIT_RIGHT:string = "spinMenuPortraitRight";
		public static SPIN_MENU_OPEN_BTN:string = "spinMenuOpenBtn";
		public static SPIN_MENU_CLOSE_BTN:string = "spinMenuCloseBtn";
		public static SPIN_MENU_OPEN_PORTRAIT_BTN:string = "spinMenuOpenPortraitBtn";
		public static SPIN_MENU_CLOSE_PORTRAIT_BTN:string = "spinMenuClosePortraitBtn";
		public static SPIN_MENU_TOGGLE_TIME:number = 200;

		public static SPIN_MENU_TURBO_MODE_ON_BTN:string = "turboModeOnBtn";
		public static SPIN_MENU_TURBO_MODE_OFF_BTN:string = "turboModeOffBtn";
		public static SPIN_MENU_AUTO_MODE_ON_BTN:string = "autoModeOnBtn";
		public static SPIN_MENU_AUTO_MODE_OFF_BTN:string = "autoModeOffBtn";
		public spinMenuButtonsNames:Array<string> = [
			HudView.SPIN_MENU_CLOSE_BTN,
			HudView.SPIN_MENU_TURBO_MODE_ON_BTN,
			HudView.SPIN_MENU_TURBO_MODE_OFF_BTN,
			HudView.SPIN_MENU_AUTO_MODE_ON_BTN,
			HudView.SPIN_MENU_AUTO_MODE_OFF_BTN
		];
		public spinMenuButtonsNamesPortraitLeft:Array<string> = [
			HudView.SPIN_MENU_CLOSE_BTN,
			HudView.SPIN_MENU_TURBO_MODE_ON_BTN,
			HudView.SPIN_MENU_TURBO_MODE_OFF_BTN,
			HudView.SPIN_MENU_AUTO_MODE_ON_BTN,
			HudView.SPIN_MENU_AUTO_MODE_OFF_BTN
		];
		public spinMenuButtonsNamesPortraitRight:Array<string> = [
			HudView.SPIN_MENU_CLOSE_BTN,
			HudView.SPIN_MENU_TURBO_MODE_ON_BTN,
			HudView.SPIN_MENU_TURBO_MODE_OFF_BTN,
			HudView.SPIN_MENU_AUTO_MODE_ON_BTN,
			HudView.SPIN_MENU_AUTO_MODE_OFF_BTN
		];

		public static LAYOUT_PARAMS:any = {};
		public spinMenuContainer:Container = null;
		public spinMenuContainerPortraitLeft:Container = null;
		public spinMenuContainerPortraitRight:Container = null;

		public static START_SPIN_AUTO_BTN:string = "startSpinAutoBtn";
		public static STOP_SPIN_AUTO_BTN:string = "stopSpinAutoBtn";
		public static START_SPIN_TURBO_SPRITE:string = "startSpinTurboSprite";
		public startSpinTurboSprite:Sprite = null;
		public static HUD_SOUND_TOGGLE:string = "hudSoundToggle";
		public static HUD_SOUND_LOADING_SPRITE:string = "hudSoundLoadingSprite";
		public static MENU_OPEN_BTN:string = "menuOpenBtn";

		public static UPDATE_COLOR_CACHE_TIME:number = 50;

		public buttonsNames:Array<string> = [
			HudView.SETTINGS_BTN,
			HudView.BACK_TO_LOBBY_BTN,
			HudView.START_AUTO_PLAY_BTN,
			HudView.STOP_AUTO_PLAY_BTN,
			HudView.GAMBLE_BTN,
			HudView.INC_SPIN_COUNT_BTN,
			HudView.DEC_SPIN_COUNT_BTN,
			HudView.INC_LINE_COUNT_BTN,
			HudView.DEC_LINE_COUNT_BTN,
			HudView.NEXT_BET_BTN,
			HudView.PREV_BET_BTN,
			HudView.MAX_BET_BTN,
			HudView.MAX_LINES_BTN,
			HudView.START_SPIN_BTN,
			HudView.START_BONUS_BTN,
			HudView.STOP_SPIN_BTN,
			HudView.PAY_TABLE_BTN,
			HudView.BET_BTN,
			HudView.LINE_COUNT_BTN,
			HudView.MULTIPLY_BTN,
			HudView.OPEN_AUTO_SPIN_TABLE_BTN,
			HudView.TURBO_OFF_BTN,
			HudView.TURBO_ON_BTN,
			HudView.AUTO_OFF_BTN,
			HudView.AUTO_ON_BTN,
			HudView.SPIN_MENU_OPEN_BTN,
			HudView.SPIN_MENU_OPEN_PORTRAIT_BTN,
			HudView.SPIN_MENU_CLOSE_PORTRAIT_BTN,
			HudView.START_SPIN_AUTO_BTN,
			HudView.STOP_SPIN_AUTO_BTN,
			HudView.MENU_OPEN_BTN,
			HudView.COLOR_BTN
		];

		// for mobile
		private leftObj:Array<string> = [
			HudView.BACK_TO_LOBBY_BTN,
			HudView.START_SPIN_BTN,
			HudView.STOP_SPIN_BTN,
			HudView.START_AUTO_PLAY_BTN,
			HudView.STOP_AUTO_PLAY_BTN,
			HudView.GAMBLE_BTN,
			HudView.AUTO_OFF_BTN,
			HudView.AUTO_ON_BTN,
			HudView.TURBO_OFF_BTN,
			HudView.TURBO_ON_BTN
		];

		// for mobile
		private rightObj:Array<string> = [
			HudView.SETTINGS_BTN,
			HudView.PAY_TABLE_BTN
		];

		public linesText:Array<Text> = [];
		public waysText:Array<Text> = [];

		public text_fileds_shadow:Array<string> = [
			HudView.BALANCE_TEXT,
			HudView.TOTAL_BET_TEXT,
			HudView.WIN_TEXT,
			HudView.BET_PER_LINE_TEXT
		]

		public hudBtnsColorContainer:Container = null;
		public hudBtnsColorContainerInited:boolean = false;
		public static HUDCONTAINER_PATH:string = "";

		public spinMenuPrepared:boolean = false;
		public spinMenuOpened:boolean = false;
		public screenMode:string = "";
		public canShowSpinMenu:boolean = false;
		public bg:Shape;

		constructor() {
			super();
		}

		public onInit():void {
			this.prepareView();
			var textField:Text;
			var i:number = 1;
			while ((textField = <Text>this.getChildByName(HudView.LINES_COUNT_TEXT + i)) != null) {
				this.linesText.push(textField);
				i++;
			}
			i=1;
			while ((textField = <Text>this.getChildByName(HudView.WAYS_COUNT_TEXT + i)) != null) {
				this.waysText.push(textField);
				i++;
			}
		}

		public prepareView():void{
			this.hudBtnsColorContainer = (HudView.HUDCONTAINER_PATH != "") ? <Container>this.getChildByName(HudView.HUDCONTAINER_PATH) : <Container>this;
			this.spinMenuContainer = <Container>this.getChildByName(HudView.SPIN_MENU_CONTAINER) || null;
			this.spinMenuContainerPortraitLeft = <Container>this.getChildByName(HudView.SPIN_MENU_CONTAINER_PORTRAIT_LEFT) || null;
			this.spinMenuContainerPortraitRight = <Container>this.getChildByName(HudView.SPIN_MENU_CONTAINER_PORTRAIT_RIGHT) || null;
			this.startSpinTurboSprite = <Sprite>this.hudBtnsColorContainer.getChildByName(HudView.START_SPIN_TURBO_SPRITE) || null;
			this.initColorContainer();
			this.prepareSpinMenu();
		}

		public initColorContainer():void{
			this.updateHudColor();
			this.hudBtnsColorContainerInited = true;
			this.updateColorCacheCyclic();
		}

		public updateHudColor(params:any = {}){
			if(HudView.HUDCONTAINER_PATH != ""){
				var matrix = new createjs.ColorMatrix().adjustColor(params.brightness || 0, params.contrast || 0, params.saturation || 0, params.hue || 0);
				this.hudBtnsColorContainer.filters = [
					new createjs.ColorMatrixFilter(matrix)
				];
				this.hudBtnsColorContainer.cache(0,0,this.hudBtnsColorContainer.getBounds().width,this.hudBtnsColorContainer.getBounds().height);
			}
		}

		public updateColorCacheCyclic():void{
			this.updateColorCache();
			setTimeout(()=>{this.updateColorCacheCyclic();}, HudView.UPDATE_COLOR_CACHE_TIME);
		}

		public updateColorCache():void{
			if(HudView.HUDCONTAINER_PATH != "" && this.hudBtnsColorContainerInited){
				this.hudBtnsColorContainer.updateCache();
			}
		}

		public prepareSpinMenu():void{
			switch(this.screenMode){
				case GameConst.SCREEN_MODE_LANDSCAPE:
				{
					if(this.spinMenuContainer) {
						this.spinMenuContainer.x = HudView.LAYOUT_PARAMS.spinMenuContainerParams.spinMenuRightX;
						var mask:Shape = new Shape();
						var mask_radius:number = HudView.LAYOUT_PARAMS.spinMenuContainerParams.mask_radius; //85
						var mask_rect_x:number = HudView.LAYOUT_PARAMS.spinMenuContainerParams.mask_rect_x; // 483;
						var mask_rect_width:number = HudView.LAYOUT_PARAMS.spinMenuContainerParams.mask_rect_width; // 300;
						var mask_rect_height:number = HudView.LAYOUT_PARAMS.spinMenuContainerParams.mask_rect_height; // 300;
						var mask_rect_center_y:number = HudView.LAYOUT_PARAMS.spinMenuContainerParams.mask_rect_center_y; // 255;
						mask.graphics.beginFill("0").drawRect(HudView.LAYOUT_PARAMS.spinMenuContainerParams.spinMenuLeftX, 0, HudView.LAYOUT_PARAMS.spinMenuContainerParams.mask_rect_x - HudView.LAYOUT_PARAMS.spinMenuContainerParams.spinMenuLeftX+mask_radius, 1000);
						mask.graphics.beginFill("0").drawRoundRect (mask_rect_x, mask_rect_center_y, mask_rect_width, mask_rect_height, -1*mask_radius);
						mask.graphics.beginFill("0").drawRoundRect (mask_rect_x, mask_rect_center_y-mask_rect_height, mask_rect_width, mask_rect_height, -1*mask_radius);
						this.spinMenuContainer.mask = mask;
					}
					this.enableSpinMenuPortrait(false);
					break;
				}
				case GameConst.SCREEN_MODE_PORTRAIT:
				{
					this.enableSpinMenuPortrait(true);
					if(this.spinMenuContainer) {
						this.spinMenuContainer.visible = false;
						this.changeButtonState(HudView.SPIN_MENU_OPEN_BTN, false, false);
					}
					break;
				}
			}

			if(this.startSpinTurboSprite && !this.spinMenuPrepared){
				this.startSpinTurboSprite.visible = false;
			}
			this.spinMenuPrepared = true;

			if(this.spinMenuOpened){
				this.openSpinMenu();
			}else{
				this.closeSpinMenu();
			}
		}

		public enableSpinMenuPortrait(enable:boolean){
			if(this.spinMenuContainerPortraitLeft || this.spinMenuContainerPortraitRight) {
				this.changeButtonState(HudView.SPIN_MENU_OPEN_PORTRAIT_BTN, enable && this.canShowSpinMenu, enable && this.canShowSpinMenu);
				this.changeButtonState(HudView.SPIN_MENU_CLOSE_PORTRAIT_BTN, enable && this.canShowSpinMenu, enable && this.canShowSpinMenu);
			}
			if(this.spinMenuContainerPortraitLeft){
				this.spinMenuContainerPortraitLeft.visible = enable && (this.spinMenuOpened==enable);
			}
			if(this.spinMenuContainerPortraitRight){
				this.spinMenuContainerPortraitRight.visible = enable && (this.spinMenuOpened==enable);
			}
		}

		public addFieldsShadows():void{
			for(var i:number = 0; i<this.text_fileds_shadow.length; ++i){
				GameController.setFieldStroke(this, this.text_fileds_shadow[i], 0, GameController.SHADOW_COLOR, 1, 1, 2);
			}
			for(var i:number = 0; i<this.linesText.length; ++i){
				GameController.setTextStroke(this.linesText[i], 0, GameController.SHADOW_COLOR, 1, 1, 2);
			}
		}

		public resizeForMobile():void {
			var stage:Stage = this.getStage();
			if (stage == null) {
				return;
			}
			if(!HudView.LAYOUT_PARAMS.noButtonsFitScreen) {
				var i:number;
				var displayObject:DisplayObject;
				var startX:number;
				var canvas:HTMLCanvasElement = stage.canvas;
				var rightX:number = -stage.x / stage.scaleX;
				var leftX:number = (canvas.width - stage.x) / stage.scaleX;

				startX = (this.getBtn(HudView.STOP_AUTO_PLAY_BTN)) ? this.getBtn(HudView.STOP_AUTO_PLAY_BTN).x : this.getBtn(HudView.START_SPIN_BTN).x;
				for (i = 0; i < this.leftObj.length; i++) {
					displayObject = this.getChildByName(this.leftObj[i]);
					if (displayObject != null) {
						displayObject.x = leftX - displayObject.getBounds().width;
					}
				}
				if (this.getText(HudView.AUTO_SPINS_COUNT_TEXT)) {
					this.getText(HudView.AUTO_SPINS_COUNT_TEXT).x += this.getBtn(HudView.STOP_AUTO_PLAY_BTN).x - startX;
				}

				// move settings menu
				var settingsMenu:DisplayObject = this.getChildByName(HudView.SETTINGS_MENU);
				if (settingsMenu != null) {
					displayObject = this.getChildByName(this.rightObj[0]);
					settingsMenu.x = rightX + (settingsMenu.x - displayObject.x);
				}

				for (i = 0; i < this.rightObj.length; i++) {
					displayObject = this.getChildByName(this.rightObj[i]);
					if (displayObject != null) {
						displayObject.x = rightX;
					}
				}
			}
			if(HudView.LAYOUT_PARAMS.hudBackground) {
				this.removeChild(this.bg);
				this.bg = new Shape();
				this.bg.graphics.beginFill(HudView.LAYOUT_PARAMS.hudBackground.color).drawRect(HudView.LAYOUT_PARAMS.hudBackground.x, HudView.LAYOUT_PARAMS.hudBackground.y, HudView.LAYOUT_PARAMS.hudBackground.width, HudView.LAYOUT_PARAMS.hudBackground.height);
				this.addChild(this.bg);
			}
		}

		public setScreenMode(screenMode:string):void{
			this.screenMode = screenMode;
			this.updateScreenMode();
		}

		public updateScreenMode():void{
			this.updateHudParams();
			this.prepareSpinMenu();
		}

		public updateHudParams():void{
			try {
				this.getBtn(HudView.START_SPIN_BTN).x = HudView.LAYOUT_PARAMS.spinBtn.x || this.getBtn(HudView.START_SPIN_BTN).x;
				this.getBtn(HudView.START_SPIN_BTN).y = HudView.LAYOUT_PARAMS.spinBtn.y || this.getBtn(HudView.START_SPIN_BTN).y;
				this.getBtn(HudView.START_SPIN_AUTO_BTN).x = HudView.LAYOUT_PARAMS.spinBtn.x || this.getBtn(HudView.START_SPIN_AUTO_BTN).x;
				this.getBtn(HudView.START_SPIN_AUTO_BTN).y = HudView.LAYOUT_PARAMS.spinBtn.y || this.getBtn(HudView.START_SPIN_AUTO_BTN).y;
				this.getBtn(HudView.STOP_SPIN_AUTO_BTN).x = HudView.LAYOUT_PARAMS.spinBtn.x || this.getBtn(HudView.STOP_SPIN_AUTO_BTN).x;
				this.getBtn(HudView.STOP_SPIN_AUTO_BTN).y = HudView.LAYOUT_PARAMS.spinBtn.y || this.getBtn(HudView.STOP_SPIN_AUTO_BTN).y;
				this.startSpinTurboSprite.x = HudView.LAYOUT_PARAMS.spinBtn.x + this.getBtn(HudView.START_SPIN_BTN).getBounds().width / 2 - this.startSpinTurboSprite.getBounds().width / 2 || this.startSpinTurboSprite.x;
				this.startSpinTurboSprite.y = HudView.LAYOUT_PARAMS.spinBtn.y + this.getBtn(HudView.START_SPIN_BTN).getBounds().height / 2 - this.startSpinTurboSprite.getBounds().height / 2 || this.startSpinTurboSprite.y;
				this.getText(HudView.AUTO_SPINS_COUNT_TEXT).x = HudView.LAYOUT_PARAMS.spinCountTf.x || this.getText(HudView.AUTO_SPINS_COUNT_TEXT).x;
				this.getText(HudView.AUTO_SPINS_COUNT_TEXT).y = HudView.LAYOUT_PARAMS.spinCountTf.y || this.getText(HudView.AUTO_SPINS_COUNT_TEXT).y;
				this.getBtn(HudView.BACK_TO_LOBBY_BTN).y = HudView.LAYOUT_PARAMS.backToLobbyBtn.y || this.getBtn(HudView.BACK_TO_LOBBY_BTN).y;
				this.getBtn(HudView.BACK_TO_LOBBY_BTN).x = HudView.LAYOUT_PARAMS.backToLobbyBtn.x || this.getBtn(HudView.BACK_TO_LOBBY_BTN).x;
				this.getBtn(HudView.HUD_SOUND_TOGGLE).x = HudView.LAYOUT_PARAMS.hudSoundToggle.x || this.getBtn(HudView.HUD_SOUND_TOGGLE).x;
				this.getBtn(HudView.HUD_SOUND_TOGGLE).y = HudView.LAYOUT_PARAMS.hudSoundToggle.y || this.getBtn(HudView.HUD_SOUND_TOGGLE).y;
				this.getBtn(HudView.HUD_SOUND_LOADING_SPRITE).x = HudView.LAYOUT_PARAMS.hudSoundLoadingSprite.x || this.getBtn(HudView.HUD_SOUND_LOADING_SPRITE).x;
				this.getBtn(HudView.HUD_SOUND_LOADING_SPRITE).y = HudView.LAYOUT_PARAMS.hudSoundLoadingSprite.y || this.getBtn(HudView.HUD_SOUND_LOADING_SPRITE).y;
				this.getBtn(HudView.MENU_OPEN_BTN).x = HudView.LAYOUT_PARAMS.menuOpenBtn.x || this.getBtn(HudView.MENU_OPEN_BTN).x;
				this.getBtn(HudView.MENU_OPEN_BTN).y = HudView.LAYOUT_PARAMS.menuOpenBtn.y || this.getBtn(HudView.MENU_OPEN_BTN).y;
			}catch (e){}
		}

		public changeButtonState(btnName:string, visible:boolean, enable:boolean, containers:Container[] = [this.hudBtnsColorContainer]):void {
			if(containers.length) {
				for(var i:number=0; i<containers.length; ++i) {
					if(containers[i]) {
						var button:Button = this.getBtn(btnName, containers[i]);
						if (button) {
							button.visible = visible;
							button.setEnable(enable);
						}
					}
				}
			}
		}

		public setLineCountText(value:string):void {
			for (var i:number = 0; i < this.linesText.length; i++) {
				this.linesText[i].text = value;
			}
		}

		public setWayCountText(value:string):void {
			for (var i:number = 0; i < this.waysText.length; i++) {
				this.waysText[i].text = value;
			}
		}

		public dispose():void {
			this.parent.removeChild(this);
		}

		/////////////////////////////////////////
		//	GETTERS
		/////////////////////////////////////////

		public getBtn(btnName:string, container:Container = this.hudBtnsColorContainer):Button {
			return <Button>container.getChildByName(btnName);
		}

		public getText(textFieldName:string, container:Container = this):Text {
			return <Text>container.getChildByName(textFieldName);
		}

		public changeTextState(textFieldName:string, visible:boolean):void{
			var textField:Text = this.getText(textFieldName);
			if (textField) {
				textField.visible = visible;
			}
		}

		public openSpinMenu():void{
			switch(this.screenMode){
				case GameConst.SCREEN_MODE_LANDSCAPE:
				{
					if(this.spinMenuContainer) {
						this.changeButtonState(HudView.SPIN_MENU_OPEN_BTN, false, false);
						this.changeButtonState(HudView.SPIN_MENU_CLOSE_BTN, true, true, [this.spinMenuContainer, this.spinMenuContainerPortraitLeft, this.spinMenuContainerPortraitRight]);
						this.spinMenuContainer.visible = true;
						Tween.get(this.spinMenuContainer).to({x: HudView.LAYOUT_PARAMS.spinMenuContainerParams.spinMenuLeftX}, HudView.SPIN_MENU_TOGGLE_TIME);
					}
					break;
				}
				case GameConst.SCREEN_MODE_PORTRAIT:
				{
					if(this.spinMenuContainerPortraitLeft || this.spinMenuContainerPortraitRight){
						this.changeButtonState(HudView.SPIN_MENU_OPEN_PORTRAIT_BTN, false, false);
						this.changeButtonState(HudView.SPIN_MENU_CLOSE_PORTRAIT_BTN, true, true);
						this.spinMenuContainerPortraitLeft.visible = true;
						this.spinMenuContainerPortraitRight.visible = true;
					}
					break;
				}
			}
			this.spinMenuOpened = true;
		}

		public closeSpinMenu():void{
			switch(this.screenMode){
				case GameConst.SCREEN_MODE_LANDSCAPE:
				{
					if(this.spinMenuContainer) {
						Tween.get(this.spinMenuContainer).to({x: HudView.LAYOUT_PARAMS.spinMenuContainerParams.spinMenuRightX}, HudView.SPIN_MENU_TOGGLE_TIME).call(()=> {
							this.hideSpinMenu();
						});
					}
					break;
				}
				case GameConst.SCREEN_MODE_PORTRAIT:
				{
					Tween.get(this.spinMenuContainerPortraitLeft).call(()=> {
						this.hideSpinMenu();
					});
					break;
				}
			}
			this.spinMenuOpened = false;
		}

		public hideSpinMenu():void{
			if(this.spinMenuContainer) {
				if(HudView.LAYOUT_PARAMS.spinMenuContainerParams && HudView.LAYOUT_PARAMS.spinMenuContainerParams.spinMenuRightX){
					this.spinMenuContainer.x = HudView.LAYOUT_PARAMS.spinMenuContainerParams.spinMenuRightX;
				}
				this.spinMenuContainer.visible = false;
				this.updateSpinMenuView();
			}
			if(this.spinMenuContainerPortraitLeft){
				this.spinMenuContainerPortraitLeft.visible = false;
			}
			if(this.spinMenuContainerPortraitRight){
				this.spinMenuContainerPortraitRight.visible = false;
			}
			this.spinMenuOpened = false;
		}

		public setFieldVisible(fieldname:string, visible:boolean, container = this.hudBtnsColorContainer):void{
			var field:Sprite = <Sprite>container.getChildByName(fieldname);
			if(field){
				field.visible = visible;
			}
		}

		public updateSoundToggle(isSoundLoaded:boolean, isSoundOn:boolean){
			try {
				var soundToggle:Toggle = <Toggle>this.getChildByName(HudView.HUD_SOUND_TOGGLE);
				soundToggle.setIsOn(isSoundOn);
				var soundLoadingSprite:Sprite = <Sprite>this.getChildByName(HudView.HUD_SOUND_LOADING_SPRITE);
				if(isSoundOn && !isSoundLoaded){
					soundToggle.visible = false;
					soundLoadingSprite.visible = true;
					soundLoadingSprite.gotoAndPlay(0);
				}else{
					soundToggle.visible = true;
					soundLoadingSprite.visible = false;
				}
			}catch(e){
				console.log("ERROR :", e);
			}
		}

		public updateSpinMenuView():void{
			switch(this.screenMode){
				case GameConst.SCREEN_MODE_LANDSCAPE:
				{
					this.changeButtonState(HudView.SPIN_MENU_OPEN_BTN, !this.spinMenuOpened && this.canShowSpinMenu, !this.spinMenuOpened && this.canShowSpinMenu);
					this.changeButtonState(HudView.SPIN_MENU_CLOSE_BTN, this.spinMenuOpened && this.canShowSpinMenu, this.spinMenuOpened && this.canShowSpinMenu, [this.spinMenuContainer, this.spinMenuContainerPortraitLeft, this.spinMenuContainerPortraitRight]);
					this.changeButtonState(HudView.SPIN_MENU_OPEN_PORTRAIT_BTN, false, false);
					this.changeButtonState(HudView.SPIN_MENU_CLOSE_PORTRAIT_BTN, false, false);
					break;
				}
				case GameConst.SCREEN_MODE_PORTRAIT:
				{
					this.changeButtonState(HudView.SPIN_MENU_OPEN_PORTRAIT_BTN, !this.spinMenuOpened && this.canShowSpinMenu, !this.spinMenuOpened && this.canShowSpinMenu);
					this.changeButtonState(HudView.SPIN_MENU_CLOSE_PORTRAIT_BTN, this.spinMenuOpened && this.canShowSpinMenu, this.spinMenuOpened && this.canShowSpinMenu);
					this.changeButtonState(HudView.SPIN_MENU_OPEN_BTN, false, false);
					this.changeButtonState(HudView.SPIN_MENU_CLOSE_BTN, false, false, [this.spinMenuContainer, this.spinMenuContainerPortraitLeft, this.spinMenuContainerPortraitRight]);
					break;
				}
			}
		}
	}
}