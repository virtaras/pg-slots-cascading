module engine {
	import Container = createjs.Container;
	import Text = createjs.Text;
	import Layout = layout.Layout;
	import Tween = createjs.Tween;
	import Shape = createjs.Shape;
	import DisplayObject = createjs.DisplayObject;

	export class JackpotView extends Layout {
		public static SCROLL_TIME:number = 8000;
		public static LAYOUT_NAME:string = "JackpotView";
		public static JACKPOT_SCROLL_NAME:string = "jackpot_scroll";
		public static JACKPOT_BLACKLINE_NAME:string = "jackpot_blackline";
		public static JACKPOT_TF_NAME:string = "jackpotTF";
		public static JACKPOT_TF_TEXT:string = "";
		private jackpotScroll:Container;
		private jackpotBlackline:DisplayObject;
		private jackpotTF:Text;
		private scrollStarted:boolean = false;

		constructor() {
			super();
		}

		public onInit():void {
			this.jackpotBlackline = <DisplayObject>this.getChildByName(JackpotView.JACKPOT_BLACKLINE_NAME);
			this.jackpotScroll = <Container>this.getChildByName(JackpotView.JACKPOT_SCROLL_NAME);
			this.jackpotTF = <Text>this.jackpotScroll.getChildByName(JackpotView.JACKPOT_TF_NAME);
		}

		public initJackpot():void{
			this.setScrollingMask();
			this.startScrolling();
		}

		public setScrollingMask():void{
			this.mask = new Shape();
			this.mask.graphics.beginFill("0").drawRect(this.jackpotBlackline.x + this.x, this.jackpotBlackline.y + this.y, this.jackpotBlackline.getBounds().width, this.jackpotBlackline.getBounds().height);
		}

		public startScrolling():void{
			var x:number = this.jackpotBlackline.getBounds().width;
			Tween.get(this.jackpotScroll, {override: true}).to({x: -1 * x}, JackpotView.SCROLL_TIME).call(()=> {
				this.scrollStarted = true;
				this.jackpotScroll.x = x;
				this.startScrolling();
			});
		}

		public setJackpot(jackpot:number){
			if(!this.scrollStarted) this.startScrolling();
			this.jackpotTF.text = JackpotView.JACKPOT_TF_TEXT + MoneyFormatter.format(jackpot, false);
		}
	}
}