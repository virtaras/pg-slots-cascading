module engine {
	import Layout = layout.Layout;
	import Tween = createjs.Tween;
	import Ticker = createjs.Ticker;
	import Rectangle = createjs.Rectangle;
	import Sprite = createjs.Sprite;

	export class FingerFullScreenView extends Layout {
		public static LAYOUT_NAME:string = "FingerFullScreenView";
		public static BG_NAME:string = "finger_bg";
		public static FINGER_NAME:string = "finger";

		public finger:Sprite = null;

		constructor() {
			super();
		}

		public onInit():void {
			this.getChildByName(FingerFullScreenView.BG_NAME).alpha = 0.6;
			this.finger = <Sprite>this.getChildByName(FingerFullScreenView.FINGER_NAME);
		}

		public play():void {
			this.finger.gotoAndPlay(0);
		}

		public stop():void {
			this.finger.gotoAndStop(0);
		}
	}
}