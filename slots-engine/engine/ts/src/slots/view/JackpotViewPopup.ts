module engine {
	import Text = createjs.Text;
	import Layout = layout.Layout;
	import Button = layout.Button;

	export class JackpotViewPopup extends Layout {
		public static LAYOUT_NAME:string = "JackpotViewPopup";
		public static JACKPOT_TF_NAME:string = "TxtJackpot";
		public static JACKPOT_CONTINUE_BTN_NAME:string = "btnContinue";
		public static LAYOUT_PARAMS:any = {};
		private jackpotTF:Text;

		constructor() {
			super();
		}

		public buttonsNames:Array<string> = [
			JackpotViewPopup.JACKPOT_CONTINUE_BTN_NAME
		];

		public onInit():void {
			this.jackpotTF = <Text>this.getChildByName(JackpotViewPopup.JACKPOT_TF_NAME);
			this.on("click", function(){}); // prevent click on lower layers
		}

		public setText(txt:any):void{
			this.jackpotTF.text = txt;
		}

		public getButton(name:string):Button {
			return <Button>this.getChildByName(name);
		}

		public changeButtonState(btnName:string, visible:boolean, enable:boolean):void {
			var button:Button = this.getButton(btnName);
			if (button) {
				button.visible = visible;
				//button.setEnable(enable);
			}
		}

		public setParams(x:number=0, y:number=0, scaleX:number=1, scaleY:number=1):void{
			this.x = x;
			this.y = y;
			this.scaleX = scaleX;
			this.scaleY = scaleY;
		}
	}
}