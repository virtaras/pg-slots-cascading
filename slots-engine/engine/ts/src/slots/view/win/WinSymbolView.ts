module engine {
	import Container = createjs.Container;
	import DisplayObject = createjs.DisplayObject;
	import Sprite = createjs.Sprite;
	import LayoutCreator = layout.LayoutCreator;
	import Tween = createjs.Tween;

	export class WinSymbolView extends Container {
		public winSymbol:DisplayObject;
		private highlight:Sprite;
		public _is_wild_expanding:boolean = false;
		public _is_in_wild_expanding_reel:boolean = false;
		public _is_in_wild_expanding_reel_static:boolean = false;
		public _is_wild_sticky:boolean = false;
		public _is_played_with_winlines:boolean = true;
		public _is_started:boolean = false;
		public _highlight_stop_on_played:boolean = false;

		constructor() {
			super();
		}

		public create(winSymbolCreator:LayoutCreator, highlightCreator:LayoutCreator, common:CommonRefs):void {
			this._highlight_stop_on_played = common.config.highlight_stop_on_played || this._highlight_stop_on_played;
			if (winSymbolCreator != null) {
				var winSymbolContainer:Container = new Container();
				winSymbolCreator.create(winSymbolContainer);
				this.winSymbol = winSymbolContainer.getChildAt(0);
				this.addChild(winSymbolContainer);
			}

			if (highlightCreator != null) {
				var highlightContainer:Container = new Container();
				highlightCreator.create(highlightContainer);
				this.highlight = <Sprite>highlightContainer.getChildAt(0);
				if(common.isMobile){
					this.highlight.scaleX *= GameController.MOBILE_TO_WEB_SCALE;
					this.highlight.scaleY *= GameController.MOBILE_TO_WEB_SCALE;
				}
				if(common.config.highlight_under_symbol){
					this.addChildAt(highlightContainer, 0);
				}else{
					this.addChild(highlightContainer);
				}
			}
		}

		public play():void {
			if(!this._is_started) {
				if (this.winSymbol != null && this.winSymbol instanceof Sprite) {
					var sprite = (<Sprite>this.winSymbol);
					sprite.gotoAndPlay(0);
				}
				if (this.highlight != null) {
					if (this._highlight_stop_on_played) {
						var frameCount:number = this.highlight.spriteSheet.getNumFrames(null) - 1;
						this.highlight.addEventListener("tick", (event)=> {
							if (this.highlight.currentFrame == frameCount) {
								this.highlight.stop();
								this.highlight.removeAllEventListeners("tick");
							}
						});
					}
					this.highlight.gotoAndPlay(0);
				}
			}
			this._is_started = true;
		}

		public setY(value:number):void {
			this.y = value;
		}
		public setX(value:number):void {
			this.x = value;
		}
	}
}