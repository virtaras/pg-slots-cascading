module engine {
	import Container = createjs.Container;
	import Text = createjs.Text;

	export class TouchMenu {
		private static TEXT_PREFIX:string = "text";

		private view:Container;
		private currentIdx:number;
		private allValues:Array<number>;
		private onUpdate:Function;
		private step:number;
		private texts:Array<Text>;

		constructor(view:Container) {
			this.view = view;
		}

		public init(currentIdx:number, allValues:Array<number>, onUpdate:Function):void {
			this.currentIdx = currentIdx;
			this.allValues = allValues;
			this.onUpdate = onUpdate;

			this.initTextFields();
			this.updateValue();
			this.step = this.view.getBounds().width / this.texts.length / 2;

			this.view.on("mousedown", (eventDownObj:any)=>{
				var div:number;
				var startIdx:number = this.currentIdx;
				this.view.on("pressmove", (eventMoveObj:any)=>{
					div = Utils.float2int((eventDownObj.stageX - eventMoveObj.stageX) / this.step);
					var currentIdx:number = startIdx + div;
					if (currentIdx != this.currentIdx && currentIdx >= 0 && currentIdx < this.allValues.length) {
						this.changeIdx(currentIdx);
					}
				});
				this.view.on("pressup", ()=>{
					this.view.removeAllEventListeners("pressmove");
					this.view.removeAllEventListeners("pressup");
				});
			});
		}

		private changeIdx(idx:number):void {
			this.currentIdx = idx;
			this.updateValue();
			this.onUpdate(this.currentIdx);
		}

		private updateValue():void {
			var idx:number;
			var div:number = (this.texts.length - 1) / 2;
			for (var i:number = 0; i < this.texts.length; i++) {
				idx = this.currentIdx + i - div;
				if (idx >= 0 && idx < this.allValues.length) {
					this.texts[i].text = this.allValues[idx].toString();
				}
				else {
					this.texts[i].text = "";
				}
			}
		}

		private initTextFields():void {
			var textField:Text;
			var i:number = 1;
			this.texts = [];
			while ((textField = <Text>this.view.getChildByName(TouchMenu.TEXT_PREFIX + i)) != null) {
				this.texts.push(textField);
				i++;
			}
		}
	}
}