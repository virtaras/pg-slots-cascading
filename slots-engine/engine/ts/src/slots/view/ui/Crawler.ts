module engine {
	import Container = createjs.Container;
	import Text = createjs.Text;

	export class Crawler {
		private view:Container;
		crawlerLine:Container = null;
		crawler:Container = null;
		crawl:Container = null;
		crawlTf:Text = null;
		maxValue:number = null;
		onDown:Function = null;
		onMove:Function = null;
		ouUp:Function = null;
		availableValues:any = null;
		startX:number = 0;
		currentX:number = 0;
		lineLength:number = 0;

		constructor(view:Container) {
			this.view = view;
		}

		public initCrawler(crawlerLine:Container, crawler:Container, crawl:Container, crawlTf:Text, maxValue:number, minValue:number = 0, onDown:Function = ()=>{}, onMove:Function = ()=>{}, ouUp:Function = ()=>{}, availableValues:any = null):void{
			this.crawlerLine = crawlerLine;
			this.crawler = crawler;
			this.crawl = crawl;
			this.crawlTf = crawlTf;
			this.maxValue = maxValue;
			this.onDown = onDown;
			this.onMove = onMove;
			this.ouUp = ouUp;
			this.availableValues = availableValues;
			this.startX = crawlerLine.x;
			var endX:number = this.startX + crawlerLine.getBounds().width - crawler.getBounds().width / 4;
			this.currentX = crawler.x;
			this.lineLength = endX - this.startX;
			//crawler.regX = crawler.getBounds().width / 2;
			crawl.on("mousedown", (eventDownObj:any)=>{
				onDown();
				this.view.on("pressmove", (eventMoveObj:any)=>{
					onMove();
					var div:number = Utils.float2int((eventMoveObj.stageX - eventDownObj.stageX)/this.view.getStage().scaleX);
					if((this.currentX + div)>=this.startX && (this.currentX + div)<endX){
						eventDownObj.stageX = eventMoveObj.stageX;
					}
					this.currentX = Math.min(Math.max((this.currentX + div), this.startX), endX);
					crawler.x = this.currentX;
					if(availableValues && availableValues.length){
						crawlTf.text = availableValues[Math.min(Math.max(Math.floor((this.currentX-this.startX)/this.lineLength*availableValues.length), 0), availableValues.length-1)].toString();
					}else{
						crawlTf.text = Math.max(Math.floor(maxValue*(this.currentX-this.startX)/this.lineLength), minValue).toString();
					}
				});
				this.view.on("pressup", ()=>{
					ouUp();
					this.view.removeAllEventListeners("pressmove");
					this.view.removeAllEventListeners("pressup");
				});
			});
		}

		public setValue(value:number):void{
			this.crawlTf.text = value.toString();
			if(this.availableValues && this.availableValues.length) {
				this.currentX = this.crawler.x = this.startX + this.availableValues.indexOf(value) / (this.availableValues.length-1) * this.lineLength;
			}else{
				this.currentX = this.crawler.x = this.startX + value / this.maxValue * this.lineLength;
			}
		}

		//public static getAbsoluteX(container:Container){
		//	var abs_x:number = container.x;
		//	while(container.parent){
		//		container = container.parent;
		//		abs_x += container.x;
		//	}
		//	return abs_x;
		//}
	}
}