module engine {
	export class NotificationList {
		public static BACK_TO_LOBBY:string = "back_to_lobby";
		public static HIDE_PRELOADER:string = "hide_preloader";

		public static SERVER_INIT:string = "server_init";
		public static SERVER_SEND_SPIN:string = "send_spin";
		public static SERVER_GOT_SPIN:string = "got_spin";
		public static SERVER_SEND_BONUS:string = "send_bonus";
		public static SERVER_GOT_BONUS:string = "got_bonus";
		public static SERVER_SET_WHEEL:string = "set_wheel";
		public static SERVER_DISCONNECT:string = "disconnect";
		public static SERVER_NOT_RESPONSE:string = "server_not_response";
		public static SERVER_GET_BALANCE_REQUEST:string = "server_get_balance";

		public static RES_LOADED:string = "res_loaded";
		public static LOAD_SOUNDS:string = "load_sounds";
		public static SOUNDS_LOADED:string = "sounds_loaded";

		public static KEYBOARD_CLICK:string = "keyboard_click";
		public static ON_SCREEN_CLICK:string = "on_screen_click";

		public static TRY_START_AUTO_PLAY:string = "try_start_auto_play";
		public static END_AUTO_PLAY:string = "end_auto_play";
		public static TRY_START_SPIN:string = "try_start_spin";
		public static START_SPIN:string = "start_spin";
		public static START_SPIN_AUDIO:string = "start_spin_audio";
		public static EXPRESS_STOP:string = "express_stop";
		public static STOPPED_REEL_ID:string = "stopped_reel_id";
		public static STOPPED_REEL_ID_PREPARE:string = "stopped_reel_id_prepare";
		public static STOPPED_ALL_REELS:string = "stopped_all_reels";

		public static CHANGED_LINE_COUNT:string = "changed_line_count";
		public static OPEN_PAY_TABLE:string = "open_pay_table";
		public static CLOSE_PAY_TABLE:string = "close_pay_table";
		public static START_PAY_TABLE_SOUND:string = "start_pay_table_sound";
		public static END_PAY_TABLE_SOUND:string = "end_pay_table_sound";
		public static OPEN_AUTO_SPIN_MENU:string = "open_auto_spin_menu";
		public static TOGGLE_COLOR_MENU:string = "toggle_color_menu";
		public static CLOSE_COLOR_MENU:string = "close_color_menu";
		public static TOGGLE_SETTINGS_MENU:string = "toggle_settings_menu";
		public static CLOSE_SETTINGS_MENU:string = "close_settings_menu";
		public static SOUND_TOGGLE:string = "sound_toggle";
		public static EXTRA_CHOICE_FS_TOGGLE:string = "extra_choice_toggle";
		public static SOUND_TOGGLE_UPDATE:string = "sound_toggle_update";

		public static SHOW_WIN_LINES:string = "show_win_lines";
		public static SHOW_WIN_LINES_REPEAT:string = "show_win_lines_repeat";
		public static WIN_LINES_SHOWED:string = "win_lines_showed";
		public static REMOVE_WIN_LINES:string = "remove_vin_lines";
		public static SHOW_LINES:string = "show_line";
		public static HIDE_LINES:string = "hide_lines";

		public static SHOW_HEADER:string = "show_header";
		public static REMOVE_HEADER:string = "remove_header";

		public static UPDATE_BALANCE_TF:string = "update_balance";
		public static SHOW_WIN_TF:string = "show_win";
		public static COLLECT_WIN_TF:string = "collect_win_tf";

		public static CREATE_BONUS:string = "create_bonus";
		public static START_BONUS:string = "start_bonus";
		public static START_BONUS_FS:string = "start_bonus_fs";
		public static START_BONUS_RESPINS:string = "start_bonus_respins";
		public static END_BONUS:string = "end_bonus";
		public static START_SECOND_FREE_GAME:string = "start_second_free_game";
		public static START_AYU_BONUS:string = "start_ayu_bonus";
		public static START_BLADES_BONUS:string = "start_blades_bonus";
		public static START_JUNGLE_QUEEN_BONUS:string = "start_jungle_queen_bonus";
		public static HIDE_CARD:string = "hide_card";
		public static SHOW_CARD:string = "show_card";

		public static SHOW_ALL_SYMBOLS:string = "show_all_symbols";
		public static HIDE_SYMBOLS:string = "hide_symbols";

        public static GET_ALL_IDS:string = "get_all_ids";
        public static LAZY_LOAD:string = "lazy_load";
        public static LAZY_LOAD_COMP:string = "lazy_load_compl";

		public static AUTO_PLAY_COMP:string = "auto_play_compl";
		public static AUTO_PLAY_CONT:string = "auto_play_cont";
		public static SHOW_AUTO_MODAL:string = "show_auto_modal";

		public static LOAD_PAY_TABLE:string = "load_pay_tables";
		public static START_BONUS_AUDIO:string = "start_bonus_audio";
		public static TRY_START_AUTO_PLAY_AUDIO:string = "try_start_auto_play_audio";
		public static SHOW_WILD_REEL_FADEIN:string = "show_wild_reel_fadein";
		public static SHOW_WILD_EXPANDING:string = "show_wild_expanding";
		public static SHOW_WILDS_EXPANDING_STATIC:string = "show_wilds_expanding_static";
		public static SHOW_WILD_STICKY:string = "show_wild_sticky";
		public static REMOVE_WILDS_EXPANDING_STATIC:string = "remove_wilds_expanding_static";

		public static RECURSION_PROCESS_FALLING:string = "recursion_process_falling";
		public static RECURSION_PROCESSED_FALLING:string = "recursion_processed_falling";
		public static RECURSION_PROCESSED:string = "recursion_processed";
		public static RECURSION_PROCESS_ROTATION:string = "recursion_process_rotation";
		public static LOADER_LOADED:string = "loader_loaded";

		public static SHOW_ERRORS:string = "show_errors";
		public static UPDATE_JACKPOT:string = "update_jackpot";
		public static UPDATE_JACKPOT_SERVER:string = "update_jackpot_server";
		public static SHOW_JACKPOT_POPUP:string = "show_jackpot_popup";
		public static OK_BTN_ERROR_CLICKED:string = "ok_btn_error_clicked";

		public static UPDATE_TURBO_MODE:string = "update_turbo_mode";
		public static OPEN_GAME_MENU:string = "update_game_menu";
		public static AUTO_PLAY_COUNT_UPDATE:string = "auto_play_count_update";
		public static REELS_WAY_COUNT_UPDATE:string = "reels_way_count_update";
		public static DARK_SYMBOLS:string = "dark_symbols";
		public static BET_PER_LINE_COUNT_UPDATE:string = "bet_per_line_count_update";
		public static LINES_COUNT_UPDATE:string = "lines_count_update";
		public static PLAY_MORE_FREE_SPINS_SOUND:string = "more_free_spins_sound";
		public static UPDATE_SEQUENCE_TYPE:string = "change_sequence_type";
		public static START_NORMAL_GAME_BG_SOUND:string = "start_normal_game_bg_sound";
		public static ON_RESIZE:string = "on_resize";
		public static UPDATE_HUD_COLOR:string = "update_hud_color";
		public static UPDATE_SCREEN_MODE:string = "update_screen_mode";
		public static CONFIG_LOADED:string = "config_loaded";

	}
}
