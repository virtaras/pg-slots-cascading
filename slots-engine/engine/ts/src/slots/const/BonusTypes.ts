module engine {
	export class BonusTypes {
		public static GAMBLE:string = "gamble";
		public static FREE_SPINS:string = "free_spins";
		public static RE_SPINS:string = "re_spins";
		public static MINI_GAME:string = "mini_game";
		public static SELECT_GAME:string = "select_item";
		public static AYU_SELECT_3:string = "ayu_select_3";
		public static BLADES_SELECT_5:string = "blades_select_5";
		public static JUNGLE_QUEEN_BONUS_WHEEL:string = "jungle_queen_bonus_wheel";
	}
}
