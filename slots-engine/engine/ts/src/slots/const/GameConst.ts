module engine {
	export class GameConst {
		public static DEMO_AUTHORIZATION_URL:string = "onlinecasino/common.asmx/DemoSession";
		public static AUTHORIZATION_URL:string = "onlinecasino/common.asmx/Authorization";
		public static AUTHORIZATION_ROOM:string = "onlinecasino/common.asmx/authorizationUIS";
		public static CLOSE_SESSION_URL:string = "onlinecasino/common.asmx/Close";
		public static GET_BALANCE_URL:string = "onlinecasino/common.asmx/GetBalance";
		public static GET_BETS_URL:string = "onlinecasino/games/%s.asmx/GetBets";
		public static SPIN_URL:string = "onlinecasino/games/%s.asmx/Spin";
		public static JACKPOT_URL:string = "onlinecasino/games/%s.asmx/GetJackpot";
		public static BONUS_URL:string = "onlinecasino/games/%s.asmx/BonusGame";
		public static SET_WHEEL_URL:string = "onlinecasino/games/%s.asmx/SetWheel";

		public static MAIN_RES_FOLDER:string = "assets/";
		public static LAYOUT_FOLDER:string = "layout/";
		public static FONTS_FOLDER:string = "fonts/";
		public static SOUNDS_FOLDER:string = "sounds/";

		public static COMMON_LAYOUTS_FOLDER:string = "common_assets/";

		public static GAME_CONFIG_NAME:string = "config";

		public static GAME_FPS:number = 30;
		public static MOBILE_TO_WEB_SCALE:number = 2;
		public static SCREEN_MODE_LANDSCAPE:string = "screen_mode_landscape";
		public static SCREEN_MODE_PORTRAIT:string = "screen_mode_portrait";
	}
}
