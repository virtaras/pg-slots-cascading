module engine {
	export class SoundsList {
		//-------------SOUND NAMES---------------------------------
		public static SYMBOL_SOUNDS:string[] = [];

		public static REGULAR_CLICK_BTN:string = "regular_click_btn";
		public static SPIN_CLICK_BTN:string = "spin_click_btn";

		public static REGULAR_SPIN_BG:string = "regular_spin_bg";
		public static AUTO_SPIN_BG:string = "auto_spin_bg";
		public static FREE_SPIN_BG:string = "free_spin_bg";
		public static PAY_TABLE_BG:string = "pay_table_bg";

		public static COLLECT:string = "collect";
		public static REEL_STOP:string = "reel_stop";
		public static FREE_SPIN_COLLECT:string = "free_spin_collect";
		public static REGULAR_WIN:string = "regular_win";
		public static LINE_WIN_ENUM:string = "line_win_enum";

		public static GAMBLE_BG:string = 'gamble_bg';
		public static GAMBLE_SHUFFLE:string = 'gamble_shuffle';
		public static GAMBLE_TURN:string = 'gamble_turn';

		public static GAMBLE_AMBIENT:string = 'gamble_ambient';
		public static CARD_TURN:string = 'card_turn';
		public static CARD_SHUFFLE:string = 'card_shuffle';
		public static RECURSION_ROTATION:string = 'recursion_rotation';
		public static AYU_BONUS_CHOOSE_BG:string = 'ayu_bonus_choose';
		public static BLADES_BONUS_CHOOSE_BG:string = 'blades_bonus_choose';
		public static JUNGLE_QUEEN_BONUS_WHEEL_BG:string = 'jungle_queen_bonus_wheel';
		public static MORE_FREE_SPINS_SOUND:string = 'more_free_spins_sound';

		public static NORMAL_GAME_BG:string = 'normal_game_bg';
		public static WILD_EXPANDING_APPEAR:string = 'wild_expanding_appear';
		public static JACKPOT_POPUP_APPEAR:string = 'jackpot_popup_appear';

		public static SYMBOL_PREFIX:string = 'symbol_';

	}
}