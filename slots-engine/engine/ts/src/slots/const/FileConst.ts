module engine {
	export class FileConst {
		public static LAYOUT_EXTENSION:string = ".layout";
		public static JSON_EXTENSION:string = ".json";
		public static WOFF_EXTENSION:string = ".woff";
		public static TTF_EXTENSION:string = ".ttf";
	}
}
