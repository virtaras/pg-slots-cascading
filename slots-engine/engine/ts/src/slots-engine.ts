///<reference path="../../dts/createjs.d.ts" />
///<reference path="../../dts/cryptojs.d.ts" />
///<reference path="../../dts/easeljs.d.ts" />
///<reference path="../../dts/layoutjs.d.ts" />
///<reference path="../../dts/preloadjs.d.ts" />
///<reference path="../../dts/soundjs.d.ts" />

//============GAME CLASS======================================
///<reference path="slots/const/BonusTypes.ts" />
///<reference path="slots/const/GameConst.ts" />
///<reference path="slots/const/NotificationList.ts" />
///<reference path="slots/const/SoundsList.ts" />
///<reference path="slots/vo/WinAniVO.ts" />
///<reference path="slots/vo/BonusVO.ts" />
///<reference path="slots/vo/WinLineVO.ts" />
///<reference path="slots/model/ServerData.ts" />
///<reference path="slots/model/CommonRefs.ts" />
///<reference path="slots/utils/Utils.ts" />
///<reference path="slots/utils/Keyboard.ts" />
///<reference path="slots/utils/XMLUtils.ts" />
///<reference path="slots/utils/MoneyFormatter.ts" />
///<reference path="slots/utils/FullScreen.ts"/>
///<reference path="slots/vo/AssetVO.ts" />
///<reference path="slots/const/FileConst.ts" />
///<reference path="slots/utils/easing/BaseEasing.ts" />
///<reference path="slots/utils/easing/LinearEasing.ts" />
///<reference path="slots/utils/easing/BezierEasing.ts" />
///<reference path="slots/utils/AjaxRequest.ts" />
//============BONUS CLASS======================================
///<reference path="slots/bonus/selectitem/ItemVO.ts" />

//============GAME VIEW======================================
///<reference path="slots/view/ui/AnimationTextField.ts" />
///<reference path="slots/view/ui/TouchMenu.ts" />
///<reference path="slots/view/ui/Crawler.ts" />
///<reference path="slots/view/win/WinSymbolView.ts" />z
///<reference path="slots/view/win/WildSymbolView.ts" />z
///<reference path="slots/view/reel/ReelView.ts" />
///<reference path="slots/view/reel/ReelsView.ts" />
///<reference path="slots/view/PayTableHudView.ts" />
///<reference path="slots/view/HudView.ts" />
///<reference path="slots/view/LinesView.ts" />
///<reference path="slots/view/LoaderView.ts" />
///<reference path="slots/view/RotateScreenView.ts" />
///<reference path="slots/view/FingerFullScreenView.ts" />
///<reference path="slots/modals/view/AutoPlayView.ts" />
///<reference path="slots/modals/view/ConnectionView.ts" />
///<reference path="slots/modals/view/ErrorView.ts" />
///<reference path="slots/view/JackpotView.ts" />
///<reference path="slots/view/JackpotViewPopup.ts" />
///<reference path="slots/view/MenuView.ts" />


///<reference path="slots/setwheel/SetWheelView.ts" />
//============BONUS VIEW=================================
///<reference path="slots/bonus/selectitem/SelectItemView.ts" />
///<reference path="slots/bonus/gamble/GambleView.ts" />
///<reference path="slots/bonus/gamble/GambleView5.ts" />
///<reference path="slots/bonus/freespins/view/FreeSpinsView.ts" />
///<reference path="slots/bonus/freespins/view/FreeSpinsStartView.ts" />
///<reference path="slots/bonus/freespins/view/FreeSpinsMoreView.ts" />
///<reference path="slots/bonus/freespins/view/FreeSpinsResultView.ts" />
///<reference path="slots/bonus/freespins/view/FreeSpinsRestoreView.ts" />
///<reference path="slots/bonus/ayu/AYUBonusView.ts" />
///<reference path="slots/bonus/jq/JungleQueenBonusView.ts" />
///<reference path="slots/bonus/blades/BladesBonusView.ts" />
///<reference path="slots/bonus/lucky88/Lucky88BonusView.ts" />
///<reference path="slots/bonus/respins/view/RespinsRestoreView.ts" />


//============BASE CONTROLLERS===============================
///<reference path="slots/controller/base/ControllerManager.ts" />
///<reference path="slots/controller/base/BaseController.ts" />
//============GAME CONTROLLERS===============================
///<reference path="slots/controller/hud/state/base/IState.ts" />
///<reference path="slots/controller/hud/state/AutoSpinState.ts" />
///<reference path="slots/controller/hud/state/BonusState.ts" />
///<reference path="slots/controller/hud/state/RegularSpinState.ts" />
///<reference path="slots/controller/hud/state/PayTableState.ts" />
///<reference path="slots/controller/hud/state/DefaultState.ts" />
///<reference path="slots/controller/win/WinLine.ts" />
///<reference path="slots/controller/win/BaseWinLines.ts" />
///<reference path="slots/controller/win/WinLines.ts" />
///<reference path="slots/controller/win/WinLinesTogether.ts" />
///<reference path="slots/controller/BonusHolderController.ts" />
///<reference path="slots/controller/LinesController.ts" />
///<reference path="slots/controller/BackgroundController.ts" />
///<reference path="slots/controller/HeaderController.ts" />
///<reference path="slots/controller/ServerController.ts" />
///<reference path="slots/controller/PayTableController.ts" />
///<reference path="slots/controller/SoundController.ts" />
///<reference path="slots/controller/hud/AutoSpinTableController.ts" />
///<reference path="slots/controller/hud/ColorPanelController.ts" />
///<reference path="slots/controller/hud/StatsController.ts" />
///<reference path="slots/controller/hud/SettingsController.ts" />
///<reference path="slots/controller/hud/HudController.ts" />
///<reference path="slots/controller/ReelController.ts" />
///<reference path="slots/controller/ReelControllerCascade.ts" />
///<reference path="slots/controller/wild/ReelWildExpandingController.ts" />
///<reference path="slots/controller/wild/ReelWildStickyController.ts" />
///<reference path="slots/controller/mover/ReelMover.ts" />
///<reference path="slots/controller/mover/ReelMoverCascade.ts" />
///<reference path="slots/controller/mover/ReelMoverTypes.ts" />
///<reference path="slots/controller/LoaderController.ts" />
///<reference path="slots/controller/WinController.ts" />
///<reference path="slots/controller/GameController.ts" />
///<reference path="slots/setwheel/SetWheelController.ts" />
///<reference path="slots/modals/ModalWindowController.ts" />
///<reference path="slots/controller/JackpotController.ts" />
///<reference path="slots/controller/MenuController.ts" />

//============BONUS CONTROLLERS==============================
///<reference path="slots/bonus/BaseBonusController.ts" />
///<reference path="slots/bonus/gamble/GambleController.ts" />
///<reference path="slots/bonus/gamble/GambleController5.ts" />
///<reference path="slots/bonus/freespins/FreeSpinsController.ts" />
///<reference path="slots/bonus/respins/ReSpinsControllerHotsevens.ts" />
///<reference path="slots/bonus/selectitem/SelectItemController.ts" />
///<reference path="slots/bonus/ayu/AYUBonusController.ts" />
///<reference path="slots/bonus/jq/JungleQueenBonusController.ts" />
///<reference path="slots/bonus/blades/BladesBonusController.ts" />
///<reference path="slots/bonus/lucky88/Lucky88BonusController.ts" />

///<reference path="slots/controller/error/ErrorController.ts" />







